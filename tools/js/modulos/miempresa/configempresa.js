$( document ).ready(function() {

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        ignore: ""
    });

    $('#form_save_pers_juridica').validate({
        rules:
        {
          ruc:
          {
            required:true,
            number: true,
            rangelength:[11,11]
          },
          razon_social:{ required:true },
          direccion_fiscal:{ required:true },
          id_departamento:{ required:true },
          id_provincia:{ required:true },
          id_distrito:{ required:true },
          email:{email: true }     
        },
        messages: 
        {
          ruc:
          {
            required:"Ingrese RUC",
            number: "Solo #s",
            rangelength:"RUC Incorrecto"
          },
          razon_social:{ required: "" },
          direccion_fiscal:{ required: "" },
          id_departamento:{ required:"" },
          id_provincia:{ required:"" },
          id_distrito:{ required:"" },
          email:{email: "Incorrecto" }    
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');        
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.form-group').length) 
            {
              error.insertAfter(element.parent());
              //element.closest('.control-group').find('.help-block').html(error.text()); 
            } 
            else { 
              if(element.parent('.col-md-12').length) { error.insertAfter(element.parent());}
              else if(element.parent('.col-md-9').length) { error.insertAfter(element.parent());}
              else {}
            }
        },
        submitHandler: function() {
            $.ajax({
                url: $('base').attr('href') + 'configempresa/save_miempresa',
                type: 'POST',
                data: $('#form_save_pers_juridica').serialize(),
                dataType: "json",
                beforeSend: function() {},
                success: function(response) {
                  if (response.code==1) 
                  {
                    var id_personaj = $('#id_persona_juridica').val();
                    if(parseInt(id_personaj)>0)
                      swal('¡¡Configuración','editada correctamente!!','success');
                    else
                      swal('¡¡Configuración','creada correctamente!!','success');  
                  }
                },
                complete: function() {}
            });
        }
    });

    $(function () {
      $('#datetimepicker1').datetimepicker({
        locale: moment.locale('es'),
        format: 'DD-MM-YYYY'
      });
    });
});

$(document).on('change','#id_departamento',function(){
    $('#id_provincia').html('');
    $('#id_distrito').html('');
    var id_departamento = $(this).val();
    if(parseInt(id_departamento)>0)
    {
        $.ajax({
            url: $('base').attr('href') + 'generico/cbx_provincia',
            type: 'POST',
            data: 'id_departamento='+id_departamento,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) 
            {
                if(response.code==1)
                {
                    $('#id_provincia').html(response.data);
                }
            },
            complete: function() {}
        });
    }
});

$(document).on('change','#id_provincia',function(){
    $('#id_distrito').html('');
    var id_provincia = $(this).val();
    if(parseInt(id_provincia)>0)
    {
        $.ajax({
            url: $('base').attr('href') + 'generico/cbx_distrito',
            type: 'POST',
            data: 'id_provincia='+id_provincia,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) 
            {
                if(response.code==1)
                {
                    $('#id_distrito').html(response.data);
                }
            },
            complete: function() {}
        });
    }
});
    //1:claro , 2:entel, 3:bitel, 4:movistar, 5:twenty, 6:virgin

$(document).on('click','.add_cel',function(){
    var create = true;
    $('#cel_container div input').each(function(key,value){
        if($(this).val()=="")
            create=false;
    });

    if(create==true)
    {
        var opt_ =  "<option value>Seleccione Operador</option>"+
            "<option value=1>Claro</option>"+
            "<option value=2>Entel</option>"+
            "<option value=3>Bitel</option>"+
            "<option value=4>Movistar</option>"+
            "<option value=5>Twenty</option>"+
            "<option value=6>Virgin</option>";

        var c = 1 ;
        $('#cel_container div').each(function(key,value){
            c++;
        });

        var txt =   "<div class='form-group form-inline'>"+
                        "<input class='form-control' type='text' name='contacto[cel]["+c+"]' style='margin-right: 10px;' maxlength=9>"+
                        "<select class='form-control' name='contacto[op]["+c+"]' style='margin-right: 10px;'>"+opt_+"</select>"+
                        "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>"+
                          "<i class='fa fa-trash-o'></i>"+
                        "</a>"+
                    "</div>";

        $('#cel_container').append(txt);        
    }
    else
    {
        swal('¡Error!','Aún hay campos vacíos','error');
    }
});

$(document).on('click','.add_telf',function(){
    var create = true;
    $('#telf_container div input').each(function(key,value){
        if($(this).val()=="")
            create=false;
    });

    if(create==true)
    {
        var c = 1 ;
        $('#telf_container div').each(function(key,value){
            c++;
        });

        var txt =   "<div class='form-group form-inline'>"+
                        "<input class='form-control' type='text' name='contacto[telf]["+c+"]' style='margin-right: 10px;' maxlength=9>"+
                        "<input class='form-control' type='text' name='contacto[anex]["+c+"]' style='margin-right: 10px;'>"+
                        "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>"+
                          "<i class='fa fa-trash-o'></i>"+
                        "</a>"+
                    "</div>";

        $('#telf_container').append(txt);        
    }
    else
    {
        swal('¡Error!','Aún hay campos vacíos','error');
    }
});

$(document).on('click','.del_this',function(){
    $(this).closest('.form-inline').remove();
});