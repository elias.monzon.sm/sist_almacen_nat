$( document ).ready(function() {

});

$(function () {
    $("#descripcion").autocomplete({
        type:'POST',
        serviceUrl: $('base').attr('href')+"codigo/codigo_autocomplete",
        onSelect: function (suggestion) 
        {
            $('#id_codigo').val(suggestion.data);

            buscar_marcaxcodigo();
        }
    });
});

function buscar_marcaxcodigo() {

  var id_codigo = parseInt($('#id_codigo').val());
  id_codigo = id_codigo>0 ? id_codigo : 0;
  if(id_codigo>0)
  {
    $.ajax({
      url: $('base').attr('href') + 'marca/get_marcaxcodigo',
      type: 'POST',
      data: 'id_codigo='+id_codigo+'&cbx=true',
      dataType: "json",
      beforeSend: function() {
          //showLoader();
      },
      success: function(response) {
        $('#id_marca').html(response.data);
      },
      complete: function(response) {
        
      }
    });
  }
}

$(document).on('click','.buscar',function () {
  buscar();
});

function buscar()
{
  if(validar())
  {
    var id_codigo = parseInt($('#id_codigo').val());
    var id_marca = parseInt($('#id_marca').val());

    param = 'id_codigo='+id_codigo;

    if(id_marca>0)
    {
      param +='&id_marca='+id_marca;
    }

    $.ajax({
      url: $('base').attr('href') + 'stockxnumeroparte/buscar',
      type: 'POST',
      data: param,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        $('#lista_articulos tbody').html(response.data);
      },
      complete: function(response) {
        
      }
    });
  }
}

function validar() {
  var id_codigo = parseInt($('#id_codigo').val());
  if(id_codigo<=0)
  {
    swal('Debe','escojer un # DE PARTE.','error');
    return false;
  }

  return true;
}