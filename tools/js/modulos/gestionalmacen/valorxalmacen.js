$(document).on('click','.buscar',function(){
  var id_alm = $('#id_almacen').val();
  if(id_alm)
  {
    $.ajax({
      url: $('base').attr('href') + 'valorxalmacen/stockxalmacen',
      type: 'POST',
      data: 'id_almacen='+id_alm,
      dataType: "json",
      beforeSend: function() {
      
      },
      success: function(response) {
          if (response.code==1) 
          {
            $('tbody').html(response.data);
          }
      },
      complete: function() {

      }
    });
  }
  else
  {
    swal('¡¡Error!!','Debe seleccionar un almacén','error');
  }
});