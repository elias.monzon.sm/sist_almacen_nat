$( document ).ready(function() {

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });

    $('#form_save_usuario').validate({
      rules:
      {
        usuario: {
          required:true, 
          minlength: 2,
          remote: {
            url: $('base').attr('href') + 'usuarios/validar_usuario',
            type: "post",
            data: {
              usuario: function() { return $( "#usuario" ).val(); },
              id_usuario: function() { return $('#id_usuario').val(); }
            }
          }
        }
      },
      messages: 
      {
        usuario: {required:"usuario", minlength: "Más de 2 Letras", remote: "Ya existe" }
      },      

      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
        if(element.parent('.col-md-6').length) 
        {
          error.insertAfter(element.parent()); 
        }
      },
      submitHandler: function() {
        $.ajax({
          url: $('base').attr('href') + 'usuarios/save_usuarios',
          type: 'POST',
          data: $('#form_save_usuario').serialize(),
          dataType: "json",
          beforeSend: function() {
              //showLoader();
          },
          success: function(response) {
            if (response.code==1) {
              window.location.reload();
            }
          },
          complete: function(response) {
            var id_usuario = parseInt($('#id_usuario').val());
            id_usuario = (id_usuario>0) ? (id_usuario) : ("0");
            var text = (id_usuario=="0") ? ("Guardo!") : ("Edito!");
            if(response.code == 0)
            {
              text = response.message;
            }
            swal(text, 'Este usuario se '+text+'.', 'success');
            limp_todo('form_save_usuario');
            $('#editusuario').modal('hide');
            $('#id_usuario').val('');
          }
        });
      }
    });
});

$(document).on('click', '#datatable-buttons .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_usuarios(0);
});

$(document).on('click', '.add_usuario', function (e)
{
  limp_todo('form_save_usuario');
});

$(document).on('click', '.btn_limpiar', function (e) {
  limp_todo('form_save_usuario');
  $('#editusuario').modal('hide');
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_usuarios(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_usuarios(page);
});

$(document).on('click', '#datatable-buttons .buscar', function (e) {
  var page = 0;
  buscar_usuarios(page);
});

function buscar_usuarios(page)
{
  var alm = $('#alm').val();
  var temp = "page="+page;
  if(alm.trim().length)
  {
    temp=temp+'&alm='+alm;
  }
  $.ajax({
    url: $('base').attr('href') + 'usuarios/buscar_usuarios',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      //showLoader();
    },
    success: function(response) {
      if (response.code==1) {
        $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

$(document).on('click', '.edit', function (e) {
  var idusuario = $(this).parents('tr').attr('idusuario');
  $.ajax({
    url: $('base').attr('href') + 'usuarios/edit',
    type: 'POST',
    data: 'id_usuario='+idusuario,
    dataType: "json",
    beforeSend: function() {
      limp_todo('form_save_usuario');
    },
    success: function(response) {
      if (response.code==1) {
        $('#id_usuario').val(response.data.id_usuario);
        $('#usuario').val(response.data.usuario);
        $('#pwd').val(response.data.pwd);

        $('#estado label').removeClass('active');
        $('#estado input').prop('checked', false);

        var num = response.data.estado;
        $('#estado #estado_'+num).prop('checked', true);
        $('#estado #estado_'+num).parent('label').addClass('active');
        

      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click', '.delete', function (e) {
  e.preventDefault();
  var idusuario = $(this).parents('tr').attr('idusuario');
  var page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
  var nomb = "Usuario";

  swal({
    title: 'Estas Seguro?',
    text: "De Desactivar este "+nomb,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, estoy seguro!'
  }).then(function(isConfirm) {
    if (isConfirm) {     
      $.ajax({
        url: $('base').attr('href') + 'usuarios/save_usuarios',
        type: 'POST',
        data: 'id_usuario='+idusuario+'&estado=0',
        dataType: "json",
        beforeSend: function() {
          //showLoader();
        },
        success: function(response) {
          if (response.code==1) {              
            window.location.reload();
          }
        },
        complete: function() {
          var text = "Desactivó!";
          swal(text, 'Este '+nomb+' se '+text+'.', 'success');
          limp_todo('form_save_usuario');
        }
      });
    }
  });    
});

function limp_todo(form, diverror)
{
  $('#'+form+' input').each(function (index, value){
    if($(this).attr('type')=="checkbox")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
      $(this).prop('checked', false);
    }

    if($(this).attr('type')=="text")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      $(this).val('');

      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
    }    

    if($(this).attr('type')=="hidden"){
      $(this).val('');
    }    
  });
}