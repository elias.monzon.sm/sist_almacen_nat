$( document ).ready(function() {

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });

    $('#form_save_tipodocumento').validate({
      rules:
      {
        tipodocumento: {
          required:true, 
          minlength: 2,
          remote: {
            url: $('base').attr('href') + 'tipodocumento/validar_tipodocumento',
            type: "post",
            data: {
              tipodocumento: function() { return $( "#tipodocumento" ).val(); },
              id_tipodocumento: function() { return $('#id_tipodocumento').val(); }
            }
          }
        }
      },
      messages: 
      {
        tipodocumento: {required:"tipodocumento", minlength: "Más de 2 Letras", remote: "Ya existe" }
      },      

      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
        if(element.parent('.col-md-6').length) 
        {
          error.insertAfter(element.parent()); 
        }
      },
      submitHandler: function() {
        $.ajax({
          url: $('base').attr('href') + 'tipodocumento/save_tipodocumento',
          type: 'POST',
          data: $('#form_save_tipodocumento').serialize(),
          dataType: "json",
          beforeSend: function() {
              //showLoader();
          },
          success: function(response) {
            if (response.code==1) {
              var page = 0;
              if($('#paginacion_data ul.pagination li.active a').length>0)
              {
                page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
              }                     

              buscar_tipodocumentos(page);
            }
            else
            {
              if($('#tipodocumento').parents('.form-group').attr('class')=="form-group")
              {
                $('#tipodocumento').parents('.form-group').addClass('has-error');
              }
              
              if($('#tipodocumento-error').length>0)
              {
                $('#tipodocumento-error').html(response.message);
              }
              else
              {
                $('#tipodocumento').parents('.col-md-6').append("<span id='tipodocumento-error' class='help-block'>"+response.message+"</span>");
              }
            }
          },
          complete: function(response) {
            var id_tipodocumento = parseInt($('#id_tipodocumento').val());
            id_tipodocumento = (id_tipodocumento>0) ? (id_tipodocumento) : ("0");
            var text = (id_tipodocumento=="0") ? ("Guardo!") : ("Edito!");
            if(response.code == 0)
            {
              text = response.message;
            }
            swal(text, 'Esta tipodocumento se '+text+'.', 'success');
            limp_todo('form_save_tipodocumento');
            $('#edittipodocumento').modal('hide');
            $('#id_tipodocumento').val('');
          }
        });
      }
    });
});

$(document).on('click', '#datatable-buttons .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_tipodocumentos(0);
});

$(document).on('click', '.add_tipodocumento', function (e)
{
  limp_todo('form_save_tipodocumento');
});

$(document).on('click', '.btn_limpiar', function (e) {
  limp_todo('form_save_tipodocumento');
  $('#edittipodocumento').modal('hide');
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_tipodocumentos(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_tipodocumentos(page);
});

$(document).on('click', '#datatable-buttons .buscar', function (e) {
  var page = 0;
  buscar_tipodocumentos(page);
});

function buscar_tipodocumentos(page)
{
  var alm = $('#alm').val();
  var temp = "page="+page;
  if(alm.trim().length)
  {
    temp=temp+'&alm='+alm;
  }
  $.ajax({
    url: $('base').attr('href') + 'tipodocumento/buscar_tipodocumentos',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      //showLoader();
    },
    success: function(response) {
      if (response.code==1) {
        $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

$(document).on('click', '.edit', function (e) {
  var idtipodocumento = $(this).parents('tr').attr('idtipodocumento');
  $.ajax({
    url: $('base').attr('href') + 'tipodocumento/edit',
    type: 'POST',
    data: 'id_tipodocumento='+idtipodocumento,
    dataType: "json",
    beforeSend: function() {
      limp_todo('form_save_tipodocumento');
    },
    success: function(response) {
      if (response.code==1) {
        $('#id_tipodocumento').val(response.data.id_tipodocumento);
        $('#tipodocumento').val(response.data.tipodocumento);

        $('#estado label').removeClass('active');
        $('#estado input').prop('checked', false);

        var num = response.data.estado;
        $('#estado #estado_'+num).prop('checked', true);
        $('#estado #estado_'+num).parent('label').addClass('active');
        
        var afecta = response.data.afecta_stock;
        if(afecta==1)
        {
          $('#afecta_stock').prop('checked',true);
        }
        else
        {
          $('#afecta_stock').prop('checked',false);
        }
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click', '.delete', function (e) {
  e.preventDefault();
  var idtipodocumento = $(this).parents('tr').attr('idtipodocumento');
  var page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
  var nomb = "Tipo de Documento";

  swal({
    title: 'Estas Seguro?',
    text: "De Eliminar esta "+nomb,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, estoy seguro!'
  }).then(function(isConfirm) {
    if (isConfirm) {     
      $.ajax({
        url: $('base').attr('href') + 'tipodocumento/save_tipodocumento',
        type: 'POST',
        data: 'id_tipodocumento='+idtipodocumento+'&estado=0',
        dataType: "json",
        beforeSend: function() {
          //showLoader();
        },
        success: function(response) {
          if (response.code==1) {              
            buscar_tipodocumentos(page);
          }
        },
        complete: function() {
          var text = "Elimino!";
          swal(text, 'Este '+nomb+' se '+text+'.', 'success');
          limp_todo('form_save_tipodocumento');
        }
      });
    }
  });    
});

function limp_todo(form, diverror)
{
  $('#'+form+' input').each(function (index, value){
    if($(this).attr('type')=="checkbox")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
      $(this).prop('checked', false);
    }

    if($(this).attr('type')=="text")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      $(this).val('');

      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
    }    

    if($(this).attr('type')=="hidden"){
      $(this).val('');
    }    
  });
}