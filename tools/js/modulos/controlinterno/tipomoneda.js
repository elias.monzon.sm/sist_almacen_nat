$( document ).ready(function() {

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });

    $('#form_save_tipomoneda').validate({
      rules:
      {
        tipomoneda: {
          required:true, 
          minlength: 2,
          remote: {
            url: $('base').attr('href') + 'tipomoneda/validar_tipomoneda',
            type: "post",
            data: {
              tipomoneda: function() { return $( "#tipomoneda" ).val(); },
              id_tipomoneda: function() { return $('#id_tipomoneda').val(); }
            }
          }
        },
        factor_cambio: {required:true}
      },
      messages: 
      {
        tipomoneda: {required:"tipomoneda", minlength: "Más de 2 Letras", remote: "Ya existe" },
        factor_cambio: {required: "Ingrese el factor."}
      },      

      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
        if(element.parent('.col-md-6').length) 
        {
          error.insertAfter(element.parent()); 
        }
      },
      submitHandler: function() {
        $.ajax({
          url: $('base').attr('href') + 'tipomoneda/save_tipomoneda',
          type: 'POST',
          data: $('#form_save_tipomoneda').serialize(),
          dataType: "json",
          beforeSend: function() {
              //showLoader();
          },
          success: function(response) {
            if (response.code==1) {
              var page = 0;
              if($('#paginacion_data ul.pagination li.active a').length>0)
              {
                page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
              }                     

              buscar_tipomonedas(page);
            }
            else
            {
              if($('#tipomoneda').parents('.form-group').attr('class')=="form-group")
              {
                $('#tipomoneda').parents('.form-group').addClass('has-error');
              }
              
              if($('#tipomoneda-error').length>0)
              {
                $('#tipomoneda-error').html(response.message);
              }
              else
              {
                $('#tipomoneda').parents('.col-md-6').append("<span id='tipomoneda-error' class='help-block'>"+response.message+"</span>");
              }
            }
          },
          complete: function(response) {
            var id_tipomoneda = parseInt($('#id_tipomoneda').val());
            id_tipomoneda = (id_tipomoneda>0) ? (id_tipomoneda) : ("0");
            var text = (id_tipomoneda=="0") ? ("Guardo!") : ("Edito!");
            if(response.code == 0)
            {
              text = response.message;
            }
            swal(text, 'Esta tipomoneda se '+text+'.', 'success');
            limp_todo('form_save_tipomoneda');
            $('#edittipomoneda').modal('hide');
            $('#id_tipomoneda').val('');
          }
        });
      }
    });
});

$(document).on('click', '#datatable-buttons .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_tipomonedas(0);
});

$(document).on('click', '.add_tipomoneda', function (e)
{
  limp_todo('form_save_tipomoneda');
});

$(document).on('click', '.btn_limpiar', function (e) {
  limp_todo('form_save_tipomoneda');
  $('#edittipomoneda').modal('hide');
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_tipomonedas(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_tipomonedas(page);
});

$(document).on('click', '#datatable-buttons .buscar', function (e) {
  var page = 0;
  buscar_tipomonedas(page);
});

function buscar_tipomonedas(page)
{
  var alm = $('#alm').val();
  var temp = "page="+page;
  if(alm.trim().length)
  {
    temp=temp+'&alm='+alm;
  }
  $.ajax({
    url: $('base').attr('href') + 'tipomoneda/buscar_tipomonedas',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      //showLoader();
    },
    success: function(response) {
      if (response.code==1) {
        $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

$(document).on('click', '.edit', function (e) {
  var idtipomoneda = $(this).parents('tr').attr('idtipomoneda');
  $.ajax({
    url: $('base').attr('href') + 'tipomoneda/edit',
    type: 'POST',
    data: 'id_tipomoneda='+idtipomoneda,
    dataType: "json",
    beforeSend: function() {
      limp_todo('form_save_tipomoneda');
    },
    success: function(response) {
      if (response.code==1) {
        $('#id_tipomoneda').val(response.data.id_tipomoneda);
        $('#tipomoneda').val(response.data.tipomoneda);
        $('#simbolo').val(response.data.simbolo);
        $('#abreviatura').val(response.data.abreviatura);

        $('#estado label').removeClass('active');
        $('#estado input').prop('checked', false);

        var num = response.data.estado;
        $('#estado #estado_'+num).prop('checked', true);
        $('#estado #estado_'+num).parent('label').addClass('active');
        
        var afecta = response.data.afecta_stock;
        if(afecta==1)
        {
          $('#afecta_stock').prop('checked',true);
        }
        else
        {
          $('#afecta_stock').prop('checked',false);
        }
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click', '.delete', function (e) {
  e.preventDefault();
  var idtipomoneda = $(this).parents('tr').attr('idtipomoneda');
  var page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
  var nomb = "Tipo de Moneda";

  swal({
    title: 'Estas Seguro?',
    text: "De Eliminar esta "+nomb,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, estoy seguro!'
  }).then(function(isConfirm) {
    if (isConfirm) {     
      $.ajax({
        url: $('base').attr('href') + 'tipomoneda/save_tipomoneda',
        type: 'POST',
        data: 'id_tipomoneda='+idtipomoneda+'&estado=0',
        dataType: "json",
        beforeSend: function() {
          //showLoader();
        },
        success: function(response) {
          if (response.code==1) {              
            buscar_tipomonedas(page);
          }
        },
        complete: function() {
          var text = "Elimino!";
          swal(text, 'Este '+nomb+' se '+text+'.', 'success');
          limp_todo('form_save_tipomoneda');
        }
      });
    }
  });    
});

function limp_todo(form, diverror)
{
  $('#'+form+' input').each(function (index, value){
    if($(this).attr('type')=="checkbox")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
      $(this).prop('checked', false);
    }

    if($(this).attr('type')=="text")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      $(this).val('');

      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
    }    

    if($(this).attr('type')=="hidden"){
      $(this).val('');
    }    
  });
}