$( document ).ready(function() {
    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });
    
    /*Proveedor*/
    $('#form-config-preciov').validate({
        rules:
        {          
          id_tipomoneda: { required:true},
          precio_venta: {required:true}
        },
        messages: 
        {          
          id_tipomoneda: { required:"Seleccione el tipo de moneda"},
          precio_venta: { required:"Ingrese el precio de venta" }
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');  
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.col-md-6').length) { error.insertAfter(element.parent()); }
        },
        submitHandler: function() {
            var id_codigo = $('#id_codigo').val();
            var id_marca = $('#id_marcas').val();
            var temp = "id_codigo="+id_codigo+"&id_marca="+id_marca;

            $.ajax({
                url: $('base').attr('href') + 'codigo/save_precioventa',
                type: 'POST',
                data: $('#form-config-preciov').serialize()+'&'+temp,
                dataType: "json",
                beforeSend: function() {

                },
                success: function(response) {
                    if (response.code == 1) {
                        window.location.reload();
                    }                    
                },
                complete: function(response) {
                    if (response.responseJSON.code == "1") {
                        
                    }
                }
            });
        }
    });
});

$(document).on('click','.btn_cancelsave',function () {
    window.location.reload();
})
/*<---->*/