$(document).on('change','#id_marcas',function(){
    var id = $(this).val();
    if(id)
    {
        var base = $('base').attr('href');
        var tab_active = $('#myTab li.active').attr('tabs');
        var url_modulo = $('#url_modulo').val();
        var id_codigo = $('#id_codigo').val();
        window.location = base+url_modulo+"/config/"+id_codigo+"/"+tab_active+"/"+id;
    }
});