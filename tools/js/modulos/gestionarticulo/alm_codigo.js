$(document).on('click','.existpa',function(){
    var padre = $(this).parents('.contenedor');
    var id_almacen = padre.find('.id_almacen').val();
    var id_codigo = $('#id_codigo').val();
    var id_marca = $('#id_marcas').val();
    var estado = $(this).is(':checked') ? 1 : 0;
    
    var temp = 'id_almacen='+id_almacen+'&id_codigo='+id_codigo+'&id_marca='+id_marca+'&estado='+estado;

    $.ajax({
        url: $('base').attr('href') + 'codigo/check_almacen',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
            if (response.code == 1) {
                window.location.reload();
            }
        },
        complete: function () {
            //hideLoader();
        }
    });
});

$(document).on('change','.ubialm',function () {
    var id_ubialm = $(this).val();
    var padre = $(this).parents('.contenedor');
    var id_almacen = padre.find('.id_almacen').val();
    var id_codigo = $('#id_codigo').val();
    var id_marca = $('#id_marcas').val();

    var temp = 'id_almacen='+id_almacen+'&id_codigo='+id_codigo+'&id_marca='+id_marca+'&id_almacen_ubicacion='+id_ubialm;
    $.ajax({
        url: $('base').attr('href') + 'codigo/savecodubixalmacen',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
            if (response.code == 1) {
                window.location.reload();
            }
        },
        complete: function () {
            //hideLoader();
        }
    });
});

$('input[name=stock_minimo]').on('keydown',function () {
    var stock_minimo = $(this).val();
    //alert(stock_minimo);
    var padre = $(this).parents('.contenedor');
    var id_almacen = padre.find('.id_almacen').val();
    var id_codigo = $('#id_codigo').val();
    var id_marca = $('#id_marcas').val();

    var temp = 'id_almacen='+id_almacen+'&id_codigo='+id_codigo+'&id_marca='+id_marca+'&stock_minimo='+stock_minimo;
    $.ajax({
        url: $('base').attr('href') + 'codigo/savecodstockmin',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
            if (response.code == 1) {
                //window.location.reload();
            }
        },
        complete: function () {
            //hideLoader();
        }
    });
});
