$( document ).ready(function() {
    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });
    /*Proveedor*/
    $('#form-add-pj').validate({
        rules:
        {          
          id_proveedor: { required:true}
        },
        messages: 
        {          
          id_proveedor: { required:"Buscar y Seleccionar" }
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');  
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.col-md-8').length) { error.insertAfter(element.parent()); }
        },
        submitHandler: function() {
            var id_codigo = $('#id_codigo').val();
            var id_marca = $('#id_marcas').val();
            var temp = "tipo_persona="+2+"&id_codigo="+id_codigo+"&id_marca="+id_marca;

            $.ajax({
                url: $('base').attr('href') + 'codigo/save_proveedor_articulo',
                type: 'POST',
                data: $('#form-add-pj').serialize()+'&'+temp,
                dataType: "json",
                beforeSend: function() {
                },
                success: function(response) {
                    if (response.code == 1) {
                        window.location.reload();
                    }                    
                },
                complete: function(response) {

                }
            });
        }
    });


    $('#form-add-pn').validate({
        rules:
        {          
          id_proveedor: { required:true}
        },
        messages: 
        {          
          id_proveedor: { required:"Buscar y Seleccionar" }
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');  
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.col-md-8').length) { error.insertAfter(element.parent()); }
        },
        submitHandler: function() {
            var id_codigo = $('#id_codigo').val();
            var id_marca = $('#id_marcas').val();
            var temp = "tipo_persona="+1+"&id_codigo="+id_codigo+"&id_marca="+id_marca;

            $.ajax({
                url: $('base').attr('href') + 'codigo/save_proveedor_articulo',
                type: 'POST',
                data: $('#form-add-pn').serialize()+'&'+temp,
                dataType: "json",
                beforeSend: function() {
                },
                success: function(response) {
                    if (response.code == 1) {
                        window.location.reload();
                    }                    
                },
                complete: function(response) {

                }
            });
        }
    });
});

function get_used_prov(tipo_persona) {
    var c = 0;
    var i = new Array();
    $('#tb_proveedor tbody tr[tipo_persona="'+tipo_persona+'"]').each(function (index,value) {
        var id = $(this).attr('idpersona');
        i.push(id);
        c++;
        
    });
    return (c) ? i.join() : null;
}

$(function () {
    /*Proveedor*/
    $( "#ruc" ).autocomplete({ 
        params:{
            'w_not': function () { return get_used_prov(2);}
        },
        type:'POST',
        noCache: true,
        serviceUrl: $('base').attr('href')+"persona_juridica/get_ruc",
        onSelect: function (suggestion) {

            $('#id_persona_juridica').val(suggestion.data.id_persona_juridica);
            $('#nombrecomercial').val(suggestion.data.nombre_comercial);
            $('#rsocialprov').val(suggestion.data.razon_social);
            $('#ruc').val(suggestion.data.ruc);
        }
    });

    $( "#nombrecomercial" ).autocomplete({
        params:{
            'w_not': function () { return get_used_prov(2);}
        },
        type:'POST',
        noCache: true,
        serviceUrl: $('base').attr('href')+"persona_juridica/get_nombrecomercial",
        onSelect: function (suggestion) {
            $('#id_persona_juridica').val(suggestion.data.id_persona_juridica);
            $('#nombrecomercial').val(suggestion.data.nombre_comercial);
            $('#rsocialprov').val(suggestion.data.razon_social);
            $('#ruc').val(suggestion.data.ruc);
        }
    });

    $( "#rsocialprov" ).autocomplete({
        params:{
            'w_not': function () { return get_used_prov(2);}
        },
        type:'POST',
        noCache: true,
        serviceUrl: $('base').attr('href')+"persona_juridica/get_razonsocial",
        onSelect: function (suggestion) {
            $('#id_persona_juridica').val(suggestion.data.id_persona_juridica);
            $('#nombrecomercial').val(suggestion.data.nombre_comercial);
            $('#rsocialprov').val(suggestion.data.razon_social);
            $('#ruc').val(suggestion.data.ruc);
        }
    });

    $( "#dni" ).autocomplete({
        params:{
            'w_not': function () { return get_used_prov(1);}
        },
        type:'POST',
        noCache: true,
        serviceUrl: $('base').attr('href')+"persona_natural/get_dni",
        onSelect: function (suggestion) {
            console.log(suggestion);
            $('#id_persona').val(suggestion.data.id_persona);
            $('#nombres').val(suggestion.data.nombres);
            $('#apellidos').val(suggestion.data.apellidos);
            $('#dni').val(suggestion.data.dni);
        }
    });

    $( "#apellidos" ).autocomplete({
        params:{
            'w_not': function () { return get_used_prov(1);}
        },
        type:'POST',
        serviceUrl: $('base').attr('href')+"persona_natural/get_apellidos",
        noCache: true,
        onSelect: function (suggestion) {
            $('#id_persona').val(suggestion.data.id_persona);
            $('#nombres').val(suggestion.data.nombres);
            $('#apellidos').val(suggestion.data.apellidos);
            $('#dni').val(suggestion.data.dni);
        }
    });

    $( "#nombres" ).autocomplete({
        params:{
            'w_not': function () { return get_used_prov(1);}
        },
        type:'POST',
        noCache: true,
        serviceUrl: $('base').attr('href')+"persona_natural/get_nombres",
        onSelect: function (suggestion) {
            $('#id_persona').val(suggestion.data.id_persona);
            $('#nombres').val(suggestion.data.nombres);
            $('#apellidos').val(suggestion.data.apellidos);
            $('#dni').val(suggestion.data.dni);
        }
    });
});

$(document).on('click', '#tipo_persona label', function (e) {
  var padre = $(this);
  var tipo_persona = padre.find('input').val();
  
  $('#lbl_rsocialprov').parents('.form-group').show();
  if(tipo_persona == "1")
  {    
    $('#form-add-pj').removeClass('collapse');
    $('#form-add-pn').addClass('collapse');
  }
  else
  {
    $('#form-add-pj').addClass('collapse');
    $('#form-add-pn').removeClass('collapse');
  }
  limpiarproveedor();
});

$(document).on('hidden.bs.modal', '#proveedor', function (e)
{
  limpiarproveedor();
});


$(document).on('click', '.add_proveedor', function (e)
{
    limpiarproveedor();
    $('#id_provart').val(0)
});

function limp_todo(form, diverror)
{
  $('#'+form+' input').each(function (index, value){
    if($(this).attr('type')=="checkbox")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
      $(this).prop('checked', false);
    }
    
    if($(this).attr('type')=="text")
    { 
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      $(this).val('');

      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
    }    

    if($(this).attr('type')=="hidden"){
      $(this).val('');
    }    
  });

  $('#'+form+' select').each(function (index, value){
    if($(this).parents('.form-group').attr('class')=="form-group has-error")
    {
      $(this).parents('.form-group').removeClass('has-error');
    }
    $(this).val('');

    id = $(this).attr('id');
    if($('#'+id+'-error').length>0)
    {
      $('#'+id+'-error').html('');
    }
  });

  if($('#'+diverror).length>0)
  {
    $('#'+diverror).html('');
  }

  $('#'+form+' textarea').each(function (index, value){
    $(this).val('');
  });

  var valimp = $( "form#"+form ).validate();
  valimp.resetForm();
}

function limpiarproveedor()
{
    limp_todo('form-add-pj');
    limp_todo('form-add-pj');
}


$(document).on('click','.delete_prov',function () {
   var padre = $(this).parents('tr');
   var tipo = padre.attr('tipo_persona');
   var idpersona = padre.attr('idpersona');
   var idmarca = $('#id_marcas').val()
   var id_codigo = $('#id_codigo').val();

   var param = 'tipo_persona='+tipo+'&id_persona='+idpersona+'&id_marca='+idmarca+'&id_codigo='+id_codigo;
    $.ajax({
        url: $('base').attr('href') + 'codigo/delete_prov',
        type: 'POST',
        data: param,
        dataType: "json",
        beforeSend: function() {
            //showLoader();
        },
        success: function(response) {
            if (response.code==1) {
              var tipo = $('#myTab li.active').attr('tabs');
              var id_art_sucursal = $('#id_art_sucursal').val();
              window.location.reload();
            }
        },
        complete: function() {
            //hideLoader();
        }
    });
});
