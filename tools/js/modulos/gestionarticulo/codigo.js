$( document ).ready(function() {

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        ignore: ""
    });

    $('#form_save_codigo').validate({
        rules:
        {
            codigo:
            {
                required:true,
                minlength: 2,
                remote: {
                    url: $('base').attr('href') + 'codigo/validar_codigo',
                    type: "post",
                    data: {
                        codigo: function() { return $( "#codigo" ).val(); },
                        id_codigo: function() { return $( "#id_codigo" ).val(); }
                    }
                  }
            }       
        },
        messages: 
        {
            codigo:
            {   
                required:"Ingresar código",
                minlength: "Más de 2 Letras",
                remote: "Este código ya existe."
            }
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');       
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            error.insertAfter(element.parent()); 
        },
        submitHandler: function() {
            var ids_marc = idsxtr();
                       
            if(ids_marc)
            {
                $.ajax({
                    url: $('base').attr('href') + 'codigo/save_codigo',
                    type: 'POST',
                    data: $('#form_save_codigo').serialize()+"&id_marca="+ids_marc,
                    dataType: "json",
                    beforeSend: function() {
                    
                    },
                    success: function(response) {
                        if (response.code==1) 
                        {
                            swal({
                                title : 'Listo!',
                                text: 'Este codigo se Guardo!',
                                type: 'success',
                                confirmButtonText: 'Listo!',
                            }).then(function () {
                                location.reload();
                            });
                        }
                        else
                        {
                            swal('No Guardo!', 'Hubo un error', 'error');
                        }
                    },
                    complete: function() {

                    }
                });
            }
            else
            {
                swal('Debe elegir','al menos una marca','error');
            }
        }
    });
});

function idsxtr (){
    var c = 0;
    var i = new Array();
    $('#marcas_tbody tr').each(function(index,value){
        c++;
        console.log($(this).attr('idmarca'));
        i.push($(this).attr('idmarca'));
    });

    return (c) ? i.join() : null;
} 

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_codigo(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_codigo(page);
});
    
$(function () {
    $("#marca_autoc").autocomplete({
        type:'POST',
        serviceUrl: $('base').attr('href')+"marca/get_marca_autocomplete",
        params: { 'w_n': function(){ return idsxtr();} },
        onSelect: function (suggestion) 
        {
            var id_marca = suggestion.data;
            var marca = suggestion.value;

            $('#selected_marca').attr('idmarca',id_marca).val(marca);
            
        }
    });
});

$(document).on('click','.add_marca',function () {
    $('.def').remove();
    var id_marca = parseInt($('#selected_marca').attr('idmarca'));
    if(id_marca)
    {   
        var marca = $('#selected_marca').val();
        var txt =   "<tr idmarca='"+id_marca+"'>"+
                        "<td class='text-center'>"+marca+"</td>"+
                        "<td class='text-center'><a class='btn btn-danger delete_marca btn-xs'><i class='fa fa-trash-o'></i></a></td>"+
                    "</tr>";
        $('#marcas_tbody').append(txt);
        $('#selected_marca').attr('idmarca',null).val('');
        $('#marca_autoc').val('');
    }
    else
    {
        swal('¡¡No ha seleccionado','ninguna marca!!','error');
    }
});

$(document).on('click','.delete_marca',function(){
    $(this).parents('tr').remove();
});

$(document).on('click','.btn_limpiar',function(){
    limpiar_form();
    $('#edit_codigo').modal('hide');
});

function limpiar_form(){
    $('#form_save_codigo input[type=hidden]').val('');
    $('#form_save_codigo input[type=text]').val('');
    $('#form_save_codigo textarea').val('');
    $('#estado label').removeClass('active');
    $('#estado input').prop('checked',false);
    $('#estado #estado_1').prop('checked',true);
    $('#estado #estado_1').parent().addClass('active');
    $('#myModalLabel').text('Crear nuevo Número de parte');
    $('#marcas_tbody').html('<tr class="def"><td colspan=2><h3 class="text-center text-success">No hay registro</h3></td></tr>');
}

$(document).on('click','.edit',function(){
    limpiar_form();
    var id = $(this).parents('tr').attr('id_codigo');
    if(id)
    {
        $.ajax({
            url: $('base').attr('href') + 'codigo/edit',
            type: 'POST',
            data: 'id_codigo='+id,
            dataType: "json",
            beforeSend: function() {
            
            },
            success: function(response) {
                if (response.code==1) 
                {
                    var r = response.data;
                    $('#id_codigo').val(r.id_codigo);

                    var estado = r.estado;
                    $('#estado input').prop('checked',false);
                    $('#estado label').removeClass('active');

                    $('#estado #estado_'+estado).prop('checked',true);
                    $('#estado #estado_'+estado).parent().addClass('active');

                    $('#descripcion').val(r.descripcion);
                    $('#descripcion_2').val(r.descripcion_2);
                    var desc_c = r.descripcion_completa;
                    $('#descripcion_completa').val(desc_c);
                    $('#codigo').val(r.codigo);
                    $('#marcas_tbody').html(r.table);
                    $('#myModalLabel').text('Editar Número de parte: '+r.codigo);
                }
                else
                {
                    swal('Hubo un error al momento', ' de obtener los Datos!!', 'error');
                }
            },
            complete: function() {}
        });
    }
});

$(document).on('click','.delete',function(e) {
    e.preventDefault();
    var id_codigo = $(this).parents('tr').attr('id_codigo');
    var page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
    var nomb = "Código";
  
    swal({
        title: 'Estas Seguro?',
        text: "De Eliminar este "+nomb,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, estoy seguro!'
    }).then(function(isConfirm) {
        if (isConfirm) {     
            $.ajax({
                url: $('base').attr('href') + 'codigo/save_codigo',
                type: 'POST',
                data: 'id_codigo='+id_codigo+'&estado=0',
                dataType: "json",
                beforeSend: function() {
                    //showLoader();
                },
                success: function(response) {
                    if (response.code==1) {              
                        buscar_codigo(page);
                    }
                },
                complete: function() {
                    var text = "Elimino!";
                    swal(text, 'Este '+nomb+' se '+text+'.', 'success');
                    limpiar_form();
                }
            });
        }
    });  
});

function buscar_codigo(page)
{
    var temp = "page="+page;
    var cod = $('#cod_busc').val();
    var desc = $('#desc_busc').val();
    var desc2 = $('#desc2_busc').val();
    var estado = $('#estado_busc').val();
    var descripcion_completa = $('#descripcion_completa_busc').val();

    if(cod.trim().length)
    {
        temp=temp+'&cod='+cod;
    }
    if(desc.trim().length)
    {
        temp=temp+'&desc='+desc;
    }
    if(desc2.trim().length)
    {
        temp=temp+'&desc_2='+desc2;
    }
    if(descripcion_completa.trim().length)
    {
        temp=temp+'&descripcion_completa='+descripcion_completa;
    }

    if(parseInt(estado)>-1)
    {
        temp=temp+'&estado='+estado;
    }
    $.ajax({
        url: $('base').attr('href') + 'codigo/buscar_codigos',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
        //showLoader();
        },
        success: function(response) {
        if (response.code==1) {
            $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
            $('#paginacion_data').html(response.data.paginacion);
        }
        },
        complete: function() {
        //hideLoader();
        }
    });
}

$(document).on('click','.buscar',function () {
   buscar_codigo(0); 
});

$(document).on('click','.limpiarfiltro',function(){
    $('#filtro input').val('');
    $('#filtro select').val('-1');
    buscar_codigo(0);
});

$(document).on('click','.nuevo',function(){
   limpiar_form(); 
});