$( document ).ready(function() {

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });

    $('#form_login').validate({
      rules:
      {
        usuario: { required:true},
        password: {required:true}
      },
      messages: 
      {
        usuario: {required:"Ingrese usuario"},
        contraseña: {required:"Ingrese contraseña"}
      },      

      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
        //error.insertAfter(element.parent()); 
      },
      submitHandler: function() {
        $.ajax({
          url: $('base').attr('href') + 'login/iniciar_sesion',
          type: 'POST',
          data: $('#form_login').serialize(),
          dataType: "json",
          beforeSend: function() {
              //showLoader();
          },
          success: function(response) {
            if (response.code==1) {
              swal({
                title: '¡¡Usuario Correcto!!',
                text: "¡¡Bienvenido!!",
                type: 'success',
                confirmButtonText: 'Listo!'
              }).then((result) => {
                if (result.value) {
                  window.location.replace($('base').attr('href')+"menu");
                }
              })
            }
            else
            {
              swal('Usuario o Contraseña','Incorrecta','error');
            }
          },
          complete: function(response) {
            
          }
        });
      }
    });
});
