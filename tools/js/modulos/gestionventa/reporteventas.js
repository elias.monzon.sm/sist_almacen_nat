$( document ).ready(function() {
  $('#fecha_ini').datetimepicker({
    locale: moment.locale('es'),
    format: 'DD-MM-YYYY'
  });

  $('#fecha_fi').datetimepicker({
      locale: moment.locale('es'),
      format: 'DD-MM-YYYY',
      useCurrent: false
  });
});

$("#fecha_ini").on("dp.change", function (e) {
    $('#fecha_fi').data("DateTimePicker").minDate(e.date);
});
$("#fecha_fi").on("dp.change", function (e) {
    $('#fecha_ini').data("DateTimePicker").maxDate(e.date);
});

$(document).on('click','.buscar',function(){
  var fecha_ini = $('#fecha_inicio').val();
  var fecha_fin = $('#fecha_fin').val();

  var temp = 'fecha_ini='+fecha_ini+'&fecha_fin='+fecha_fin;
  if(!fecha_fin || !fecha_ini)
  {
    swal('Seleccione los rangos de ','fechas en el filtro','error');
  }
  else{
    $.ajax({
        url: $('base').attr('href') + 'reporteventas/get_reporteventas',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
          
        },
        success: function(response) {
            if (response.code==1) 
            {
              $('#bodyindex').html(response.data);      
            }
            else
            {
              swal('No se encontraron','registros de ventas.','info');
              $('#bodyindex').html(response.data);   
            }
        },
        complete: function() {

        }
    });
  }
});