$( document ).ready(function() {

    jQuery.validator.setDefaults({
      debug: true,
      success: "valid",
      ignore: ""
    });

    $('#form_save_servicio').validate({
      rules:
      {
        servicio: {
          required:true, 
          minlength: 2,
          remote: {
            url: $('base').attr('href') + 'servicios/validar_servicio',
            type: "post",
            data: {
              servicio: function() { return $( "#servicio" ).val(); },
              id_servicio: function() { return $('#id_servicio').val(); }
            }
          }
        },
        id_tipomoneda: {
          required: true
        }
      },
      messages: 
      {
        servicio: {required:"servicio", minlength: "Más de 2 Letras", remote: "Ya existe" },
        id_tipomoneda : { required : "Seleccione"}
      },      

      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
        if(element.parent('.col-md-6').length) 
        {
          error.insertAfter(element.parent()); 
        }
      },
      submitHandler: function() {
        $.ajax({
          url: $('base').attr('href') + 'servicios/save_servicio',
          type: 'POST',
          data: $('#form_save_servicio').serialize(),
          dataType: "json",
          beforeSend: function() {
              //showLoader();
          },
          success: function(response) {
            if (response.code==1) {
              var page = 0;
              if($('#paginacion_data ul.pagination li.active a').length>0)
              {
                page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
              }                     

              buscar_servicios(page);
            }
            else
            {
              if($('#servicio').parents('.form-group').attr('class')=="form-group")
              {
                $('#servicio').parents('.form-group').addClass('has-error');
              }
              
              if($('#servicio-error').length>0)
              {
                $('#servicio-error').html(response.message);
              }
              else
              {
                $('#servicio').parents('.col-md-6').append("<span id='servicio-error' class='help-block'>"+response.message+"</span>");
              }
            }
          },
          complete: function(response) {
            var id_servicio = parseInt($('#id_servicio').val());
            id_servicio = (id_servicio>0) ? (id_servicio) : ("0");
            var text = (id_servicio=="0") ? ("Guardo!") : ("Edito!");
            if(response.code == 0)
            {
              text = response.message;
            }
            swal(text, 'Este servicio se '+text+'.', 'success');
            limp_todo('form_save_servicio');
            $('#editservicio').modal('hide');
            $('#id_servicio').val('');
          }
        });
      }
    });
});

$(document).on('click', '#datatable-buttons .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_servicios(0);
});

$(document).on('click', '.add_servicio', function (e)
{
  limp_todo('form_save_servicio');
});

$(document).on('click', '.btn_limpiar', function (e) {
  limp_todo('form_save_servicio');
  $('#editservicio').modal('hide');
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_servicios(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_servicios(page);
});

$(document).on('click', '#datatable-buttons .buscar', function (e) {
  var page = 0;
  buscar_servicios(page);
});

function buscar_servicios(page)
{
  var alm = $('#alm').val();
  var id_tipomoneda = $('#id_tipomoneda_busc').val();

  var temp = "page="+page;
  if(alm.trim().length)
  {
    temp=temp+'&alm='+alm;
  }
  if(parseInt(id_tipomoneda)>0)
  {
    temp=temp+'&id_tipomoneda='+id_tipomoneda;
  }
  $.ajax({
    url: $('base').attr('href') + 'servicios/buscar_servicios',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      //showLoader();
    },
    success: function(response) {
      if (response.code==1) {
        $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

$(document).on('change', '#id_departamento', function (e)
{
  var id_departamento = $(this).val();

  $.ajax({
    url: $('base').attr('href') + 'ubigeo/combox_prov',
    type: 'POST',
    data: 'id_departamento='+id_departamento,
    dataType: "json",
    success: function(response) {
      if (response.code == "1") {
        $('#id_provincia').html(response.data);
        $('#id_distrito').html("<option value=''>DISTRITO</option>");
      }
    },
    complete: function() {
        //hideLoader();
    }
  });
});


$(document).on('change', '#id_provincia', function (e)
{
  var id_provincia = $(this).val();

  $.ajax({
    url: $('base').attr('href') + 'ubigeo/combox_dist',
    type: 'POST',
    data: 'id_provincia='+id_provincia,
    dataType: "json",
    success: function(response) {
      if (response.code == "1") {
        $('#id_distrito').html(response.data);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click', '.edit', function (e) {
  var idservicio = $(this).parents('tr').attr('idservicio');
  $.ajax({
    url: $('base').attr('href') + 'servicios/edit',
    type: 'POST',
    data: 'id_servicio='+idservicio,
    dataType: "json",
    beforeSend: function() {
      limp_todo('form_save_servicio');
    },
    success: function(response) {
      if (response.code==1) {
        $('#id_servicio').val(response.data.id_servicio);
        $('#servicio').val(response.data.servicio);

        $('#Estedo label').removeClass('active');
        $('#Estedo input').prop('checked', false);

        var num = response.data.Estedo;
        $('#Estedo #Estedo_'+num).prop('checked', true);
        $('#Estedo #Estedo_'+num).parent('label').addClass('active');
        $('#id_tipomoneda').val(response.data.id_tipomoneda);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click', '.delete', function (e) {
  e.preventDefault();
  var idservicio = $(this).parents('tr').attr('idservicio');
  var page = $('#paginacion_data ul.pagination li.active a').attr('tabindex');
  var nomb = "servicio";

  swal({
    title: 'Estes Seguro?',
    text: "De Eliminar Este "+nomb,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, estoy seguro!'
  }).then(function(isConfirm) {
    if (isConfirm) {     
      $.ajax({
        url: $('base').attr('href') + 'servicios/save_servicio',
        type: 'POST',
        data: 'id_servicio='+idservicio+'&Estedo=0',
        dataType: "json",
        beforeSend: function() {
          //showLoader();
        },
        success: function(response) {
          if (response.code==1) {              
            buscar_servicios(page);
          }
        },
        complete: function() {
          var text = "Elimino!";
          swal(text, 'Este '+nomb+' se '+text+'.', 'success');
          limp_todo('form_save_servicio');
        }
      });
    }
  });    
});

function limp_todo(form, diverror)
{
  $('#'+form+' input').each(function (index, value){
    if($(this).attr('type')=="checkbox")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
      $(this).prop('checked', false);
    }

    if($(this).attr('type')=="text")
    {
      if($(this).parents('.form-group').attr('class')=="form-group has-error")
      {
        $(this).parents('.form-group').removeClass('has-error');
      }
      $(this).val('');

      id = $(this).attr('id');
      if($('#'+id+'-error').length>0)
      {
        $('#'+id+'-error').html('');
      }
    }    

    if($(this).attr('type')=="hidden"){
      $(this).val('');
    }    
  });
}