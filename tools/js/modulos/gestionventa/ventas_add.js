$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });

  $('#fecha_busc').daterangepicker({
    singleDatePicker: true,
    format: 'DD-MM-YYYY',
    calender_style: "picker_4"
    }, 
    function(start, end, label) {}
  );
  
  $('#form_save_salidaxventa').validate({

    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        error.insertAfter(element.parent()); 
    },
    submitHandler: function() {

      if(validar())
      {
        $.ajax({
          url: $('base').attr('href') + 'ventas/save_salidaxventa',
          type: 'POST',
          data: $('#form_save_salidaxventa').serialize(),
          dataType: "json",
          beforeSend: function() {
          
          },
          success: function(response) {
            if (response.code==1) 
            {
              swal({
                title: 'Documento de Venta',
                text: "¡Guardado correctamente!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Listo!'
              }).then((result) => {
                if (result.value) {
                  window.location.href = $('base').attr('href') + 'ventas';
                }
              })
            }
            else
            {
                swal('No Guardo!', 'Hubo un error', 'error');
            }
          },
          complete: function() {

          }
        });
      }
    }
  });
});

function get_selected_arti() {
  var c = 0;
  var i = new Array();
  $('#produc_content tr[tipo="arti"]').each(function (index,value) {
    var $this = $(this);
    var idcod = $this.attr('idcod');
    var idmar = $this.attr('idmar');

    var tx = idcod+","+idmar;
    i.push(tx);
    c++;
  });

  return (c) ? i.join("),(") : "";
}

function buscar_arti(page) {
  var w_not = get_selected_arti();
  var id_tipomoneda = $('#id_tipomoneda').val();
  var temp = 'page='+page+'&w_not='+w_not+'&id_tipomoneda='+id_tipomoneda ;
  var codigo = $('#codigo').val();
  var marca = $('#marca').val();
  var descripcion = $('#descripcion').val();
  var descripcion_2 = $('#descripcion_traducida').val();

  if(marca.trim().length)
  {
    temp += '&marca='+marca;
  }

  if(descripcion.trim().length)
  {
    temp += '&descripcion='+descripcion;
  }

  if(codigo.trim().length)
  {
    temp += '&codigo='+codigo;
  }

  if(descripcion_2.trim().length)
  {
    temp += '&descripcion_2='+descripcion_2;
  }


    $.ajax({
      url: $('base').attr('href') + 'ventas/get_artiventa',
      type: 'POST',
      data: temp,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.code==1) {
          $('#buscar_arti tbody').html(response.data.rta);
          $('#arti_paginacion_data').html(response.data.paginacion);
        }
      },
      complete: function() {
        
      }
    });
}

$(document).on('click','.buscarart',function(){
  var id_moneda = $('#id_tipomoneda').val();
  if(id_moneda)
  {
    $('#buscarart').modal('show');
    buscar_arti(0);  
  }
  else
  {
    swal('error','Seleccione un Tipo de Moneda','error');
  }
});

function sumartotal() 
{
  var c = 0, c1 = 0;
  var ptotal = 0, val = 0;
  var row = "";    
  
  $('#produc_content tr[tipo="ser"]').each(function (index,value) {
    ptotal += parseFloat($(this).find('input.pt').val());
    simbol = $(this).find('.simbol').text();
  });

  $('#produc_content tr[tipo="arti"]').each(function (index,value) {
    ptotal += parseFloat($(this).find('input.pt').val());
    simbol = $(this).find('.simbol').text();
  });

  if(ptotal)
  {
    ptotal=parseFloat(ptotal);
    var sign = simbol;
    $('.tot').remove();

    row = "<tr class='tot'>"+
            "<td colspan=5>TOTAL</td>"+
            "<td><span class='txsign'>"+sign+" </span>"+ptotal.toFixed(2)+"<input type='hidden' name='p_total' value='"+ptotal+"'></td>"+
            "<td colspan=2>"+
          "</tr>";
  }
  $('tr.tot').remove();
  $('#produc_content').append(row);
}

$(document).on('change','.sele',function () {
  var val = $(this).val();
  var txtalm = $(this).parent().find('input');
  txtalm.val('');
  if(val)
  {
    var tx = $(this).find('option:selected').text();
    var nm = "txtalm["+val+"]";
    txtalm.val(tx);
    txtalm.prop('name',nm);

  }
});

function limpiarformsave() {
  $('#edita_arti').val('no');
  $('#cantidad').val('');
  $('#preciou').val('');
  $('#preciot').val('');
}

$(document).on('click','.delete_art',function(){
  $(this).parents('tr').remove();
  sumartotal();
});

$(document).on('click','#myTab li',function () {
  var active = $(this).attr('tabs');
  if(active=="articulos")
  {
    buscar_arti(0);
  }
  else
  {
    buscar_serv(0);
  }
});

function buscar_serv(page){
  var w_not = get_selected_serv();
  var servicio = $('#servicios').val();
  var temp = 'page='+page+'&w_not='+w_not ;
  if(servicio.trim().length)
  {
    temp += '&servicio='+servicio;
  }
  
  $.ajax({
    url: $('base').attr('href') + 'ventas/get_serventa',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        $('#buscar_serv tbody').html(response.data.rta);
        $('#serv_paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

function get_selected_serv(){}

$(document).on('click','.add_arti',function () {
  var padre = $(this).parents('tr');
  var id_codigo = padre.attr('id_codigo');
  var id_marca = padre.attr('id_marca');
  var codigo = padre.find('td.tdcod').text();
  var marca = padre.find('td.tdmarca').text();
  var desc = padre.find('td.desc1').text();
  $.ajax({
    url: $('base').attr('href') + 'ventas/get_addartiventa',
    type: 'POST',
    data: 'id_codigo='+id_codigo+'&id_marca='+id_marca,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        var res = response.data;
        var simbol = res.precio_uni.simbolo;
        var factor_cambio = res.precio_uni.factor_cambio;
        var preciov = res.precio_uni.precio_venta;
        var cbx = res.cbx;
        var tx = "<tr tipo='arti' idcod='"+id_codigo+"' idmar='"+id_marca+"'>"+
                    "<td>"+codigo+"<input type='hidden' name='id_codigomarca["+id_codigo+"]["+id_marca+"]' value='["+id_codigo+"]["+id_marca+"]'><input type='hidden' class='txcod' name='txtcod["+id_codigo+"]' value='"+codigo+"'>"+
                    "<td>"+desc+"<input type='hidden' name='factor_cambio["+id_codigo+"]["+id_marca+"]' value='"+factor_cambio+"'></td>"+
                    "<td>"+marca+"<input type='hidden' class='txmar' name='txtmar["+id_marca+"]' value='"+marca+"'></td>"+
                    "<td><input type='text' class='cant' name='cant["+id_codigo+"]["+id_marca+"]'></td>"+
                    "<td><span class='txsign'>"+simbol+" </span><input style='width:70%;' class='pv' type='text' value='"+preciov+"' name='pu["+id_codigo+"]["+id_marca+"]'></td>"+
                    "<td><span class='txsign'>"+simbol+" </span><input type='text' style='width:70%;' readonly class='pt' name='pt["+id_codigo+"]["+id_marca+"]'></td>"+
                    "<td><select class='sele' name='alm["+id_codigo+"]["+id_marca+"]'>"+cbx+"</select></td>"+
                    "<td class='text-center'><a class='btn btn-danger delete_art btn-xs'><i class='fa fa-trash-o'></i></a></td>"+
                    "</tr>";
        $('#defaultr').remove();
        $('#produc_content').append(tx);
        sumartotal(); 
        buscar_arti(0);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('keyup','.cant',function () {
  var val = parseFloat($(this).val());
  if(val>0)
  {
    var pu = parseFloat($(this).parents('tr').find('.pv').val());
    var pt = val * pu;
    pt = pt ? pt : "0.00";
    $(this).parents('tr').find('.pt').val(pt);
  }
  else
  {
    $(this).val('');
    $(this).parents('tr').find('.pt').val('0.00');
  }
  sumartotal();
});

$(document).on('keyup','.pv',function () {
  var val = parseFloat($(this).val());

  if(val>0)
  {
    var cant = parseFloat($(this).parents('tr').find('.cant').val());
    var pt = val * cant;
    pt = pt ? pt : "0.00";
    $(this).parents('tr').find('.pt').val(pt);
  }
  else
  {
    $(this).val('');
    $(this).parents('tr').find('.pt').val('0.00');
  }

  sumartotal();
});

$(document).on('click','.add_serv',function () {
  var padre = $(this).parents('tr');
  var id_servicio = padre.attr('id_servicio');
  var servicio = padre.find('td.tdserv').text();
  $.ajax({
    url: $('base').attr('href') + 'ventas/get_addserviventa',
    type: 'POST',
    data: 'id_servicio='+id_servicio,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        var res = response.data;
        var simbol = res.simbolo;
        var factor_cambio = res.factor_cambio;

          var tx = "<tr tipo='ser' idserv='"+id_servicio+"'>"+
              "<td></td>"+
              "<td>"+servicio+"<input type='hidden' name='serv["+id_servicio+"]' value='"+id_servicio+"'><input type='hidden' name='txtserv["+id_servicio+"]' value='"+servicio+"'></td>"+
              "<td></td>"+
              "<td><input type='text' class='cant' name='cantserv["+id_servicio+"]'><input type='hidden' name='servfactor_cambio["+id_servicio+"]' value='"+factor_cambio+"'></td>"+
              "<td><span class='simbol'>"+simbol+" </span><input style='width:70%;' class='pv' type='text' name='puserv["+id_servicio+"]'></td>"+
              "<td><span>"+simbol+" </span><input type='text' style='width:70%;' readonly class='pt' name='ptserv["+id_servicio+"]'></td>"+
              "<td></td>"+
              "<td class='text-center'><a class='btn btn-danger delete_art btn-xs'><i class='fa fa-trash-o'></i></a></td>"+
              "</tr>";

        $('#defaultr').remove();
        $('#produc_content').append(tx);
        sumartotal();
        buscar_serv(0);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
});

$(document).on('click','.bandeja_cli',function () 
{
  buscar_cliente(0);
});

function buscar_cliente(page) {
  var nombre_comercial = $('.nombres_com').val();
  var razon_social = $('.razon_soc').val();
  var ruc_dni = $('.ruc_dni').val();
  var param = 'page='+page;

  if(nombre_comercial.trim().length>0)
  {
    param += '&nombres_com='+nombre_comercial;
  }

  if(razon_social.trim().length>0)
  {
    param += '&razon_soc='+razon_social;
  }

  if(ruc_dni.trim().length>0)
  {
    param += '&ruc_dni='+ruc_dni;
  }

  $.ajax({
    url: $('base').attr('href') + 'ventas/get_clientes',
    type: 'POST',
    data: param,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        $('#buscar_cliente tbody').html(response.data.rta);
        $('#buscarcliente #paginacion_datap').html(response.data.paginacion);
      }
    },
    complete: function() {

    }
  });
}

$(document).on('click','.agre_cliente',function () {
  var padre = $(this).parents('tr');
  var rucdni = padre.attr('rucdni');
  var doc = padre.attr('doc');
  var rz = padre.find('td.rz').html();
  var idcliente = padre.attr('idcliente');
  var tipope = padre.attr('tipope');

  $('#id_persona').val(idcliente);
  $('#tipo_persona').val(tipope);

  var tex = doc+': '+rucdni+','+ rz;
  $('#ruc_dni').val(tex);
  $('#buscarcliente').modal('hide')
});

$(document).on('change','#id_tipodocumento',function () {
  var val = $(this).val();
  if(val)
  {
    var maneja_stock = $(this).find('option:selected').attr('maneja_stock');
    $('#afecta_kardex').val(maneja_stock);
  }
});

function validarstock()
{
  pasa = true
  $('#produc_content tr[tipo="arti"]').each(function (index,value) {
    var $this = $(this);
    var cant = $this.find('.cant').val();
    var stockr = parseFloat($this.find('select option:selected').attr('stock'));
    var stockr = (stockr) ? stockr : 0;
    
    if(cant>stockr)
    {
      if(pasa == true)
      {
        pasa = false;
      }
      $this.css('background-color','#dd4b39');
    }
    else
    {
      $(this).css('background-color','#f9f9f9');
    }
  });

  return pasa;
}

function validar_almacenseleccionados() 
{
  pasa = true
  $('#produc_content tr[tipo="arti"]').each(function (index,value) {
    var $this = $(this);
    var select = $this.find('select').val();
    if(!select)
    {
      if(pasa == true)
      {
        pasa = false;
      }
    }
  });

  return pasa;
}

$(document).on('click', '#buscarcliente #datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_cliente(page);
});

$(document).on('click', '#buscarcliente #datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_cliente(page);
});

$(document).on('click', '#buscarcliente .buscarcliente', function (e) {
  var page = 0;
  buscar_cliente(page);
});

$(document).on('click','.limpiarfiltro',function(){
  $('#filtrocliente input').val('');
  buscar_cliente(0);
});

function validar() {
  if(parseInt($('#id_tipomoneda').val())<=0)
  {
    swal('Seleccione','un tipo de moneda','error');
    return false;    
  }

  if(parseInt($('#id_tipodocumento').val())<=0)
  {
    swal('Seleccione','un tipo de documento','error');
    return false;
  }

  if($('input[name=p_total]').length < 1 || parseFloat($('input[name=p_total]').val())<=0 )
  {
    swal('No se ha agregado','ningún producto o el Costo Total no es mayor a 0','error');
    return false;
  }

  if(!validar_almacenseleccionados())
  {
    swal('Falta seleccionar un','Almacen!!','error');
    return false;
  }

  if(!validarstock())
  {
    swal('Revisar el stock disponible','en los articulo marcados de rojo!!!','error');
    return false;
  }

  return true;
}

$(document).on('click', '#tab_serv #datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_serv(page);
});

$(document).on('click', '#tab_serv #datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_serv(page);
});

$(document).on('click', '#buscar_serv .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_serv(0);
});

$(document).on('click', '#buscar_serv .buscar', function (e) {
  buscar_serv(0);
});

$(document).on('click', '#tab_arti #datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_arti(page);
});

$(document).on('click', '#tab_arti #datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_arti(page);
});

$(document).on('click', '#tab_arti #buscar_arti .limpiarfiltro', function (e) {
  var id = $(this).parents('tr').attr('id');
  $('#'+id).find('input[type=text]').val('');
  buscar_arti(0);
});

$(document).on('click', '#buscar_arti .buscar', function (e) {
  buscar_arti(0);
});