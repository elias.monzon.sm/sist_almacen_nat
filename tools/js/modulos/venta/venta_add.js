$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });


  $('#form_venta_arti').validate({
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
          error.insertAfter(element.parent()); 
      },
      submitHandler: function() {
          $.ajax({
              url: $('base').attr('href') + 'venta/concluir_venta',
              type: 'POST',
              data: $('#form_venta_arti').serialize(),
              dataType: "json",
              beforeSend: function() {
                
              },
              success: function(response) {
                if (response.code==1) 
                {
                  swal({
                    title : 'Listo!',
                    text: 'Esta venta se guardo!',
                    type: 'success',
                    confirmButtonText: 'Listo!',
                  }).then(function () {
                    window.location.replace($('base').attr('href') + 'venta');
                  });
                }
                else
                {
                  swal('No Guardo!', 'Hubo un error', 'error');
                }
              },
              complete: function() {

              }
          });/**/
      }
  });

});

$(document).on('click','.add_artiventa',function(){
  var id_arti = $(this).parents('tr').attr('id_articulo');
  var arti = $(this).parents('tr').find('td.arti').text();
  $.ajax({
      url: $('base').attr('href') + 'ingresoarti/get_stockxarti',
      type: 'POST',
      data: 'id_articulo='+id_arti,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.code>0) 
        {
          $('#arti').text(arti);
          $('#id_articulo').val(id_arti);
          if(response.code==2)
          {
            $('#stock').val(response.data.stock);
            $('#precio_uni').text(response.data.precio_unitario);
          }
        }
      },
      complete: function() {

      }
  });
});

$(document).on('focusout','#cantidad',function(){
  var cantidad = $(this).val();
  var precio_uni = parseFloat($('#precio_uni').text());
  console.log(cantidad);
  console.log(precio_uni);
  var precio = (!isNaN(parseFloat(cantidad))) ? cantidad*precio_uni : "0.00";
  $('#total').val(precio);
});

$(document).on('click','.save_table',function(){
  var stock = parseFloat($('#stock').val());
  var cantidad = parseFloat($('#cantidad').val());
  var id = $('#id_articulo').val();
  console.log(stock);
  console.log(cantidad);
  if(stock>0 && stock>=cantidad)
  {
    var arti = $('#arti').text();
    var precio_uni = $('#precio_uni').text();
    var precio_total = $('#total').val();
    
    var btn = "<a class='btn btn-primary delete_this btn-xs'><i class='fa fa-trash'></i> Eliminar</a>";

    var txt ="<tr>";
    txt += "<td><center>"+arti+"<input type='hidden' name='arti["+id+"]' value='"+id+"'></center></td>";
    txt +="<td><center>S/. "+precio_uni+"<input type='hidden' name='preu["+id+"]' value="+precio_uni+"></center></td>";
    txt +="<td class='p_total'><center>S/. "+precio_total+"<input type='hidden' name='pret["+id+"]' value="+precio_total+"></center></td>";
    txt +="<td><center>"+cantidad+"<input type='hidden' name='cant["+id+"]' value="+cantidad+"></center></td>";
    txt +="<td><center>"+btn+"</center></td>";

    $('#body_articulos_add').append(txt);
    swal('Artículo agregado','Exitosamente!!','success');
    $('#ventaxarti').modal('hide');
  }
  else
  {
    swal('Error!','Stock Insuficiente!!!','error');
  }

});

$(document).on('click','.delete_this',function(){
  $(this).parents('tr').remove();
  swal('Este artículo','ha sido eliminado de la lista.','info');
});