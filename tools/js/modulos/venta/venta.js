$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });

  
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_ventas(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_ventas(page);
});

function buscar_ventas(page)
{
    var temp = "page="+page;

    $.ajax({
        url: $('base').attr('href') + 'ventas/buscar_ventas',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
        //showLoader();
        },
        success: function(response) {
        if (response.code==1) {
            $('#datatable-buttons tbody#bodyindex').html(response.data.data);
            $('#paginacion_data').html(response.data.paginacion);
        }
        },
        complete: function() {
        //hideLoader();
        }
    });
}