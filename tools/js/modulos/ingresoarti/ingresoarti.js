$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });

  $('#form_ingreso_arti').validate({
      rules:
      {
        cantidad:
        {
          required:true
        }
      },
      messages: 
      {
        cantidad:
        {
          required:"Ingresar cantidad"
        }
      },      

      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');       
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) 
      {
          error.insertAfter(element.parent()); 
      },
      submitHandler: function() {
          $.ajax({
              url: $('base').attr('href') + 'ingresoarti/add_stock',
              type: 'POST',
              data: $('#form_ingreso_arti').serialize(),
              dataType: "json",
              beforeSend: function() {
                
              },
              success: function(response) {
                if (response.code==1) 
                {
                  swal({
                    title : 'Listo!',
                    text: 'Stock Actualizado!',
                    type: 'success',
                    confirmButtonText: 'Listo!',
                  }).then(function () {
                    location.reload();
                  });
                }
                else
                {
                  swal('No se actualizó!', 'Hubo un error', 'error');
                }
              },
              complete: function() {
                $('#ingresostock').modal('hide');
              }
          });/**/
      }
  });
});

$(document).on('change','#id_marca',function(){
  var id_marca = $(this).val();

  $.ajax({
      url: $('base').attr('href') + 'codigo/cbx_codigo',
      type: 'POST',
      data: 'id_marca='+id_marca,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.code==1) 
        {
          $('#id_codigo').html(response.data);
        }
      },
      complete: function() {

      }
  });
});

$(document).on('change','#marca_busc',function(){
  var id_marca = $(this).val();

  $.ajax({
      url: $('base').attr('href') + 'codigo/cbx_codigo',
      type: 'POST',
      data: 'id_marca='+id_marca,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.code==1) 
        {
          $('#cod_busc').html(response.data);
        }
      },
      complete: function() {

      }
  });
});

$(document).on('click','.buscar',function(){
  var id_marca = $('#marca_busc').val();
  var id_cod = $('#cod_busc').val();
  var arti = $('#arti_busc').val();
  var temp = "";
  if(id_marca)
  {
    temp=temp+'&id_marca='+id_marca;
  }
  if(id_cod)
  {
    temp=temp+'&id_cod='+id_cod;
  }
  if(arti.trim().length)
  {
    temp=temp+'&arti='+arti;
  }

  $.ajax({
      url: $('base').attr('href') + 'articulo/buscar_articulos',
      type: 'POST',
      data: temp,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.error_code==1) 
        {
          $('#datatable-buttons tbody').html(response.data);
        }
      },
      complete: function() {

      }
  });
});

$(document).on('click','.limpiarfiltro',function(){
  $('#marca_busc').val('');
  $('#cod_busc').html('');
  $('#filtro input').val('');

  $('.buscar').trigger('click');
});

$(document).on('click','.add_stock',function(){
  var id_arti = $(this).parents('tr').attr('id_articulo');
  var arti = $(this).parents('tr').find('td.arti').text();
  $.ajax({
      url: $('base').attr('href') + 'ingresoarti/get_stockxarti',
      type: 'POST',
      data: 'id_articulo='+id_arti,
      dataType: "json",
      beforeSend: function() {
        
      },
      success: function(response) {
        if (response.code>0) 
        {
          $('#arti').text(arti);
          $('#id_articulo').val(id_arti);
          if(response.code==2)
          {
            $('#stock_actual').text(response.data.stock);
            $('#stockprecio_actual').text("S/. " + response.data.preciostock);
          }
        }
      },
      complete: function() {

      }
  });
});