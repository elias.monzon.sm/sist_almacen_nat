$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });

  $('#form_save_add_articulo').validate({

    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        error.insertAfter(element.parent()); 
    },
    submitHandler: function() {
      var sign = $('#simbol').val();
       $.ajax({
          url: $('base').attr('href') + 'ingresoxcompra/save_add_articulo',
          type: 'POST',
          async: false,
          data: $('#form_save_add_articulo').serialize()+'&sign='+sign,
          dataType: "json",
          beforeSend: function() {
          
          },
          success: function(response) {
              if (response.code==1) 
              {
                var exist_arti = $('#exist_arti').val();
                var edita_arti = $('#edita_arti').val();
                $('#addarticulo').modal('hide');
                if(exist_arti=="si")
                {
                  if(edita_arti=="si")
                  {
                    var idcod = $('#id_codigo').val();
                    var idmar = $('#id_marca').val();
                    $("tr[tdcodma='["+idcod+"]["+idmar+"]']").replaceWith(response.data);
                  }
                  else
                  {
                    $('#produc_content').append(response.data);
                  }
                }
                else
                {
                  $('#exist_arti').val("si");
                  $('#produc_content').html(response.data);
                }

                $('#buscar_arti .limpiarfiltro').click();
                buscar_arti(0);
                sumartotal();
                validar_row();
              }
              else
              {
                  swal('No Guardo!', 'Hubo un error', 'error');
              }
          },
          complete: function() {

          }
      });
    }
  });


  $('#fecha_busc').daterangepicker({
    singleDatePicker: true,
    format: 'DD-MM-YYYY',
    calender_style: "picker_4"
    }, 
    function(start, end, label) {}
  );
  
  $('#form_save_ingresoxcompra').validate({

    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        error.insertAfter(element.parent()); 
    },
    submitHandler: function() {
      if($('input[name=p_total]').length)
      {
        var id_tipomoneda = $('#id_tipomoneda').val();
        if(id_tipomoneda)
        {
          if($('select.sele').filter(function() { return this.value == ""; }).length>0)
          {
            swal('Tiene artículos','Sin almacen configurado.','error');
          }
          else
          {
            if($('#fecha_busc').val())
            {
              $.ajax({
                url: $('base').attr('href') + 'ingresoxcompra/save_ingresoxcompra',
                type: 'POST',
                data: $('#form_save_ingresoxcompra').serialize(),
                dataType: "json",
                beforeSend: function() {
                
                },
                success: function(response) {
                    if (response.code==1) 
                    {
                      swal({
                        title: 'Documento de Compra',
                        text: "¡Guardado correctamente!",
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Listo!'
                      }).then((result) => {
                        if (result.value) {
                          window.location.href = $('base').attr('href') + 'ingresoxcompra';
                        }
                      })
                    }
                    else
                    {
                        swal('No Guardo!', 'Hubo un error', 'error');
                    }
                },
                complete: function() {

                }
              });
            }
            else
            {
              swal('Debe','seleccionar una fecha de documento!!','error');
            }
          }
        }
        else
        {
          swal('¡¡Debe seleccionar','un tipo de moneda!!','error');
        }
      }
      else
      {
        swal('No se ha agregado','ningún producto o el Costo Total no es mayor a 0','error');
      }
    }
  });
});

$(document).on('click', '.add_proveedor', function (e) {
  var page = 0;

  buscar_proveedor(page);
});

function buscar_proveedor(page)
{
  var nombres_com = $('tr#filtroprovee input.nombres_com').val(); 
  var razon_soc = $('tr#filtroprovee input.razon_soc').val();
  var ruc_dni = $('tr#filtroprovee input.ruc_dni').val();
  
  var temp = "page="+page;
  if(nombres_com.trim().length)
  {
    temp=temp+'&nombres_com='+nombres_com;
  }

  if(razon_soc.trim().length)
  {
    temp=temp+'&razon_soc='+razon_soc;
  }

  if(ruc_dni.trim().length)
  {
    temp=temp+'&ruc_dni='+ruc_dni;
  }

  $.ajax({
      url: $('base').attr('href') + 'ingresoxcompra/buscar_proveedor',
      type: 'POST',
      data: temp,
      dataType: "json",
      beforeSend: function() {
          //showLoader();
      },
      success: function(response) {
          if (response.code==1) {
            $('#buscar_provee tbody').html(response.data.rta);
            $('#paginacion_datap').html(response.data.paginacion);
          }
      },
      complete: function() {
          //hideLoader();
      }
  });
}

$(document).on('click', '.agre_provee', function (e) { 
  var padre = $(this).parents('tr');
  var rucdni = padre.attr('rucdni');
  var doc = padre.attr('doc');
  var rz = padre.find('td.rz').html();
  var idprov = padre.attr('idproveedor');
  var tipope = padre.attr('tipope');

  $('#id_persona').val(idprov);
  $('#tipo_persona').val(tipope);

  var tex = doc+': '+rucdni+','+ rz;
  $('#ruc_dni').val(tex);
  $('#buscarprovee').modal('hide');
  $('#exist_arti').val('no');
  $('#lista_articulos tbody').html("<tr><td colspan='8'><h2 class='text-center text-success'>No hay Elementos</h2></td></tr>")
});

function get_selected_arti() {
  var c = 0;
  var i = new Array();
  $('#produc_content tr.artirow').each(function (index,value) {
    var idcod = $(this).attr('idcod');
    var idmar = $(this).attr('idmarca');
    var tx = idcod+","+idmar;
    i.push(tx);
    c++;
  });

  return (c>0) ? i.join("),(") : 0;
}

function buscar_arti(page) {
  var tipo_persona = $('#tipo_persona').val();
  var id_persona = $('#id_persona').val();

  
  var marca = $('#marca_busc').val();
  var codigo = $('#codigo_busc').val();
  var descripcion_trad = $('#descripcion_traducida_busc').val();
  var descripcion = $('#descripcion_busc').val();
  var w_not = get_selected_arti();
  var temp = 'tipo_persona='+tipo_persona+'&id_persona='+id_persona+'&page='+page+'&w_not='+w_not;
  if(marca.trim().length)
  {
    temp =temp+'&marca='+marca;
  }

  if(codigo.trim().length)
  {
    temp =temp+'&codigo='+codigo;
  }

  if(descripcion_trad.trim().length)
  {
    temp=temp+'&descripcion_trad='+descripcion_trad;
  }

  if(descripcion.trim().length)
  {
    temp=temp+'&descripcion='+descripcion;
  }
 
  $.ajax({
    url: $('base').attr('href') + 'codigo/get_artixproveedor',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        $('#buscar_arti tbody').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}
$(document).on('click','.buscarart',function(){
  var tipo_persona = $('#tipo_persona').val();
  var id_persona = $('#id_persona').val();

  if(id_persona && tipo_persona)
  {
    $('#buscarart').modal('show');
    buscar_arti(0);
  }
  else
  {
    swal('Debe','seleccionar a un proveedor','error');
  }
});

$(document).on('click','.get_arti',function(){
  limpiarformsave();
  var padre = $(this).parents('tr');
  var id_codigo = padre.attr('id_codigo');
  var id_marca = padre.attr('id_marca');

  $('#id_marca').val(id_marca);
  $('#id_codigo').val(id_codigo);

  var cod = padre.find('.tdcod').text();
  var mrca = padre.find('.tdmarca').text();
  var desc = padre.find('.desc1').text();


  $('#marca').val(mrca);
  $('#codigo').text(cod);
  $('#descripcion').val(desc);
  $('#addarticulo').modal('show');
});

$(document).on('focusout', '#preciou', function (e) {
  var cantidad = parseFloat($('#cantidad').val());
  var pu = parseFloat($(this).val());
  var pt = (typeof(pu)=='number' && !isNaN(pu)) ? (pu*cantidad) : ("");
  pt = parseFloat(pt);
  pt = (typeof(pt)=='number' && !isNaN(pt)) ? (pt) : ("0.00");
  $('#preciot').val(pt);
  if($('#preciot').closest('.form-group'))
  {
    if($('#preciot').closest('.form-group').attr('class')=="form-group has-error")
    {
      $('#preciot').closest('.form-group').removeClass('has-error');
    }

    if($('#preciot-error-error').length)
    {
      $('#preciot-error-error').html('');
    }
  }
});

$(document).on('focusout', '#preciot', function (e) {
  var cantidad = parseFloat($('#cantidad').val());
  var pt = parseFloat($(this).val());
  var pu = (typeof(pt)=='number' && !isNaN(pt)) ? (pt/cantidad) : ("");
  pu = parseFloat(pu);
  pu = (typeof(pu)=='number' && !isNaN(pu)) ? (pu) : ("0.00");

  $('#preciou').val(pu);
  if($('#preciou').closest('.form-group'))
  {
    if($('#preciou').closest('.form-group').attr('class')=="form-group has-error")
    {
      $('#preciou').closest('.form-group').removeClass('has-error');
    }

    if($('#preciou-error-error').length)
    {
      $('#preciou-error-error').html('');
    }
  }
});

function sumartotal() 
{
  var row = "";    
  if($('.artirow').length>0)
  {
    var punitario = 0, ptotal = 0, val = 0;
    $('#produc_content tr.artirow td').each(function (index,value) {
      var tip = $(this).attr('class');
      switch(tip)
      {
        case 'puni':
            val = $(this).find('input[type="hidden"]').val();
            punitario += parseFloat(val);
          break;
        case 'ptot':
            val = $(this).find('input[type="hidden"]').val();
            ptotal += parseFloat(val);
          break;
      }
    });

    var sign = $('#simbol').val(); 
    $('.tot').remove();

    row = "<tr class='tot'>"+
            "<td colspan=4>TOTAL</td>"+
            "<td><span class='txsign'>"+sign+" </span>"+punitario.toFixed(2)+"<input type='hidden' name='p_unitario' value='"+punitario+"'></td>"+
            "<td><span class='txsign'>"+sign+" </span>"+ptotal.toFixed(2)+"<input type='hidden' name='p_total' value='"+ptotal+"'></td>"+
            "<td colspan=2>"+
          "</tr>";
    $('#produc_content').append(row);
  }
  else
  {
    $('#exist_arti').val("no");
    row = '<tr><td colspan="8"><h2 class="text-center text-success">No hay registro</h2></td></tr>';
    $('#produc_content').html(row);
  }

  
}

$(document).on('change','.sele',function () {
  var val = $(this).val();
  var txtalm = $(this).parent().find('input');
  txtalm.val('');
  if(val)
  {
    var tx = $(this).find('option:selected').text();
    var nm = "txtalm["+val+"]";
    txtalm.val(tx);
    txtalm.prop('name',nm);

  }
});

$(document).on('change','#id_tipomoneda',function () {
  var idtip = $(this).val();
  if(idtip)
  {
    var selected = $(this).find('option:selected');
    var factor = selected.attr('factor');
    var sign = selected.attr('sign');
    $('#simbol').val(sign);
    $('#factor_cambio').val(factor);
    $('.txsign').text(sign+" ");
  }  
});

function limpiarformsave() {
  $('#edita_arti').val('no');
  $('#cantidad').val('');
  $('#preciou').val('');
  $('#preciot').val('');
}

$(document).on('click','.delete_art',function(){
  $(this).parents('tr').remove();
  sumartotal();
});

$(document).on('click','.edit',function(){
  limpiarformsave();
  $('#edita_arti').val("si");
  var padre = $(this).parents('tr');
  var id_codigo = padre.attr('idcod');

  var id_marca = padre.attr('idmarca');

  $('#id_marca').val(id_marca);
  $('#id_codigo').val(id_codigo);

  var cod = padre.find('td.cod .txcod').val();
  var mrca = padre.find('td.marca .txmar').val();
  var desc = padre.find('td.desc').text();
  $('#marca').text(mrca);
  $('#codigo').text(cod);
  $('#descripcion').val(desc);
  var pu = padre.find('.puni input').val();
  var pt = padre.find('.ptot input').val();
  var ct = padre.find('.cant input').val();

  $('#cantidad').val(ct);
  $('#preciou').val(pu);
  $('#preciot').val(pt);
});

$(document).on('click','#buscar_arti .buscar',function () {
   buscar_arti(0); 
});

$(document).on('click','#buscar_arti .limpiarfiltro',function(){
    $('#filtro input').val('');
    buscar_arti(0);
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_arti(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_arti(page);
});

function validar_row()
{
  $('#lista_articulos tr.artirow').each(function(index,value){
    if(!$(this).find('td.alm select').val())
    {
     $(this).find('td.alm select').css('background','#9e3f3f');
     $(this).find('td.alm select').css('color','white');
    }
  });
}