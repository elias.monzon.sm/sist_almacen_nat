$( document ).ready(function() {
    $('#datet_fecha_inicial').datetimepicker({
        format: 'DD-MM-YYYY',
        locale: moment.locale('es'),
        useCurrent: false
    });

    $('#datet_fecha_fin').datetimepicker({
        format: 'DD-MM-YYYY',
        locale: moment.locale('es')
    });
});

$("#datet_fecha_inicial").on("dp.change", function (e) {
    $('#datet_fecha_fin').data("DateTimePicker").minDate(e.date);
});
$("#datet_fecha_fin").on("dp.change", function (e) {
    $('#datet_fecha_inicial').data("DateTimePicker").maxDate(e.date);
});