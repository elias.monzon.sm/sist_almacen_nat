$( document ).ready(function() {

  jQuery.validator.setDefaults({
    debug: true,
    success: "valid",
    ignore: ""
  });

  $('#form_save_ingresoxcompra').validate({

    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');       
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        error.insertAfter(element.parent()); 
    },
    submitHandler: function() {
      var sign = $('#simbol').val();
      if($('select.sele').filter(function() { return this.value == ""; }).length>0)
      {
        swal('Tiene artículos','Sin almacen configurado.','error');
      }
      else
      {

       $.ajax({
          url: $('base').attr('href') + 'ingresoxcompra/editar_documentocompra_body',
          type: 'POST',
          async: false,
          data: $('#form_save_ingresoxcompra').serialize(),
          dataType: "json",
          beforeSend: function() {},
          success: function(response) {
            if (response.code==1) 
            {
              swal({
                  title: 'Documento de Compra',
                  text: "¡Guardado correctamente!",
                  type: 'success',
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Listo!'
                }).then((result) => {
                  if (result.value) {
                    window.location.reload();
                  }
                });
              setTimeout(function(){window.location.reload();},3000);
            }
            else
            {
                swal('No Guardo!', 'Hubo un error', 'error');
            }
          },
          complete: function() {

          }
        });
      }
    }
  });
});

$(document).on('focusout', '.pu', function (e) {
	var t = $(this);
  var cantidad = parseFloat(t.parents('tr').find('input.cant').val());
  var pu = parseFloat(t.val());
  var pt = (typeof(pu)=='number' && !isNaN(pu)) ? (pu*cantidad) : ("");
  pt = parseFloat(pt);
  pt = (typeof(pt)=='number' && !isNaN(pt)) ? (pt) : ("0.00");
  t.parents('tr').find('.pt').val(pt);
});

$(document).on('focusout', '.pt', function (e) {
	var t = $(this);
  var cantidad = parseFloat(t.parents('tr').find('input.cant').val());
  var pt = parseFloat(t.val());
  var pu = (typeof(pt)=='number' && !isNaN(pt)) ? (pt/cantidad) : ("");
  pu = parseFloat(pu);
  pu = (typeof(pu)=='number' && !isNaN(pu)) ? (pu) : ("0.00");

  t.parents('tr').find('.pu').val(pu);
});

$(document).on('focusout', '.cant', function (e) {
	var t = $(this);
  var pu = parseFloat(t.parents('tr').find('input.pu').val());
  var cantidad = parseFloat(t.val());
  var pt = (typeof(pu)=='number' && !isNaN(pu)) ? (pu*cantidad) : ("");
  pt = parseFloat(pt);
  pt = (typeof(pt)=='number' && !isNaN(pt)) ? (pt) : ("0.00");

  t.parents('tr').find('.pt').val(pt);
});

$(document).on('click','.buscarart',function(){
  var tipo_persona = $('#tipo_persona').val();
  var id_persona = $('#id_persona').val();
  $('#buscarart').modal('show');
  buscar_arti(0);
});

function buscar_arti(page) {
  var tipo_persona = $('#tipo_persona').val();
  var id_persona = $('#id_persona').val();

  
  var marca = $('#marca_busc').val();
  var codigo = $('#codigo_busc').val();
  var descripcion_trad = $('#descripcion_traducida_busc').val();
  var descripcion = $('#descripcion_busc').val();
  var w_not = get_selected_arti();
  var temp = 'tipo_persona='+tipo_persona+'&id_persona='+id_persona+'&page='+page+'&w_not='+w_not;
  if(marca.trim().length)
  {
    temp =temp+'&marca='+marca;
  }

  if(codigo.trim().length)
  {
    temp =temp+'&codigo='+codigo;
  }

  if(descripcion_trad.trim().length)
  {
    temp=temp+'&descripcion_trad='+descripcion_trad;
  }

  if(descripcion.trim().length)
  {
    temp=temp+'&descripcion='+descripcion;
  }
 
  $.ajax({
    url: $('base').attr('href') + 'codigo/get_artixproveedor',
    type: 'POST',
    data: temp,
    dataType: "json",
    beforeSend: function() {
      
    },
    success: function(response) {
      if (response.code==1) {
        $('#buscar_arti tbody').html(response.data.rta);
        $('#paginacion_data').html(response.data.paginacion);
      }
    },
    complete: function() {
      //hideLoader();
    }
  });
}

function get_selected_arti() {
  var c = 0;
  var i = new Array();
  $('#produc_content tr.artirow').each(function (index,value) {
    var idcod = $(this).attr('idcod');
    var idmar = $(this).attr('idmarca');
    var tx = idcod+","+idmar;
    i.push(tx);
    c++;
  });

  return (c) ? i.join("),(") : "";
}
$(document).on('click','.get_arti',function() {
  var $t = $(this).parents('tr');
  var id_codigo = $t.attr('id_codigo');
  var id_marca = $t.attr('id_marca');
  var sign = $('#simbol').val();

  var param = 'id_codigo='+id_codigo+'&id_marca='+id_marca+'&sign='+sign+'&precio_total=0&precio_unitario=0&cantidad=0&edit=1';
  $.ajax({
    url: $('base').attr('href') + 'ingresoxcompra/save_add_articulo',
    type: 'POST',
    async: false,
    data: param,
    dataType: "json",
    beforeSend: function() {
    
    },
    success: function(response) {
        if (response.code==1) 
        {
          var html = $(response.data);
          $('#produc_content').append(html);
          html.find('td.cant input').addClass('cant');
          html.find('td.puni input').addClass('pu');
          html.find('td.ptot input').addClass('pt');
          $('#produc_content input[type=text]').addClass('form-control');
          validar_row();
          $('#buscarart').modal('hide');
        }
        else
        {
            swal('No Guardo!', 'Hubo un error', 'error');
        }
    },
    complete: function() {

    }
  });
});

$(document).on('click','.delete_art',function(){
  $(this).parents('tr').remove();
});

function validar_row()
{
  $('#lista_articulos tr.artirow').each(function(index,value){
    if(!$(this).find('td.alm select').val())
    {
     $(this).find('td.alm select').css('background','#9e3f3f');
     $(this).find('td.alm select').css('color','white');
    }
  });
}

$(document).on('click','#buscar_arti .buscar',function () {
   buscar_arti(0); 
});

$(document).on('click','#buscar_arti .limpiarfiltro',function(){
    $('#filtro input').val('');
    buscar_arti(0);
});

$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_arti(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_arti(page);
});
