$(document).on('click', '#datatable_paginate li.paginate_button', function (e) {
  var page = $(this).find('a').attr('tabindex');
  buscar_documentos(page);
});

$(document).on('click', '#datatable_paginate a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_documentos(page);
});


function buscar_documentos(page)
{
    var temp = "page="+page;

    $.ajax({
        url: $('base').attr('href') + 'ingresoxcompra/buscar_documentos',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
        //showLoader();
        },
        success: function(response) {
        if (response.code==1) {
            $('#datatable-buttons tbody#bodyindex').html(response.data.rta);
            $('#paginacion_data').html(response.data.paginacion);
        }
        },
        complete: function() {
        //hideLoader();
        }
    });
}