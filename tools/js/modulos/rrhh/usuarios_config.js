$(document).on('click','.cambiar',function(){
    var id_menu = $(this).val();
    var id_usuario = $('#id_usuario').val();
    if($(this).is(':checked'))
    {
        //Agrega el permiso
        $.ajax({
            url: $('base').attr('href') + 'usuarios/add_permiso',
            type: 'POST',
            data: 'id_menu='+id_menu+'&id_usuario='+id_usuario,
            dataType: "json",
            beforeSend: function () {
                //showLoader();
            },
            success: function (response) {
                if (response.code == 1) {
                }
            },
            complete: function () {
                //hideLoader();
            }    
        });
    }
    else
    {
        //Elimina el permiso
        $.ajax({
            url: $('base').attr('href') + 'usuarios/delete_permiso',
            type: 'POST',
            data: 'id_menu='+id_menu+'&id_usuario='+id_usuario,
            dataType: "json",
            beforeSend: function () {
                //showLoader();
            },
            success: function (response) {
                if (response.code == 1) {
                }
            },
            complete: function () {
                //hideLoader();
            }    
        });
    }
    
});