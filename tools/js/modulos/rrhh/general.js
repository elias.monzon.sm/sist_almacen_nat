$( document ).ready(function() {
  $.validator.addMethod("time24", function(value, element) {
    var exp = value;
    if($.trim(exp).length>0) {
      return /^([0-1]?[0-9]|2[0-3]):[0-5][0-9] [APap][mM]$/.test(value);
    }
    else {
      return true;
    }
  }, "");

  $.validator.addMethod("formespn",
    function(value, element) {
      if(value.trim().length)
        return /^(0?[1-9]|[12]\d|3[01])[\.\/\-](0?[1-9]|1[012])[\.\/\-]([12]\d)?(\d\d)$/.test( value );
      else
        return true;
  }, "Ingrese est Formato dd-mm-yyyy");

  $.validator.addMethod("forni", function(value, element) {
      var exp = value;
      var num = parseInt($('input[name="id_documentoidentidad"]:checked').val()); console.log(num);
      if(num==1 || num == 0)
      {
        if (exp<0) { return false; }
        else {
            if($.isNumeric(exp) && exp.trim().length==8){return true; }
            else{ return false; }
        }
      }
      else
      {
        if(exp.trim().length<20) {return true}
        else { return false;}
      }
        
  }, "Corregir");

  $('#form_save_personal').validate({
    rules:
    {
      dni:
      {
        required:true,
        forni: true,
        remote: {
          url: $('base').attr('href') + 'persona_natural/validar_dni',
          type: "post",
          data: {
            dni: function() { return $( "#dni" ).val(); },
            id_persona: function() { return $('#id_persona').val(); },
            id_documentoidentidad: function() { return $('input:radio[name=id_documentoidentidad]:checked').val(); }             
          }
        }
      },
      nombres:{ required:true },
      apellidos:{ required:true },
      nombre_corto:{
        required:true,
        minlength: 4,
        remote: {
          url: $('base').attr('href') + 'persona/validar_nombcorto',
          type: "post",
          data: {
            nombre_corto: function() { return $( "#nombre_corto" ).val(); },
            id_persona: function() { return $('#id_persona').val(); }
          }
        }
      },
      sexo:{ required:true },
      fecha_nacimiento:{formespn:true}      
    },
    messages:{
      dni: { required:"Ingrese DNI", remote: "Ya existe"},
      nombres:{ required:"" },
      apellidos:{ required: "" },
      nombre_corto: { required:"", minlength: "Más de 4 Letras", remote: "Ya existe"},
      sexo:{ required: "" }
    },      

    highlight: function(element) {
        $(element).closest('.control-group').addClass('has-error');        
    },
    unhighlight: function(element) {
        $(element).closest('.control-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        if(element.parent('.control-group').length) 
        {
          error.insertAfter(element.parent());
        }
    },
    submitHandler: function() {
        $.ajax({
            url: $('base').attr('href') + 'persona_natural/save_persona',
            type: 'POST',
            data: $('#form_save_personal').serialize(),
            dataType: "json",
            beforeSend: function() {
                //showLoader();
            },
            success: function(response) {
              if (response.code==1) {
                swal({
                  title: 'Guardo!',
                  text: "Satisfactoriamente!",
                  type: 'success',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Listo!'
                }).then((result) => {
                  if (result.value) {
                    location.reload();                  
                  }
                })
                
              }
            },
            complete: function(response) {
              
            }
        });/**/
    }
  });

  $('#fecha_nacimiento').datetimepicker({
    locale: moment.locale('es'),
    format: 'DD-MM-YYYY'
  });
});


$(document).on('click','#div_tipodocumento input[type="radio"]',function(){
    $('#dnibusc').prop('readonly',false).val('');
    $('#nodocu').prop('checked',true);
    var tipo_doc = $(this).val();
    if(tipo_doc==2)
        $('#txt_tipo').html('Autogenera Carnet de Extrangería');
    else
        $('#txt_tipo').html('Autogenera DNI');
});

$(document).on('click','#div_autogenera input',function(){
    var tipo_doc = $('#div_tipodocumento input:checked').val();
    var autogenera_ = $(this).val();
    if(autogenera_==1)
    {
      $.ajax({ 
        url: $('base').attr('href') + 'persona_natural/autogenera_docu',
        type: 'POST',
        data: 'tipo_doc='+tipo_doc,
        dataType: "json",
        beforeSend: function() {},
        success: function(response) {
            if (response.code==1) 
            {
                $('#dni').prop('readonly',true).val(response.data);
            }
        },
        complete: function() {}
      });
    }
    else
    {
      $('#dni').prop('readonly',false).val('');
    }
});

$(document).on('change','#id_pais',function(){
  $('#id_departamento').html('');
  $('#id_provincia').html('');
  $('#id_distrito').html('');
  var id = $(this).val();
  if(id==175 || id==238)
  {
    $('#id_provincia').html('').prop('readonly',false);
    $('#id_distrito').html('').prop('readonly',false);

    $.ajax({ 
        url: $('base').attr('href') + 'generico/cbx_departamentos',
        type: 'POST',
        data: 'id_pais='+id,
        dataType: "json",
        beforeSend: function() {},
        success: function(response) {
            if (response.code==1) 
            {
              $('#id_departamento').html(response.data);
            }
        },
        complete: function() {}
      });
  }
  else
  {
    $('#id_provincia').html('').prop('readonly',true);
    $('#id_distrito').html('').prop('readonly',true);
  }
});

$(document).on('change','#id_departamento',function(){
    $('#id_provincia').html('');
    $('#id_distrito').html('');
    var id_departamento = $(this).val();
    if(parseInt(id_departamento)>0)
    {
        $.ajax({
            url: $('base').attr('href') + 'generico/cbx_provincia',
            type: 'POST',
            data: 'id_departamento='+id_departamento,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) 
            {
                if(response.code==1)
                {
                    $('#id_provincia').html(response.data);
                }
            },
            complete: function() {}
        });
    }
});

$(document).on('change','#id_provincia',function(){
    $('#id_distrito').html('');
    var id_provincia = $(this).val();
    if(parseInt(id_provincia)>0)
    {
        $.ajax({
            url: $('base').attr('href') + 'generico/cbx_distrito',
            type: 'POST',
            data: 'id_provincia='+id_provincia,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) 
            {
                if(response.code==1)
                {
                    $('#id_distrito').html(response.data);
                }
            },
            complete: function() {}
        });
    }
});

$(document).on('click','.add_cel',function(){
    var create = true;
    $('#cel_container div input').each(function(key,value){
        if($(this).val()=="")
            create=false;
    });

    if(create==true)
    {
        var opt_ =  "<option value>Seleccione Operador</option>"+
            "<option value=1>Claro</option>"+
            "<option value=2>Entel</option>"+
            "<option value=3>Bitel</option>"+
            "<option value=4>Movistar</option>"+
            "<option value=5>Twenty</option>"+
            "<option value=6>Virgin</option>";

        var c = 1 ;
        $('#cel_container div').each(function(key,value){
            c++;
        });

        var txt =   "<div class='form-group form-inline'>"+
                        "<input class='form-control' type='text' name='contacto[cel]["+c+"]' style='margin-right: 10px;' maxlength=9>"+
                        "<select class='form-control' name='contacto[op]["+c+"]' style='margin-right: 10px;'>"+opt_+"</select>"+
                        "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>"+
                          "<i class='fa fa-trash-o'></i>"+
                        "</a>"+
                    "</div>";

        $('#cel_container').append(txt);        
    }
    else
    {
        swal('¡Error!','Aún hay campos vacíos','error');
    }
});

$(document).on('click','.add_telf',function(){
    var create = true;
    $('#telf_container div input').each(function(key,value){
        if($(this).val()=="")
            create=false;
    });

    if(create==true)
    {
        var c = 1 ;
        $('#telf_container div').each(function(key,value){
            c++;
        });

        var txt =   "<div class='form-group form-inline'>"+
                        "<input class='form-control' type='text' name='contacto[telf]["+c+"]' style='margin-right: 10px;' maxlength=9>"+
                        "<input class='form-control' type='text' name='contacto[anex]["+c+"]' style='margin-right: 10px;'>"+
                        "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>"+
                          "<i class='fa fa-trash-o'></i>"+
                        "</a>"+
                    "</div>";

        $('#telf_container').append(txt);        
    }
    else
    {
        swal('¡Error!','Aún hay campos vacíos','error');
    }
});

$(document).on('click','.del_this',function(){
    $(this).closest('.form-inline').remove();
});