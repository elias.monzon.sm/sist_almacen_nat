$(document).on('click','.add_pj',function(){
    limpiar_filtro();
});

function buscar_pj(page)
{
    var nom_comercial = $('.nombres_com').val();
    var razon_social = $('.razon_soc').val();
    var ruc = $('.ruc_dni').val();

    var c = 0;
    var i = new Array();
    $('#bodyindex tr').each(function(index,value){
        var idpj = $(this).attr('idpj');
        console.log(idpj);
        i.push(idpj);
        c++;
    });

    i = (c) ? i.join() : new Array();

    var temp = "page="+page;
    if(nom_comercial.trim().length)
        temp += "&n_comercial="+nom_comercial
    
    if (razon_social.trim().length)
        temp += "&r_social=" + razon_social
    
    if (ruc.trim().length)
        temp += "&ruc=" + ruc

    if (i.length>0)
        temp += "&w_n=" + i;

    $.ajax({
        url: $('base').attr('href') + 'persona_juridica/buscar_personajud_1',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function () {
            //showLoader();
        },
        success: function (response) {
            if (response.code == 1) {
                $('#buscar_provee tbody').html(response.data.rta);
                $('#paginacion_datap').html(response.data.paginacion);
            }
        },
        complete: function () {
            //hideLoader();
        }
    });
}

function limpiar_filtro() {
    $('#filtroprovee input').val('');
    buscar_pj(0);
}

$(document).on('click','.add_pj',function(){
    var id_pj = $(this).parents('tr').attr('idpersonajuridica');
    var id_persona = $('#id_persona').val();

    if(id_pj>0 && id_persona>0)
    {
        $.ajax({
            url: $('base').attr('href') + 'persona_natural/add_contacto',
            type: 'POST',
            data: 'id_persona='+id_persona+'&id_persona_juridica='+id_pj,
            dataType: "json",
            beforeSend: function () {},
            success: function (response) {
                if (response.code == 1) {
                    swal({
                        title: '¡¡Se guardó',
                        text: "Este proveedor correctamente!!!",
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Listo!!'
                    }).then((result) => {
                        if (result.value) {
                            window.location.reload();
                        }
                    });
                }
            },
            complete: function () {}
        });
    }
});

$(document).on('click','.delete',function(){
    var id = $(this).parents('tr').attr('idpj');
    var id_persona = $('#id_persona').val();
    if(id)
    {
        $.ajax({
            url: $('base').attr('href') + 'persona_natural/delete_contacto',
            type: 'POST',
            data: 'id_persona_juridica='+id+'&id_persona='+id_persona,
            dataType: "json",
            beforeSend: function () {

            },
            success: function (response) {
                if (response.code == 1) {
                    swal({
                        title: '¡¡Se eliminó',
                        text: "Este proveedor correctamente!!!",
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Listo!!'
                    }).then((result) => {
                        if (result.value) {
                            window.location.reload();
                        }
                    });
                }
            },
            complete: function () {

            }
        });
    }
});