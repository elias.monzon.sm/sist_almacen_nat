$(document).ready(function() 
{

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        ignore: ""
    });

    $('#form_save_persona').validate({
        rules:
        {
            ruc:{ required:true },
            nombre_comercial:{ required:true },
            razon_social:{ required:true },
        },
        messages: 
        {
            ruc:{ required: "RUC" },
            nombre_comercial:{ required: "Nombre Comercial" },
            razon_social:{ required:"Razón Social" },
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');        
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.form-group').length) 
            {
                error.insertAfter(element.parent());
                //element.closest('.control-group').find('.help-block').html(error.text()); 
            } 
            else 
            { 
                if(element.parent('.col-md-12').length) { error.insertAfter(element.parent());}
                else if(element.parent('.col-md-9').length) { error.insertAfter(element.parent());}
                else {}
            }
        },
        submitHandler: function() {
            $.ajax({
                url: $('base').attr('href') + 'persona_juridica/save_personajud',
                type: 'POST',
                data: $('#form_save_persona').serialize(),
                dataType: "json",
                beforeSend: function() {},
                success: function(response) {
                    if (response.code==1) 
                    {
                        txt = (parseInt($('#id_persona').val())>0) ? "Editó" : "Creó";
                        swal('Esta persona','se '+txt+' correctamente.','success');
                        $('#editpersona').modal('hide');
                        limpiar_form();
                        buscar_persona_juridica(0);
                    }
                },
                complete: function() {}
            });
        }
    });

    $(function () {
        $('#aniversario').datetimepicker({
            locale: moment.locale('es'),
            format: 'DD-MM-YYYY'
        });
    });
});

$(document).on('click','.edit',function(){
    limpiar_form();
    var id_personajuridica = $(this).parents('tr').attr('idpersonajuridica');
    if (parseInt(id_personajuridica)>0)
    {
        $.ajax({ 
            url: $('base').attr('href') + 'persona_juridica/get_one_personajud',
            type: 'POST',
            data: 'id_persona_juridica='+id_personajuridica,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) {
                if (response.code==1) 
                {
                    var d = response.data;
                    $('#estado input').prop('checked',false);
                    $('#estado label').removeClass('active');

                    var estado = d.estado;
                    $('#id_persona_juridica').val(d.id_persona_juridica);
                    $('#estado #estado_'+estado).prop('checked',true);
                    $('#estado #estado_'+estado).parent().addClass('active');
                
                    $('#nombre_comercial').val(d.nombre_comercial);
                    $('#razon_social').val(d.razon_social);
                    $('#aniversario').val(d.aniversario);
                    $('#ruc').val(d.ruc);
                    var checked = (d.es_cliente==1) ? true : false;
                    $('#es_cliente').prop('checked',checked);

                    checked = (d.es_proveedor==1) ? true : false;
                    $('#es_proveedor').prop('checked',checked);

                    checked = (d.es_contacto==1) ? true : false;
                    $('#es_contacto').prop('checked',checked);
                }
            },
            complete: function() {}
        });
    }
});

function limpiar_form() 
{
    $('#id_persona_juridica').val('');
    $('#nombre_comercial').val('');
    $('#razon_social').val('');
    $('#ruc').val('');
    $('#aniversario').val('');

    $('#estado input').prop('checked',false);
    $('#estado label').removeClass('active');

    $('#estado #estado_1').prop('checked',true);
    $('#estado #estado_1').parent().addClass('active');

    $('#es_cliente').prop('checked', false);
    $('#es_proveedor').prop('checked', false);
    $('#es_contacto').prop('checked', false);
}

$(document).on('click','.add_pn',function(){
    limpiar_form();
});


function buscar_persona_juridica(page) 
{
    var nombre_comercial = $('#nombrecomercial_busc').val();
    var ruc = $('#ruc_busc').val(); 
    var razon_social = $('#razonsocial_busc').val();
    var estado = $('#estado_busc').val();

    console.log(estado);
    var temp = "page="+page;

    if (razon_social)
        temp += '&r_social=' + razon_social;

    if (nombre_comercial)
        temp += '&n_comercial=' + nombre_comercial;

    if (ruc)
        temp += '&ruc=' + ruc;

    if(parseInt(estado)==1 || parseInt(estado)==0)
        temp+='&estado='+estado;

    $.ajax({
        url: $('base').attr('href') + 'persona_juridica/buscar_personajud',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
        
        },
        success: function(response) {
            if (response.code==1) 
            {
                $('#bodyindex').html(response.data.rta);
                $('#paginacion_data').html(response.data.paginacion);
            }
        },
        complete: function() {

        }
    });
}

$(document).on('click','.buscar',function(){
    buscar_persona_juridica(0);
});

$(document).on('click','.limpiarfiltro',function() {
    $('#filtro input[type="text"]').val('');
    $('#filtro select').val('');
    $('thead input[type="checkbox"]').prop('checked',false);
    buscar_persona_juridica(0);
});

$(document).on('click','.delete',function(){
    var id = $(this).parents('tr').attr('idpersonajuridica');

    swal({
        title: '¿Estas seguro ',
        text: "de Inactivar a esta persona?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, estoy seguro!'
    }).then(function(isConfirm) 
    {
        if (isConfirm) {     
            $.ajax({
                url: $('base').attr('href') + 'persona_juridica/inactive_persona',
                type: 'POST',
                data: 'id_persona='+id,
                dataType: "json",
                beforeSend: function() {
                },
                success: function(response) {
                    if (response.code==1) 
                    {
                        buscar_persona_juridica(0);
                    }
                },
                complete: function() {
                }
            });
        }
    }); 
});

$(document).on('click', '#paginacion_data li.paginate_button', function (e) {  
    var page = $(this).find('a').attr('tabindex');
    buscar_persona_juridica(page);
});

$(document).on('click', '#paginacion_data a.paginate_button', function (e) {
    var page = $(this).attr('tabindex');
    buscar_persona_juridica(page);
});