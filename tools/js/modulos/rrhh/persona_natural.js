$(document).ready(function() 
{

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        ignore: ""
    });

    $('#form_save_persona').validate({
        rules:
        {
            id_documentoidentidad:{required:true,},
            genera:{ required:true },
            nombres:{ required:true },
            apellidos:{ required:true },
            fecha_nacimiento:{ required:true },
            sexo:{ required:true },
            dni:{ 
                required:true,
                number : true 
            }
        },
        messages: 
        {
            id_documentoidentidad:
            {
                required:"Seleccione Tipo de Documento",
            },
            genera:{ required: "Seleccione" },
            nombres:{ required: "Ingrese nombre" },
            apellidos:{ required:"Ingrese apellidos" },
            fecha_nacimiento:{ required:"Ingrese fecha" },
            sexo:{ required:"Ingrese Sexo" },
            dni:{email: "Ingrese dni" }    
        },      

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');        
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) 
        {
            if(element.parent('.form-group').length) 
            {
                error.insertAfter(element.parent());
                //element.closest('.control-group').find('.help-block').html(error.text()); 
            } 
            else 
            { 
                if(element.parent('.col-md-12').length) { error.insertAfter(element.parent());}
                else if(element.parent('.col-md-9').length) { error.insertAfter(element.parent());}
                else {}
            }
        },
        submitHandler: function() {
            $.ajax({
                url: $('base').attr('href') + 'persona_natural/save_persona',
                type: 'POST',
                data: $('#form_save_persona').serialize(),
                dataType: "json",
                beforeSend: function() {},
                success: function(response) {
                    if (response.code==1) 
                    {
                        txt = (parseInt($('#id_persona').val())>0) ? "Editó" : "Creó";
                        swal('Esta persona','se '+txt+' correctamente.','success');
                        $('#editpersona').modal('hide');
                        limpiar_form();
                        buscar_persona(0);
                    }
                },
                complete: function() {}
            });
        }
    });

    $(function () {
        $('#fecha_nacimientoo').datetimepicker({
            locale: moment.locale('es'),
            format: 'DD-MM-YYYY'
        });
    });
});

$(document).on('click','#div_tipodocumento input[type="radio"]',function(){
    $('#dnibusc').prop('readonly',false).val('');
    $('#nodocu').prop('checked',true);
    var tipo_doc = $(this).val();
    if(tipo_doc==2)
        $('#txt_tipo').html('Autogenera Carnet de Extrangería');
    else
        $('#txt_tipo').html('Autogenera DNI');
});

$(document).on('click','#div_autogenera input[value="1"]',function(){
    var tipo_doc = $('#div_tipodocumento input:checked').val();
    
    $.ajax({ 
        url: $('base').attr('href') + 'persona_natural/autogenera_docu',
        type: 'POST',
        data: 'tipo_doc='+tipo_doc,
        dataType: "json",
        beforeSend: function() {},
        success: function(response) {
            if (response.code==1) 
            {
                $('#dnibusc').prop('readonly',true).val(response.data);
            }
        },
        complete: function() {}
    });
});

$(document).on('click','.edit',function(){
    limpiar_form();
    var id_persona = $(this).parents('tr').attr('idpersona');
    if(parseInt(id_persona)>0)
    {
        $.ajax({ 
            url: $('base').attr('href') + 'persona_natural/get_one_persona',
            type: 'POST',
            data: 'id_persona='+id_persona,
            dataType: "json",
            beforeSend: function() {},
            success: function(response) {
                if (response.code==1) 
                {
                    var d = response.data;
                    $('#estado input').prop('checked',false);
                    $('#estado label').removeClass('active');

                    var estado = d.estado;
                    $('#id_persona').val(d.id_persona);
                    $('#estado #estado_'+estado).prop('checked',true);
                    $('#estado #estado_'+estado).parent().addClass('active');
                
                    $('#id_documentoidentidad [value="'+d.id_documentoidentidad+'"]').prop('checked',true);
                    if(d.dni == null)
                    {
                        $('#div_autogenera input[value="1"]').prop('checked',true)
                        $('#dnibusc').prop('readonly',true).val(d.dni_genera);
                    }
                    else
                    {
                        $('#div_autogenera input[value="0"]').prop('checked',true)
                        $('#dnibusc').val(d.dni);
                    }
                    $('#nombres').val(d.nombres);
                    $('#apellidos').val(d.apellidos);
                    $('#fecha_nacimientoo').val(d.fecha_nacimiento);

                    $('#gender input').prop('checked',false);
                    $('#gender label').removeClass('active');

                    var sexo = d.sexo;
                    $('#gender #sexo_'+sexo).prop('checked',true);
                    $('#gender #sexo_'+sexo).parent().addClass('active');

                    var checked = (d.es_cliente==1) ? true : false;
                    $('#es_cliente').prop('checked',checked);

                    checked = (d.es_proveedor==1) ? true : false;
                    $('#es_proveedor').prop('checked',checked);

                    checked = (d.es_contacto==1) ? true : false;
                    $('#es_contacto').prop('checked',checked);
                }
            },
            complete: function() {}
        });
    }
});

function limpiar_form() 
{
    $('#id_persona').val('');
    $('#nombres').val('');
    $('#apellidos').val('');
    $('#fecha_nacimientoo').val('');
    $('#dnibusc').val('');
    $('#dnibusc').prop('readonly',false);

    $('#estado input').prop('checked',false);
    $('#estado label').removeClass('active');

    $('#estado #estado_1').prop('checked',true);
    $('#estado #estado_1').parent().addClass('active');

    $('#div_tipodocumento input').prop('checked',false);
    $('#div_tipodocumento input[value="1"]').prop('checked',true);

    $('#div_autogenera input').prop('checked',false);
    $('#div_autogenera #nodocu').prop('checked',true);        

    $('#gender input').prop('checked',false);
    $('#gender label').removeClass('active');

    $('#div_docu input').prop('checked',false);
}

$(document).on('click','.add_pn',function(){
    limpiar_form();
});


function buscar_persona(page) 
{
    var nombres = $('#nombres_busc').val();
    var apellidos = $('#apellido_busc').val();
    var dni = $('#dni_busc').val();
    var fech_nac = $('#fecha_nacimiento').val();
    var sexo = $('#sexo_busc').val();
    var estado = $('#estado_busc').val();
    var tipodoc = $('#tipodoc_busc').val();
    console.log(estado);
    var temp = "page="+page;

    if(nombres)
        temp+='&nombres='+nombres;

    if(apellidos)
        temp+='&apellidos='+apellidos;

    if(dni)
        temp+='&dni='+dni;

    if(sexo)
        temp+='&sexo='+sexo;

    if(fech_nac)
        temp+='&fecha_nacimiento='+fech_nac;

    if($('#solo_clientes')[0].checked)
        temp+='&solo_clientes=1';

    if($('#solo_proveedor')[0].checked)
        temp+='&solo_proveedor=1';

    if($('#solo_contacto')[0].checked)
        temp+='&solo_contacto=1';

    if(parseInt(tipodoc)>0)
        temp+='&tipodoc='+tipodoc;

    if(parseInt(estado)==1 || parseInt(estado)==0)
        temp+='&estado='+estado;

    $.ajax({
        url: $('base').attr('href') + 'persona_natural/buscar_persona',
        type: 'POST',
        data: temp,
        dataType: "json",
        beforeSend: function() {
        
        },
        success: function(response) {
            if (response.code==1) 
            {
                $('#bodyindex').html(response.data.rta);
                $('#paginacion_data').html(response.data.paginacion);
            }
        },
        complete: function() {

        }
    });
}

$(document).on('click','.buscar',function(){
    buscar_persona(0);
});

$(document).on('click','.limpiarfiltro',function() {
    $('#filtro input[type="text"]').val('');
    $('#filtro select').val('');
    $('thead input[type="checkbox"]').prop('checked',false);
    buscar_persona(0);
});

$(document).on('click','.delete',function(){
    var id = $(this).parents('tr').attr('idpersona');

    swal({
        title: '¿Estas seguro ',
        text: "de Inactivar a esta persona?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, estoy seguro!'
    }).then(function(isConfirm) 
    {
        if (isConfirm) {     
            $.ajax({
                url: $('base').attr('href') + 'persona_natural/inactive_persona',
                type: 'POST',
                data: 'id_persona='+id,
                dataType: "json",
                beforeSend: function() {
                },
                success: function(response) {
                    if (response.code==1) 
                    {
                        buscar_persona(0);
                    }
                },
                complete: function() {
                }
            });
        }
    }); 
});

$(document).on('click', '#paginacion_data li.paginate_button', function (e) {  
  var page = $(this).find('a').attr('tabindex');
  buscar_persona(page);
});

$(document).on('click', '#paginacion_data a.paginate_button', function (e) {
  var page = $(this).attr('tabindex');
  buscar_persona(page);
});