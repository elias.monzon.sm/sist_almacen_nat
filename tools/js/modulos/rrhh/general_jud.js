$( document ).ready(function() {
  $.validator.addMethod("time24", function(value, element) {
    var exp = value;
    if($.trim(exp).length>0) {
      return /^([0-1]?[0-9]|2[0-3]):[0-5][0-9] [APap][mM]$/.test(value);
    }
    else {
      return true;
    }
  }, "");

  $.validator.addMethod("formespn",
    function(value, element) {
      if(value.trim().length)
        return /^(0?[1-9]|[12]\d|3[01])[\.\/\-](0?[1-9]|1[012])[\.\/\-]([12]\d)?(\d\d)$/.test( value );
      else
        return true;
  }, "Ingrese est Formato dd-mm-yyyy");

  $.validator.addMethod("forni", function(value, element) {
      var exp = value;
      var num = parseInt($('input:radio[name=id_documentoidentidad]:checked').val());// console.log(num);
      if(num==1)
      {
        if (exp <= 0) { return false; }
        else {
            if($.isNumeric(exp) && exp.trim().length==8){ return true; }
            else{ return false; }
        }
      }
      else
      {
        if(exp.trim().length<20) {return true}
        else { return false;}
      }
        
  }, "Corregir");

  $('#form_save_pers_juridica').validate({
    rules:
    {
      ruc:{ required:true },
      razon_social:{ required:true },
      direccion_fiscal:{ required:true }
    },
    messages:{
      ruc: { required:"Ingrese RUC" },
      razon_social:{ required:"" },
      direccion_fiscal:{ required: "Dirección" }
    },      

    highlight: function(element) {
        $(element).closest('.control-group').addClass('has-error');        
    },
    unhighlight: function(element) {
        $(element).closest('.control-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) 
    {
        if(element.parent('.col-md-9').length) 
        {
          error.insertAfter(element.parent());
        }
    },
    submitHandler: function() {
      $.ajax({
          url: $('base').attr('href') + 'persona_juridica/save_personajud',
        type: 'POST',
        data: $('#form_save_pers_juridica').serialize(),
        dataType: "json",
        beforeSend: function() {
            //showLoader();
        },
        success: function(response) {
          if(response.code==1)
          {
            swal({
                title: 'Datos Guardados',
                text: "con éxito!!",
                type: 'success',
                confirmButtonColor: '#3085d6',
                confirmButtonText: '¡Listo!'
              }).then(function (isConfirm) {
                if (isConfirm) {
                    window.location.reload(true);
                  }
              }); 
          }
          else{

          }
    
        },
        complete: function(response) {
          
        }
      });
    }
  });

    $('#aniversario').datetimepicker({
        format: 'DD-MM-YYYY',
    });
});

$(document).on('change','#id_pais',function(){
  $('#id_departamento').html('');
  $('#id_provincia').html('');
  $('#id_distrito').html('');
  var id = $(this).val();
  if(id==175 || id==238)
  {
    $('#id_provincia').html('').prop('readonly',false);
    $('#id_distrito').html('').prop('readonly',false);

    $.ajax({ 
        url: $('base').attr('href') + 'generico/cbx_departamentos',
        type: 'POST',
        data: 'id_pais='+id,
        dataType: "json",
        beforeSend: function() {},
        success: function(response) {
            if (response.code==1) 
            {
              $('#id_departamento').html(response.data);
            }
        },
        complete: function() {}
      });
  }
  else
  {
    $('#id_provincia').html('').prop('readonly',true);
    $('#id_distrito').html('').prop('readonly',true);
  }
});

$(document).on('change', '#id_departamento', function (e)
{
  var padre = $(this);
  var id_departamento = padre.val();
  var tipo = padre.attr('tipo');
  
  $.ajax({
      url: $('base').attr('href') + 'generico/cbx_provincia',
      type: 'POST',
      data: 'id_departamento='+id_departamento,
      dataType: "json",
      success: function(response) {
        if (response.code == "1") {
          $('select[name=id_provincia]').html(response.data);
          $('select[name=id_distrito]').html("<option value=''>DISTRITO</option>");        
        }
      },
      complete: function() {
        //hideLoader();
      }
  });
});


$(document).on('change', '#id_provincia', function (e)
{
  var padre = $(this);
  var id_provincia = padre.val();

  $.ajax({
      url: $('base').attr('href') + 'generico/cbx_distrito',
      type: 'POST',
      data: 'id_provincia='+id_provincia,
      dataType: "json",
      success: function(response) {
        if (response.code == "1") {
          $('select[name=id_distrito]').html(response.data);         
        }
      },
      complete: function() {
          //hideLoader();
      }
  });
});

$(document).on('click', '.add_cel', function () {
    var create = true;
    $('#cel_container div input').each(function (key, value) {
        if ($(this).val() == "")
            create = false;
    });

    if (create == true) {
        var opt_ = "<option value>Seleccione Operador</option>" +
            "<option value=1>Claro</option>" +
            "<option value=2>Entel</option>" +
            "<option value=3>Bitel</option>" +
            "<option value=4>Movistar</option>" +
            "<option value=5>Twenty</option>" +
            "<option value=6>Virgin</option>";

        var c = 1;
        $('#cel_container div').each(function (key, value) {
            c++;
        });

        var txt = "<div class='form-group form-inline'>" +
            "<input class='form-control' type='text' name='contacto[cel][" + c + "]' style='margin-right: 10px;' maxlength=9>" +
            "<select class='form-control' name='contacto[op][" + c + "]' style='margin-right: 10px;'>" + opt_ + "</select>" +
            "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>" +
            "<i class='fa fa-trash-o'></i>" +
            "</a>" +
            "</div>";

        $('#cel_container').append(txt);
    }
    else {
        swal('¡Error!', 'Aún hay campos vacíos', 'error');
    }
});

$(document).on('click', '.add_telf', function () {
    var create = true;
    $('#telf_container div input').each(function (key, value) {
        if ($(this).val() == "")
            create = false;
    });

    if (create == true) {
        var c = 1;
        $('#telf_container div').each(function (key, value) {
            c++;
        });

        var txt = "<div class='form-group form-inline'>" +
            "<input class='form-control' type='text' name='contacto[telf][" + c + "]' style='margin-right: 10px;' maxlength=9>" +
            "<input class='form-control' type='text' name='contacto[anex][" + c + "]' style='margin-right: 10px;'>" +
            "<a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>" +
            "<i class='fa fa-trash-o'></i>" +
            "</a>" +
            "</div>";

        $('#telf_container').append(txt);
    }
    else {
        swal('¡Error!', 'Aún hay campos vacíos', 'error');
    }
});

$(document).on('click', '.del_this', function () {
    $(this).closest('.form-inline').remove();
});

