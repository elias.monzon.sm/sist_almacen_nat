    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa "></i> Maestro de Códigos<small> En Desarrollo</small></h3>
                  <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                  <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="btn-group pull-right"><a class="btn btn-primary nuevo btn-sm" data-toggle="modal" data-target="#edit_codigo" ><i class="fa fa-file"></i> Nuevo</a></div> 
                  <div class="clearfix"></div>
                  </div>
                </div>

                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30"></p>
                        <div class="table-responsive">
                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th><center>Código</center></th>
                                <th><center>Descripción Description</center></th>
                                <th><center>Producto</center></th>
                                <th><center>Aplicación</center></th>
                                <th><center>Marcas</center></th>
                                <th><center>Estado</center></th>
                                <th><center>Acciones</center></th>
                              </tr>
                              <tr id="filtro">
                                <td></td>
                                <td>
                                  <input id="cod_busc" type="text" class="form-control" placeholder="Codigo">
                                </td>
                                <td>
                                  <input id="descripcion_completa_busc" type="text" class="form-control" placeholder="Descripción Completa">
                                </td>
                                <td>
                                  <input id="desc_busc" type="text" class="form-control" placeholder="Descripción">
                                </td>
                                <td>
                                  <input id="desc2_busc" type="text" class="form-control" placeholder="Desc. Trad">
                                <td>
                                <td>
                                  <select id="estado_busc" class="form-control">
                                    <option value="-1">Seleccione Estado</option>
                                    <option value="1">Activo</option>
                                    <option value="0">Inactivo</option>
                                  </select>
                                </td>
                                <td>
                                  <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                                    <i class="fa fa-search"></i>
                                  </a>
                                  <a href="javascript:void(0);" class="btn btn-default limpiarfiltro btn-sm">
                                    <i class="fa fa-refresh"></i>
                                  </a>
                                </td>
                              </tr>
                            </thead>
                            <tbody id="bodyindex">
  <?php if(isset($rta)) { echo $rta; } ?>                          
                            </tbody>
                          </table>
                        
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="btn-group pull-right" id="paginacion_data">
                          <?php if(isset($paginacion)) {echo $paginacion;} ?>                        
                        </div> 
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
      </div>
<?php 
	if(isset($modal)) 
	{
	    echo $modal;
	} 
?>
