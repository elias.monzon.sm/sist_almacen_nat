<?php
    $menu_padre = (isset($mostrar['menu_padre']) && strlen(trim($mostrar['menu_padre']))>0) ? ($mostrar['menu_padre']) : ("");
    $nombre_modulo = (isset($mostrar['menu']) && strlen(trim($mostrar['menu']))>0) ? ($mostrar['menu']) : ("");
    $icon_modulo = (isset($mostrar['icon']) && strlen(trim($mostrar['icon']))>0) ? ($mostrar['icon']) : ("");
    $title = (isset($title) && strlen($title)) ? ($title) : ("");
    $nom_prod = (isset($nom_prod) && strlen($nom_prod)) ? ($nom_prod) : ("");
    $nombre = (!empty($nombre)) ? ($nombre) : ("");
    $subtitle = (!empty($subtitle)) ? $subtitle : NULL;
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $title;?> <small><?php echo $nombre;?></small></h3>
                  <div class="clearfix"></div>                  
                </div>
                <ol class="breadcrumb">
                      <li>
                          <a url="<?php echo base_url().$url_modulo;?>" href="javascript:void(0);"><i class="fa fa-table"></i> <?php echo $nombre_modulo;?></a>
                      </li>
                      <li class="active">
                          <?php echo $mod_title;?>
                      </li>
                  </ol>
                <div class="x_content">
                  <input id="url_modulo" type="hidden" value="<?php echo $url_modulo;?>" name="url_modulo" />
                  <input id="id_codigo" type="hidden" value="<?php echo $id_codigo;?>" name="id_codigo" />
                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
<?php if(isset($tabs)) { echo $tabs; } ?>
                    <div id="myTabContent" class="tab-content">
                      <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 9px;">
                      <div class="col-md-3 col-sm-3 col-xs-3"></div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <select class='form-control' id='id_marcas'>
                          <?php if(isset($select_)){ echo $select_;} ?>
                        </select>
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3"></div>
<?php if(isset($form)) { echo $form; } ?> 
                      </div>

                    </div>

                  </div>

                </div>
              </div>
            </div>

          </div>
          <div class="clearfix"></div>
        </div>
      <!-- /page content -->
      <?php if(isset($modal)) { echo $modal; } ?>