<!-- Modal -->
<div class="modal fade" id="proveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Agregar Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="crear-evento">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div id="tipo_persona" class="btn-group" data-toggle="buttons">
                              <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input id="tipo_persona_1" type="radio" name="tipo_persona" value="1" checked >Persona Juridica
                              </label>
                              <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
                                <input id="tipo_persona_2" type="radio" name="tipo_persona" value="2"> Persona Natural
                              </label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"><p></p></div><input type="hidden" value="" name="id_provart" id="id_provart">
                    <form class="form-horizontal" id="form-add-pj" name="formcontactos" role="form">
                        <div class="form-group">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="hidden" value="" id="id_persona_juridica" name="id_persona_juridica">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ruc" class="control-label col-md-4 col-sm-4 col-xs-12">RUC</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" name="ruc" id="ruc" class="form-control" value=""aria-describedby="ruc" placeholder="RUC">
                                <div id="autoruc" class="auto_completar"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rsocialprov" class="control-label col-md-4 col-sm-4 col-xs-12">Razón Social</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" id="rsocialprov" name="rsocial" class="form-control" value="" aria-describedby="rsocial" placeholder="Razón Social">
                                <div id="autorsocial" class="auto_completar"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Nombres Comercial</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" class="form-control" name="nombrecomercial" value="" id="nombrecomercial" placeholder="Nombres Comercial">
                                <div id="autonombrecomercial" class="auto_completar"></div>
                            </div>                                
                        </div>

                        <div class="btn-toolbar">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Agregar</button>
                            </div>
                            <div class="btn-group pull-right">
                                <button class="btn btn-warning btn_cancelpj"  data-dismiss="modal">
                                    <span class="fa fa-ban"></span>
                                    <span class="hide-on-phones">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <form class="form-horizontal collapse" id="form-add-pn" name="frpn" role="form">
                        <div class="form-group">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="hidden" value="" id="id_persona" name="id_persona">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dni" class="control-label col-md-4 col-sm-4 col-xs-12">DNI/Carnet de Ext.</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" id="dni" name="dni" class="form-control" value=""aria-describedby="dni" placeholder="DNI">
                                <div id="autondni" class="auto_completar"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="apellidos" class="control-label col-md-4 col-sm-4 col-xs-12">Apellidos</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" name="apellidos" id="apellidos" class="form-control" value="" aria-describedby="apellidos" placeholder="Apellidos">
                                <div id="autoapellidos" class="auto_completar"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nombres"class="control-label col-md-4 col-sm-4 col-xs-12">Nombres</label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <input type="text" id="nombres" name="nombres" class="form-control" value="" aria-describedby="nombres" placeholder="Nombres">
                                <div id="autonombres" class="auto_completar"></div>
                            </div>
                        </div>

                        <div class="btn-toolbar">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Agregar</button>
                            </div>
                            <div class="btn-group pull-right">
                                <button class="btn btn-warning btn_cancelpn"  data-dismiss="modal">
                                    <span class="fa fa-ban"></span>
                                    <span class="hide-on-phones">Cancelar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                        
        </div>
    </div>
</div>