<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url()."tools/imgs/gyg2.ico"; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $this->session->userdata('usuario'); ?></p>
        <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">PANEL DE NAVEGACIÓN</li>
<?php
  //echo "<pre>"; print_r($menu_lat); echo "</pre>";
  $menu = $this->uri->segment(1);  
  if(!empty($menu_lat))
  {
    foreach ($menu_lat as $val) 
    {
      $data_padre = explode(":", $val['menu']);
      $childs = $val['hijos'];
      $clss = "";
      $attr = "";
      foreach ($childs as $kk => $v) {
        $data_hijo = explode(":", $v);
        if($menu==$data_hijo[1])
        {
          $clss = "menu-open";
          $attr = "style='display: block;'";
        }
      }
?>
    <li class="treeview <?php echo($clss); ?>">
      <a href="#">
        <i class="fa <?php echo $data_padre[1]; ?>"></i> <span><?php echo $data_padre[0]; ?></span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu" <?php echo $attr; ?>>
<?php 
      foreach ($childs as $kk => $val2) 
      {
        $url_add = "";
        $icon_add = "";
        if($kk==12)
        {
          $fdata = stockcero();
          if($fdata>0)
          {
            $url_add = '/stockcero';
            $icon_add = "<span class='pull-right-container'><span class='label label-danger pull-right'>".$fdata."</span></span>";
          }
        }

        $data_hijo = explode(":", $val2);
?>
        
          <li><a href="<?php echo base_url().$data_hijo[1].$url_add; ?>"><i class="fa <?php echo $data_hijo[2]; ?>"></i><?php echo $data_hijo[0].$icon_add; ?></a></li>
<?php
      }
?>
      </ul>
    </li>
<?php
    }
  }

?>
</aside>
<style type="text/css">
  .content
  {
    padding-left: 30px;
    padding-right: 30px;
  }
</style>
<div class="content-wrapper" style="background-color: unset">
  <!-- Content Header (Page header) -->
  <!-- Main content -->
  <section class="content">
    