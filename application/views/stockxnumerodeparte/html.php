<?php

  $url_modulo = (!empty($url_modulo)) ? $url_modulo : "codigo";
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'rta_index':      

        if(isset($all_data[0]) && is_array($all_data))
        {   
          foreach ($all_data as $key => $value)
          {
            $clv = (!empty($value['precio_venta'])) ? null :"bg-danger";
            $txtv = (!empty($value['precio_venta'])) ? $value['precio_venta'] :"Sin Configurar";
            $stock = (!empty($value['stock'])) ? number_format($value['stock'],2) :"0.00";
            $ptota =  (!empty($value['valortotal'])) ? number_format($value['valortotal'],3) :"0.00";
            $fech = (!empty($value['fecha_ingreso'])) ? date('d-m-Y h:i:s A',strtotime($value['fecha_ingreso'])) : null;
            $simbolo = (!empty($value['precio_venta'])) ? $value['simbolo'] : null;
?>
            <tr idkardex="<?php echo $value['id_kardex']; ?>">
              <td class="bg-primary"><?php echo $value['codigo']; ?></td>
              <td class="bg-primary"><?php echo $value['descripcion']; ?></td>
              <td class="bg-primary"><?php echo $value['marca']; ?></td>
              <td class="<?php echo $clv; ?>">
                <span class="pull-left">
                  <b><i><?php echo $simbolo; ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $txtv; ?></i></b>
                </span>
              </td>
              <td class="bg-info"><?php echo $stock." uni."; ?></td>
              <td>
                <span class="pull-left">
                  <b><i><?php echo $simbolo; ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $ptota; ?></i></b>
                </span>
              <td><?php echo $fech; ?></td>
            </tr>
<?php
          }    
        }
        else
        { ?> 
          <tr>
            <td colspan="7">
              
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php       }
      break;
      case 'rta_indexcero':      

        if(!empty($all_data) && is_array($all_data))
        {   
          foreach ($all_data as $key => $value)
          {
            $txtv = (!empty($value['precio_venta'])) ? $value['precio_venta'] :"Sin Configurar";
            $stock = (!empty($value['stock'])) ? number_format($value['stock'],2) :"0.00";
            $ptota =  (!empty($value['valortotal'])) ? number_format($value['valortotal'],3) :"0.00";
            $fech = (!empty($value['fecha_ingreso'])) ? date('d-m-Y h:i:s A',strtotime($value['fecha_ingreso'])) : null;
            $simbolo = (!empty($value['precio_venta'])) ? $value['simbolo'] : null;

?>
            <tr>
              <td class="bg-danger"><?php echo $value['codigo']; ?></td>
              <td class="bg-danger"><?php echo $value['descripcion']; ?></td>
              <td class="bg-danger"><?php echo $value['marca']; ?></td>
              <td class="bg-danger"><?php echo $value['almacen']; ?></td>
              <td class="bg-danger">
                <span class="pull-left">
                  <b><i><?php echo $simbolo; ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $txtv; ?></i></b>
                </span>
              </td>
              <td class="bg-danger"><?php echo $stock." uni."; ?></td>
              <td class="bg-danger">
                <span class="pull-left">
                  <b><i><?php echo $simbolo; ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $ptota; ?></i></b>
                </span>
              <td class="bg-danger"><?php echo $fech; ?></td>
            </tr>
<?php
          }    
        }
        else
        { ?> 
          <tr>
            <td colspan="8">
              
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php       }
      break;
      
    }
  }        
?>