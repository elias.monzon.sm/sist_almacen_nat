<?php
  $menu_padre = (isset($mostrar['menu_padre']) && strlen(trim($mostrar['menu_padre']))>0) ? ($mostrar['menu_padre']) : ("");
  $nombre_modulo = (isset($mostrar['menu']) && strlen(trim($mostrar['menu']))>0) ? ($mostrar['menu']) : ("");
  $icon_modulo = (isset($mostrar['icon']) && strlen(trim($mostrar['icon']))>0) ? ($mostrar['icon']) : ("");
  $url_modulo = (isset($mostrar['url']) && strlen(trim($mostrar['url']))>0) ? ( ($mostrar['url']=="index") ? ("") :($mostrar['url']) ) : ("");  
  $mod_title = (isset($mod_title) && strlen(trim($mod_title))>0) ? ($mod_title) : ("");

  $id_alm = (!empty($id_almacen)) ? ($id_almacen) : ("");
  $sub_title = $nombre_modulo;

?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="clearfix"></div>
  <input type="hidden" value="<?php echo base_url().$url_modulo;?>" id="linkmodulo" />
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3> <i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $mod_title;?> <small><?php echo $sub_title;?></small></h3>
          <div class="clearfix"></div>
        </div>
        
        <div class="x_content">
          <div class="clearfix"><br></div>
            
            <div class="col-md-12 col-sm-12 col-xs-12">                                    
              <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2  col-xs-12">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12 txtalingright">Número de Parte:</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="descripcion" class="form-control">
                    <input type="hidden" id="id_codigo">
                    <div class="autocomplete-suggestions" id="autocomplete-container"></div>
                  </div>
                </div>
                <!-- /.form-group -->  
              </div>
            </div>
            <div class="clearfix"><br></div>
            <div class="col-md-12 col-sm-12 col-xs-12">                                    
              <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2  col-xs-12">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12 selectpicker txtalingright">Marca:</label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select id="id_marca" class="form-control"></select>
                  </div>
                </div>
                <!-- /.form-group -->  
              </div>
            </div>
      
            <div class="clearfix"></div><br>
            <div class="btn-toolbar">
              <div class="btn-group pull-right">
                <a href="javascript:void(0);" class="btn btn-default btn-sm buscar">&nbsp;<i class="fa fa-search"></i> Buscar<span class="hide-on-phones"></span>&nbsp;&nbsp;</a>
              </div>
            </div>

          <div class="clearfix"></div>
          <div class="row x_title1">
            <div class="col-md-6 col-sm-6 col-xs-12"><h2>Tabla de Registro</h2></div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">                      
              <div class="table-responsive">
                <table id="lista_articulos" class="table-bordered table table-striped dt-responsive nowrap dataTable no-footer dtr-inline">
                  <thead>
                    <tr>
                      <th>Codigo</th>
                      <th>Descripción</th>
                      <th>Marca</th>
                      <th>Precio Unitario Configurado</th> 
                      <th>Stock Existente</th>
                      <th>Valor Monetario Existente</th>
                      <th>Fecha de Actualización</th>
                    </tr>              
                    </thead>                      
                  <tbody>
                    <tr>
                      <td colspan="7"><h2 class="text-center text-success">No hay registros</h2></td>
                    </tr> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      <!-- /page content -->               

      </div>
    </div>
  </div>

 