<?php
  $menu_padre = (isset($mostrar['menu_padre']) && strlen(trim($mostrar['menu_padre']))>0) ? ($mostrar['menu_padre']) : ("");
  $nombre_modulo = (isset($mostrar['menu']) && strlen(trim($mostrar['menu']))>0) ? ($mostrar['menu']) : ("");
  $icon_modulo = (isset($mostrar['icon']) && strlen(trim($mostrar['icon']))>0) ? ($mostrar['icon']) : ("");
  $url_modulo = (isset($mostrar['url']) && strlen(trim($mostrar['url']))>0) ? ( ($mostrar['url']=="index") ? ("") :($mostrar['url']) ) : ("");  
  $mod_title = (isset($mod_title) && strlen(trim($mod_title))>0) ? ($mod_title) : ("");

  $id_alm = (!empty($id_almacen)) ? ($id_almacen) : ("");
  $sub_title = $nombre_modulo;
  $nomalm = "";
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>
          <input type="hidden" value="<?php echo base_url().$url_modulo;?>" id="linkmodulo" />
          <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3> <i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $mod_title;?> <small><?php echo $sub_title;?></small></h3>
                  <div class="clearfix"></div>
                </div>
                
                <div class="x_content">
                  <div class="clearfix"></div><br>
                  <form class="form-horizontal" id="form_busc_kardex">                     
<div class="col-md-12 col-sm-12 col-xs-12">                                    
                      <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2  col-xs-12">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12 txtalingright">Almacén:</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="id_almacen" class="form-control" name="id_almacen">
<?php if(!empty($cbox_alma)) { echo $cbox_alma; } ?>                              
                          </select>
                      </div>
                    </div>
                    <!-- /.form-group -->  
                      </div>
                      </div>
              
                      </div>
                      </div>
                      <div class="clearfix"></div><br>
                      <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                          <a href="javascript:void(0);" class="btn btn-default btn-sm buscar">&nbsp;<i class="fa fa-search"></i> Buscar<span class="hide-on-phones"></span>&nbsp;&nbsp;</a>
                        </div>
                      </div>
                      <!-- /.btn-toolbar -->                       
                  </form>
                  <div class="clearfix"></div>
                  <div class="row x_title1">
                    <div class="col-md-6 col-sm-6 col-xs-12"><h2>Tabla de Registro</h2></div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">                      
                      <div class="table-responsive">
                        <table id="lista_articulos" class="table-bordered table table-striped dt-responsive nowrap dataTable no-footer dtr-inline">
                          <thead>
                            <tr>
                              <th>Codigo</th>
                              <th>Descripción</th>
                              <th>Marca</th>
                              <th>Precio Unitario Configurado</th> 
                              <th>Stock Existente</th>
                              <th>Valor Monetario Existente</th>
                              <th>Fecha de Actualización</th>
                            </tr>              
                            </thead>                      
                          <tbody>
                            <tr>
                              <td colspan="7"><h2 class="text-center text-success">No hay registros</h2></td>
                            </tr> 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
          </div>
          <!-- /page content -->               

                </div>
              </div>
            </div>
          </div>
<?php if(!empty($rta)) { ?>    
<!-- Datatables -->
    <script>
   
      $(document).ready(function() {
        var nombre = "Lista_Articulos";

        var handleDataTableButtons = function() {
            $("#lista_articulos").DataTable({
              fixedHeader: true,
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm",
                  filename: nombre
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm",
                  filename: nombre,
                  pageSize: 'A4'
                },
                {
                  extend: "print",
                  className: "btn-sm",
                  text: 'Imprimir',
                  autoPrint: true,
                  orientation: 'landscape',
                  pageSize: 'A4'
                }
              ],
              paging: false,
              "ordering": false,
              "searching": false,
              "autoWidth": false
            });
        };
        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();
        TableManageButtons.init();
      });


    </script>
    <!-- /Datatables --> 
    <?php } ?>
    <style>
      .table.fixedHeader-floating {display: none;}
    </style>  