<?php
    if(isset($tipo) && strlen(trim($tipo))>0)
    {
        switch ($tipo) {
            case 'rta_index':
      if(isset($all_data[0]) && is_array($all_data))
        {     
        $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
        
        foreach ($all_data as $key => $value) {
            $estado = ($value['estado']==1) ? ("Activo") : ("Inactivo");
            $afecta = ($value['afecta_stock']==1)? ("Si") : "No";
?>
                    <tr idtipodocumento="<?php echo $value['id_tipodocumento']; ?>">
                        <td><?php echo $key+$orden;?></td>
                        <td><?php echo $value['tipodocumento'];?></td>
                        <td><?php echo $afecta;?></td>
                        <td><?php echo $estado;?></td>
                        <td>

                            <a href="javascript:void(0);" data-toggle="modal" data-target="#edittipodocumento" class="btn btn-warning edit btn-xs"><i class="fa fa-pencil"></i></a>
<?php if($value['estado']==1) {?>                          
                            <a href="javascript:void(0);" class="btn btn-danger delete btn-xs"><i class="fa fa-trash-o"></i></a>
<?php }?>  
                        </td>
                    </tr>
<?php
        }      
    }
    else
    { ?> <tr><td colspan="5"><h2 class="text-center text-success">No hay registro</h2></td></tr> <?php }
            break;

            default:
                # code...
                break;
        }
    }                
?>