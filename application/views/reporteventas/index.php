    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa "></i> Reporte de ventas<small> En Desarrollo</small></h3>
                  <div class="clearfix"></div>
                </div>

                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="col-md-3 col-sm-3 col-xs-3"></div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-3 text-right">Fecha Inicio: </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <fieldset>
                                  <div class="control-group">
                                    <div class="controls">
                                      <div class="input-prepend input-group" id="fecha_ini">
                                        <span class="add-on input-group-addon">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </span>
                                        <input id="fecha_inicio" type="text" class="form-control" placeholder="Fecha Inicio">
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-3"></div>
                          <div class="clearfix"></div>
                          <p></p>
                          <div class="col-md-3 col-sm-3 col-xs-3"></div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-3 text-right">Fecha Fin:</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <fieldset>
                                  <div class="control-group">
                                    <div class="controls">
                                      <div class="input-prepend input-group" id="fecha_fi">
                                        <span class="add-on input-group-addon">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </span>
                                        <input id="fecha_fin" type="text" class="form-control" placeholder="Fecha Fin">
                                      </div>
                                    </div>
                                  </div>
                                </fieldset>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3 col-xs-3">
                            <button type="button" class="btn btn-success buscar">Buscar</button>
                          </div>
                        </div>
                      </div>
                      <p>&nbsp;</p>
                      <div class="table-responsive col-md-12 col-sm-12 col-xs-12">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th><center>Serie</center></th>
                              <th><center>Tipo de documento</center></th>
                              <th><center>Fecha</center></th>
                              <th><center>Cliente</center></th>
                              <th><center>Monto Total</center></th>
                              <th><center>Acciones</center></th>
                            </tr>
                          </thead>
                          <tbody id="bodyindex">
                            <tr>
                              <td colspan="7">
                                <h2 class="text-center text-success">No hay datos</h2>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
      </div>
<?php 
	if(isset($modal)) 
	{
	    echo $modal;
	} 
?>
