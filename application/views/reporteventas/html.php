<?php

  $url_modulo = (!empty($url_modulo)) ? $url_modulo : "codigo";
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'rta_index':      

        if(isset($all_data[0]) && is_array($all_data))
        {   
          $tot = 0;
          foreach ($all_data as $key => $value)
          {
            $url = base_url()."ventas/ver_documento/".$value['id_venta'];
            $tot += $value['precio_total'];
?>
            <tr>
              <td><?php echo $key+1; ?></td>
              <td><center><?php echo $value['codigo']."-".$value['serie'];?></center></td>
              <td><center><?php echo $value['tipodocumento'];?></center></td>
              <td><center><?php echo date('d-m-Y',strtotime($value['fecha_ingreso'])); ?></center></td>
              <td><center><?php echo $value['cliente'];?></center></td>
              <td><center><?php echo $value['simbolo'].number_format($value['precio_total'],2); ?></center></td>
              <td>
                <center>
                  <a class="btn btn-info edit btn-xs" target="_blank" href='<?php echo $url; ?>'><i class="fa fa-eye"></i></a>
                </center> 
              </td>
            </tr>
<?php
          }
?>
          <tr>
            <td colspan="5" class="text-right"><b>Monto Total</b></td>
            <td><?php echo $value['simbolo'].number_format($tot,2); ?></td>
            <td></td>
          </tr>
<?php   
        }
        else
        { ?> 
          <tr>
            <td colspan="7">
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php   
        }
      break;
    }
  }        
?>