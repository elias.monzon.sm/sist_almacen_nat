  <!-- Modal -->
<div class="modal fade" id="edit_codigo" style="overflow: auto !important;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="crear-evento">
          <div class="clearfix">
              <form class="form-horizontal" id="form_save_codigo">

                  <input type="hidden" value="" name="id_codigo" id="id_codigo">
                  <div class="form-group">
                    <label for="busc_provee" class="control-label col-md-3 col-sm-3 col-xs-12">Estado</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="estado" class="btn-group" data-toggle="buttons">
                          <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                            <input type="radio" id="estado_1" name="estado" value="1" checked> &nbsp; Activo &nbsp;
                          </label>
                          <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                            <input type="radio" id="estado_0" name="estado" value="0"> Inactivo
                          </label>
                        </div>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 descripcioncol-sm-3 col-xs-12">Código *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="codigo" id="codigo" class="form-control"/>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción Completa</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <textarea name="descripcion_completa" id="descripcion_completa" class="form-control">
                      </textarea>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción Corta</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="descripcion" id="descripcion" class="form-control"/>
                    </div>
                  </div>
                  
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Descripción Corta Traducida</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="descripcion_2" id="descripcion_2" class="form-control"/>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-s-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Marca *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" id="marca_autoc" class="form-control" autocomplete="off">
                      <input type="hidden" id="selected_marca" idmarca="" value="" >
                      <div class="autocomplete-suggestions" id="autocomplete-container"></div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                      <a class="btn btn-primary add_marca btn-xs" ><i class="fa fa-plus-square"></i> add</a>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <table class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                        <thead>
                          <th class="text-center">Marca</th>
                          <th class="text-center">Acción</th>
                        </thead>
                        <tbody id="marcas_tbody"></tbody>
                      </table>
                    </div>
                  </div>

                  <div class="btn-toolbar1 col-md-12 col-sm-12 col-xs-12">
                    <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="fa fa-save"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                    </div>
                    <div class="btn-group pull-right">
                      <a class="btn btn-warning btn_limpiar">
                        <span class="fa fa-eraser"></span>
                        <span class="hide-on-phones">Cancelar</span>
                      </a>
                    </div>
                  </div>
              </form>

          </div>
        </div>
      </div>
    </div>
  </div>