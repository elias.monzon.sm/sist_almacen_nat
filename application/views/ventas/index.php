  <?php 
    $mod_title = (!empty($mod_title)) ? $mod_title : null;
  ?>  
    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa "></i><?php echo $mod_title; ?><small> En Desarrollo</small></h3>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="btn-group pull-right"><a class="btn btn-primary btn-sm" href="<?php echo base_url()."ventas/add"; ?>" ><i class="fa fa-file"></i> Nuevo</a></div> 
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30"></p>
                        <div class="table-responsive">
                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                        <thead>
                          <tr>
                            <th><center>Código</center></th>
                            <th><center>Serie</center></th>
                            <th><center>Fecha de Documento</center></th>
                            <th><center>Cliente</center></th>
                            <th><center>Tipo de Documento</center></th>
                            <th><center>Precio Total</center></th>
                            <th><center>Acciones</center></th>
                          </tr>
                        </thead>
                        <tbody id="bodyindex">
  <?php if(isset($rta)) { echo $rta; } ?>                          
                            </tbody>
                          </table>
                        <div class="table-responsive">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="btn-group pull-right" id="paginacion_data">
                          <?php if(isset($paginacion)) {echo $paginacion;} ?>                        
                        </div> 
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
      </div>
