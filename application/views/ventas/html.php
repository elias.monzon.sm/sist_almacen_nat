<?php
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'rta_index':
        if(!empty($all_data))
        {
          foreach ($all_data as $key => $value) {
            $expl = explode(":", $value['tipomoneda']);
            $tipomon = $expl[0];
            $sgn = $expl[1];
            $url = base_url()."ventas/ver_documento/".$value['id_venta'];
?>
            <tr idventa="<?php echo $value['id_venta']; ?>">
              <td><?php echo $value['codigo']; ?></td>
              <td><?php echo $value['serie']; ?></td>
              <td><?php echo date('d-m-Y',strtotime($value['fecha_ingreso'])); ?></td>
              <td><?php echo $value['cliente']; ?></td>
              <td><?php echo $tipomon; ?></td>
              <td><?php echo $value['precio_total']; ?></td>
              <td>
                <a class="btn btn-info btn-xs" target="_blank" href='<?php echo $url; ?>'>
                  <i aria-hidden='true' class='fa fa-eye'></i>
                </a>
              </td>
            </tr>
<?php
          }
        }
        else
        {
?>
          <tr><td colspan="7"><h2 class="text-center text-success">No hay Datos</h2></td></tr> 
<?php
        }
      break;
      case 'bandeja_rta':
        if(!empty($all_data))
        {
          foreach ($all_data as $key => $value) {
?>
            <tr id_codigo="<?php echo $value['id_codigo']; ?>" id_marca="<?php echo $value['id_marca'] ?>">
              <td class="tdcod"><?php echo $value['codigo'] ?> </td>
              <td class="tdmarca"><?php echo $value['marca'] ?></td>
              <td class="desc1"><?php echo $value['descripcion'] ?></td>
              <td class="desc2"><?php echo $value['descripcion_2'] ?></td>
              <td>
                <a class="btn btn-info btn-xs add_arti" href='javascript:void(0);'>
                  <i aria-hidden='true' class='fa fa-product-hunt'></i> Add
                </a>
              </td>
            </tr>
<?php
          }
        }
        else
        {
?>
          <tr><td colspan="7"><h2 class="text-center text-success">No hay Artículos</h2></td></tr> 
<?php
        }
      break;
      case 'rta_ventaserv':
        if(!empty($all_data))
        {
          foreach ($all_data as $value) 
          {
?>
            <tr id_servicio="<?php echo $value['id_servicio']; ?>">
              <td class="tdserv"><?php echo $value['servicio'] ?> </td>
              <td>
                <a class="btn btn-info btn-xs add_serv" href='javascript:void(0);'>
                  <i aria-hidden='true' class='fa fa-product-hunt'></i> Add
                </a>
              </td>
            </tr>
<?php
          }
        }
        else
        {
?>
          <tr><td colspan="4"><h2 class="text-center text-success">No hay Artículos</h2></td></tr>     
<?php
        }
      break;
      case 'buscar_clie':

        if(isset($all_data) && is_array($all_data))
        {
          $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
          $url_ = (isset($url_modulo) && strlen(trim($url_modulo))>0) ? ($url_modulo) : ("clientes");
          //print_r($all_data);
            foreach ($all_data as $key => $value) {
              $tipope = (!empty($value['tipo_persona'])) ? ($value['tipo_persona']) : ("");
              $r_d =   ($value['tipo_persona'] == 1) ? ("Dni") : ("RUC");
?>
                      <tr tipope="<?php echo $tipope;?>" rucdni="<?php echo $value['ruc'];?>" doc="<?php echo $r_d;?>" idcliente="<?php echo $value['id_persona']; ?>">
                        <td><?php echo $key+$orden;?></td>
                        <td><?php echo $value['nombre_comercial'];?></td>
                        <td class="rz"><?php echo $value['razon_social'];?></td>
                        <td><?php echo $r_d.": ".$value['ruc'];?></td>
                        <td>
                            <a class="btn btn-info btn-xs agre_cliente" href='javascript:void(0);'>
                            <i aria-hidden='true' class='fa fa-product-hunt'></i> Add</a>
                        </td>
                      </tr>
<?php
            }      
        }
        else
        { 
?> 
          <tr><td colspan="5"><h2 class="text-center text-success">No hay Clientes</h2></td></tr> 
<?php    }
            break;
    }
  }        
?>