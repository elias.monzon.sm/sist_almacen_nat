<!-- Modal -->
<div class="modal fade" id="addarticulo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Agregar Artículo</h4>
            </div>
            <div class="modal-body">
                <div class="crear-evento">
                  <form class="form-horizontal" id="form_save_add_articulo">
                    <input type="hidden" id="id_codigo" name="id_codigo">
                    <input type="hidden" id="id_marca" name="id_marca">
                    <input type="hidden" id="edita_arti">
                    
                    <div class="form-group">
                      <label for="codigo" class="control-label text-center col-md-3 col-sm-3 col-xs-12">Código</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <label  style="text-align:center;" class="control-label text-center col-md-12 col-sm-12 col-xs-12" for="codigo" id="codigo"></label>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="descripcion" class="control-label col-md-3 col-sm-3 col-xs-12">Marca *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control text-center" value="" id="marca" name="marca" aria-describedby="marca" disabled="" placeholder="Marca">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="descripcion" class="control-label col-md-3 col-sm-3 col-xs-12">Descripción *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control text-center" value="" id="descripcion" name="descripcion" aria-describedby="preciou" disabled="" placeholder="Descripción">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="umcompra" class="control-label col-md-3 col-sm-3 col-xs-12">UM Compra *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <label style="text-align:center;" class="control-label col-md-12 col-sm-12 col-xs-12" for="umcompra" id="umcompra">Unidad</label>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="cantidad" class="control-label col-md-3 col-sm-3 col-xs-12">Cantidad *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" value="" id="cantidad" name="cantidad" aria-describedby="cantidad" placeholder="Cantidad">
                      </div>                                
                    </div>

                    <div class="form-group">
                      <label for="preciou" class="control-label col-md-3 col-sm-3 col-xs-12 cambsp"><span>Precio</span> U *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" value="" id="preciou" name="precio_unitario" aria-describedby="preciou" placeholder="Precio U.">
                      </div>                                
                    </div>

                    <div class="form-group">
                      <label for="preciot" class="control-label col-md-3 col-sm-3 col-xs-12 cambsp"><span>Precio</span> T *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" value="" id="preciot" name="precio_total" aria-describedby="preciot" placeholder="Precio T.">
                      </div>
                    </div>          

                    <div class="btn-toolbar">
                      <div class="btn-group pull-right">
                          <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="fa fa-save"></i><span class="hide-on-phones"></span>&nbsp;Agregar</button>
                      </div>
                      <div class="btn-group pull-right">
                        <a class="btn btn-link btn_cancelar" href="javascript:void(0);">
                            <span class="fa fa-ban"></span>
                            <span class="hide-on-phones">Cancelar</span>
                        </a>
                      </div>
                    </div>
                    <!-- /.btn-toolbar -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>