<!-- Modal -->
<div class="modal modal-wide fade" id="buscarart" Role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Artículos</h4>
      </div>
      <div class="modal-body">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
              <li role="presentation" tabs="articulos" class="active">
                <a data-toggle="tab" href="#tab_arti">Artículos</a>
              </li>
              <li role="presentation" tabs="servicios" class="tab">
                <a href="#tab_serv" data-toggle="tab">Servicios</a>
              </li>
          </ul>
          
          <div class="tab-content clearfix">

          <div id="tab_arti" class="tab-pane fade in active" role="tabpanel">
            <div class="crear-evento">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <table id="buscar_arti" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                    <thead>
                      <tr>
                        <th>Código</th>
                        <th>Marca</th>
                        <th>Descripcion</th>
                        <th>Descripción Traducida</th>
                        <th>Acciones</th>
                      </tr>
                      <tr id="filtro_arti">
                        <th>
                          <input class="form-control" type="text" id="codigo"></input>
                        </th>
                        <th>
                          <input class="form-control" type="text" id="marca"></input>
                        </th>
                        <th>
                          <input class="form-control" type="text" id="descripcion"></input>
                        </th>
                        <th>
                          <input class="form-control" type="text" id="descripcion_traducida"></input>
                        </th>
                        <th>
                          <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                            <i class="fa fa-search"></i>
                          </a>
                          <a href="javascript:void(0);" class="btn btn-default limpiarfiltro btn-sm">
                            <i class="fa fa-refresh"></i>
                          </a>
                        </th>
                      </tr>
                    </thead>                      
                    <tbody>
                      <tr><td colspan="6"><h2 class="text-center text-success">No agregó Artículos</h2></td></tr>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="btn-group pull-right" id="arti_paginacion_data"></div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div id="tab_serv" class="tab-pane fade" role="tabpanel">
            <div class="crear-evento">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <table id="buscar_serv" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                    <thead>
                      <tr>
                        <th>Servicios</th>
                        <th>Acciones</th>
                      </tr>
                      <tr id="filtro">
                        <th>
                          <input class="form-control" type="text" id="servicios"></input>
                        </th>
                        <th>
                          <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                            <i class="fa fa-search"></i>
                          </a>
                          <a href="javascript:void(0);" class="btn btn-default limpiarfiltro btn-sm">
                            <i class="fa fa-refresh"></i>
                          </a>
                        </th>
                      </tr>
                    </thead>                      
                    <tbody>
                      <tr><td colspan="2"><h2 class="text-center text-success">No agregó Artículos</h2></td></tr>
                    </tbody>
                  </table>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="btn-group pull-right" id="serv_paginacion_data"></div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        </div>

        
      </div>
    </div>
  </div>
</div>