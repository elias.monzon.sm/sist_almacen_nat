<?php
  $nombre_modulo = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icon_modulo = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null;
  //print_r($all_data);
  $moneda = $main['tipomoneda'];
  $simbolo_moneda = $main['simbolo'];
  $det = $main['det'];
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>
          <style type="text/css">
          input.cir {
            -webkit-border-radius: 50px !important;
            -moz-border-radius: 50px !important;
            border-radius: 50px !important;
            }
          </style>
          <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3> <i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $mod_title;?> </h3>
                  <div class="clearfix"></div>
                </div>
               
                <div class="x_content">
                  <form class="form-horizontal form-label-left">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="col-md-4 col-sm-4 col-xs-12"></div>
                          <!-- /.col-md-4 -->                      
                        </div>
                      <!-- /.col-md-12 -->
                      </div>
                      <div><p></p></div>
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Cliente: </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" class="cir form-control" disabled value="<?php echo $main['cliente'] ?>"/>
                        </div>                                 
                      </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="fecha_busc">Fecha de venta: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $main['fecha_ingreso']; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->                        

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Moneda: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $main['tipomoneda']; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Documento: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $main['tipodocumento']; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="direccion_fiscal">Numero de Documento: </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>#</b></span>
                            <input disabled type="text" class="form-control" value="<?php echo $main['codigo']; ?>">
                          </div>
                        </div>                          
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>N°</b></span>
                            <input disabled type="text" class="form-control" value="<?php echo $main['serie'] ?>">
                          </div>
                        </div>                          
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="clearfix"></div>
                    <div class="form-group" style="border-bottom: 1px solid #e5e5e5;">
                      <label for="celular" class="col-md-6 col-sm-6 col-xs-12"><h3>Lista de Artículos</h3></label>
                      <div class="clearfix"></div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                          <table id="lista_articulos" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>Número de Parte</th>
                                <th>Artículo/Servicio</th>
                                <th>Marca</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Precio Total</th>
                                <th>Almacén</th>
                              </tr>
                            </thead>
                            <tbody>
<?php 
                            foreach ($det as $val) 
                            {
?>
                              <tr>
                                <td><?php echo $val['codigo']; ?></td>
                                <td><?php echo $val['descripcion_completa']; ?></td>
                                <td><?php echo $val['marca']; ?></td>
                                <td><?php echo $val['cantidad']; ?></td>
                                <td>
                                  <span class="pull-left">
                                    <b><i><?php echo $simbolo_moneda ?></i></b>
                                  </span>
                                  <span class="pull-right">
                                    <b><i><?php echo $val['precio_unitario']; ?></i></b>
                                  </span>
                                </td>
                                <td>
                                  <span class="pull-left">
                                    <b><i><?php echo $simbolo_moneda ?></i></b>
                                  </span>
                                  <span class="pull-right">
                                    <b><i><?php echo $val['precio_total']; ?></i></b>
                                  </span>
                                </td>
                                <td><?php echo $val['almacen']; ?></td>
                              </tr>
<?php
                            }
?>
                              <tr>
                                <td colspan="5" class="text-right"><b>TOTAL</b></td>
                                <td>
                                  <span class="pull-left">
                                    <b><i><?php echo $simbolo_moneda ?></i></b>
                                  </span>
                                  <span class="pull-right">
                                    <b><i><?php echo number_format($main['precio_total'],2); ?></i></b>
                                  </span>
                                </td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                    </div>
                </form>              
                <!-- /.form -->
          </div>
          <!-- /page content -->               

                </div>
              </div>
            </div>
          </div>
