<?php
  $menu = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icono = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null; 

  $id_persona_juridica = (!empty($miempresa['id_persona_juridica'])) ? $miempresa['id_persona_juridica'] : null;
  $ruc = (!empty($miempresa['ruc'])) ? $miempresa['ruc'] : null;
  $razon_social = (!empty($miempresa['razon_social'])) ? $miempresa['razon_social'] : null;
  $nombre_comercial = (!empty($miempresa['nombre_comercial'])) ? $miempresa['nombre_comercial'] : null;
  $direccion_fiscal = (!empty($miempresa['direccion_fiscal'])) ? $miempresa['direccion_fiscal'] : null;
  $pagina_web = (!empty($miempresa['pagina_web'])) ? $miempresa['pagina_web'] : null;
  $aniversario = (!empty($miempresa['aniversario'])) ? date('d-m-Y',strtotime($miempresa['aniversario'])) : null; 
  $email = (!empty($miempresa['email'])) ? $miempresa['email'] : null;
  $contacto_cel = (!empty($miempresa['contacto_cel']))? $miempresa['contacto_cel'] : null;
  $contacto_tlf = (!empty($miempresa['contacto_tlf']))? $miempresa['contacto_tlf'] : null; 
?>

<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  	<h3>
  		<i class="fa <?php echo $icono; ?>"></i> 
  		<b><?php echo $menu;?> <small> En Desarrollo</small></b>
  	</h3>
  	<div class="x_content">
  		<form class="form-horizontal" id="form_save_pers_juridica">
        <input type="hidden" value="<?php echo $id_persona_juridica; ?>" id="id_persona_juridica" name="id_persona_juridica" />

      	<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">

            	<div class="form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ruc">Ruc *</label>
              	<div class="col-md-9 col-sm-9 col-xs-12">
                		<input type="text" maxlength="11" class="form-control" id="ruc" value="<?php echo $ruc; ?>" name="ruc" aria-describedby="ruc" placeholder="Ruc"/>
              	</div>                                 
            	</div>
              	<!-- /.form-group -->                        

              	<div class="form-group">
                	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="razon_social">Razon Social *</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" value="<?php echo $razon_social; ?>" name="razon_social" id="razon_social" aria-describedby="razon_social" placeholder="Razon Social">
                    </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion_fiscal">Dirección Fiscal *</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      	<input type="text" class="form-control" value="<?php echo $direccion_fiscal; ?>" name="direccion_fiscal" aria-describedby="direccion_fiscal" placeholder="Dirección">
                    </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_departamento">Departamento (DF) *</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                  		<select class="form-control selectpicker" name="id_departamento" id="id_departamento">
                        <?php if(!empty($cbx_departamentos)){echo $cbx_departamentos;}?>  
                      </select>
                    </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_provincia">Provincia (DF) *</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      	<select class="form-control selectpicker" name="id_provincia" id="id_provincia">
                          <?php if(!empty($cbx_provincias)){echo $cbx_provincias;}?>
                        </select>
                    </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_distrito">Distrito (DF) *</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      	<select class="form-control selectpicker" name="id_distrito" id="id_distrito">
                          <?php if(!empty($cbx_distritos)){echo $cbx_distritos;}?>
                        </select>
                    </div>
              	</div>
              	<!-- /.form-group -->

            </div>
            <!-- /.col-xs-6 -->

            <div class="col-md-6 col-sm-6 col-xs-12">

              	<div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre_comercial">Nombre Comercial</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">          
                      	<input name="nombre_comercial"  id="nombre_comercial" value="<?php echo $nombre_comercial; ?>" type="text" class="form-control" aria-describedby="nombre_comercial" placeholder="Nombre Comercial">
                    </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pagina_web">Página Web</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    	<input type="text" class="form-control" id="pagina_web" value="<?php echo $pagina_web; ?>" name="pagina_web" aria-describedby="pagina_web" placeholder="Página Web">
                  </div>
              	</div>
              	<!-- /.form-group -->

              	<div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aniversario">Aniversario</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    	<fieldset>
                      	<div class="form-group">
                          	<div class='input-group date' id='datetimepicker1'>
                              <input type='text' name="aniversario" id="aniversario" class="form-control" value="<?php echo $aniversario; ?>" />
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                            </div>
                     		</div>
                    	</fieldset>
                  </div>
              	</div>
              	<!-- /.form-group -->

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pagina_web">E-mail</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" class="form-control" id="email" value="<?php echo $email; ?>" name="email" aria-describedby="pagina_web" placeholder="Página Web">
                  </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                  <legend class="col-md-12 col-sm-12 col-xs-12">Celular 
                    <a href="javascript:void(0);" class="btn btn-danger add_cel btn-xs">
                      <i class="fa fa-plus-square"></i>
                    </a>
                  </legend>
                  <div class="col-md-12 col-sm-12 col-xs-12" id="cel_container">
                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">Operadora</label>
<?php 
                    if(!empty($contacto_cel))
                    {
                      $c = 1;
                      foreach ($contacto_cel as $value) 
                      {
?>
                        <div class='form-group form-inline'>
                          <input class='form-control' type='text' name='contacto[cel][<?php echo $c; ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['celular'];  ?>">
                          <select class='form-control' name='contacto[op][<?php echo $c; ?>]' style='margin-right: 10px;'>
                            <?php if(isset($value['cbx'])){echo $value['cbx'];}?>
                          </select>
                          <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                            <i class='fa fa-trash-o'></i>
                          </a>
                        </div>
<?php
                        $c++;
                      }
                    }
?>

                  </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                  <legend class="col-md-12 col-sm-12 col-xs-12">Teléfono
                    <a href="javascript:void(0);" class="btn btn-danger add_telf btn-xs">
                      <i class="fa fa-plus-square"></i>
                    </a>
                  </legend>
                  <div class="col-md-12 col-sm-12 col-xs-12" id="telf_container">
                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">Anexo</label>
<?php 
                    if(!empty($contacto_tlf))
                    {
                      $c = 1;
                      foreach ($contacto_tlf as $value) 
                      {
?>
                        <div class='form-group form-inline'>
                          <input class='form-control' type='text' name='contacto[telf][<?php echo($c); ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['telefono']; ?>">
                          <input class='form-control' type='text' name='contacto[anex][<?php echo($c); ?>]' style='margin-right: 10px;' value="<?php echo $value['anexo']; ?>">
                          <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                            <i class='fa fa-trash-o'></i>
                          </a>
                        </div>
<?php
                        $c++;
                      }
                    }
?>
                  </div>
                </div>
                <!-- /.form-group -->

            </div>
      	</div>
      	<!-- /.row -->

      	<div class="clearfix"></div>

      	<div class="btn-toolbar">
        	<div class="btn-group pull-right">
          	<button type="submit" class="btn btn-success btn-submit btn-sm">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
        	</div>
         	<div class="btn-group pull-right">
          	<a class="btn btn-warning btn_cancel btn-sm" href="<?php echo base_url().$url;?>">
            	<span class="fa fa-ban"></span>
            	<span class="hide-on-phones">Cancelar</span>
          	</a>
        	</div>
      	</div>
  		</form>
  	</div>
  </div>
</div>

