<?php
  $nombre_modulo = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icon_modulo = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null;
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $nombre_modulo;?> <small>Mantenimiento</small></h3>
                  <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="row x_title1">
                  <div class="col-md-6 col-sm-6 col-xs-12"><h2><?php echo $mod_title;?></h2></div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="btn-group pull-right"><a class="btn btn-primary add_servicio btn-sm" data-toggle="modal" data-target="#editservicio" href="javascript:void(0);"><i class="fa fa-file"></i> Nuevo</a></div> 
                  <div class="clearfix"></div>
                  </div>
                </div>
                <style type="text/css">
                  th,td{
                    text-align: center;
                  }
                </style>
                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30"></p>
                        <div class="table-responsive">
                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Servicio</th>
                                <th>Moneda de Venta</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                              </tr>
                              <tr id="filtro">
                                <td></td>
                                <td>
                                  <input id="alm" type="text" class="form-control" aria-describedby="alm" placeholder="servicio">
                                </td>
                                <td>
                                  <select id="id_tipomoneda_busc" class="form-control">
                                    <?php echo (!empty($cbx_tipomoneda)) ? $cbx_tipomoneda : null; ?>
                                  </select>
                                </td>
                                <td></td>
                                <td>
                                  <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                                    <i class="fa fa-search"></i>
                                  </a>
                                  <a href="javascript:void(0);" class="btn btn-default limpiarfiltro btn-sm">
                                    <i class="fa fa-refresh"></i>
                                  </a>
                                </td>
                              </tr>
                            </thead>
                            <tbody id="bodyindex">
  <?php if(isset($rta)) { echo $rta; } ?>                          
                            </tbody>
                          </table>
                        <div class="table-responsive">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="btn-group pull-right" id="paginacion_data">
                          <?php if(isset($paginacion)) {echo $paginacion;} ?>                        
                        </div> 
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
          
<?php if(isset($modal)) {echo $modal;} ?>      