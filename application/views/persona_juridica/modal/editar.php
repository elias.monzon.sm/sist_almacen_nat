<!-- Modal -->
<div class="modal fade" id="editpersona" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Persona Juridica</h4>
            </div>
            <div class="modal-body">
                <div class="crear-evento">
                    <form class="form-horizontal" id="form_save_persona">
                        <input type="hidden" value="" name="id_persona_juridica" id="id_persona_juridica">

                        <div class="form-group">
                            <label for="busc_provee" class="control-label col-md-3 col-sm-3 col-xs-12">Estado</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div id="estado" class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" id="estado_1" name="estado" value="1" checked> &nbsp; Activo &nbsp;
                                    </label>
                                    <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" id="estado_0" name="estado" value="0"> Inactivo
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dnibusc">RUC
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input maxlength="11" type="text" class="form-control" id="ruc" value="" name="ruc" aria-describedby="dni"/>
                            </div>                                 
                        </div>
                        <!-- /.form-group -->
                
                        <div class="form-group">
                            <label for="nombres" class="control-label col-md-3 col-sm-3 col-xs-12">Nombre Comercial *</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" value="" id="nombre_comercial" name="nombre_comercial" aria-describedby="nombres" placeholder="Nombre Comercial">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="apellidos" class="control-label col-md-3 col-sm-3 col-xs-12">Razón Social *</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" value="" id="razon_social" name="razon_social" aria-describedby="apellidos" placeholder="Razón Social">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="codigo" class="control-label col-md-3 col-sm-3 col-xs-12">Aniversario</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <fieldset>
                                    <div class="control-group">
                                        <div class="controls">
                                            <div class="input-prepend input-group" id="fecha_nac">
                                                <span class="add-on input-group-addon">
                                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                </span>
                                                <input id="aniversario" name="aniversario" type="text" value="" class="form-control" placeholder="Aniversario" aria-describedby="Aniversario">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <label for="modulo" class="control-label col-md-3 col-sm-3 col-xs-12">Tipo</label>
                            <div class="col-md6 col-sm-6 col-xs-12" id="div_docu">
                                <div class="col-md-12 error_check">
                                    <input type="checkbox" id="es_cliente" name="es_cliente" value="1"> Es Cliente
                                    <br>
                                    <input type="checkbox" id="es_proveedor" name="es_proveedor" value="1"> Es Proveedor
                                </div>
                            </div>
                        </div>                   
                        
                        <div class="btn-toolbar">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success btn-submit">&nbsp;
                                    <i class="fa fa-save"></i>
                                    <span class="hide-on-phones"></span>&nbsp;Guardar
                                </button>
                            </div>
                            <div class="btn-group pull-right">
                                <a class="btn btn-warning btn_limpiar" href="javascript:void(0);">
                                    <span class="fa fa-eraser"></span>
                                    <span class="hide-on-phones">Cancelar</span>
                                </a>
                            </div>
                        </div>
                        <!-- /.btn-toolbar -->
                    </form>
                </div>
            </div>                    
        </div>
    </div>
</div>