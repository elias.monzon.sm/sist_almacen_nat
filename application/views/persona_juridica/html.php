<?php
    if(isset($tipo) && strlen(trim($tipo))>0)
    {
        switch ($tipo) {
            case 'rta_index':

                if(isset($data[0]) && is_array($data))
                {     
                    $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
                
                    foreach ($data as $key => $value) 
                    {
                        $estado = ($value['estado']==1) ? ("Activo") : ("Inactivo"); 
                        $urlform = base_url().$url_modulo."/config/general/".$value['id_persona_juridica'];
                        $es_cliente = ($value['es_cliente']==1) ? "fa-check-square-o" : "fa-square-o";
                        $es_proveedor = ($value['es_proveedor']==1) ? "fa-check-square-o" : "fa-square-o";
                        $ruc = (!empty($value['ruc'])) ? $value['ruc'] : "";
?>
                        <tr idpersonajuridica="<?php echo $value['id_persona_juridica']; ?>">
                            <td><?php echo $key+$orden;?></td>
                            <td><?php echo $value['nombre_comercial'];?></td>
                            <td><?php echo $value['razon_social'];?></td>
                            <td><?php echo $ruc;?></td>
                            <td><?php echo $value['aniversario'];?></td>
                            <td><i class="fa <?php echo $es_cliente;?>"></i></td>
                            <td><i class="fa <?php echo $es_proveedor;?>"></i></td>
                            <td><?php echo $estado;?></td>
                            <td>
<?php if(empty($value['miempresa'])){ ?>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#editpersona" class="btn btn-warning edit btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="<?php echo $urlform;?>" target="_blank" class="btn btn-info btn-xs">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
<?php if($value['estado']==1) {?>  
                                <a href="javascript:void(0);" class="btn btn-danger delete btn-xs">
                                    <i class="fa fa-trash-o"></i>
                                </a>
<?php } }?>                            
                            </td>
                        </tr>
<?php
                    }      
                }
                else
                { 
?> 
                    <tr>
                        <td colspan="13">
                            <h2 class="text-center text-success">No hay registros</h2>
                        </td>
                    </tr> 
<?php           }
            break;
            case 'general':
                $url_modulo = (!empty($url_modulo)) ? $url_modulo : null;
                $id_persona_juridica = (!empty($persona['id_persona_juridica'])) ? $persona['id_persona_juridica'] : null;
                $ruc = (!empty($persona['ruc'])) ? $persona['ruc'] : null;
                $nombre_comercial = (!empty($persona['nombre_comercial'])) ? $persona['nombre_comercial'] : null;
                $direccion_fiscal = (!empty($persona['direccion_fiscal'])) ? $persona['direccion_fiscal'] : null;
                $email = (!empty($persona['email'])) ? $persona['email'] : null; 
                $pagina_web = (!empty($persona['pagina_web'])) ? $persona['pagina_web'] : null;
                $contacto_cel = (!empty($persona['contacto_cel'])) ? $persona['contacto_cel'] : null;
                $contacto_telef = (!empty($persona['contacto_telef'])) ? $persona['contacto_telef'] : null;
                $razon_social = (!empty($persona['razon_social'])) ? $persona['razon_social'] :null;
                $aniversario = (!empty($persona['aniversario'])) ? date('d-m-Y',strtotime($persona['aniversario'])) : null;
                
                $chke_acti = ($persona['estado']==1) ? "checked" : null;
                $lbl1_acti = ($persona['estado']==1) ? "active" : null;
                $chke_desac = ($persona['estado']==0) ? "checked" : null;
                $lbl2_acti = ($persona['estado']==0) ? "active" : null;

                $contacto_cel = (!empty($persona['contacto_cel'])) ? ($persona['contacto_cel'])  :null;
                $contacto_telef = (!empty($persona['contacto_telef'])) ? ($persona['contacto_telef']) :null;
                
?>
                <form class="form-horizontal" id="form_save_pers_juridica">
                    <input type="hidden" value="<?php echo $id_persona_juridica; ?>" name="id_persona_juridica" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div id="estado" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default <?php echo $lbl1_acti; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="estado" <?php echo $chke_acti; ?> value="1"> &nbsp; Activo &nbsp;
                                        </label>
                                        <label class="btn btn-default <?php echo $lbl2_acti; ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="estado" <?php echo $chke_desac; ?> value="0"> Inactivo
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ruc">Ruc *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" maxlength="11" class="form-control" id="ruc" value="<?php echo $ruc; ?>" name="ruc" aria-describedby="ruc" placeholder="Ruc"/>
                                </div>                                 
                            </div>
                            <!-- /.form-group -->                        

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="razon_social">Razon Social *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $razon_social; ?>" name="razon_social" id="razon_social" aria-describedby="razon_social" placeholder="Razon Social">
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion_fiscal">País *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control selectpicker" name="id_pais" id="id_pais">
                                        <?php if(isset($pais)){ echo $pais;} ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_departamento">Departamento (DF) *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control selectpicker" name="id_departamento" id="id_departamento">
                                        <?php if(isset($departamento)){ echo $departamento;} ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_provincia">Provincia (DF) *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control selectpicker" name="id_provincia" id="id_provincia">
                                        <?php if(isset($provincia)){ echo $provincia;} ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_distrito">Distrito (DF) *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control selectpicker" name="id_distrito" id="id_distrito">
                                        <?php if(isset($distrito)){ echo $distrito;} ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <legend class="col-md-6 col-sm-6 col-xs-6">Celular 
                                    <a href="javascript:void(0);" class="btn btn-danger add_cel btn-xs">
                                        <i class="fa fa-plus-square"></i>
                                    </a>
                                </legend>
                                <div class="col-md-12 col-sm-12 col-xs-12" id="cel_container">
                                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">Operadora</label>
<?php 
                            if(!empty($contacto_cel))
                            {
                                $c = 1;
                                //echo "<pre>"; print_r($contacto_cel); echo "</pre>";
                                foreach ($contacto_cel as $key => $value) 
                                {
?>
                                    <div class='form-group form-inline'>
                                        <input class='form-control' type='text' name='contacto[cel][<?php echo $c; ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['celular'];  ?>">
                                        <select class='form-control' name='contacto[op][<?php echo $c; ?>]' style='margin-right: 10px;'>
                                            <?php if(isset($value['cbx'])){echo $value['cbx'];}?>
                                        </select>
                                        <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                                            <i class='fa fa-trash-o'></i>
                                        </a>
                                    </div>
<?php
                                    $c++;
                                }
                            }
?>
                                </div>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre_comercial">Nombre Comercial</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">          
                                    <input name="nombre_comercial"  id="nombre_comercial" value="<?php echo $nombre_comercial; ?>" type="text" class="form-control" aria-describedby="nombre_comercial" placeholder="Nombre Comercial">
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pagina_web">Página Web</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" id="pagina_web" value="<?php echo $pagina_web; ?>" name="pagina_web" aria-describedby="pagina_web" placeholder="Página Web">
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aniversario">Aniversario</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <fieldset>
                                        <div class="form-group">
                                            <div class="controls">
                                                <div style="width: 100%;" class="xdisplay_inputx has-feedback">
                                                    <input type="text" value="<?php echo $aniversario; ?>" name="aniversario" class="form-control has-feedback-left" id="aniversario" placeholder="&nbsp;Aniversario" aria-describedby="inputSuccess2Status2">
                                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                    <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion_fiscal">Dirección Fiscal *</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" value="<?php echo $direccion_fiscal; ?>" name="direccion_fiscal" aria-describedby="direccion_fiscal" placeholder="Dirección">
                                </div>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <legend class="col-md-6 col-sm-6 col-xs-6">Teléfono
                                    <a href="javascript:void(0);" class="btn btn-danger add_telf btn-xs">
                                        <i class="fa fa-plus-square"></i>
                                    </a>
                                </legend>
                                <div class="col-md-12 col-sm-12 col-xs-12" id="telf_container">
                                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                                    <label class="col-md-4 col-sm-4 col-xs-6 text-center">Anexo</label>
<?php 
                                    if(!empty($contacto_telef))
                                    {
                                        $c = 1;
                                        foreach ($contacto_telef as $value) 
                                        {
?>
                                            <div class='form-group form-inline'>
                                                <input class='form-control' type='text' name='contacto[telf][<?php echo($c); ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['telefono']; ?>">
                                                <input class='form-control' type='text' name='contacto[anex][<?php echo($c); ?>]' style='margin-right: 10px;' value="<?php echo $value['anexo']; ?>">
                                                <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                                                    <i class='fa fa-trash-o'></i>
                                                </a>
                                            </div>
<?php
                                            $c++;
                                        }
                                    }
?>
                                </div>
                            </div>
                            <!-- /.form-group -->

                        </div>

                    </div>
                    <!-- /.row -->

                    <div class="clearfix"><p></p></div>
                    <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-success btn-submit btn-sm">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                        </div>
                        <div class="btn-group pull-right">
                            <a class="btn btn-warning btn_cancelrrhhatiendecat btn-sm" href="<?php echo base_url().$url_modulo;?>">
                                <span class="fa fa-ban"></span>
                                <span class="hide-on-phones">Cancelar</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.btn-toolbar -->
                </form>
<?php
                break;
                case 'rta_index_2':
                if(isset($data[0]) && is_array($data))
                {     
                    $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
                
                    foreach ($data as $key => $value) 
                    {
                        $ruc = (!empty($value['ruc'])) ? $value['ruc'] : "";
?>
                        <tr idpersonajuridica="<?php echo $value['id_persona_juridica']; ?>">
                            <td><?php echo $key+$orden;?></td>
                            <td><?php echo $value['nombre_comercial'];?></td>
                            <td><?php echo $value['razon_social'];?></td>
                            <td><?php echo $ruc;?></td>
                            <td>
                                <a href="javascript:void(0);" class="btn btn-success add_pj btn-xs">
                                    <i class="fa fa-pencil">ADD</i>
                                </a>
                            </td>
                        </tr>
<?php
                    }      
                }
                else
                { 
?> 
                    <tr>
                        <td colspan="5">
                            <h2 class="text-center text-success">No hay registros</h2>
                        </td>
                    </tr> 
<?php           }
                break;
            default:
                # code...
                break;
        }
    }                
?>