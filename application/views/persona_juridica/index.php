<?php
  $menu = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icono = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null; 
?>
<!-- page content -->
    <style type="text/css">
        th {
            white-space: nowrap;
            text-align: center !important; 
        }
    </style>
    <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3>
                                <i class="fa <?php echo $icono; ?>"></i> 
                                <?php echo $menu;?> 
                                <small>Mantenimiento</small>
                            </h3>
                            <div class="btn-group pull-right">
                                <a class="btn btn-primary add_pn btn-sm" data-toggle="modal" data-target="#editpersona" href="javascript:void(0);">
                                    <i class="fa fa-file"></i> Nuevo
                                </a>
                            </div> 
                            <div class="clearfix"></div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="x_content">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <p class="text-muted font-13 m-b-30"></p>
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nombre<br>Comercial</th>
                                                            <th>Razón Social</th>
                                                            <th>RUC</th>
                                                            <th>Fecha N./ Aniversario</th>
                                                            <th>Clien. <input type="checkbox"  id="solo_clientes"></th>
                                                            <th>Prov. <input type="checkbox"  id="solo_proveedor"></th>
                                                            <th>Estado</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        <tr id="filtro">
                                                            <td></td>
                                                            <td class="padd5">
                                                                <input id="nombrecomercial_busc" type="text" class="form-control" placeholder="Nombre Comercial">
                                                            </td>
                                                            <td class="padd5">
                                                                <input id="razonsocial_busc" type="text" class="form-control" placeholder="Razón Social">
                                                            </td>
                                                            <td class="padd5">
                                                                <input id="ruc_busc" type="text" class="form-control padd5" aria-describedby="dni_busc" placeholder="RUC">
                                                            </td>
                                                            <td>
                                                                <fieldset>
                                                                    <div class="control-group">
                                                                        <div class="controls">
                                                                            <div class="input-prepend input-group" id="fechanac">
                                                                                <span class="add-on input-group-addon">
                                                                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                                                                </span>
                                                                                <input name="fecha_aniversario" id="fecha_aniversario" type="text" value="" class="form-control " placeholder="Fecha Creación" aria-describedby="fecha_nacimiento">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset> 
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <select id="estado_busc" class="form-control">
                                                                    <option value="" selected="selected">Estado</option>
                                                                    <option value="1">Activo</option>
                                                                    <option value="0">Inactivo</option>
                                                                </select></td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                                                                    <i class="fa fa-search"></i>
                                                                </a>
                                                                <a href="javascript:void(0);" class="btn btn-default btn-sm limpiarfiltro">
                                                                    <i class="fa fa-refresh"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </thead>
                                                <tbody id="bodyindex">
                                                    <?php if(isset($rta)) { echo $rta; } ?>                         
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="btn-group pull-right" id="paginacion_data">
                                                <?php if(isset($paginacion)) {echo $paginacion;} ?>          
                                            </div> 
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      <!-- /page content -->
<?php if(isset($modal)) {echo $modal;} ?>