    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa "></i> <h3>Reporte de Compras x Número de Parte</h3>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">

                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12 text-right">Fecha Inicial: </label>
                    <div class='input-group date col-md-6 col-sm-6 col-xs-12' id='datet_fecha_inicial'>
                      <input type='text' class="form-control" />
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <!-- form-group-->
                  <div class="clearfix"></div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12 text-right">Fecha Fin: </label>
                    <div class='input-group date col-md-6 col-sm-6 col-xs-12' id='datet_fecha_fin'>
                      <input type='text' class="form-control" />
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <!-- form-group-->
                  <div class="clearfix"></div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12 text-right">Número de Parte: </label>
                    <div style="padding-left:0px;" class='col-md-6 col-sm-6 col-xs-12' >
                      <select class="form-control selectpicker" data-live-search="true" title="Seleccione un Elemento" data-size="5">
<?php
                      if(!empty($codigos))
                      {
                        foreach ($codigos as $v) 
                        {
                          echo "<option value=\"".v['id_codigo']."\" data-tokens=\"".$v['codigo']." ".$v['descripcion_completa']."\">".$v['codigo']." ".$v['descripcion_completa']."</option>";
                        }
                      }
                      else
                      {
                        echo "<option>No hay datos</option>";
                      }
?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>

                  <div class="clearfix"></div>

                  <div class="col-md-3 col-sm-3 col-xs-12"></div>                  
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12 text-right">Marca: </label>
                    <div class='col-md-6 col-sm-6 col-xs-12' style="padding-left:0px;" >
                      <select class="form-control">
                        <option>No hay datos</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3 col-xs-12"></div>
                </div>

                <div class="col-md-12 col-sm-6 col-xs-12">
                  <div class="btn-group pull-right"><a class="btn btn-success buscar btn-lg"><i class="fa fa-search"></i> Iniciar Búsqueda</a></div> 
                <div class="clearfix"></div>
                </div>

                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30"></p>
                        <div class="table-responsive hidden">
                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th><center>Código</center></th>
                                <th><center>Marca</center></th>
                                <th><center>Descripción</center></th>
                                <th><center>Cantidad <br> Comprada</center></th>
                                <th class="thpreciot"><center>Precio Total Comprado</center></th>
                                <th class="thpreciop"><center>Precio Promedio de Compra</center></th>
                                <th><center>Acciones</center></th>
                              </tr>
                            </thead>
                            <tbody id="bodyindex">
                            </tbody>
                          </table>
                        </div>
                        <div>
                          
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="btn-group pull-right" id="paginacion_data">
                          </div> 
                        </div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
