<?php

  $url_modulo = (!empty($url_modulo)) ? $url_modulo : "codigo";
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'rta_index':      

        if(isset($all_data[0]) && is_array($all_data))
        {   
          foreach ($all_data as $key => $value)
          {
            $estado = $value['estado'] ==1 ? "Activo" : "Inactivo";
            $nMarcas = count(explode(",", $value['id_marca']));
?>
            <tr id_codigo="<?php echo $value['id_codigo']; ?>">
              <td></td>
              <td><center><?php echo $value['codigo'];?></center></td>
              <td><center><?php echo $value['descripcion_completa'];?></center></td>
              <td><center><?php echo $value['descripcion'];?></center></td>
              <td><center><?php echo $value['descripcion_2'];?></center></td>
              <td><center><a class="view_marcas" href="javascript:void(0);" data-toggle="modal" data-target="#marcas_modal" ><?php echo "Tiene ".$nMarcas." marcas registradas."; ?></a></center>

              </td>
              <td><?php echo $estado; ?></td>
              <td>
                <center>
                  <a class="btn btn-warning edit btn-xs" data-toggle="modal" data-target="#edit_codigo"><i class="fa fa-pencil"></i></a>
<?php
                if($value['estado']==1)
                {
?>
                  <a class="btn btn-info btn-xs" target="_blank" href="<?php echo base_url().$url_modulo."/config/".$value['id_codigo']."/almacen/-"; ?>"><i class="fa fa-cogs"></i></a>
                  <a class="btn btn-danger delete btn-xs"><i class="fa fa-trash-o"></i></a>
<?php
                }                  
?>
                </center> 
              </td>
            </tr>
<?php
          }    
        }
        else
        { ?> 
          <tr>
            <td colspan="7">
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php   
        }
      break;
      case 'table':
        if(!empty($data))
        {
          $marcas_=explode(",", $data);
          foreach ($marcas_ as $v){
            $marC[$v] = null;
          }
          foreach ($marC as $key =>$r) {
            $marca = $marcas[$key];
?>
            <tr idmarca='<?php echo $key; ?>'>
              <td class='text-center'><?php echo $marca; ?></td>
              <td class='text-center'>
                <a class='btn btn-danger delete_marca btn-xs'><i class='fa fa-trash-o'></i></a>
              </td>
            </tr>
<?php
          }
        }
      break;
      case 'tabs':
        $menu_all = (isset($menu_all)) ? ($menu_all) : (NULL);
        $acti = (isset($active)) ? ($active) : (NULL);
        $url_modulo = (isset($url_modulo)) ? ($url_modulo) : ("eadequipo");
        $id_codigo = (!empty($id_codigo)) ? $id_codigo : NULL;
?>
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
<?php
        if(!empty($menu_all))
        {
          foreach ($menu_all as $key => $value) 
          {
            $class = ($acti == $key) ? ("active") : ("tab");
            $url = base_url().$url_modulo."/config/".$id_codigo."/".$key."/-";
                
?>
            <li role="presentation" tabs="<?php echo $key;?>" class="<?php echo $class;?>">
              <a href="<?php echo $url;?>"><?php echo $value;?></a>
            </li>
<?php      }
        }
?>
        </ul>
<?php            
      break;
      case 'almacen':
        if(!empty($almc))
        {
          $i = 0;
          foreach ($almc as $k => $v) 
          {
            $i++;
            $checked = false;
            $hidden  = "hidden";
            if(!empty($all_data))
            {
              foreach ($all_data as $vv) 
              {
                if($vv['id_almacen']==$k)
                {
                  $checked = "checked";
                  $hidden  = "";
                  break 1;
                }
              }
            }
?>
            <div class="col-md-6 col-sm-6 col-xs-12 contenedor">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <h3 class="nav-tabs"><input type="checkbox" <?php echo $checked; ?> name="" class="existpa" value="1">  <?php echo $v; ?></h3>
              </div>
              <input type="hidden" value="<?php echo $k; ?>" class="id_almacen">

              <div class="col-md-12 col-sm-12 col-xs-12 <?php echo $hidden; ?>">

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                  <label class="control-label col-md-6 col-sm-6 col-xs-6">Escoja ubicación</label>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <input type="hidden" name="id_ubicacion">
                    <select class="form-control ubialm" name='id_ubicacion'>
<?php echo $ubixalm[$vv['id_almacen']]['cbx']; ?>
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                  <label class="control-label col-md-6 col-sm-6 col-xs-6">Stock Mínimo</label>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <input type="text" class="form-control" name= "stock_minimo" value="<?php echo $stockminxalm[$vv['id_almacen']]; ?>">
                  </div>
                </div>

              </div>
            </div> 
<?php
            if($i==2)
            {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }
          }
        }
      break;
      case 'compras':
?>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <fieldset><legend>Proveedor
            <a href="javascript:void(0);" class="add_proveedor" data-toggle="modal" data-target="#proveedor"><i class="fa fa-plus-square"></i></a></legend></fieldset>
            <div class="table-responsive">
              <table id="tb_proveedor" class="table table-bordered table-hover table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombres Comercial</th>
                    <th>Razón Social</th>                                    
                    <th>RUC / DNI</th>
                    <th>Acciones</th>                                   
                  </tr>
                  </thead>
                <tbody>
<?php

                if(isset($all_data['0']['id_codigo_proveedor']))
                {
                  foreach ($all_data as $key => $value) 
                  {
?>
                    <tr idpersona="<?php echo $value['id_persona']; ?>" tipo_persona="<?php echo $value['tipo_persona']; ?>">
                      <td><?php echo ($key+1); ?></td>
                      <td><?php echo $value['nombre_comercial']; ?></td>
                      <td><?php echo $value['razon_social']; ?></td>
                      <td><?php echo $value['ruc']; ?></td>
                      <td class="text-center"><a class="btn btn-danger delete_prov btn-xs"><i class="fa fa-trash-o"></i></a></td>
                    </tr>
<?php
                  }
                } 
                else
                {
?>
                  <tr><td colspan="4"><h2 class="text-center text-success">No se hay registro</h2></td></tr>
<?php  
                }  
?>          
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
<?php
      break;
      case 'ventas':
        $precio = (!empty($all_data['precio_venta'])) ? $all_data['precio_venta'] : null;
        $id_codigo_venta = (!empty($all_data['id_codigo_venta'])) ? $all_data['id_codigo_venta'] : null;
?>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <legend>Configuración de Venta: </legend>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <form class="form-horizontal" id="form-config-preciov" role="form">
                  <input type="hidden" name="id_codigo_venta" id="id_codigo_venta" value="<?php echo $id_codigo_venta; ?>">
                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-6 col-sm-6 col-xs-6">Tipo de Moneda</label>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                      <select class="form-control" name="id_tipomoneda" id="id_tipomoneda">
                        <?php echo (!empty($cbx_mon)) ? $cbx_mon : null; ?>
                      </select>               
                    </div>
                  </div>

                  <div class="clearfix"></div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12" style="margin-top: 8px;">
                    <label class="control-label col-md-6 col-sm-6 col-xs-6">Precio de Venta</label>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                      <input class="form-control" value="<?php echo $precio; ?>" type="text" name="precio_venta" id="precio_venta"/> 
                    </div>
                  </div>
                 
                  <div class="clearfix"></div>

                  <div class="btn-toolbar">
                    <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                    </div>
                    <div class="btn-group pull-right">
                      <button class="btn btn-warning btn_cancelpn"  data-dismiss="modal">
                        <span class="fa fa-ban"></span>
                        <span class="hide-on-phones">Cancelar</span>
                      </button>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>

<?php
      break;  
    }
  }        
?>