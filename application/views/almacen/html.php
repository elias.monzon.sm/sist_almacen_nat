<?php
    if(isset($tipo) && strlen(trim($tipo))>0)
    {
        switch ($tipo) {
            case 'rta_index':
      if(isset($all_data[0]) && is_array($all_data))
        {     
        $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
        
        foreach ($all_data as $key => $value) {
            $estado = ($value['estado']==1) ? ("Activo") : ("Inactivo");
?>
                      <tr idalmacen="<?php echo $value['id_almacen']; ?>">
                        <td><?php echo $key+$orden;?></td>
                        <td class="alm"><?php echo $value['almacen'];?></td>
                        <td><?php echo $estado;?></td>
                        <td>

                            <a href="javascript:void(0);" data-toggle="modal" data-target="#editalmacen" class="btn btn-warning edit btn-xs"><i class="fa fa-pencil"></i></a>
<?php if($value['estado']==1) {?>                          
                            <a href="javascript:void(0);" class="btn btn-danger delete btn-xs"><i class="fa fa-trash-o"></i></a>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#bandeja_ubi" class="btn btn-info open_ubi btn-xs"><i class="fa fa-plus-square"></i></a>
<?php }?>  
                        </td>
                      </tr>
<?php
        }      
    }
    else
    { ?> <tr><td colspan="5"><h2 class="text-center text-success">No hay registro</h2></td></tr> <?php }
            break;

            case 'ubicacion':
                if(!empty($data))
                {
                    foreach ($data as $key => $value) 
                    {
                        $estado = ($value['estado']==1) ? "Activo" : "Inactivo";
?>
                        <tr idalmacenubi="<?php echo $value['id_almacen_ubicacion']; ?>">
                            <td><?php echo $value['ubicacion'];?></td>
                            <td><?php echo $estado;?></td>
                            <td>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#editubi" class="btn btn-warning editubi btn-xs"><i class="fa fa-pencil"></i></a>
<?php                       if($value['estado']==1) {?>
                                <a href="javascript:void(0);" class="btn btn-danger deleteubi btn-xs"><i class="fa fa-trash-o"></i></a>
<?php } ?>    
                            </td>
                        </tr>
<?php
                    }
                }
                else
                {
?>
                    <tr>
                        <td colspan="3">
                            <h2 class="text-center text-success">No hay registro</h2>
                        </td>
                    </tr>
<?php
                }
                break;

        }
    }                
?>