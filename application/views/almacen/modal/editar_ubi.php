<!-- Modal -->
<div class="modal fade" id="editubi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="h4crearubi">Agregar Ubicación</h4>
            </div>
            <div class="modal-body">
                <div class="crear-evento">
                  <form class="form-horizontal" id="form_save_almacenubi">
                   <input type="hidden" value="" name="id_almacen_ubicacion" id="id_almacen_ubicacion">
                   <input type="hidden" value="" name="id_almacen" id="id_almacen2">
                    <div class="form-group">
                      <label for="busc_provee" class="control-label col-md-3 col-sm-3 col-xs-12">Estado</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div id="estadoubi" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" id="estado_1" name="estado" value="1" checked> &nbsp; Activo &nbsp;
                            </label>
                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" id="estado_0" name="estado" value="0"> Inactivo
                            </label>
                          </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="almacen" class="control-label col-md-3 col-sm-3 col-xs-12">Ubicación *</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" class="form-control" value="" id="ubicacion" name="ubicacion" aria-describedby="ubicacion" placeholder="Ubicación">
                      </div>
                    </div>

                    <div class="btn-toolbar">
                      <div class="btn-group pull-right">
                          <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="fa fa-save"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                      </div>
                      <div class="btn-group pull-right">
                        <a class="btn btn-warning btn_limpiarubi" href="javascript:void(0);">
                            <span class="fa fa-eraser"></span>
                            <span class="hide-on-phones">Cancelar</span>
                        </a>
                      </div>
                    </div>
                    <!-- /.btn-toolbar -->
                    </form>
                </div>
            </div>
                        
        </div>
    </div>
</div>