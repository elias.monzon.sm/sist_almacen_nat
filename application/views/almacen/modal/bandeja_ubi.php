<!-- Modal -->
<div class="modal fade" id="bandeja_ubi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Almacén <small id="almc"></small></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <a class="btn btn-primary add_ubi pull-right btn-sm" data-toggle="modal" data-target="#editubi" href="javascript:void(0);"><i class="fa fa-file"></i> Nuevo</a>
            <p class="text-muted font-13 m-b-30"></p>
            <div class="clearfix"></div>
            <input type="hidden" class="alm_bandeja">
            <div class="table-responsive" style="margin-top: 8px;">
              <table id="datatable-buttons_ubi" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                <thead>
                  <tr>
                    <th>Ubicación</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                  </tr>
                  <tr id="filtro_ubi">
                    <td>
                      <input id="ubi_busc" type="text" class="form-control" aria-describedby="alm" placeholder="Ubicación">
                    </td>
                    <td></td>
                    <td>
                      <a href="javascript:void(0);" class="btn btn-default buscar_ubi btn-sm">
                        <i class="fa fa-search"></i>
                      </a>
                      <a href="javascript:void(0);" class="btn btn-default limpiarfiltro_ubi btn-sm">
                        <i class="fa fa-refresh"></i>
                      </a>
                    </td>
                  </tr>
                </thead>
                <tbody id="bodyindex_ubi"></tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="btn-group pull-right" id="paginacion_data"></div> 
              </div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>