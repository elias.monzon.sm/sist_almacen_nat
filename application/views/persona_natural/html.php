<?php
    if(isset($tipo) && strlen(trim($tipo))>0)
    {
        switch ($tipo) {
            case 'rta_index':

                if(isset($data[0]) && is_array($data))
                {     
                    $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
                
                    foreach ($data as $key => $value) 
                    {
                        $estado = ($value['estado']==1) ? ("Activo") : ("Inactivo"); 
                        $urlform = base_url().$url_modulo."/config/general/".$value['id_persona'];
                        $es_cliente = ($value['es_cliente']==1) ? "fa-check-square-o" : "fa-square-o";
                        $es_proveedor = ($value['es_proveedor']==1) ? "fa-check-square-o" : "fa-square-o";
                        $es_contacto = ($value['es_contacto']==1) ? "fa-check-square-o" : "fa-square-o";
                        $dni = (!empty($value['dni'])) ? $value['dni'] : str_pad($value['dni_genera'], 8, "0", STR_PAD_LEFT);
                        $tipo_doc = ($value['id_documentoidentidad']==1) ? "DNI":"Carnet de Extrangería";
?>
                        <tr idpersona="<?php echo $value['id_persona']; ?>">
                            <td><?php echo $key+$orden;?></td>
                            <td><?php echo $value['nombres'];?></td>
                            <td><?php echo $value['apellidos'];?></td>
                            <td><?php echo $tipo_doc;?></td>
                            <td><?php echo $dni;?></td>
                            <td><?php echo $value['fecha_nacimiento'];?></td>
                            <td><?php echo strtoupper($value['sexo']);?></td>
                            <td><i class="fa <?php echo $es_cliente;?>"></i></td>
                            <td><i class="fa <?php echo $es_proveedor;?>"></i></td>
                            <td><i class="fa <?php echo $es_contacto;?>"></i></td>
                            <td><?php echo $estado;?></td>
                            <td>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#editpersona" class="btn btn-warning edit btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="<?php echo $urlform;?>" target="_blank" class="btn btn-info btn-xs">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
<?php if($value['estado']==1) {?>  
                                <a href="javascript:void(0);" class="btn btn-danger delete btn-xs">
                                    <i class="fa fa-trash-o"></i>
                                </a>
<?php }?>                            
                            </td>
                        </tr>
<?php
                    }      
                }
                else
                { 
?> 
                    <tr>
                        <td colspan="13">
                            <h2 class="text-center text-success">No hay registros</h2>
                        </td>
                    </tr> 
<?php           }
            break;
            case 'general':
                $url_modulo = $this->uri->segment(1);
                $id_persona = (isset($persona['id_persona']) && ($persona['id_persona'])>0) ? ($persona['id_persona']) : (0);
                $dni = (isset($persona['dni']) && strlen(trim($persona['dni']))>0) ? ($persona['dni']) : ("");
                $dni = (!empty($dni)) ? $dni : str_pad($persona['dni_genera'], 8, "0", STR_PAD_LEFT);
                $nombres = (isset($persona['nombres']) && strlen(trim($persona['nombres']))>0) ? ($persona['nombres']) : ("");
                $apellidos = (isset($persona['apellidos']) && strlen(trim($persona['apellidos']))>0) ? ($persona['apellidos']) : ("");
                $sexo = (isset($persona['sexo']) && strlen(trim($persona['sexo']))>0) ? ($persona['sexo']) : ("m");

                $email = (isset($persona['email']) && strlen(trim($persona['email']))>0) ? ($persona['email']) : ("");

                $fecha_nacimiento = (isset($persona['fecha_nacimiento']) && strlen(trim($persona['fecha_nacimiento']))>0) ? ($persona['fecha_nacimiento']) : ("");
                $direccion = (isset($persona['direccion']) && strlen(trim($persona['direccion']))>0) ? ($persona['direccion']) : ("");
                $edad = (isset($persona['edad']) && strlen(trim($persona['edad']))>0) ? ($persona['edad']) : ("");
                $estado = (isset($persona['estado']) && strlen(trim($persona['estado']))>0) ? ($persona['estado']) : ("1");
                
                $contacto_cel = (!empty($data['contacto_cel'])) ? $data['contacto_cel'] : null;
                $contacto_telef = (!empty($persona['contacto_telef'])) ? $persona['contacto_telef'] : null;
                if($persona['id_documentoidentidad']==1)
                {
                    $chkd_dni = "checked";
                    $chkd_ce = "";
                    $lbl_doc = "DNI";
                }
                else{
                    $chkd_dni = "";
                    $chkd_ce = "checked";   
                    $lbl_doc = "Carnet de Ext.";
                }            

                $act_a = "";
                $che_a = "";
                if($estado == "1")
                {
                    $act_a = "active";
                    $che_a = "checked=''";
                }

                $act_i = "";
                $che_i = "";
                if($estado == "0")
                {
                    $act_i = "active";
                    $che_i = "checked=''";
                } 

                $act_m = "";
                $che_m = "";
                if(strtolower($sexo) == "m")
                {
                    $act_m = "active";
                    $che_m = "checked=''";
                }

                $act_f = "";
                $che_f = "";
                if(strtolower($sexo) == "f")
                {
                    $act_f = "active";
                    $che_f = "checked=''";
                }
                $orden = 0;
                $fecha = new DateTime(date('Y-m-d'));
                $fecha_nacimiento__ = (!empty($persona['fecha_nacimiento__'])) ? new DateTime($persona['fecha_nacimiento__']) : null;
                $diff = (!empty($fecha_nacimiento__))? $fecha_nacimiento__->diff($fecha) : null;
                $diff = (!empty($diff)) ? $diff->y : $diff;
?>
                <form class="form-horizontal" id="form_save_personal">
                    <input type="hidden" value="<?php echo $id_persona;?>" id="id_persona" name="id_persona" />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group" id="div_tipodocumento">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Documento</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <label class="checkbox-inline">
                                            <input type="radio" <?php echo $chkd_ce; ?> name="id_documentoidentidad" value="2"> Carnet de Extranjeria              
                                        </label>
                                        <label class="checkbox-inline">
                                            <input checked="" type="radio" <?php echo $chkd_dni; ?> name="id_documentoidentidad" value="1"> DNI              
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label" for="estado"></label>
                                    <div id="estado" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default <?php echo $act_a;?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="estado" value="1" <?php echo $che_a;?> > &nbsp; Activo &nbsp;
                                        </label>
                                        <label class="btn btn-default <?php echo $act_i;?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="estado" value="0" <?php echo $che_i;?> > Inactivo
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-6-->
                            <div class="clearfix"></div>
                            <div class="form-group col-md-6" id="div_autogenera">

                                <label id="divdocu" class="col-md-5 col-sm-5 col-xs-12 pull-left" for="dnibusc">
                                    <span id="txt_tipo">Autogenera <?php echo $lbl_doc;?></span>
                                </label>    
                                <div class="checkbox-inline">
                                    <?php 
                                        $attr2 = (empty($persona['dni'])) ? "checked" : null;
                                        $attr1 = (!empty($persona['dni'])) ? "checked" : null; 
                                    ?>
                                    <input type="radio" name="genera" <?php echo $attr2; ?> value="1" type="radio"  /> Si
                                </div>
                                        
                                <div class="checkbox-inline">

                                    <input id="nodocu" type="radio" <?php echo $attr1; ?> name="genera" value="0" type="radio" /> No
                                </div>
                                
                            </div>
                            <!-- /.col-md-6 -->   

                        </div>
                        <!-- /.col-md-12 -->

                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="dni"><?php echo $lbl_doc; ?></label>
                                    <input maxlength="8" type="text" class="form-control" id="dni" value="<?php echo $dni; ?>" name="dni" aria-describedby="dni" placeholder=""/>
                                </div>                                 
                            </div>
                            <!-- /.col-md-4 -->
                            
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="nacionalidad">Nacionalidad</label>
                                    <select style="width: 100%" class="select2_single form-control js-event-log js-example-events" tabindex="-1" id="id_pais" name="id_pais">
                                        <?php if(isset($pais)) { echo $pais;} ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="email">Email</label>
                                    <input type="text" class="form-control" value="<?php echo $email;?>" name="email" aria-describedby="email" id="email" placeholder="Email">
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="control-group">
                                  <label class="control-label" for="apellidos">Apellidos *</label>
                                  <input type="text" class="form-control" value="<?php echo $apellidos;?>" name="apellidos" id="apellidos" aria-describedby="apellidos" placeholder="Apellidos">
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="id_departamento">Departamento</label>
                                    <select name="id_departamento" id="id_departamento" class="form-control id_departamento" tipo="">
                                    <?php if(!empty($depa)) { echo $depa; } ?>  
                                    </select>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="nombres">Nombres *</label>                                 
                                    <input name="nombres"  value="<?php echo $nombres;?>" type="text" class="form-control" aria-describedby="nombres" id="nombres" placeholder="Nombres">
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                            
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="id_provincia">Provincia</label>
                                    <select name="id_provincia" id="id_provincia" class="form-control id_provincia" tipo="">
                                    <?php if(isset($prov)) { echo $prov; } ?> 
                                    </select>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->
                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="sexo">SEXO *</label>
                                    <div class="clearfix"></div>
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default <?php echo $act_m;?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" id="sexo_m" name="sexo" value="m" <?php echo $che_m;?> > &nbsp; Maculino &nbsp;
                                        </label>
                                        <label class="btn btn-default <?php echo $act_f;?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" id="sexo_f" name="sexo" value="f" <?php echo $che_f;?> > Femenino
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->
                            
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="id_distrito">Distrito</label>
                                    <select name="id_distrito" id="id_distrito" class="form-control id_distrito" tipo="">
                                        <?php if(isset($dist)) { echo $dist; } ?> 
                                    </select>
                                </div>
                            </div>
                            <!-- /.col-md-4 --> 

                        </div>
                        <!-- /.col-md-12 -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="control-group">
                                    <label class="control-label" for="fecha_nacimiento">Fecha Nacimiento</label>
                                    <fieldset>
                                        <div class="control-group">
                                            <div class="controls">
                                                <div style="width: 100%;" class="xdisplay_inputx has-feedback">
                                                    <input type="text" value="<?php echo $fecha_nacimiento;?>" name="fecha_nacimiento" class="form-control has-feedback-left" id="fecha_nacimiento" placeholder="Fecha Nacimiento">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <!-- /.col-md-4 -->

                            <div class="col-md-4">
                                <div class="control-group pull-left">
                                    <label class="control-label" for="fecha_nacimiento">Edad</label>
                                    <label for="edad" class="control-label bordelabel txtalingleft col-md-12 col-sm-12 col-xs-12"><?php echo $diff." Años";?></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"><p>&nbsp;</p></div>

                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                        <legend class="col-md-6 col-sm-6 col-xs-6">Celular 
                            <a href="javascript:void(0);" class="btn btn-danger add_cel btn-xs">
                                <i class="fa fa-plus-square"></i>
                            </a>
                        </legend>
                            <div class="col-md-12 col-sm-12 col-xs-12" id="cel_container">
                                <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                                <label class="col-md-4 col-sm-4 col-xs-6 text-center">Operadora</label>
<?php 
                    if(!empty($contacto_cel))
                    {
                        $c = 1;
                        //echo "<pre>"; print_r($contacto_cel); echo "</pre>";
                        foreach ($contacto_cel as $key => $value) 
                        {
?>
                        <div class='form-group form-inline'>
                            <input class='form-control' type='text' name='contacto[cel][<?php echo $c; ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['celular'];  ?>">
                            <select class='form-control' name='contacto[op][<?php echo $c; ?>]' style='margin-right: 10px;'>
                                <?php if(isset($value['cbx'])){echo $value['cbx'];}?>
                            </select>
                            <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                                <i class='fa fa-trash-o'></i>
                            </a>
                        </div>
<?php
                        $c++;
                      }
                    }
?>
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group col-md-6 col-sm-6 col-xs-6">
                    <legend class="col-md-6 col-sm-6 col-xs-6">Teléfono
                        <a href="javascript:void(0);" class="btn btn-danger add_telf btn-xs">
                            <i class="fa fa-plus-square"></i>
                        </a>
                    </legend>
                    <div class="col-md-12 col-sm-12 col-xs-12" id="telf_container">
                        <label class="col-md-4 col-sm-4 col-xs-6 text-center">#</label>
                        <label class="col-md-4 col-sm-4 col-xs-6 text-center">Anexo</label>
<?php 
                    if(!empty($contacto_telef))
                    {
                        $c = 1;
                        foreach ($contacto_telef as $value) 
                        {
?>
                            <div class='form-group form-inline'>
                                <input class='form-control' type='text' name='contacto[telf][<?php echo($c); ?>]' style='margin-right: 10px;' maxlength=9 value="<?php echo $value['telefono']; ?>">
                                <input class='form-control' type='text' name='contacto[anex][<?php echo($c); ?>]' style='margin-right: 10px;' value="<?php echo $value['anexo']; ?>">
                                <a href='javascript:void(0);' class='btn btn-danger del_this btn-xs'>
                                    <i class='fa fa-trash-o'></i>
                                </a>
                            </div>
<?php
                        $c++;
                      }
                    }
?>
                    </div>
                </div>
                <!-- /.form-group -->


                    <div class="clearfix"><p></p></div>

                    <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-success btn-submit btn-sm">&nbsp;<i class="glyphicon glyphicon-floppy-disk"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                        </div>
                        <div class="btn-group pull-right">
                            <a class="btn btn-warning btn_cancelrrhhatiendecat btn-sm" href="<?php echo base_url().$url_modulo;?>">
                                <span class="fa fa-ban"></span>
                                <span class="hide-on-phones">Cancelar</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.btn-toolbar -->
                </form>              
                <!-- /.form -->
<?php            
                break;
                case 'tabs':
                    $menu_all = (isset($menu_all)) ? ($menu_all) : (NULL);
                    $acti = (isset($active)) ? ($active) : (NULL);
                    $url_modulo = (isset($url_modulo)) ? ($url_modulo) : ("persona_natural");
                    $id_persona_juridica = (!empty($id_persona_juridica)) ? $id_persona_juridica : NULL;
?>
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
<?php
                    if(!empty($menu_all))
                    {
                        foreach ($menu_all as $key => $value) 
                        {
                            $class = ($acti == $key) ? ("active") : ("tab");
                            $url = base_url().$url_modulo."/config/".$key."/".$id_persona_juridica;
?>
                            <li role="presentation" tabs="<?php echo $key;?>" class="<?php echo $class;?>">
                                <a href="<?php echo $url;?>"><?php echo $value;?></a>
                            </li>
<?php                   }
                    }
?>
                    </ul>
<?php           
                break;
                case 'contacto':
?>  
                <input type="hidden" value="<?php echo $id_persona;?>" id="id_persona">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <legend style="margin-top: 20px;"> Listado de Empresas <a class="btn btn-success add_pj btn-xs" data-toggle="modal" data-target="#buscar_jur">
                                    <i class="fa fa-plus-square"></i>
                                </a></legend>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-2 col-xs-2"></div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="table-responsive">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre Comercial</th>
                                        <th>Razón Social</th>
                                        <th>RUC</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyindex">
<?php 
                                if(!empty($data))
                                {
                                    foreach ($data as $key => $value) 
                                    {
?>
                                        <tr idpj="<?php echo $value['id_persona_juridica']; ?>">
                                            <td><?php echo $key+1;?></td>
                                            <td><?php echo $value['ruc'];?></td>
                                            <td><?php echo $value['nombre_comercial'];?></td>
                                            <td><?php echo $value['razon_social'];?></td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-danger delete btn-xs"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
<?php 
                                    }
                                }
?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2"></div>
                </div>
<?php
                break;
            default:
                # code...
                break;
        }
    }                
?>