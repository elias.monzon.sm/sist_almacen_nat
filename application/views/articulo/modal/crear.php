  <!-- Modal -->
<div class="modal fade" id="crearmarca" style="overflow: auto !important;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Crear nuevo Articulo</h4>
      </div>
      <div class="modal-body">
        <div class="crear-evento">
          <div class="tab-content clearfix">
            <div id="tab_content1" class="tab-pane active" role="tabpanel">
              <form class="form-horizontal" id="form_save_articulo">

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Marca *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <select id="id_marca" name="id_marca" class="form-control">
                        <?php if(isset($data)){ echo $data; }?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Código *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <select id="id_codigo" name="id_codigo" class="form-control">
                      </select>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Artículo *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="articulo" id="articulo" class="form-control"/>
                    </div>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Precio Unitario *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="precio_unitario" id="precio_unitario" class="form-control"/>
                    </div>
                  </div>

                  <div class="btn-toolbar1 col-md-12 col-sm-12 col-xs-12">
                    <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="fa fa-save"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                    </div>
                    <div class="btn-group pull-right">
                      <a class="btn btn-warning btn_limpiar">
                        <span class="fa fa-eraser"></span>
                        <span class="hide-on-phones">Cancelar</span>
                      </a>
                    </div>
                  </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>