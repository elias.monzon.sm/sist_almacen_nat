<?php
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'index':      

        if(isset($data[0]) && is_array($data))
        {   
          foreach ($data as $key => $value)
          {
?>
            <tr id_articulo="<?php echo $value['id_articulo']; ?>">
              <td><center><?php echo $value['marca'];?></center></td>
              <td><center><?php echo $value['codigo'];?></center></td>
              <td><center><?php echo $value['articulo'];?></center></td>
              <td><center><?php echo "S/. ".number_format($value['precio_unitario'],2);?></center></td>
              <td>
                <center>
                  <a class="btn btn-warning edit_marca btn-xs" data-toggle="modal" data-target="#new_marca"><i class="fa fa-pencil"></i></a>
                  <a class="btn btn-danger edit_marca btn-xs" data-toggle="modal" data-target="#new_marca"><i class="fa fa-trash-o"></i></a>
                </center> 
              </td>
            </tr>
<?php
          }    
        }
        else
        { ?> 
          <tr>
            <td colspan="4">
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php       }
      break;
        
    }
  }        
?>