  <script src="<?php echo base_url()."tools/js/modulos/articulo/"; ?>articulo.js"></script>
    <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa "></i> Maestro de Artículo<small> En Desarrollo</small></h3>
                  <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                  <div class="btn-group pull-right"><a class="btn btn-primary add_eadacta btn-sm" data-toggle="modal" data-target="#crearmarca" ><i class="fa fa-file"></i> Nuevo</a></div> 
                  <div class="clearfix"></div>
                </div>
              </div>

                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <p class="text-muted font-13 m-b-30"></p>
                        <div class="table-responsive">
                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th style="width:30%"><center>Marca</center></th>
                                <th style="width:20%"><center>Código</center></th>
                                <th style="width:20%"><center>Artículo</center></th>
                                <th style="width:20%"><center>Precio Unitario</center></th>
                                <th style="width:40%"><center>Acciones</center></th>
                              </tr>
                              <tr id='filtro'>
                                <th>
                                  <select id="marca_busc" class="form-control" tabindex="-1">
                                    <?php if(isset($cbx_marca)){ echo $cbx_marca; } ?>
                                  </select>
                                </th>
                                <th>
                                  <select id="cod_busc" class="form-control" tabindex="-1">
                                  </select>
                                </th>
                                <th>
                                  <input type="text" id="arti_busc" class="form-control text-center" tabindex="-1">
                                </th>
                                <th></th>
                                <th>
                                  <center>
                                <a class="btn btn-default buscar btn-sm">
                                    <i class="fa fa-search"></i>
                                </a>
                                <a class="btn btn-default limpiarfiltro btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                            </center>
                                </th>
                              </tr>
                            </thead>
                            <tbody id="bodyindex">
      <?php if(isset($rta)) { echo $rta; } ?>                          
                            </tbody>
                          </table>
                        <div class="table-responsive">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </div>
<?php 
	if(isset($modal)) 
	{
	    echo $modal;
	} 
?>
