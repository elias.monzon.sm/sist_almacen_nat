<!-- Modal -->
<div class="modal fade" id="buscarprovee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="crear-evento">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">                      
                      <div class="table-responsive">
                        <table id="buscar_provee" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Nombres Comercial</th>
                              <th>Razón Social</th>
                              <th>RUC / DNI</th>
                              <th></th>
                            </tr>
                            <tr id="filtroprovee">
                              <td></td>
                              <td>
                                <input type="text" class="form-control nombres_com" aria-describedby="nombres" placeholder="Nombres Comercial">
                              </td>
                              <td>
                                <input type="text" class="form-control razon_soc" aria-describedby="razon_social" placeholder="Razón Social">
                              </td>
                              <td>
                                <input type="text" class="form-control ruc_dni" aria-describedby="ruc" placeholder="RUC / DNI"/>
                              </td>
                              <td>
                                <a href="javascript:void(0);" class="btn btn-default buscarcliente">
                                  <i class="fa fa-search"></i>
                                </a>
                                <a href="javascript:void(0);" class="btn btn-default limpiarfiltro">
                                  <i class="fa fa-refresh" aria-hidden="true"></i>
                                </a>
                              </td>
                            </tr>              
                            </thead>                      
                            <tbody>                              
                              <tr><td colspan="7"><h2 class="text-center text-success">No agregó Proveedor</h2></td></tr>
                            </tbody>
                        </table>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="btn-group pull-right" id="paginacion_datap"></div> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
                        
        </div>
    </div>
</div>