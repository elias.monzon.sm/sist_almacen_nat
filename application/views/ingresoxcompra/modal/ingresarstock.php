  <!-- Modal -->
<div class="modal fade" id="ingresostock" style="overflow: auto !important;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Ingresar Stock  <small id="arti">asdsa</small></h4>
      </div>
      <div class="modal-body">
        <div class="crear-evento">
          <div class="tab-content clearfix">
            <div id="tab_content1" class="tab-pane active" role="tabpanel">
              <form class="form-horizontal" id="form_ingreso_arti">
                  <input type="hidden" name="id_articulo" id="id_articulo">
                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                    <label class="control-label col-md-6 col-sm-3 col-xs-12 text-success">Stock Actual: </label>
                    <label class="control-label col-md-6 col-sm-3 col-xs-12" id="stock_actual">00.00</label>
                  </div>

                  <div class="form-group col-md-6 col-sm-12 col-xs-12">
                    <label class="control-label col-md-6 col-sm-3 col-xs-12 text-danger">Stock x Precio: </label>
                    <label class="control-label col-md-6 col-sm-3 col-xs-12" id="stockprecio_actual">S/. 00.00</label>
                  </div>

                  <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Cantidad *</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                      <input type="text" name="stock" id="stock" class="form-control"/>
                    </div>
                  </div>

                  <div class="btn-toolbar1 col-md-12 col-sm-12 col-xs-12">
                    <div class="btn-group pull-right">
                      <button type="submit" class="btn btn-success btn-submit">&nbsp;<i class="fa fa-save"></i><span class="hide-on-phones"></span>&nbsp;Agregar</button>
                    </div>
                    <div class="btn-group pull-right">
                      <a class="btn btn-warning btn_limpiar">
                        <span class="fa fa-eraser"></span>
                        <span class="hide-on-phones">Cancelar</span>
                      </a>
                    </div>
                  </div>
              </form>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>