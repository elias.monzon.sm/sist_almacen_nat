<!-- Modal -->
<div class="modal modal-wide fade" id="buscarart" Role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Artículos</h4>
      </div>
      <div class="modal-body">
        <div class="crear-evento">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <table id="buscar_arti" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                <thead>
                  <tr>
                    <th>Código</th>
                    <th>Marca</th>
                    <th>Descripcion</th>
                    <th>Descripción Traducida</th>
                    <th>Acciones</th>
                  </tr>
                  <tr id='filtro'>
                    <th>
                      <input type="text" id="codigo_busc" class="form-control">
                    </th>
                    <th>
                      <input type="text" id="marca_busc" class="form-control">
                    </th>
                    <th>
                      <input type="text" id="descripcion_busc" class="form-control">
                    </th>
                    <th>
                      <input type="text" id="descripcion_traducida_busc" class="form-control">
                    </th>
                    <th>
                      <a href="javascript:void(0);" class="btn btn-default buscar btn-sm">
                        <i class="fa fa-search"></i>
                      </a>
                      <a href="javascript:void(0);" class="btn btn-default limpiarfiltro btn-sm">
                        <i class="fa fa-refresh"></i>
                      </a>
                    </th>
                  </tr>
                </thead>                      
                <tbody>
                  <tr><td colspan="6"><h2 class="text-center text-success">No agregó Artículos</h2></td></tr>
                </tbody>
              </table>
              <div class="row">
                <div class="col-md-12">
                  <div class="btn-group pull-right" id="paginacion_data"></div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>