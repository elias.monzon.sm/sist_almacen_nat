<?php
  $nombre_modulo = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icon_modulo = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null; 
  $main = $all_data['main'];
  $data_mon = explode(":", $main['tipomoneda']);
  $tipmon = $data_mon[0];
  $sign = $data_mon[1];

  $det = $all_data['det'];
?>
<!-- page content -->
      <style type="text/css">
        #lista_articulos input[type=text]{
          width: 80%;
          float: right !important;
        }
      </style>
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3> <i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $mod_title;?> </h3>
                  <div class="clearfix"></div>
                </div>

                <input type="hidden" value="<?php echo $url;?>" name="url_modulo" id="url_modulo"/>
                <input type="hidden" value="<?php echo $main['id_persona']; ?>" id="id_persona"/>
                <input type="hidden" value="<?php echo $main['tipo_persona']; ?>" id="tipo_persona"/>
                <input type="hidden" value="<?php echo $sign; ?>" id="simbol"/>
   
                <div class="x_content">
                  <form class="form-horizontal form-label-left" id="form_save_ingresoxcompra">
                      <input type="hidden" id='id_documento' value="<?php echo $main['id_documento']; ?>" name="id_documento">
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Proveedor: </label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" class="cir form-control" disabled value="<?php echo $main['txt_proveedor'] ?>"/>
                        </div>                                 
                      </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="fecha_busc">Fecha de Documento: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $main['fecha_ingreso']; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->                        

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Moneda: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $tipmon; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Documento: </label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="cir form-control" disabled value="<?php echo $main['tipodocumento']; ?>"/>
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="direccion_fiscal">Numero de Documento: </label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>#</b></span>
                            <input disabled type="text" maxlength="8" value="<?php echo $main['codigo']; ?>" name="codigo" class="form-control padd3" id="n_documento">
                          </div>
                        </div>                          
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>N°</b></span>
                            <input disabled type="text" maxlength="15" value="<?php echo $main['serie'] ?>" name="serie" class="form-control padd3" id="serie">
                          </div>
                        </div>                          
                      </div>
                    </div>
                    <!-- /.form-group -->
                    <form class="form-horizontal form-label-left" id="form_save_ingresoxcompra">
                    <div class="clearfix"></div>
                    <div class="form-group" style="border-bottom: 1px solid #e5e5e5;">
                      <label for="celular" class="col-md-6 col-sm-6 col-xs-12"><h3>Lista de Artículos</h3></label>
                      <a class="btn btn-default btn-sm buscarart pull-right" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                      <div class="clearfix"></div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                          <table id="lista_articulos" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Código de Articulo</th>
                                <th>Descripcion</th>
                                <th>Marca</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Precio Total</th>
                                <th>Almacén</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id='produc_content'>
<?php
                            if(!empty($det))
                            {
                              $i = 0;
                              foreach ($det as $val) 
                              {
                                $i++;
?>
                                <tr class="artirow" idcod='<?php echo $val['id_articulo']; ?>' idmarca='<?php echo $val['id_marca']; ?>'>
                                  <td><?php echo $i; ?></td>
                                  <td><?php echo $val['txt_articulo']; ?></td>
                                  <td><?php echo $val['descripcion']; ?></td>
                                  <td><?php echo $val['txt_marca']; ?></td>
                                  <td><input type="text" disabled name="cant[<?php echo $val['id_articulo']; ?>][<?php echo $val['id_marca']; ?>]" value="<?php echo $val['cantidad']; ?>" class='form-control cant'></td>
                                  <td>
                                    <span class="pull-left">
                                      <b><i><?php echo $sign ?></i></b>
                                    </span>
                                    <span class="pull-right">
                                      <b><i><input type="text" name="pu[<?php echo $val['id_articulo']; ?>][<?php echo $val['id_marca']; ?>]" value="<?php echo $val['precio_unitario']; ?>" class='form-control pu'></i></b>
                                    </span>
                                  </td>
                                  <td>
                                    <span class="pull-left">
                                      <b><i><?php echo $sign ?></i></b>
                                    </span>
                                    <span class="pull-right">
                                      <b><i><input type="text" name="pt[<?php echo $val['id_articulo']; ?>][<?php echo $val['id_marca']; ?>]" value="<?php echo $val['precio_total']; ?>" class='form-control pt'></i></b>
                                    </span>
                                  </td>
                                  <td><?php echo $val['txt_almacen']; ?></td>
                                  <td>
                                    
                                  </td>
                                </tr>
<?php
                              }
                            }
?>            
                            </tbody>
                          </table>
                      </div>
                    </div>

                    <div class="clearfix"><p></p></div>
                    <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                            <button type="submit" id="saveb" data-loading-text="Guardar..." class="btn btn-success btn-sm">&nbsp;<i class="fa fa-save" autocomplete="off"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                        </div>
                        <div class="btn-group pull-right">
                            <a class="btn btn-link btn-sm" href="<?php echo base_url().$url;?>">
                                <span class="fa fa-ban"></span>
                                <span class="hide-on-phones">Cancelar</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.btn-toolbar -->

                </form>              
                <!-- /.form -->
          </div>
          <!-- /page content -->               

                </div>
              </div>
            </div>
          </div>
<?php if(isset($modal)) {echo $modal;} ?>             