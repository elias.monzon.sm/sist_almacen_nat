<?php
  if(isset($tipo) && strlen(trim($tipo))>0)
  {
    switch ($tipo) {
      case 'rta_index':      

        if(isset($data[0]) && is_array($data))
        {   
          foreach ($data as $key => $value)
          {
            $fechaingreso = (!empty($value['fecha_ingreso'])) ? date('d-m-Y',strtotime($value['fecha_ingreso'])) : null;
            $mon = explode(":", $value['tipomoneda']);
            $tipomon = $mon[0];
            $signo = $mon[1];
            $st = ($value['first_id'] == $value['id_documento']) ? false : true;

?>
            <tr id_documento="<?php echo $value['id_documento']; ?>">
              <td><center><?php echo $value['codigo'];?></center></td>
              <td><center><?php echo $value['serie'];?></center></td>
              <td><center><?php echo $fechaingreso;?></center></td>
              <td><center><?php echo $value['txt_proveedor'];?></center></td>
              <td><center><?php echo $tipomon;?></center></td>
              <td>
                <span class="pull-left">
                  <b><i><?php echo $signo ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $value['tipo_cambio']; ?></i></b>
                </span>
              </td>
              <td>
                <span class="pull-left">
                  <b><i><?php echo $signo ?></i></b>
                </span>
                <span class="pull-right">
                  <b><i><?php echo $value['precio_total']; ?></i></b>
                </span>
              </td>
              <td>
                <center>
                  <a class="btn btn-success btn-xs" target="_blank" href="<?php echo base_url()."ingresoxcompra/ver_documento/".$value['id_documento']; ?>"><i class="fa fa-eye"></i> Detalle</a>
                </center> 
              </td>
            </tr>
<?php
          }    
        }
        else
        { ?> 
          <tr>
            <td colspan="8">
              <h2 class="text-center text-success">No hay registro</h2>
            </td>
          </tr>
<?php       }
      break;
      case 'buscar_prove':

        if(isset($all_data) && is_array($all_data))
        {
          $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
          $url_ = (isset($url_modulo) && strlen(trim($url_modulo))>0) ? ($url_modulo) : ("clientes");
          //print_r($all_data);
            foreach ($all_data as $key => $value) {
              $tipope = (!empty($value['tipo_persona'])) ? ($value['tipo_persona']) : ("");
              $r_d =   ($value['tipo_persona'] == 1) ? ("Dni") : ("RUC");
?>
                      <tr tipope="<?php echo $tipope;?>" rucdni="<?php echo $value['ruc'];?>" doc="<?php echo $r_d;?>" idproveedor="<?php echo $value['id_persona']; ?>">
                        <td><?php echo $key+$orden;?></td>
                        <td><?php echo $value['nombre_comercial'];?></td>
                        <td class="rz"><?php echo $value['razon_social'];?></td>
                        <td><?php echo $r_d.": ".$value['ruc'];?></td>
                        <td>
                            <a class="btn btn-info btn-xs agre_provee" href='javascript:void(0);'>
                            <i aria-hidden='true' class='fa fa-product-hunt'></i> Add</a>
                        </td>
                      </tr>
<?php
            }      
        }
        else
        { 
?> 
          <tr><td colspan="5"><h2 class="text-center text-success">No hay Proveedores</h2></td></tr> 
<?php    }
            break;
            case 'artixproveedor':
              if(!empty($data))
              {
                foreach ($data as $key => $value) {
?>
                  <tr id_codigo="<?php echo $value['id_codigo']; ?>" id_marca="<?php echo $value['id_marca'] ?>">
                    <td class="tdcod"><?php echo $value['codigo'] ?> </td>
                    <td class="tdmarca"><?php echo $value['marca'] ?></td>
                    <td class="desc1"><?php echo $value['descripcion'] ?></td>
                    <td class="desc2"><?php echo $value['descripcion_2'] ?></td>
                    <td>
                      <a class="btn btn-info btn-xs get_arti" href='javascript:void(0);'>
                            <i aria-hidden='true' class='fa fa-product-hunt'></i> Add</a>
                    </td>
                  </tr>
<?php
                }
              }
              else
              {
?>
                <tr><td colspan="7"><h2 class="text-center text-success">No hay Artículos</h2></td></tr> 
<?php
              }
            break;
        
    }
  }        
?>