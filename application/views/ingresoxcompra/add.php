<?php
  $nombre_modulo = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icon_modulo = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null; 
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3> <i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $mod_title;?> </h3>
                  <div class="clearfix"></div>
                </div>

                <input type="hidden" value="<?php echo $url;?>" name="url_modulo" id="url_modulo"/>
                
                <div class="x_content">
                  <form class="form-horizontal form-label-left" id="form_save_ingresoxcompra">
                    <input type="hidden" value="<?php echo $factor_impuesto; ?>" name="factor_impuesto">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="col-md-4 col-sm-4 col-xs-12"></div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <a operacion="" data-toggle="modal" data-target="#buscarprovee" class="btn btn-primary btn-sm add_proveedor txtalingcenter" href="javascript:void(0);">
                              <span class="hide-on-phones">Agregar Proveedor</span>
                            </a>
                          </div>
                          <!-- /.col-md-4 -->                      
                        </div>
                      <!-- /.col-md-12 -->
                      </div>
                      <div><p></p></div>
                      <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Proveedor</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" class="form-control" readonly="" value="" name="ruc_dni" aria-describedby="" placeholder="Proveedor" id="ruc_dni"/>
                          <input type="hidden" value="" name="id_persona" id="id_persona"/>
                          <input type="hidden" value="" name="tipo_persona" id="tipo_persona"/>                        
                        </div>                                 
                      </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="fecha_busc">Fecha de Documento *</label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <fieldset>
                          <div class="control-group">
                            <div class="controls">
                              <div class="input-prepend input-group">
                                <span class="add-on input-group-addon">
                                  <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                </span>
                                <input type="text" value="" name="fecha_ingreso" class="form-control padd3" id="fecha_busc" placeholder="Fecha" aria-describedby="fecha_busc">
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </div>
                    </div>
                    <!-- /.form-group -->                        

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Moneda *</label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                      <select class="form-control" name="id_tipomoneda" id="id_tipomoneda">
                        <?php echo (!empty($cbx_mon)) ? $cbx_mon : null ?>
                      </select>
                      <input type="hidden" name="factor_cambio" id="factor_cambio">
                      <input type="hidden" id="simbol">
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="razon_social">Tipo de Documento *</label>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                      <select class="form-control" name="id_tipodocumento" id="id_tipodocumento">
                        <?php echo (!empty($cbx_tipodoc)) ? $cbx_tipodoc : null ?>
                      </select>
                      </div>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                      <label class="control-label col-md-4 col-sm-4 col-xs-12" for="direccion_fiscal">Numero de Documento</label>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>#</b></span>
                            <input type="text" maxlength="8" value="" name="codigo" class="form-control padd3" id="n_documento">
                          </div>
                        </div>                          
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <div class="controls">
                          <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><b>N°</b></span>
                            <input type="text" maxlength="15" value="" name="serie" class="form-control padd3" id="serie">
                          </div>
                        </div>                          
                      </div>
                    </div>
                    <!-- /.form-group -->
                    <input type="hidden" id="exist_arti">
                    <div class="clearfix"></div>
                    <div class="form-group" style="border-bottom: 1px solid #e5e5e5;">
                      <label class="col-md-6 col-sm-6 col-xs-12"><h3>Lista de Artículos</h3></label>
                      <a class="btn btn-default btn-sm buscarart pull-right" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                      <div class="clearfix"></div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                          <table id="lista_articulos" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
                            <thead>
                              <tr>
                                <th>Código de Articulo</th>
                                <th>Descripcion</th>
                                <th>Marca</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>
                                <th>Precio Total</th>
                                <th>Almacén</th>
                                <th>Acciones</th>
                              </tr>
                            </thead>
                            <tbody id='produc_content'>
                              <tr>
                                <td colspan="8"><h2 class="text-center text-success">No hay Elementos</h2></td>
                              </tr>             
                            </tbody>
                          </table>
                      </div>
                    </div>

                    <div class="clearfix"><p></p></div>
                    <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                            <button type="submit" id="saveb" data-loading-text="Guardar..." class="btn btn-success btn-sm">&nbsp;<i class="fa fa-save" autocomplete="off"></i><span class="hide-on-phones"></span>&nbsp;Guardar</button>
                        </div>
                        <div class="btn-group pull-right">
                            <a class="btn btn-link btn-sm" href="<?php echo base_url().$url;?>">
                                <span class="fa fa-ban"></span>
                                <span class="hide-on-phones">Cancelar</span>
                            </a>
                        </div>
                    </div>
                    <!-- /.btn-toolbar -->

                </form>              
                <!-- /.form -->
          </div>
          <!-- /page content -->               

                </div>
              </div>
            </div>
          </div>
<?php if(isset($modal)) {echo $modal;} ?>             