
</section>
</div>
<div class=" col-md-12 col-sm-12 col-xs-12 clearfix"></div>
<footer class="main-footer col-md-12 col-sm-12 col-xs-12 ">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.4
    </div>
    <strong>Developed by Elías Monzón @2019</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php 
  if(!empty($js))
  {
    foreach ($js as $k => $v) 
    {
      if(is_array($v))
      {
        foreach ($v as $v2) 
        {
          echo "<script src='".base_url()."tools/js/".$k."/".$v2."'></script>";        
        }
      }
      else
      {
        echo "<script src='".base_url()."tools/js/".$k.$v."'></script>";
      }
    }
  }
?>



<!-- JQUERY Validate -->
<script src="<?php echo base_url()."tools/"; ?>js/modulos/main.js"></script>
<script src="<?php echo base_url()."tools/"; ?>js/jquery.validate/jquery.validate.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()."tools/"; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url()."tools/"; ?>dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()."tools/"; ?>js/sweetalert/sweetalert2.all.js"></script>
</body>
</html>
