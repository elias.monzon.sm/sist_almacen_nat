<?php
    if(isset($tipo) && strlen(trim($tipo))>0)
    {
        switch ($tipo) {
            case 'rta_index':
      if(isset($all_data[0]) && is_array($all_data))
        {     
        $orden = (isset($orden) && $orden>0) ? ($orden) : (1);
        
        foreach ($all_data as $key => $value) {
            $estado = ($value['estado']==1) ? ("Activo") : ("Inactivo");
?>
                      <tr idusuario="<?php echo $value['id_usuario']; ?>">
                        <td><?php echo $key+$orden;?></td>
                        <td><?php echo $value['usuario'];?></td>
                        <td><?php echo md5($value['pwd']);?></td>
                        <td><?php echo $estado;?></td>
                        <td>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#editusuario" class="btn btn-warning edit btn-xs"><i class="fa fa-pencil"></i></a>
<?php if($value['estado']==1) {?>
                            <a href=<?php echo base_url()."usuarios/config/".$value['id_usuario']; ?> target="_blank" class="btn btn-info btn-xs"><i class="fa fa-cogs"></i></a>                          
                            <a href="javascript:void(0);" class="btn btn-danger delete btn-xs"><i class="fa fa-trash-o"></i></a>
<?php }?>  
                        </td>
                      </tr>
<?php
        }      
    }
    else
    { ?> <tr><td colspan="5"><h2 class="text-center text-success">No hay registro</h2></td></tr> <?php }
            break;

            case 'acceso_menus':
                if(!empty($data['all_data']))
                {
                    $c = 0;
                    foreach ($data['all_data'] as $id_menu => $vals) 
                    {
                        $c++;

?>
                        <div class="col-md-3 cont_perm" id_menu="<?php echo $id_menu; ?>">
                            <legend><?php echo $vals['menu'];?></legend>
                            <div class="hijos">
<?php 
                            foreach ($vals['hijos'] as $id_menu2 => $menuhijo) 
                            {
                                $chck = "";
                                if(!empty($menus_permitidos))
                                {
                                    foreach ($menus_permitidos as $key => $menu) {
                                        if($id_menu2==$key){
                                            $chck = "checked";
                                            unset($menus_permitidos[$key]);
                                        }
                                    }
                                }
?>
                                <div class="form-check">
                                      <input class="form-check-input cambiar" <?php echo $chck; ?> type="checkbox" value="<?php echo $id_menu2; ?>" id="hijo_<?php echo $id_menu2; ?>">
                                      <label class="form-check-label" for="defaultCheck1">
                                        <?php echo $menuhijo; ?>
                                      </label>
                                </div>
<?php                                
                            }
?>                                
                            </div>
                        </div>
<?php
                        if($c==4)
                        {
                            $c = 0;
?>
                            <div class="clearfix"></div>
<?php                            
                        }
?>                       
                        
<?php
                    }
                }
            break;
                # code...
                break;
        }
    }                
?>