<?php
  $nombre_modulo = (!empty($modulo_data['menu'])) ? $modulo_data['menu'] : null;
  $url = (!empty($modulo_data['url'])) ? $modulo_data['url'] : null;
  $icon_modulo = (!empty($modulo_data['icono'])) ? $modulo_data['icono'] : null;
?>
<!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa <?php echo $icon_modulo; ?>"></i> <?php echo $nombre_modulo;?> <small>Mantenimiento</small></h3>
                  <div class="clearfix"></div>
                </div>
                <input type="hidden" id="id_usuario" value="<?php echo $id_usuario; ?>">
                <div class="clearfix"></div>
                <div class="row x_title1">
                  <div class="col-md-6 col-sm-6 col-xs-12"><h2><?php echo $mod_title;?></h2></div>
                </div>
                <div class="row">
                  <div class="x_content">
                    <div class="row">
                      <?php echo (isset($cuadros)) ? $cuadros : null; ?>
                    </div>
                  </div>
                </div>
              </div>

