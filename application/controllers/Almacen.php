<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Almacen extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_almacen');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de Almacenes";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_almacen->buscar_almacenes(array("page"=>$page));
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('almacen/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";
        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("almacen");

            $send['tipo'] = "rta_index"; //print_r($send);

            $data['rta'] = $this->load->view('almacen/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

        $data["modal"] = $this->load->view("almacen/modal/editar", '', true);
        $data["modal"] .= $this->load->view("almacen/modal/bandeja_ubi", '', true);
        $data["modal"] .= $this->load->view("almacen/modal/editar_ubi", '', true);

		$data['js']['modulos'] = array("/gestionalmacen/almacen.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/almacen/index',$data);
        $this->load->view('footer',$data);
	}

	public function edit()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; //print_r($_POST); die();
        $id_almacen = $this->input->post('id_almacen');
        if($id_almacen === "") {}
        else
        {
            $rta = $this->m_almacen->get_one_almacen(array("id_almacen"=>$id_almacen));
            if(isset($rta['id_almacen']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }   

    public function save_almacen()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_almacen'])) 
        {
            $error = $this->m_almacen->save_almacen($_POST);

            $data['data'] = $error;
            $data['success'] = true;
            $data['error_msg'] = $error["error_msg"];
            $data['error_code'] = $error["error_code"];             
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    
    public function buscar_almacenes()
    {
       $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_almacen->buscar_almacenes($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('almacen/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("almacen");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('almacen/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function validar_almacen()
    {
        $success = false;
        
        $id_almacen = $this->input->post('id_almacen');
        $almacen = $this->input->post('almacen');
        if($almacen === "")  {}
        else
        {
            if(strlen(trim($almacen))>0)
            {
                $success = $this->m_almacen->validar_almacen($id_almacen, $almacen);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function buscar_ubicacion()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_almacen'])) 
        {
            $r = $this->m_almacen->buscar_ubicacion($_POST);
            $send['data'] = $r['all_data'];
            $send['tipo'] = 'ubicacion';

            $rta = $this->load->view('almacen/html', $send, true);

            if(!empty($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";  
            }
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    public function save_almacenubi()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_almacen_ubicacion'])) 
        {
            $error = $this->m_almacen->save_almacenubi($_POST);

            $data['data'] = $error;
            $data['success'] = true;
            $data['error_msg'] = $error["error_msg"];
            $data['error_code'] = $error["error_code"];             
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    public function validar_almacenubi()
    {
        $success = false;
        
        $id_almacen = $this->input->post('id_almacen');
        $ubicacion = $this->input->post('ubicacion');
        $id_almacen_ubicacion = $this->input->post('id_almacen_ubicacion');
        if($ubicacion === "")  {}
        else
        {
            if(strlen(trim($ubicacion))>0)
            {
                $success = $this->m_almacen->validar_almacenubi($id_almacen, $ubicacion, $id_almacen_ubicacion);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function editubi()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; //print_r($_POST); die();
        $id_almacen_ubicacion = $this->input->post('id_almacen_ubicacion');
        if($id_almacen_ubicacion === "") {}
        else
        {
            $rta = $this->m_almacen->get_one_almacenubi($id_almacen_ubicacion);
            if(isset($rta['id_almacen_ubicacion']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
}
