<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_servicios');
        $this->load->model('m_menu');
        $this->load->model('m_tipomoneda');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de servicios";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_servicios->buscar_servicios(array("page"=>$page));
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('servicios/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";
        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("servicio");

            $send['tipo'] = "rta_index"; //print_r($send);

            $data['rta'] = $this->load->view('servicios/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

        $modal_data['cbx_tipomoneda'] = $this->m_tipomoneda->cbx_tipomoneda();
        $data['cbx_tipomoneda'] = $modal_data['cbx_tipomoneda'];
        $data["modal"] = $this->load->view("servicios/modal/editar", $modal_data, true);

		$data['js']['modulos'] = array("/gestionventa/servicios.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/servicios/index',$data);
        $this->load->view('footer',$data);
	}

	public function edit()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; //print_r($_POST); die();
        $id_servicio = $this->input->post('id_servicio');
        if($id_servicio === "") {}
        else
        {
            $rta = $this->m_servicios->get_one_servicio(array("id_servicio"=>$id_servicio));
            if(isset($rta['id_servicio']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }   

    public function save_servicio()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_servicio'])) 
        {
            $error = $this->m_servicios->save_servicio($_POST);

            $data['data'] = $error;
            $data['success'] = true;
            $data['error_msg'] = $error["error_msg"];
            $data['error_code'] = $error["error_code"];             
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    
    public function buscar_servicios()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_servicios->buscar_servicios($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('servicios/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("servicio");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('servicios/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function validar_servicio()
    {
        $success = false;
        
        $id_servicio = $this->input->post('id_servicio');
        $servicio = $this->input->post('servicio');
        if($servicio === "")  {}
        else
        {
            if(strlen(trim($servicio))>0)
            {
                $success = $this->m_servicios->validar_servicio($id_servicio, $servicio);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function get_servicio_autocomplete()
    {
        $data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_n'])) ? $_POST['w_n'] : null;
            $data = $this->m_servicios->get_servicio_autocomplete($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
    }
}
