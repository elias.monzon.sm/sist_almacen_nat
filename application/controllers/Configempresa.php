<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configempresa extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_configempresa');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$all_departamentos = $this->m_generico	->departamentos(175);
		$data['miempresa'] = $this->m_configempresa->get_miempresa();

		$id_departamento_selected = null;
		if(!empty($data['miempresa']))
		{
			$id_distrito = $data['miempresa']['id_distrito'];
			$distrito = $this->m_generico->get_one_distrito($id_distrito);
			$id_departamento_selected = (!empty($distrito['idDepartamento'])) ? $distrito['idDepartamento'] : null;
			$all_provincias = $this->m_generico->provincias($id_departamento_selected);
			$all_distritos = (!empty($distrito['idProvincia'])) ? $this->m_generico->distritos($distrito['idProvincia']) : null;
			$data['cbx_provincias'] = (!empty($all_provincias) && !empty($distrito['idProvincia'])) ? cbx_simple($all_provincias,$distrito['idProvincia'],'Seleccione Provincia') : null;
			$data['cbx_distritos'] = (!empty($all_distritos) && !empty($distrito['idDistrito'])) ? cbx_simple($all_distritos,$distrito['idDistrito'],'Seleccione Distrito') : null;
			if(!empty($data['miempresa']['contacto_cel']))
			{
				foreach ($data['miempresa']['contacto_cel'] as $key => $value) 
				{
					$md = $this->m_generico->operadoras();
					$data['miempresa']['contacto_cel'][$key]['cbx'] = cbx_simple($md, $value['id_operador'], "Seleccione Operador");
				}
			}
		}
		$data['cbx_departamentos'] = cbx_simple($all_departamentos,$id_departamento_selected,'Seleccion Departamento');
		
		$data['js']['modulos'] = array("miempresa/configempresa.js");
		$data['js']['moment'] = array("moment.min.js","es.js");
		$data['js']['datetimepicker'] = array("datetimepicker.js");
		
		$data['css']['datetimepicker'] = array("datetimepicker.css");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/configempresa/index',$data);
        $this->load->view('footer',$data);
	}

	public function save_miempresa()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    if(isset($_POST['ruc'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";
	        $rta = $this->m_configempresa->save_miempresa($_POST);
	        if(!empty($rta['empresa']))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);   
	}
}
