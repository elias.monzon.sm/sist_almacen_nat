<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_menu');

	}

	public function index()
	{	
		$data_menu = $this->m_menu->armar_menu_lat();

		$this->load->view('header');
		$this->load->view('menu_lateral',$data_menu);
		$this->load->view('/menu/index');
        $this->load->view('footer');

	}
}
