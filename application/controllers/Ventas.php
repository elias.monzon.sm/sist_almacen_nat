<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_ventas');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
        $this->load->model('m_tipodocumento');
        $this->load->model('m_tipomoneda');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de Documentos";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_ventas->buscar_documentoventa(array("page"=>$page));
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('ventas/html', $send, true); //print_r($data['rta']);
        $data['paginacion'] = "";

        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ventas");
            //$send['data'] = $all_data['all_data'];
            $send['tipo'] = "rta_index"; //print_r($send);
            $send['all_data'] = $all_data['all_data'];
            $data['rta'] = $this->load->view('ventas/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

		$data['js']['modulos'] = array("venta/venta.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/ventas/index',$data);
        $this->load->view('footer',$data);
	}

    public function add()
    {
        $url_mod = $this->uri->segment(1);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        $data['mod_title'] = "Crear documento de Venta";
        $data['factor_impuesto'] = get_impuesto(1);

        $data["modal"] = $this->load->view("ventas/modal/buscar_cliente", '', true);
        $data["modal"] .= $this->load->view("ventas/modal/buscar", '', true);
        $data["modal"] .= $this->load->view("ventas/modal/add", '', true);

        $data['js']['moment'] = array("moment.min.js","es.js");
        $data['js']['daterangepicker'] = array("daterangepicker.js");
        $data['js']['modulos'] = array("/gestionventa/ventas_add.js");

        $data['css']['daterangepicker'] = array("daterangepicker.css");

        $data['cbx_tipodoc'] = $this->m_tipodocumento->cbx_tipodocventa();
        $data['cbx_tipomon'] = $this->m_tipomoneda->cbx_tipomoneda();
        $this->load->view('header',$data);
        $this->load->view('menu_lateral',$data);
        $this->load->view('/ventas/add',$data);
        $this->load->view('footer',$data);
    }
	
    public function save_salidaxventa()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        //print_r($_POST); die();
        if(!empty($_POST))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            //print_r($_POST);
            $rta = $this->m_ventas->save_salidaxventa($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function ver_documento()
    {

        $url_mod = $this->uri->segment(1);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        

        $id_documento = $this->uri->segment(3);
        if(!empty($id_documento))
        {
            $rta = $this->m_ventas->get_docu($id_documento);

            if(!empty($rta))
            {
                $data['mod_title'] = "Detalle de ".$rta['main']['tipodocumento']." : ".$rta['main']['serie']."-".$rta['main']['codigo'];
                $data['main'] = $rta['main'];

                $this->load->view('header',$data);
                $this->load->view('menu_lateral',$data);
                $this->load->view('/ventas/ver',$data);
                $this->load->view('footer',$data);
            }
        }
    }

    public function get_artiventa()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_cl = $this->m_ventas->get_artiventa($_POST);
            $rta['paginacion'] = "";
            $send['tipo'] = "bandeja_rta";
            $rta['rta'] = $this->load->view('ventas/html', $send, true);

            if(isset($all_cl['all_data'][0]) && is_array($all_cl['all_data']))
            {
                $send = $all_cl;
                $send['orden'] = $page*$limit+1;

                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ventas");

                $send['tipo'] = "bandeja_rta";
                //print_r($send); die();
                $rta['rta'] = $this->load->view('ventas/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_cl['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }
        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function get_serventa()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_cl = $this->m_ventas->get_serventa($_POST);
            $rta['paginacion'] = "";
            $send['tipo'] = "rta_ventaserv";
            $rta['rta'] = $this->load->view('ventas/html', $send, true);

            if(isset($all_cl['all_data'][0]) && is_array($all_cl['all_data']))
            {
                $send = $all_cl;
                $send['orden'] = $page*$limit+1;
                $send['tipo'] = "rta_ventaserv";
                $rta['rta'] = $this->load->view('ventas/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_cl['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1;
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }
        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function get_addartiventa()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_codigo']) && !empty($_POST['id_marca']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            //print_r($_POST);
            $rta = $this->m_ventas->get_addartiventa($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function get_addserviventa($value='')
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_servicio']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            //print_r($_POST);
            $rta = $this->m_ventas->get_addserviventa($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function get_clientes()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(isset($_POST['page']))
        {
            $page = $_POST['page'];
            $limit = $this->result_limit;
            $all_cl = $this->m_ventas->get_clientes($_POST);

            $rta['paginacion'] = "";
            $send['tipo'] = "buscar_clie";
            $rta['rta'] = $this->load->view('ventas/html', $send, true);

            if(isset($all_cl['all_data']) && is_array($all_cl['all_data']))
            {
                $send = $all_cl;
                $send['orden'] = $page*$limit+1;
                $send['tipo'] = "buscar_clie";

                $rta['rta'] = $this->load->view('ventas/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_cl['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1;
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";         
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }


    public function buscar_ventas()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['page']))
        {
            $page = $_POST['page'];
            $limit = $this->result_limit;
            $all_data = $this->m_ventas->buscar_documentoventa(array("page"=>$page));

            $send['tipo'] = "rta_index";
            $rta['data'] = $this->load->view('ventas/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";

            if(!empty($all_data['all_data'][0]))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ventas");
                //$send['data'] = $all_data['all_data'];
                $send['tipo'] = "rta_index"; //print_r($send);
                $send['all_data'] = $all_data['all_data'];

                $rta['data'] = $this->load->view('ventas/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }
}
