<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stockxnumeroparte extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_marca');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
        $this->load->model("m_almacen");
        $this->load->model("m_stockxnumeroparte");
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Stock de Número de Partes";

		$data['js']['modulos'] = array("/gestionalmacen/stockxnumerodeparte.js");
        $data['js']['autocomplete'] = array("jquery.autocomplete.min.js");


		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/stockxnumerodeparte/index',$data);
        $this->load->view('footer',$data);
	}

    public function buscar()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_codigo']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $rta = $this->m_stockxnumeroparte->buscar_stockxnumparte($_POST);

            $send['all_data'] = $rta;
            $send['tipo'] = 'rta_index';
             
            $r = $this->load->view('valorxalmacen/html',$send,true);
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
}
