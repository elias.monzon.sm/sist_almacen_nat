<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_menu');
            $this->load->model('m_usuarios');
            
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Lista de usuarios";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_usuarios->buscar_usuarios(array("page"=>$page));
        $send['url_modulo'] = $url_mod;
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('usuarios/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";

        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("usuarios");
            $send['tipo'] = "rta_index";

            $data['rta'] = $this->load->view('usuarios/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }

        $data["modal"] = $this->load->view("usuarios/modal/editar", '', true);

        $data['js']['autocomplete'] = array("jquery.autocomplete.min.js");
        $data['js']['modulos'] = array("controlinterno/usuarios.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/usuarios/index',$data);
        $this->load->view('footer',$data);
	}

	public function save_usuarios()
	{	
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST['id_usuario'])) 
	    {
	        $rta = $this->m_usuarios->save_usuarios($_POST);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
		responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
	}

	public function edit()
	{
		$data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_usuario']))
        {
            $rta = $this->m_usuarios->get_one_usuario(array("id_usuario"=>$_POST['id_usuario']));
            if(isset($rta['id_usuario']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
    
    public function validar_usuario()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        
        $id_usuario = $_POST['id_usuario'];
        $usuario = $_POST['usuario'];

        if(!empty($usuario))
        {
            $success = $this->m_usuarios->validar_usuario($id_usuario,$usuario);
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function buscar_usuarios()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_usuarios->buscar_usuarios($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('usuario/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("marca");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('usuario/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function config()
    {
        $id_user = ($this->uri->segment(3));
        if(!empty($id_user))
        {
            $url_mod = $this->uri->segment(1);
            $data = $this->m_menu->armar_menu_lat();
            $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
            $data['mod_title'] = "Configuración de Permisos";
            
            //Trae todos los menus

            $all_menus = $this->m_menu->get_menus();
            $send['menus_permitidos'] = $this->m_usuarios->menus_permitidos($id_user);
            $send['data'] = $all_menus;
            $send['tipo'] = "acceso_menus";
            
            $data['id_usuario'] = $id_user;
            $data['cuadros'] = $this->load->view('usuarios/html', $send, true);
            $data['js']['modulos'] = array("rrhh/usuarios_config.js");

            $this->load->view('header',$data);
            $this->load->view('menu_lateral',$data);
            $this->load->view('/usuarios/config',$data);
            $this->load->view('footer',$data);
        }
    }

    public function add_permiso()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_usuario']))
        {
            $rta = $this->m_usuarios->add_permiso($_POST);
            if(!empty($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function delete_permiso()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_usuario']))
        {
            $rta = $this->m_usuarios->delete_permiso($_POST);
            if(!empty($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
}
