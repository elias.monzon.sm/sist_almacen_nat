<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class valorxalmacen extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_marca');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
        $this->load->model('m_almacen');
        $this->load->model('m_valorxalmacen');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Stock de Códigos x almacen";

		$data['js']['modulos'] = array("/gestionalmacen/valorxalmacen.js");

        $all_alm = $this->m_almacen->get_all_amacen();
        $data['cbox_alma'] = cbx_simple($all_alm,null,"Seleccione Almacen");
		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/valorxalmacen/index',$data);
        $this->load->view('footer',$data);
	}

    public function stockxalmacen()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_almacen']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $rta = $this->m_valorxalmacen->stockxalmacen($_POST['id_almacen']);

            $send['all_data'] = $rta;
            $send['tipo'] = 'rta_index';
             
            $r = $this->load->view('valorxalmacen/html',$send,true);
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function stockcero()
    {
        $url_mod = $this->uri->segment(1);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        $data['mod_title'] = "Marcas con Stock en Cero";

        $rta = $this->m_valorxalmacen->stockcero();
        
        $send['all_data'] = $rta;
        $send['tipo'] = 'rta_indexcero';

        $data['form'] = $this->load->view('valorxalmacen/html',$send,true);

        $this->load->view('header',$data);
        $this->load->view('menu_lateral',$data);
        $this->load->view('/valorxalmacen/stockcero',$data);
        $this->load->view('footer',$data);
    }
}
