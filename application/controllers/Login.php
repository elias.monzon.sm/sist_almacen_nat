<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_login');
    }

	public function index(){

        $this->load->view('login/iniciar');
	}

    public function iniciar_sesion()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(!empty($_POST['usuario']) && !empty($_POST['password']))
        {
            $user = $_POST['usuario'];
            $pass = $_POST['password'];
            $r = $this->m_login->iniciar_sesion($user,$pass);
            
            if(!empty($r))
            {
                $this->session->set_userdata($r);
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
                $data['data'] = "1";
            }
        }

        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function cerrar_sesion()
    {
        session_destroy();
        redirect(base_url()."login");
    }
}