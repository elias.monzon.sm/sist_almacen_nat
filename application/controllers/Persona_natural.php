<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persona_natural extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_personanatural');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);

		$persona = $this->m_personanatural->buscar_personanat(array('page' => 0));
		$limit = 10;
		$page = 0;
		$send['orden'] = ($page*$limit)+1;
		$send['tipo'] = 'rta_index';
		$send['url_modulo'] = $data['modulo_data']['url'];
		$send['data'] = (!empty($persona['all_data'])) ? $persona['all_data'] : null;

		$data['rta'] = $this->load->view('persona_natural/html', $send, true);

		$data['paginacion'] = '';
		if(!empty($persona))
		{
			$paginar['cantidad_pag'] = $persona['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
		}
		$data["modal"] = $this->load->view("persona_natural/modal/editar", '', true);

		$data['js']['modulos'] = array("/rrhh/persona_natural.js");
		$data['js']['moment'] = array("moment.min.js","es.js");
		$data['js']['datetimepicker'] = array("datetimepicker.js");
		
		$data['css']['datetimepicker'] = array("datetimepicker.css");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/persona_natural/index',$data);
        $this->load->view('footer',$data);
	}

	public function autogenera_docu()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['tipo_doc'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->autogenera_docu($_POST);
	        if(!empty($rta))
	        {
	        	$data['data'] = str_pad($rta, 8, "0", STR_PAD_LEFT);
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);   
	}
	public function save_persona()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    if(!empty($_POST)) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->save_persona($_POST);
	        if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "Guardo!";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
	}

	public function get_one_persona()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['id_persona'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->get_one_persona($_POST);
	        if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
	}

	public function buscar_persona()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['page'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $persona = $this->m_personanatural->buscar_personanat($_POST);

			$limit = 10;
			$page = $_POST['page'];
			$send['orden'] = ($page*$limit)+1;
			$send['tipo'] = 'rta_index';
			$send['url_modulo'] = "persona_natural";
			$send['data'] = (!empty($persona['all_data'])) ? $persona['all_data'] : null;

			$rta['rta'] = $this->load->view('persona_natural/html', $send, true);

			$rta['paginacion'] = '';
			if(!empty($persona))
			{
				$paginar['cantidad_pag'] = $persona['cantidad_pag'];
	            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
	            $paginar['limit'] = $limit;

	            $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
			}
	        if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
	}


	public function inactive_persona()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['id_persona'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->inactive_persona($_POST['id_persona']);

			if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
	}

	public function config()
	{
		$url_mod = $this->uri->segment(1);
		$submenu =  $this->uri->segment(3);
		$id_persona =  $this->uri->segment(4);

		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$persona = $this->m_personanatural->config($submenu, $id_persona);
		$all_tabs = array('general' => "General", 'contacto'=>"Contacto");

		$send['tipo'] = 'tabs';
		$send['menu_all'] = $all_tabs;
		$send['active'] =$submenu;
		$send['id_persona_juridica'] = $id_persona;
		$data['tabs'] = $this->load->view('persona_natural/html', $send, true);

		$fecha = date("Y-m-d");
		$data["mod_title"] = "Editar persona";
		
		$send = array();
		$send['persona'] = $persona;
		$send['tipo'] = $submenu;

		switch ($submenu) 
		{
			case 'general':
				$pais = $this->m_generico->pais();
				$send['pais'] = cbx_simple($pais,null,"Seleccione País");
				if(!empty($persona))
				{
					$md = $this->m_generico->operadoras();
					if(!empty($persona['contacto_cel']))
					{
						foreach ($persona['contacto_cel'] as $key => $value) 
						{
							$persona['contacto_cel'][$key]['cbx'] = cbx_simple($md, $value['id_operador'], "Seleccione Operador");
						}
					}
					$send['pais'] = cbx_simple($pais,$persona['id_pais'],"Seleccione País");
					if(!empty($persona['id_departamento']))
					{
						$depa = $this->m_generico->departamentos($persona['id_pais']);
						$send['depa'] = cbx_simple($depa, $persona['id_departamento'], "Seleccione Departamento");
						if(!empty($persona['id_provincia']))
						{
							$prov = $this->m_generico->provincias($persona['id_departamento']);
							$send['prov'] = cbx_simple($prov,$persona['id_provincia'],"Seleccione Provincia");
							if(!empty($persona['id_distrito']))
							{
								$dist = $this->m_generico->distritos($persona['id_provincia']);
								$send['dist'] = cbx_simple($dist,$persona['id_distrito'],"Seleccione Distrito");
							}
						}

					}
				}
				
				$send['data'] = $persona;
				$data['js']['moment'] = array("moment.min.js","es.js");
				$data['js']['datetimepicker'] = array("datetimepicker.js");
				
				$data['css']['datetimepicker'] = array("datetimepicker.css");
			break;
			case 'contacto':
				$send['data'] = $persona;
				$send['id_persona'] = $id_persona;
				$data['modal'] = $this->load->view('persona_natural/modal/buscar_pj', $send, true);
			break;
		}
		//print_r($send);
		$data['form'] = $this->load->view('persona_natural/html', $send, true);
		$data['js']["modulos"][] = "/rrhh/".$submenu.".js";


		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/persona_natural/config',$data);
		$this->load->view('footer',$data);
	}

	public function validar_dni()
	{
        $success = false;
        
        if(!empty($_POST['dni']))
        {
            if(strlen(trim($_POST['dni']))>0)
            {
                $success = $this->m_personanatural->validar_dni($_POST);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
	}

	public function add_contacto()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['id_persona'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->add_contacto($_POST);

			if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
	}

	public function delete_contacto()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST['id_persona_juridica'])) 
	    {
	        $data['success'] = true;
	    	$data['error_msg'] = "OK";
	    	$data['error_code'] = "0";
	    	$data['data'] = "";

	        $rta = $this->m_personanatural->delete_contacto($_POST);

			if(!empty($rta))
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "1"; 
	        }
	    }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
	}

	public function get_dni()
	{
		$data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_not'])) ? $_POST['w_not'] : null;
            $data = $this->m_personanatural->get_dni($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
	}

	public function get_nombres()
	{
		$data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_not'])) ? $_POST['w_not'] : null;
            $data = $this->m_personanatural->get_nombres($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
	}

	public function get_apellidos()
	{
		$data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_not'])) ? $_POST['w_not'] : null;
            $data = $this->m_personanatural->get_apellidos($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
	}
}
