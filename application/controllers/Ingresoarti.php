<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingresoarti extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_articulo');
            $this->load->model('m_codigo');
            $this->load->model('m_marca');
            $this->load->model('m_ingresoarti');
    }

	public function index()
	{
		$cbx_marca['data'] = $this->m_marca->cbx_marca();
		$all_data = $this->m_articulo->buscar_articulos();

		$enviar['data'] = $all_data;
		$enviar['tipo'] = 'index';

		$data['modal'] = $this->load->view('ingresoarti/modal/ingresarstock', '', true);
		$data['rta'] = $this->load->view('ingresoarti/html', $enviar, true);
		$data['cbx_marca'] = $cbx_marca['data'];
		$this->load->view('header');
		$this->load->view('menu_lateral');
		$this->load->view('/ingresoarti/index',$data);
        $this->load->view('footer');
	}

	 public function get_stockxarti()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST['id_articulo'])) 
	    {
	    	$data['data'] = '';
	        $data['success'] = true;
	        $data['error_msg'] = "No Hay datos";
	        $data['error_code'] = "1";

	        $rta = $this->m_ingresoarti->get_stockxarti($_POST['id_articulo']);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "OK";
		        $data['error_code'] = "2"; 
	        }
	    }
	    responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
	}

	public function add_stock()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST['id_articulo'])) 
	    {

	        $rta = $this->m_ingresoarti->save_add_stock($_POST);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
		responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
	}
}
