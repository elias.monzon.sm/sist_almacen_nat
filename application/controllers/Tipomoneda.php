<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tipomoneda extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_tipomoneda');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de Tipo de Monedas";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_tipomoneda->buscar_tipomonedas(array("page"=>$page));
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('tipomoneda/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";
        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("tipomoneda");

            $send['tipo'] = "rta_index"; //print_r($send);

            $data['rta'] = $this->load->view('tipomoneda/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

        $data["modal"] = $this->load->view("tipomoneda/modal/editar", '', true);

		$data['js']['modulos'] = array("controlinterno/tipomoneda.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/tipomoneda/index',$data);
        $this->load->view('footer',$data);
	}

	public function edit()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; //print_r($_POST); die();
        $id_tipomoneda = $this->input->post('id_tipomoneda');
        if($id_tipomoneda === "") {}
        else
        {
            $rta = $this->m_tipomoneda->get_one_tipomoneda(array("id_tipomoneda"=>$id_tipomoneda));
            if(isset($rta['id_tipomoneda']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }   

    public function save_tipomoneda()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_tipomoneda'])) 
        {
            $error = $this->m_tipomoneda->save_tipomoneda($_POST);

            $data['data'] = $error;
            $data['success'] = true;
            $data['error_msg'] = $error["error_msg"];
            $data['error_code'] = $error["error_code"];             
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    
    public function buscar_tipomonedas()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_tipomoneda->buscar_tipomonedas($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('tipomoneda/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("tipomoneda");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('tipomoneda/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function validar_tipomoneda()
    {
        $success = false;
        
        $id_tipomoneda = $this->input->post('id_tipomoneda');
        $tipomoneda = $this->input->post('tipomoneda');
        if($tipomoneda === "")  {}
        else
        {
            if(strlen(trim($tipomoneda))>0)
            {
                $success = $this->m_tipomoneda->validar_tipomoneda($id_tipomoneda, $tipomoneda);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function get_tipomoneda_autocomplete()
    {
        $data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_n'])) ? $_POST['w_n'] : null;
            $data = $this->m_tipomoneda->get_tipomoneda_autocomplete($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
    }
}
