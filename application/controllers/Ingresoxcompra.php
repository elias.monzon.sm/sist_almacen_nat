<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingresoxcompra extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_ingresoxcompra');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
        $this->load->model('m_tipodocumento');
        $this->load->model('m_tipomoneda');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de Documentos";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_ingresoxcompra->buscar_documento(array("page"=>$page));
        
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('ingresoxcompra/html', $send, true); //print_r($data['rta']);
        $data['paginacion'] = "";

        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ingresoxcompra");
            //$send['data'] = $all_data['all_data'];
            $send['tipo'] = "rta_index"; //print_r($send);
            $send['data'] = $all_data['all_data'];
            $data['rta'] = $this->load->view('ingresoxcompra/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

		$data['js']['modulos'] = array("gestioncompra/ingresoxcompra.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/ingresoxcompra/index',$data);
        $this->load->view('footer',$data);
	}

    public function add()
    {
        $url_mod = $this->uri->segment(1);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        $data['mod_title'] = "Ingreso de Stock x Compra";
        $data['factor_impuesto'] = get_impuesto(1);

        $data["modal"] = $this->load->view("ingresoxcompra/modal/buscar_proveedor", '', true);
        $data["modal"] .= $this->load->view("ingresoxcompra/modal/buscar", '', true);
        $data["modal"] .= $this->load->view("ingresoxcompra/modal/add", '', true);

        $data['js']['moment'] = array("moment.min.js","es.js");
        $data['js']['daterangepicker'] = array("daterangepicker.js");
        $data['js']['modulos'] = array("gestioncompra/ingresoxcompra_add.js");

        $data['css']['daterangepicker'] = array("daterangepicker.css");

        $data['cbx_tipodoc'] = cbx_simple($this->m_tipodocumento->cbx_tipodocstock(),null,"Seleccion Tipo de Documento");
        $data['cbx_mon'] = $this->m_tipomoneda->cbx_tipomoneda();
        $this->load->view('header',$data);
        $this->load->view('menu_lateral',$data);
        $this->load->view('/ingresoxcompra/add',$data);
        $this->load->view('footer',$data);
    }
	
    public function buscar_proveedor()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_cl = $this->m_ingresoxcompra->proveedor_arti($_POST);
            $rta['paginacion'] = "";
            $send['tipo'] = "buscar_prove";
            $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);

            if(isset($all_cl['all_data']) && is_array($all_cl['all_data']))
            {
                $send = $all_cl;
                $send['orden'] = $page*$limit+1;

                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ingresoxcompra");

                $send['tipo'] = "buscar_prove";
                $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_cl['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }
        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function save_ingresoxcompra()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            $rta = $this->m_ingresoxcompra->save_ingresoxcompra($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function save_add_articulo()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        //print_r($_POST);
        if(!empty($_POST['id_codigo']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            $rta = $this->m_ingresoxcompra->save_add_articulo($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
        
    }

    public function ver_documento()
    {

        $url_mod = $this->uri->segment(1);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        $data['mod_title'] = "Detalle de Documento de Compra";

        $id_documento = $this->uri->segment(3);
        if(!empty($id_documento))
        {
            $data['all_data'] = $this->m_ingresoxcompra->get_docu($id_documento);
            if(!empty($data['all_data']['det']))
            {
                $this->load->view('header',$data);
                $this->load->view('menu_lateral',$data);
                $this->load->view('/ingresoxcompra/ver',$data);
                $this->load->view('footer',$data);
            }
        }
    }

    public function editar_documentocompra_body()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 

        if(!empty($_POST['id_documento']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            $rta = $this->m_ingresoxcompra->edit_documentokardex($_POST);

            if(isset($rta['status']))
            {
                $data['data'] = (!empty($rta['status'])) ? 1 : 0;;
                $data['success'] = true;
                $data['error_msg'] = $rta['message'];
                $data['error_code'] = (!empty($rta['status'])) ? 1 : 0;
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function buscar_documentos()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['page']))
        {
            $page = $_POST['page'];
            $limit = $this->result_limit;
            $all_data = $this->m_ingresoxcompra->buscar_documento($_POST);

            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);;
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['data'] = $all_data['all_data'];
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("ingresoxcompra");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    function script1(){
        $this->m_ingresoxcompra->script1();
    }

    function script2(){
        $this->m_ingresoxcompra->script2();
    }
}
