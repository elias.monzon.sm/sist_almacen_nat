<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporteventas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_reporteventas');
        $this->load->model('m_menu');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Reporte de Ventas";

        $data['js']['modulos'] = array("gestionventa/reporteventas.js");
        $data['js']['moment'] = array("moment.min.js","es.js");
        $data['js']['datetimepicker'] = array("datetimepicker.js");
        
        $data['css']['datetimepicker'] = array("datetimepicker.css");
		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/reporteventas/index',$data);
        $this->load->view('footer',$data);
	}

	function get_reporteventas()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(isset($_POST)) 
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";

            $rta = $this->m_reporteventas->buscar_ventas($_POST);
            if(!empty($rta))
            {
                $send['all_data'] = $rta;
                $send['tipo'] = 'rta_index';

                $dta = $this->load->view('reporteventas/html', $send, true);
                $data['data'] = $dta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1"; 
            }
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
}
