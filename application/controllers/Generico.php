<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generico extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_generico');
    }

    public function cbx_provincia()
    {
    	$data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(!empty($_POST['id_departamento'])) 
        {            
            $data['data'] = "";
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0"; 

            $provincias = $this->m_generico->provincias($_POST['id_departamento']);
            $rta = cbx_simple($provincias,null,'Seleccione provincia');

            $data['data'] = $rta;
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";               
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);   
    }

    public function cbx_distrito()
    {
    	$data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(!empty($_POST['id_provincia'])) 
        {            
            $data['data'] = "";
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0"; 

            $distritos = $this->m_generico->distritos($_POST['id_provincia']);
            $rta = cbx_simple($distritos,null,'Seleccione distritos');

            $data['data'] = $rta;
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";               
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);   
    }

    public function cbx_departamentos()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(!empty($_POST['id_pais'])) 
        {            
            $data['data'] = "";
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0"; 

            $pais = $this->m_generico->departamentos($_POST['id_pais']);
            $rta = cbx_simple($pais,null,'Seleccione departamento');

            $data['data'] = $rta;
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";               
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
}
?>
