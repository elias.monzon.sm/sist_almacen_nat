<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_venta');
            $this->load->model('m_articulo');
            $this->load->model('m_marca');
    }

	public function index()
	{
		$data = array();

		$ventas_hechas = $this->m_venta->get_ventas();

		$send['tipo'] = "index";
		$send['data'] = $ventas_hechas;
		$data['rta'] = $this->load->view('/venta/html',$send, true);

		$this->load->view('header');
		$this->load->view('menu_lateral');
		$this->load->view('/venta/index',$data);
        $this->load->view('footer');
	}

	public function add_venta()
	{
		$articulos = $this->m_articulo->buscar_articulos();
		$enviar['data'] = $articulos;
		$enviar['tipo'] = 'lista_articulos';
		
		$data_modal['rta'] = $this->load->view('venta/html', $enviar, true);
		$data_modal['cbx_marca'] = $this->m_marca->cbx_marca();

		$data['modal'] = $this->load->view("/venta/modal/bandeja_articulo",$data_modal, true);
		$data['modal'] .= $this->load->view("/venta/modal/ventaxarti",'', true);

		$this->load->view('header');
		$this->load->view('menu_lateral');
		$this->load->view('/venta/add_venta',$data);
        $this->load->view('footer');
	}

	public function concluir_venta()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST)) 
	    {
	        $rta = $this->m_venta->save_articulo($_POST);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
		responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
	}
}
