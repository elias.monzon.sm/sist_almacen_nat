<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_articulo');
            $this->load->model('m_codigo');
            $this->load->model('m_marca');
    }

	public function index()
	{
		$cbx_marca['data'] = $this->m_marca->cbx_marca();
		$all_data = $this->m_articulo->buscar_articulos();

		$enviar['data'] = $all_data;
		$enviar['tipo'] = 'index';

		$data['modal'] = $this->load->view('articulo/modal/crear', $cbx_marca, true);
		$data['rta'] = $this->load->view('articulo/html', $enviar, true);
		$data['cbx_marca'] = $cbx_marca['data'];
		$this->load->view('header');
		$this->load->view('menu_lateral');
		$this->load->view('/articulo/index',$data);
        $this->load->view('footer');
	}

	public function save_articulo()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST['articulo'])) 
	    {
	        $rta = $this->m_articulo->save_articulo($_POST);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
		header('Content-type: application/json');
		echo json_encode( $data );
	}

	public function buscar_articulos()
	{
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";

	    if(isset($_POST))
	    {
	      	$all_data = $this->m_articulo->buscar_articulos($_POST); //print_r($all_data);

	      	$enviar['data'] = $all_data;
		  	$enviar['tipo'] = 'index';

		  	$rta = $this->load->view('articulo/html', $enviar, true);
	      	if(!empty($rta))
	      	{
	      		$data['data'] = (isset($rta)) ? ($rta) : (FALSE);
		      	$data['success'] = true;
		      	$data['error_msg'] = "OK";
		      	$data['error_code'] = "1";
	      	}
	    }
	    header('Content-type: application/json');
		echo json_encode( $data );
	}

}
