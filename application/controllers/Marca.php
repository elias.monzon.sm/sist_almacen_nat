<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class marca extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_marca');
        $this->load->model('m_menu');
        $this->load->model('m_generico');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de marcas";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_marca->buscar_marcas(array("page"=>$page));
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('marca/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";
        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("marca");

            $send['tipo'] = "rta_index"; //print_r($send);

            $data['rta'] = $this->load->view('marca/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }/**/

        $data["modal"] = $this->load->view("marca/modal/editar", '', true);

		$data['js']['modulos'] = array("/gestionarticulo/marca.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/marca/index',$data);
        $this->load->view('footer',$data);
	}

	public function edit()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; //print_r($_POST); die();
        $id_marca = $this->input->post('id_marca');
        if($id_marca === "") {}
        else
        {
            $rta = $this->m_marca->get_one_marca(array("id_marca"=>$id_marca));
            if(isset($rta['id_marca']) && is_array($rta))
            {                
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }   

    public function save_marca()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_marca'])) 
        {
            $error = $this->m_marca->save_marca($_POST);

            $data['data'] = $error;
            $data['success'] = true;
            $data['error_msg'] = $error["error_msg"];
            $data['error_code'] = $error["error_code"];             
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);  
    }

    
    public function buscar_marcas()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_marca->buscar_marcas($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('marca/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("marca");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('marca/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function validar_marca()
    {
        $success = false;
        
        $id_marca = $this->input->post('id_marca');
        $marca = $this->input->post('marca');
        if($marca === "")  {}
        else
        {
            if(strlen(trim($marca))>0)
            {
                $success = $this->m_marca->validar_marca($id_marca, $marca);
            }                
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function get_marca_autocomplete()
    {
        $data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];
            $w_not = (!empty($_POST['w_n'])) ? $_POST['w_n'] : null;
            $data = $this->m_marca->get_marca_autocomplete($param,$w_not);  
            //print_r($data);                               
        }            
        responseAutocompletar($param, $data);
    }

    public function get_marcaxcodigo()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        if(isset($_POST['id_codigo'])) 
        {
            $rta = $this->m_marca->get_marcaxcodigo($_POST);

            if(!empty($rta))
            {
                if(isset($_POST['cbx']))
                {
                    $alld = [];
                    foreach ($rta as $va) {
                        $alld[$va['id_marca']] = $va['marca'];
                    }

                    $rta = cbx_simple($alld,null,"Seleccione una marca");
                }

                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1"; 
            }
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
   
}
