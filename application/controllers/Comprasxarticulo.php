<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comprasxarticulo extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_menu');
            $this->load->model('m_codigo');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de marcas";

        $data['codigos'] = $this->m_codigo->codigos();

        $data['js']['modulos'] = array("gestioncompra/comprasxarticulo.js");
        $data['js']['moment'] = array("moment.min.js","es.js");
        $data['js']['datetimepicker'] = array("datetimepicker.js");
        $data['js']['selectpicker'] = array("bootstrap-select.min.js");
        
        $data['css']['datetimepicker'] = array("datetimepicker.css");
        $data['css']['selectpicker'] = array("bootstrap-select.min.css");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/comprasxarticulo/index',$data);
        $this->load->view('footer',$data);
	}

	
}
