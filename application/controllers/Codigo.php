<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigo extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('m_codigo');
            $this->load->model('m_marca');
            $this->load->model('m_menu');
            $this->load->model('m_almacen');
            $this->load->model('m_tipomoneda');
    }

	public function index()
	{
		$url_mod = $this->uri->segment(1);
		$data = $this->m_menu->armar_menu_lat();
		$data['modulo_data'] = $this->m_menu->data_mod($url_mod);
		$data['mod_title'] = "Listado de marcas";

		$page = 0;
        $limit = $this->result_limit;
        $all_data = $this->m_codigo->buscar_codigos(array("page"=>$page));
        $send['url_modulo'] = $url_mod;
        $send['tipo'] = "rta_index";
        $data['rta'] = $this->load->view('marca/html', $send, true); //print_r($data['rta']);
        $rta['paginacion'] = "";

        if(!empty($all_data['all_data'][0]))
        {
            $send = $all_data; //print_r("entro");
            $send['marcas'] = $this->m_marca->allmarcas();
            $send['orden'] = ($page*$limit)+1;
            $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("codigo");
            $send['tipo'] = "rta_index";

            $data['rta'] = $this->load->view('codigo/html', $send, true);
   
            $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
            $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
            $paginar['limit'] = $limit;

            $data['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
        }

        $data["modal"] = $this->load->view("codigo/modal/editar", '', true);

        $data['js']['autocomplete'] = array("jquery.autocomplete.min.js");
        $data['js']['modulos'] = array("gestionarticulo/codigo.js");

		$this->load->view('header',$data);
		$this->load->view('menu_lateral',$data);
		$this->load->view('/codigo/index',$data);
        $this->load->view('footer',$data);
	}

	public function save_codigo()
	{	
		$data['success'] = true;
	    $data['error_msg'] = "ERROR";
	    $data['error_code'] = "0";
	    $data['data'] = "";
	    
	    if(isset($_POST['id_codigo'])) 
	    {
	        $rta = $this->m_codigo->save_codigo($_POST);
	        if($rta)
	        {
	        	$data['data'] = $rta;
		        $data['success'] = true;
		        $data['error_msg'] = "SAVED";
		        $data['error_code'] = "1"; 
	        }
	    }
		responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
	}

	public function edit()
	{
		$data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_codigo']))
        {
            $rta = $this->m_codigo->get_one_codigo(array("id_codigo"=>$_POST['id_codigo']));
            if(isset($rta['id_codigo']) && is_array($rta))
            {                
            	$send['marcas'] = $this->m_marca->allmarcas();
            	$send['data'] = $rta['id_marca'];
            	$send['tipo'] = 'table';
            	$rta['table'] = $this->load->view('codigo/html', $send, true);

                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }
    
    public function validar_codigo()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        
        $id_codigo = $_POST['id_codigo'];
        $codigo = $_POST['codigo'];

        if(!empty($codigo))
        {
            $success = $this->m_codigo->validar_codigo($id_codigo,$codigo);
        }
        print_r(prettyPrint(json_encode($success)));
    }

    public function buscar_codigos()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";
        $page = $this->input->post('page');
        if($page === "") {}
        else
        {
            $limit = $this->result_limit;
            $all_data = $this->m_codigo->buscar_codigos($_POST); //print_r($all_data);
            $send['tipo'] = "rta_index";
            $rta['rta'] = $this->load->view('codigo/html', $send, true); //print_r($data['rta']);
            $rta['paginacion'] = "";
            if(isset($all_data['all_data'][0]) && is_array($all_data['all_data']))
            {
                $send = $all_data; //print_r("entro");
                $send['orden'] = ($page*$limit)+1;
                $send['url_modulo'] = (isset($data['mostrar']['url']) && strlen(trim($data['mostrar']['url']))>0) ? ($data['mostrar']['url']) : ("codigo");

                $send['tipo'] = "rta_index"; //print_r($send);

                $rta['rta'] = $this->load->view('codigo/html', $send, true);
       
                $paginar['cantidad_pag'] = $all_data['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }
            $data['data'] = (isset($rta)) ? ($rta) : (FALSE);
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "1";
        }        
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']); 
    }

    public function config()
    {   
        $url_mod = $this->uri->segment(1);
		   
        $tab = $this->uri->segment(4);
        $id_codigo = $this->uri->segment(3);
        $data = $this->m_menu->armar_menu_lat();
        $data['modulo_data'] = $this->m_menu->data_mod($url_mod);
        $id_marca = $this->uri->segment(5);
        $all_tabs = array('almacen' => "Almacen",'compras' => "Compras", "ventas" => "Ventas");

        $send['url_modulo'] = $url_mod;
        $send['id_codigo'] = $id_codigo;
        $send['menu_all'] = $all_tabs;
        $send['active'] = $tab;
        $send['tipo'] = "tabs";
        $data['tabs'] = $this->load->view('codigo/html',$send,true);

        $data['mod_title'] = "Configuración Pestaña: ".$tab;
        
        $id_marca = ($id_marca=="-") ? null : $id_marca;
        $oneCodigo = $this->m_codigo->get_one_codigo(array('id_codigo'=>$id_codigo));
        if(!empty($oneCodigo))
        {
            $all_marcas = $this->m_marca->allmarcas();
            $marcas = array_flip(explode(",",$oneCodigo['id_marca']));
            foreach ($marcas as $key => $value) {
                $marcas_selected[$key] = $all_marcas[$key];
            }
            $data['id_codigo'] = $oneCodigo['id_codigo'];
            $data['select_'] = cbx_simple($marcas_selected,$id_marca,"Seleccione marca");
            $data['js']['modulos'] = array("gestionarticulo/redirec_codigo.js");
            if(!empty($id_marca))
            {   
                switch($tab)
                {
                    case 'almacen':
                        $data['tab_data'] = $this->m_codigo->get_codigo($tab, $id_codigo, $id_marca);
                        $all_alm = $this->m_almacen->get_all_amacen();
                        $ubixalm = $this->m_almacen->all_ubis_cbx($id_codigo,$id_marca);
                        $stockminxalm = $this->m_almacen->stockminxalm($id_codigo,$id_marca);

                        $send['all_data'] = $data['tab_data'];
                        $send['almc'] = $all_alm;
                        $send['stockminxalm'] = $stockminxalm;
                        $send['ubixalm'] = $ubixalm;
                        $data['js']['modulos'] = array("gestionarticulo/alm_codigo.js","gestionarticulo/redirec_codigo.js");
                    break;
                    case 'ventas':
                        $data['tab_data'] = $this->m_codigo->get_codigo($tab, $id_codigo, $id_marca);
                        $send['all_data'] = $data['tab_data'];
                        $data['js']['modulos'] = array("gestionarticulo/redirec_codigo.js","gestionarticulo/vents.js");
                        //print_r($send);
                        $send['cbx_mon'] = $this->m_tipomoneda->cbx_tipomoneda($data['tab_data']['id_tipomoneda']);
                        
                    break;
                    case 'compras':
                        $data['tab_data'] = $this->m_codigo->get_codigo($tab, $id_codigo, $id_marca);
                        $send['all_data'] = $data['tab_data'];

                        $data['modal'] = $this->load->view('codigo/modal/editar_proveedor', null ,true);
                        $data['js']['modulos'] = array("gestionarticulo/redirec_codigo.js","gestionarticulo/comp.js");
                        $data['js']['autocomplete'] = array("jquery.autocomplete.min.js");
                    break;
                }

                $send['tipo'] = $tab;
                $send['id_codigo'] = $id_codigo;
                $send['id_marca'] = $id_marca;
                $data['form'] = $this->load->view('codigo/html',$send,true);
            }
            
            $this->load->view('header',$data);
            $this->load->view('menu_lateral',$data);
            $this->load->view('codigo/config',$data);
            $this->load->view('footer',$data);
        }
    }

    public function check_almacen()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_almacen']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $rta = $this->m_codigo->check_almacen($_POST);
            if(isset($rta['i']))
            {
                $data['data'] = $rta['i'];
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function save_proveedor_articulo()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_marca']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $rta = $this->m_codigo->save_proveedor_articulo($_POST);
            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function get_artixproveedor()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(!empty($_POST['id_persona']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
                
            $page = $this->input->post('page');
            $limit = $this->result_limit;

            $mainr = $this->m_codigo->get_artixproveedor($_POST);
            $send['tipo'] = 'artixproveedor';

            $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);
            $rta['paginacion'] = "";
            if(!empty($mainr))
            {
                $send['data'] = $mainr['data'];
                $rta['rta'] = $this->load->view('ingresoxcompra/html', $send, true);

                $paginar['cantidad_pag'] = $mainr['cantidad_pag'];
                $paginar['actual_pag'] = $page + 1; //print_r($send); //die();
                $paginar['limit'] = $limit;

                $rta['paginacion'] = $this->load->view('paginacion/paginacion', $paginar, true);
            }

            if(isset($rta))
            {
                $data['data'] = $rta;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function delete_prov()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_persona']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $r = $this->m_codigo->delete_prov($_POST);
   
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function save_precioventa()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = ""; 
        if(!empty($_POST['id_codigo']) && !empty($_POST['id_marca']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $r = $this->m_codigo->save_precioventa($_POST);
   
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function savecodubixalmacen()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(!empty($_POST['id_almacen_ubicacion']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $r = $this->m_codigo->savecodubixalmacen($_POST);
   
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function savecodstockmin()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(isset($_POST['stock_minimo']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $r = $this->m_codigo->savecodstockmin($_POST);
   
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function get_almxcod()
    {
        $data['success'] = true;
        $data['error_msg'] = "ERROR";
        $data['error_code'] = "0";
        $data['data'] = "";

        if(!empty($_POST['id_marca']) && !empty($_POST['id_codigo']))
        {
            $data['success'] = true;
            $data['error_msg'] = "OK";
            $data['error_code'] = "0";
            $data['data'] = "";
            $r = cbx_simple($this->m_almacen->get_almxcod($_POST),null,"Seleccione un almacen");
            
            if(isset($r))
            {
                $data['data'] = $r;
                $data['success'] = true;
                $data['error_msg'] = "OK";
                $data['error_code'] = "1";
            }              
        }
        responseCode($data['success'], $data['error_msg'], $data['data'], $data['error_code']);
    }

    public function codigo_autocomplete()
    {
        $data = array();
        if(!empty($_POST['query']))
        {   
            $param = $_POST['query'];

            $data = $this->m_codigo->codigo_autocomplete($param);  
        }            
        responseAutocompletar($param, $data);
    }
}
