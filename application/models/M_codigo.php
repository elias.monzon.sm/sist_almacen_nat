<?php

	class M_codigo extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function buscar_codigos($param = "")
	    {
	        if(!empty($param))
	        {
	            $page = $param['page'];
	            unset($param['page']);      

				$limit = $this->result_limit;
	            $pages = $page*$limit;
				
				if(isset($param['estado']) && $param['estado']>-1)
					$whe['estado'] = $param['estado'];

	            $query = $this->db->select('cod.id_codigo, cod.descripcion_completa, cod.id_marca, cod.codigo, cod.descripcion, cod.descripcion_2, cod.estado')
	                              ->from('tb_codigo cod');
	            if(!empty($whe))
	                $query = $query->where($whe);

	            if(isset($param['descripcion_completa']) && strlen(trim($param['descripcion_completa']))>0)
	                $query = $query->like(array("cod.descripcion_completa"=>$param['descripcion_completa']));

	            if(isset($param['desc']) && strlen(trim($param['desc']))>0)
	                $query = $query->like(array("cod.descripcion"=>$param['desc']));

	            if(isset($param['desc_2']) && strlen(trim($param['desc_2']))>0)
	                $query = $query->like(array("cod.descripcion_2"=>$param['desc_2']));

	            if(isset($param['cod']) && strlen(trim($param['cod']))>0)
	                $query = $query->like(array("cod.codigo"=>$param['cod']));                

				$query = $query->limit($limit, $pages)
	                           ->order_by('cod.codigo', 'ASC');
	            $rta['all_data'] = $query->get()->result_array();

	            $query = $this->db->select('cod.id_codigo')
	                              ->from('tb_codigo cod');
	            if(!empty($whe))
	                $query = $query->where($whe);

	            if(isset($param['descripcion_completa']) && strlen(trim($param['descripcion_completa']))>0)
	                $query = $query->like(array("cod.descripcion_completa"=>$param['descripcion_completa']));
	            
	            if(isset($param['desc']) && strlen(trim($param['desc']))>0)
	                $query = $query->like(array("cod.descripcion"=>$param['desc']));

	            if(isset($param['desc_2']) && strlen(trim($param['desc_2']))>0)
	                $query = $query->like(array("cod.descripcion_2"=>$param['desc_2']));

	            if(isset($param['cod']) && strlen(trim($param['cod']))>0)
	                $query = $query->like(array("cod.codigo"=>$param['cod']));

	            $total_registros = $query->count_all_results(); //print_r($rtatotal);

	            $rta['total_registros'] = $total_registros;
	            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
	        }
	             
	        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
	    }

	 	public function save_codigo($data= null)
	    {
	        $err['error_msg'] = "ERROR";
	        $err['error_code'] = "0";
	        $err['id'] = "";

	        if(!empty($data))
	        {
	            if(is_array($data))
	            {
	                $fecha = date("Y-m-d H:i:s");

	                $id_codigo = $data['id_codigo'];
	                unset($data['id_codigo']);                
	                $err['error_msg'] = "OK";
	                $err['error_code'] = "1";

	                if(isset($data['codigo']))
	                {
	                    $whe['codigo'] = trim($data['codigo']);

	                    $query = $this->db->select('id_codigo');
	                    $form = $query->from('tb_codigo');
	                    
	                    if($id_codigo>0)
	                    {
	                        $whe['id_codigo !='] = $id_codigo;
	                    }
	                    $where = $form->where($whe);
	                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

	                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
	                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
	                }
	                //die();
	                if($err['error_code']=="1")
	                {
	                    if($id_codigo==0)
	                    {
	                        $id_codigo = ($this->db->insert('tb_codigo', $data)) ? ($this->db->insert_id()) : (FALSE);
	                        $err['error_code'] = ($id_codigo) ? ("1") : ("0");
	                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
	                    }
	                    else
	                    {
	                        $this->db->where('id_codigo', $id_codigo);
	                        $err['error_code'] = ($this->db->update('tb_codigo', $data)) ? ($err['error_code']) : ("0");
	                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
	                    }
	                }                    
	                $err['id'] = $id_codigo;               
	            }
	        }        
	        return $err;
	    }

	  	public function get_one_codigo($param='')
	  	{
	  		if(!empty($param))
	  		{
	  			$r = $this->db->select('*')
	  						  ->from('tb_codigo')
	  						  ->where($param)
	  						  ->get()
	  						  ->row_array();

	  			return (!empty($r)) ? $r : null;
	  		}
		}
		  
		public function validar_codigo($id_codigo = "", $codigo ="")
		{
			$success = FALSE;
			if(!empty($codigo))
			{
				$whe['codigo'] = trim($codigo);

				$query = $this->db->select('id_codigo');
				$form = $query->from('tb_codigo');                     
				if($id_codigo>0)
				{
					$whe['id_codigo !='] = $id_codigo;
				}

				$where = $form->where($whe);
				$cant = $where->count_all_results(); //print_r($this->db->last_query());

				$success= ($cant>0) ? (FALSE) : (TRUE);              
			}
			return $success;
		}

		public function get_codigo($tab = "", $id_codigo = "", $id_marca)
		{
			if(!empty($tab) && !empty($id_codigo) && !empty($id_marca))
			{
				$w['id_codigo'] = $id_codigo;
				$w['id_marca'] = $id_marca;
				switch ($tab) {
					case 'almacen':
						$data = $this->db->select('id_almacen, id_almacen')
													->from('tb_codigo_almacen')
													->where($w)
													->get()
													->result_array();
										   
						return $data;
					break;
					
					case 'compras':
						
						$data = $this->db->select('id_codigo_proveedor, id_codigo, id_marca, id_persona, tipo_persona')
													->from('tb_codigo_proveedor')
													->where($w)
													->get()
													->result_array();
						if(!empty($data))
						{
							foreach ($data as $key =>$value) 
							{
								if($value['tipo_persona']==1)
								{
									$r = $this->db->select('id_persona, nombres, apellidos, IF(dni is not null,dni,LPAD(dni_genera,8,"0")) dni, id_documentoidentidad')->from('tb_persona')->where('id_persona',$value['id_persona'])->get()->row_array(); 

									$data[$key]['ruc'] = $r['dni'];
									$data[$key]['razon_social'] = $r['apellidos'];
									$data[$key]['nombre_comercial'] = $r['nombres'];
								}
								else {
									$r = $this->db->select('*')->from('tb_persona_juridica')
															   ->where('id_persona_juridica',$value['id_persona'])
															   ->get()
															   ->row_array(); 

									$data[$key]['ruc'] = $r['ruc'];
									$data[$key]['razon_social'] = $r['razon_social'];
									$data[$key]['nombre_comercial'] = $r['nombre_comercial'];
								}
							}
						}
						
						return $data;
					break;
					case 'ventas':
						$data = $this->db->select('id_codigo_venta, id_codigo, id_marca, id_tipomoneda, precio_venta')
													->from('tb_codigo_venta')
													->where($w)
													->get()
													->row_array();

						return $data;
					break;
				}
			}
		}

		public function check_almacen($data = null)
		{
			if(!empty($data))
			{
				$delete = $data['estado'] == 0 ? true : false;
				
				if($delete==true)
				{
					unset($data['estado']);
					$rta['i'] = ($this->db->where($data)->delete('tb_codigo_almacen')) ? true : false;
				}
				else 
				{
					$rta['i'] = ($this->db->insert('tb_codigo_almacen',$data)) ? $this->db->insert_id() : false; 
				}

				return $rta;
			}
		}

		public function save_proveedor_articulo($param = null)
		{
			if(!empty($param['id_codigo']))
			{
				$insert['id_codigo'] = $param['id_codigo'];
				$insert['id_marca'] = $param['id_marca'];
				$insert['tipo_persona'] = $param['tipo_persona'];
				$insert['id_persona'] = ($param['tipo_persona']==1) ? $param['id_persona'] : $param['id_persona_juridica'];

				$r = ($this->db->insert('tb_codigo_proveedor',$insert)) ? true : false;

				return $r;
			}
		}

		public function get_artixproveedor($param='')
		{
			if(!empty($param))
			{	
	            $page = $param['page'];
	            unset($param['page']);      

				$limit = $this->result_limit;
	            $pages = $page*$limit;

				$w['codp.estado'] = 1;
				$w['codp.tipo_persona'] = $param['tipo_persona'];
				$w['codp.id_persona'] = $param['id_persona'];
				$w['cod.estado'] = 1;

				$wnotline = "";
				$li = "";
				if(!empty($param['w_not']))
				{
					$wnot = "((".$param['w_not']."))";
					$wnotline = "AND (codp.id_codigo,codp.id_marca) NOT IN".$wnot;
				}

				if(!empty($param['marca']))
				{
				    $li .= ' AND ma.marca like "%'.$param['marca'].'%" ';
				}

				if(!empty($param['codigo']))
				{
				    $li .= ' AND cod.codigo like "%'.$param['codigo'].'%" ';
				}

				if(!empty($param['descripcion']))
				{
				   $li .= ' AND cod.descripcion like "%'.$param['descripcion'].'%" ';
				}
				  
				if(!empty($param['descripcion_traducida']))
				{
				    $li .= ' AND cod.descripcion_2 like "%'.$param['descripcion_2'].'%" ';
				}

				$query = "SELECT cod.id_codigo, codp.id_marca , ma.marca, cod.codigo, cod.descripcion, cod.descripcion_2
						  FROM tb_codigo_proveedor codp
						  LEFT JOIN tb_codigo cod on cod.id_codigo=codp.id_codigo
						  LEFT JOIN tb_marca ma on ma.id_marca=codp.id_marca
						  WHERE codp.estado = 1 AND codp.tipo_persona=".$param['tipo_persona']." AND codp.id_persona=".$param['id_persona']." AND cod.estado=1 ".$li.$wnotline; 

				$query_limited = $query." limit ".$pages.",".$limit;
						  

				$rta['data'] = $this->db->query($query_limited)->result_array();
				$total_registros =  count($this->db->query($query)->result_array());

	            $rta['total_registros'] = $total_registros;
	            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
	            
				return (!empty($rta['data'])) ? $rta : null;
			}
		}

		public function delete_prov($param='')
		{
			if(!empty($param['id_codigo']))
			{
				$this->db->where($param);
				return ($this->db->delete('tb_codigo_proveedor')) ? true : false;
			}
		}

		public function save_precioventa($data= null)
		{
			if(!empty($data))
			{
				$id_codigo_venta = $data['id_codigo_venta'];
				unset($data['id_codigo_venta']);
				if(empty($id_codigo_venta))
				{
					$id = ($this->db->insert('tb_codigo_venta',$data)) ? $this->db->insert_id() : null;
				}
				else
				{
					$this->db->where('id_codigo_venta',$id_codigo_venta);

					$id = ($this->db->update('tb_codigo_venta',$data)) ? $id_codigo_venta : null;
				}

				return $id;
			}
		}

		public function savecodubixalmacen($data='')
		{
			$saved = false;
			if(!empty($data))
			{
				$upt['id_almacen_ubicacion'] = $data['id_almacen_ubicacion'];
				unset($data['id_almacen_ubicacion']);

				$this->db->where($data);

				$saved = ($this->db->update('tb_codigo_almacen',$upt)) ? true : false;
			}
			return $saved;
		}

		public function savecodstockmin($data='')
		{
			$saved = false;
			if(!empty($data))
			{
				$upt['stock_minimo'] = $data['stock_minimo'];
				unset($data['stock_minimo']);

				$this->db->where($data);

				$saved = ($this->db->update('tb_codigo_almacen',$upt)) ? true : false;
			}
			return $saved;
		}

		public function codigo_autocomplete($query='')
		{
			if(!empty($query))
			{
				$w['estado'] = 1;
	            $rta = $this->db->select('id_codigo, CONCAT_WS(" : ",codigo, descripcion, descripcion_completa) codigo')
	                            ->from('tb_codigo')
	                            ->where($w)
	                            ->group_start()
	                            	->like('codigo',$query)
	                            	->or_like('descripcion',$query)
	                            	->or_like('descripcion_completa',$query)
	                            ->group_end();

	            $rta = $rta->order_by('codigo','asc')->get()->result_array();

	            $data = array();
	            if(!empty($rta))
	            {   
	                foreach ($rta as $key => $value) 
	                {
	                    $data[$key]['value'] = $value['codigo'];
	                    $data[$key]['data'] = $value['id_codigo'];
	                }
	            }

	            return $data;
			}
		}
		
		public function codigos($w = null)
		{

			$w['estado'] = 1;
			$w['id_marca !='] = NULL;

			$rtx = $this->db->select('id_codigo, codigo, descripcion_completa')
							->from('tb_codigo')
							->where($w)
							->get()
							->result_array();


			return (!empty($rtx)) ? $rtx : null;
		}
	}
?>