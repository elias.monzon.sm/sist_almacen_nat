<?php

class M_almacen extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_almacen($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_almacen = $data['id_almacen'];
                unset($data['id_almacen']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['almacen']))
                {
                    $whe['almacen'] = trim($data['almacen']);

                    $query = $this->db->select('id_almacen');
                    $form = $query->from('tb_almacen');
                    
                    if($id_almacen>0)
                    {
                        $whe['id_almacen !='] = $id_almacen;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_almacen==0)
                    {
                        $id_almacen = ($this->db->insert('tb_almacen', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_almacen) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_almacen', $id_almacen);
                        $err['error_code'] = ($this->db->update('tb_almacen', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_almacen;               
            }
        }        
        return $err;
    }

    public function get_one_almacen($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_almacen, alm.almacen, alm.estado')
                              ->from('tb_almacen alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_almacen']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function buscar_almacenes($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $query = $this->db->select('alm.id_almacen, alm.almacen, alm.estado')
                              ->from('tb_almacen alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("almacen"=>$param['alm']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('alm.almacen', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('alm.id_almacen')
                              ->from('tb_almacen alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("almacen"=>$param['alm']));

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function validar_almacen($id_almacen = "", $almacen)
    {
        $success = FALSE;
        if($almacen === "") {}
        else
        {
            $whe['almacen'] = trim($almacen);

            $query = $this->db->select('id_almacen');
            $form = $query->from('tb_almacen');                     
            if($id_almacen>0)
            {
                $whe['id_almacen !='] = $id_almacen;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_all_amacen($param = "")
    {
        if(!empty($param))
            $whe = $param;

        $whe['estado'] = 1;

        $query = $this->db->select('alm.id_almacen, alm.almacen, alm.estado')
                          ->from('tb_almacen alm')
                          ->where($whe)
                          ->order_by('alm.almacen', 'ASC')->get()->result_array();
        if(!empty($query))
        {
            foreach ($query as $value) {
                $rta[$value['id_almacen']] = $value['almacen'];
            }
        }
        
        return (!empty($rta)) ? $rta : null;
    }

    public function buscar_ubicacion($param='')
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $w['almu.id_almacen'] = $param['id_almacen'];
            if(!empty($param['ubicacion_busc']))
            {
                $like['almu.ubicacion'] = $param['ubicacion_busc'];
            }

            $r = $this->db->select('almu.id_almacen_ubicacion, almu.ubicacion, alm.almacen, almu.estado')
                          ->from('tb_almacen_ubicacion almu')
                          ->join('tb_almacen alm','almu.id_almacen=alm.id_almacen','left')
                          ->where($w);
            if(!empty($like))
            {
                $r =    $r->like($like);
            }
                $r =    $r->limit($limit, $pages)
                          ->order_by('almu.ubicacion', 'ASC');
            $rta['all_data'] = $r->get()->result_array();
            //print_r($this->db->last_query());
            $dr = $this->db->select('almu.id_almacen_ubicacion')
                            ->from('tb_almacen_ubicacion almu')
                            ->where($w);
            if(!empty($like))
            {
                $dr =    $dr->like($like);
            }

            $total_registros = $dr->count_all_results();
            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
            
            return (!empty($rta)) ? $rta : null;
        }
    }

    public function save_almacenubi($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_almacen_ubicacion = $data['id_almacen_ubicacion'];
                unset($data['id_almacen_ubicacion']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['ubicacion']))
                {
                    $whe['ubicacion'] = $data['ubicacion'];
                    $whe['id_almacen'] = $data['id_almacen'];

                    $query = $this->db->select('id_almacen_ubicacion');
                    $form = $query->from('tb_almacen_ubicacion');
                    
                    if($id_almacen_ubicacion>0)
                    {
                        $whe['id_almacen_ubicacion !='] = $id_almacen_ubicacion;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_almacen_ubicacion==0)
                    {
                        $id_almacen_ubicacion = ($this->db->insert('tb_almacen_ubicacion', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_almacen_ubicacion) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_almacen_ubicacion', $id_almacen_ubicacion);
                        $err['error_code'] = ($this->db->update('tb_almacen_ubicacion', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_almacen_ubicacion;               
            }
        }        
        return $err;
    }

    public function validar_almacenubi($id_almacen = "", $ubicacion, $id_almacen_ubicacion)
    {
        $success = FALSE;
        if($ubicacion === "") {}
        else
        {
            $whe['id_almacen'] = trim($id_almacen);
            $whe['ubicacion'] = $ubicacion;

            $query = $this->db->select('id_almacen_ubicacion');
            $form = $query->from('tb_almacen_ubicacion');                     
            if($id_almacen_ubicacion>0)
            {
                $whe['id_almacen_ubicacion !='] = $id_almacen_ubicacion;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_one_almacenubi($id_almacen_ubicacion='')
    {
        if(!empty($id_almacen_ubicacion))
        {
            $rta = $this->db->select('id_almacen_ubicacion, id_almacen, ubicacion, estado')
                            ->from('tb_almacen_ubicacion')
                            ->where('id_almacen_ubicacion',$id_almacen_ubicacion)
                            ->get()
                            ->row_array();

            return (!empty($rta)) ? $rta : null;
        }
    }

    public function all_ubis_cbx($id_codigo='',$id_marca='')
    {
        if(!empty($id_codigo) && !empty($id_marca))
        {
            $almacenes = $this->get_all_amacen();
            $all_data = array();
            foreach ($almacenes as $id_almacen => $alm) 
            {
                $w['id_almacen'] = $id_almacen;
                $w['estado'] = 1;

                $query = $this->db->select('id_almacen_ubicacion, ubicacion')
                                  ->from('tb_almacen_ubicacion')
                                  ->where($w)
                                  ->get()
                                  ->result_array();

                $w3['id_marca'] = $id_marca;
                $w3['id_codigo'] = $id_codigo;
                $w3['id_almacen'] = $id_almacen;
                $w3['estado'] = 1;
                $qu3 = $this->db->select('id_almacen_ubicacion')
                                ->from('tb_codigo_almacen')
                                ->where($w3)
                                ->get()
                                ->row_array();

                $sele = (!empty($qu3['id_almacen_ubicacion'])) ? $qu3['id_almacen_ubicacion'] : null;                

                $d1 = array();
                if(!empty($query))
                {   
                    foreach ($query as $val) 
                    {
                        $d1[$val['id_almacen_ubicacion']] = $val['ubicacion'];
                    }
                }

                $all_data[$id_almacen]['cbx'] = cbx_simple($d1,$sele,'Seleccione ubicación');
            } 
        }
        return (!empty($all_data)) ? $all_data : null;
    }

    public function stockminxalm($id_codigo='',$id_marca='')
    {
        if(!empty($id_codigo) && !empty($id_marca))
        {
            $almacenes = $this->get_all_amacen();
            $all_data = array();
            foreach ($almacenes as $id_almacen => $alm) 
            {

                $w3['id_marca'] = $id_marca;
                $w3['id_codigo'] = $id_codigo;
                $w3['id_almacen'] = $id_almacen;
                $w3['estado'] = 1;
                $qu3 = $this->db->select('stock_minimo')
                                ->from('tb_codigo_almacen')
                                ->where($w3)
                                ->get()
                                ->row_array();

                $all_data[$id_almacen] = (!empty($qu3['stock_minimo'])) ? $qu3['stock_minimo'] : null;
            } 
        }
        return (!empty($all_data)) ? $all_data : null;
    }

    public function get_almxcod($param = null)
    {
        if(!empty($param['id_codigo']) && !empty($param['id_marca']))
        {
            $w['ca.id_codigo'] = $param['id_codigo'];
            $w['ca.id_marca'] = $param['id_marca'];

            $whe = $this->db->select('a.almacen , a.id_almacen')
                            ->from('tb_almacen a')
                            ->join('tb_codigo_almacen ca','a.id_almacen=ca.id_almacen','left')
                            ->where($w)
                            ->get()
                            ->result_array();
            if(!empty($whe))
            {
                foreach ($whe as $value) 
                {
                    $rta[$value['id_almacen']] = $value['almacen'];
                }
            }
        }
        return (!empty($rta)) ? $rta : null;
    }

    public function get_almxstockcod($param = null)
    {
        if(!empty($param['id_codigo']) && !empty($param['id_marca']))
        {
            $w['ca.id_codigo'] = $param['id_codigo'];
            $w['ca.id_marca'] = $param['id_marca'];

            $whe = $this->db->select('a.almacen , a.id_almacen, (select k.stock from tb_kardex k where k.id_almacen=a.id_almacen and k.id_codigo='.$param['id_codigo']." and k.id_marca=".$param['id_marca']." order by k.fecha_ingreso desc limit 1) stock")
                            ->from('tb_almacen a')
                            ->join('tb_codigo_almacen ca','a.id_almacen=ca.id_almacen','left')
                            ->where($w)
                            ->get()
                            ->result_array();
            //print_r($this->db->last_query());
            $tx = "<option>Crear almacenes </option>";
            if(!empty($whe))
            {
                $tx = "<option disabled selected>Seleccione un almacén</option>";
                foreach ($whe as $value) {
                    $tx .="<option value='".$value['id_almacen']."' stock=".$value['stock'].">".$value['almacen']."</option>";
                }
            }
        }
        return (!empty($tx)) ? $tx : null;
    }
}