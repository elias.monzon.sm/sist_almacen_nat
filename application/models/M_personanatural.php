<?php

	class M_personanatural extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function buscar_personanat($param = '')
	  	{
	  		
	  		if(isset($param['page']))
	  		{	
	  			if(!empty($param['nombres']))
	  				$like['p.nombres'] = $param['nombres'];
	  			
	  			if(!empty($param['apellidos']))
	  				$like['p.apellidos'] = $param['apellidos'];
	  			
	  			if(!empty($param['dni']))
	  				$like['p.dni'] = $param['dni'];

	  			if(!empty($param['sexo']))
	  				$like['p.sexo'] = $param['sexo'];

	  			if(!empty($param['fech_nac']))
	  				$like['p.fecha_nacimiento'] = date('Y-m-d',strtotime($param['fech_nac']));

	  			if(!empty($param['solo_clientes']))
	  				$w['p.es_cliente'] = 1;

	  			if(!empty($param['solo_proveedor']))
	  				$w['p.es_proveedor'] = 1;

	  			if(!empty($param['solo_contacto']))
	  				$w['p.es_contacto'] = 1;

	  			if(!empty($param['tipodoc']))
	  				$w['p.id_documentoidentidad'] = $param['tipodoc'];

	  			if(isset($param['estado']))
	  				$w['p.estado'] = $param['estado'];

	  			$page = $param['page'];
	  			$limit = 10;
	  			$pages = $page*$limit;
	  			unset($param['page']);

	  			$rta['all_data'] = $this->db->select('p.id_persona, p.nombres, p.apellidos, p.nombre_corto, p.email, p.dni, p.dni_genera, p.sexo, date_format(p.fecha_nacimiento,"%d-%m-%Y") fecha_nacimiento, p.es_cliente, p.es_proveedor, p.estado, p.id_documentoidentidad, p.es_contacto')
	  										->from('tb_persona p');
	  			if(!empty($w))
	  			{
	  				$rta['all_data'] = $rta['all_data']->where($w);
	  			}
	  			if(!empty($like))
	  			{
	  				$rta['all_data'] = $rta['all_data']->like($like);
	  			}
	  				$rta['all_data'] = $rta['all_data']->limit($limit,$pages)
	  										->order_by('nombres','ASC')
	  										->get()
	  										->result_array();
	  			//print_r($this->db->last_query());
	  			$rta['total_registros'] = $this->db->select('p.id_persona')
	  												->from('tb_persona p');
	  			if(!empty($w))
	  			{
	  				$rta['total_registros'] = $rta['total_registros']->where($w);
	  			}
	  			if(!empty($like))
	  			{
	  				$rta['total_registros'] = $rta['total_registros']->like($like);
	  			}
	  				$rta['total_registros'] = $rta['total_registros']->count_all_results();

	  			$rta['cantidad_pag'] = ceil($rta['total_registros']/$limit);

	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function autogenera_docu($param='')
	  	{
	  		if(!empty($param['tipo_doc']))
	  		{
	  			$w['id_documentoidentidad'] = $param['tipo_doc'];
	  			$rta = $this->db->select('max(dni_genera) dni_genera')
	  							->from('tb_persona')
	  							->where($w)
	  							->order_by('dni_genera','desc')
	  							->get()
	  							->row_array();

	  			return (!empty($rta)) ? intval($rta['dni_genera'])+1 : 1;
	  		}
	  	}

	  	public function save_persona($data='')
	  	{
	  		//print_r($data); die();
	  		$err['error_msg'] = "ERROR";
	        $err['error_code'] = "0";
	        $err['id'] = "";

	        if(!empty($data) && is_array($data)) 
	        {
                $fecha = date("Y-m-d H:i:s");

                $id_persona = $data['id_persona'];
                unset($data['id_persona']);                

                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['id_documentoidentidad']))
                {
                    $whe['id_documentoidentidad'] = ($data['id_documentoidentidad']);
                    
                    if(!empty($data['genera']))
                    	$whe['dni_genera'] = intval($data['dni']);
                    else
                    	$whe['dni'] = $data['dni'];

                    $query = $this->db->select('id_persona');
                    $form = $query->from('tb_persona');
                    
                    if($id_persona>0)
                    {
                        $whe['id_persona !='] = $id_persona;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                
                if($err['error_code']=="1")
                {
                    $insert['estado'] = (isset($data['estado'])) ? $data['estado'] : 0;
                	$insert['id_documentoidentidad'] = (!empty($data['id_documentoidentidad'])) ? $data['id_documentoidentidad'] : null;
                	$insert['dni_genera'] = (isset($data['dni_genera'])) ? int($data['dni_genera']) : null;	
                	$insert['dni'] = (!empty($data['dni'])) ? $data['dni'] : null;

                	$insert['nombres'] = (!empty($data['nombres'])) ? $data['nombres'] : null;
                	$insert['apellidos'] = (!empty($data['apellidos'])) ? $data['apellidos'] : null;
                	$insert['fecha_nacimiento'] = (!empty($data['fecha_nacimiento'])) ? date('Y-m-d',strtotime($data['fecha_nacimiento'])) : null;
                	$insert['sexo'] = (!empty($data['sexo'])) ? $data['sexo'] : null;
                	$insert['es_cliente'] = (!empty($data['es_cliente'])) ? intval($data['es_cliente']) : null;
                	$insert['es_proveedor'] = (!empty($data['es_proveedor'])) ? intval($data['es_proveedor']) : null;
                	$insert['es_contacto'] = (!empty($data['es_contacto'])) ? intval($data['es_contacto']) : null;
                    if(empty($id_persona))
                    {
                    	$id_persona = ($this->db->insert('tb_persona', $insert)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_persona) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                    	$insert['dni_genera'] = ($data['genera']==1) ? intval($data['dni']) : null;
                    	$insert['dni'] = (empty($data['genera'])) ? $data['dni'] : null;
                        $insert['email'] = (!empty($data['email'])) ? $data['email'] : null;
                        $insert['id_pais'] = (!empty($data['id_pais'])) ? $data['id_pais'] : null;
                        $insert['id_departamento'] = (!empty($data['id_departamento'])) ? $data['id_departamento'] : null;
                        $insert['id_provincia'] = (!empty($data['id_provincia'])) ? $data['id_provincia'] : null;
                        $insert['id_distrito'] = (!empty($data['id_distrito'])) ? $data['id_distrito'] : null;
                        $contacto_ = (isset($data['contacto'])) ? $data['contacto'] : null;
                        $this->db->where('id_persona', $id_persona);
                        $err['error_code'] = ($this->db->update('tb_persona', $insert)) ? ($err['error_code']) : ("0");
                        $this->db->delete('tb_contactopersona',array("id_persona"=>$id_persona, "tipo_persona"=>"1"));

                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);

                    	if(!empty($contacto_) && $err['error_code']=="1")
			  			{
			  				$insert_['id_persona'] = $id_persona;
			  				$insert_['tipo_persona'] = 1;
			  				if(!empty($contacto_['cel']))
			  				{
			  					foreach ($contacto_['cel'] as $k_ => $val) 
				  				{
									$insert_['id_operador'] = $contacto_['op'][$k_];
									$insert_['id_tipo'] = 1;
									$insert_['celular'] = $val;
				  					$err['contacto'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
				  				}
			  				}

			  				if(!empty($contacto_['telf']))
			  				{
			  					if(isset($insert_['id_operador'])){unset($insert_['id_operador']);}
			  					if(isset($insert_['celular'])){unset($insert_['celular']);}

			  					foreach ($contacto_['telf'] as $k_ => $val) 
				  				{
									$insert_['id_tipo'] = 2;
									$insert_['telefono'] = $val;
									$insert_['anexo'] = $contacto_['anex'][$k_];
				  					$err['contacto'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
				  				}
			  				}
			  			}
                    }
                }
                    
                $err['id'] = $id_persona;               
	            
	        }        
	        return $err;
	  	}

	  	public function get_one_persona($param='')
	  	{
	  		if(!empty($param['id_persona']))
	  		{
	  			$w['p.id_persona'] = $param['id_persona'];

	  			$rta = $this->db->select('p.id_persona, p.nombres, p.apellidos, p.dni, lpad(p.dni_genera,8,"0") dni_genera, p.id_documentoidentidad, date_format(p.fecha_nacimiento,"%d-%m-%Y") fecha_nacimiento,  p.es_cliente, p.es_proveedor, p.sexo, p.estado')
	  							->from('tb_persona p')
	  							->where($w)
	  							->get()
	  							->row_array();

	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function inactive_persona($id_persona= null)
	  	{
	  		if(!empty($id_persona))
	  		{
	  			$this->db->where('id_persona',$id_persona);
	  			return ($this->db->update('tb_persona',array("estado"=>'0'))) ? true : false;
	  		}
	  	}

	  	public function config($tab='', $id_persona='')
	  	{
	  		if(!empty($tab) && !empty($id_persona))
	  		{
	  			switch ($tab) 
	  			{
	  				case 'general':
	  					$w['p.id_persona'] = $id_persona;
	  					
						$rt = $this->db->select('p.id_persona, p.apellidos, p.nombres, p.email, p.id_documentoidentidad, p.dni, p.dni_genera, p.sexo, p.pagina_web, p.direccion, p.id_pais, p.id_estadocivil, p.id_departamento, p.id_provincia, p.id_distrito, date_format(p.fecha_nacimiento,"%d-%m-%Y") fecha_nacimiento, p.fecha_nacimiento fecha_nacimiento__')
									   ->from('tb_persona p')
									   ->where($w)
									   ->get()
									   ->row_array();
						$w['p.tipo_persona'] = 1;
						$w['p.id_tipo'] = 1;

						$rt['contacto_cel'] = $this->db->get_where('tb_contactopersona p',$w)->result_array();
						
						$w['p.id_tipo'] = 2;									
	  					$rt['contacto_telef'] = $this->db->get_where('tb_contactopersona p',$w)->result_array();

	  				break;
	  				case 'contacto':
						$w['id_persona'] = $id_persona;
						
						$rta = $this->db->select('group_concat(id_persona_juridica) cadena')
									   ->from('tb_contacto_persona_empresa')
									   ->where($w)
									   ->get()
									   ->row_array();

						if(!empty($rta))
						{
							$rt = $this->db->select('id_persona_juridica, nombre_comercial, razon_social, ruc')
											->from('tb_persona_juridica')
											->where_in('id_persona_juridica',explode(",",$rta['cadena']))
											->get()
											->result_array();
						}

	  				break;
	  			}

	  			return (!empty($rt)) ? $rt : null;
 	  		}
	  	}

	  	public function validar_dni($param = null)
	    {
	        $success = FALSE;
	        if($param)
	        {
	            $whe['pn.dni'] = trim($param['dni']);
	            $whe['pn.id_documentoidentidad'] = $param['id_documentoidentidad'];
	            
	            $query = $this->db->select('pn.id_persona');
	            $form = $query->from('tb_persona pn');                
	            if(!empty($param['id_persona']))
	            {
	                $whe['pn.id_persona !='] = $param['id_persona'];
	            }

	            $where = $form->where($whe);
	            $cant = $where->count_all_results(); //print_r($this->db->last_query());

	            $success= ($cant>0) ? (FALSE) : (TRUE);              
	        }
	        return $success;
		}
		
		public function add_contacto($param = null)
		{
			if(!empty($param['id_persona']))
			{
				$rta = ($this->db->insert('tb_contacto_persona_empresa',$param)) ? true : false;

				return $rta;
			}
		}

		public function delete_contacto($where)
		{
			if(!empty($where['id_persona']) && !empty($where['id_persona_juridica']))
			{	
				$this->db->where($where);
				return ($this->db->delete('tb_contacto_persona_empresa')) ? true : false;
			}
		}

		public function get_dni($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona, nombres, apellidos, IF(dni is not null,dni,LPAD(dni_genera,8,"0")) dni, id_documentoidentidad')
								->from('tb_persona')
								->where($w)
								->like('dni',$query)
								->or_like('dni_genera',$query);
				if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona',explode(",", $w_not));

				$rta = $rta->order_by('id_persona','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$tip = ($value['id_documentoidentidad']==1) ? "DNI: " : "Carnet. Ex.: ";
						$data[$key]['value'] = $tip.$value['dni'].":".$value['nombres'].",".$value['apellidos'];
						$data[$key]['data']['id_persona'] = $value['id_persona'];
						$data[$key]['data']['nombres'] = $value['nombres'];
						$data[$key]['data']['apellidos'] = $value['apellidos'];
						$data[$key]['data']['dni'] = $value['dni'];
					}
				}

				return $data;
			}
		}

		public function get_nombres($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona, nombres, apellidos, IF(dni is not null,dni,LPAD(dni_genera,8,"0")) dni, id_documentoidentidad')
								->from('tb_persona')
								->where($w)
								->like('nombres',$query);
				if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona',explode(",", $w_not));

				$rta = $rta->order_by('id_persona','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$tip = ($value['id_documentoidentidad']==1) ? "DNI: " : "Carnet. Ex.: ";
						$data[$key]['value'] = $tip.$value['dni'].":".$value['nombres'].",".$value['apellidos'];
						$data[$key]['data']['id_persona'] = $value['id_persona'];
						$data[$key]['data']['nombres'] = $value['nombres'];
						$data[$key]['data']['apellidos'] = $value['apellidos'];
						$data[$key]['data']['dni'] = $value['dni'];
					}
				}

				return $data;
			}
		}

		public function get_apellidos($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona, nombres, apellidos, IF(dni is not null,dni,LPAD(dni_genera,8,"0")) dni, id_documentoidentidad')
								->from('tb_persona')
								->where($w)
								->like('apellidos',$query);
					if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona',explode(",", $w_not));

				$rta = $rta->order_by('id_persona','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$tip = ($value['id_documentoidentidad']==1) ? "DNI: " : "Carnet. Ex.: ";
						$data[$key]['value'] = $tip.$value['dni'].":".$value['nombres'].",".$value['apellidos'];
						$data[$key]['data']['id_persona'] = $value['id_persona'];
						$data[$key]['data']['nombres'] = $value['nombres'];
						$data[$key]['data']['apellidos'] = $value['apellidos'];
						$data[$key]['data']['dni'] = $value['dni'];
					}
				}

				return $data;
			}
		}
	}

	
?>