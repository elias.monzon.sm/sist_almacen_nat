<?php

	class M_generico extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function departamentos($id_pais=null)
	  	{
	  		if(!empty($id_pais)) $w['idPais'] = $id_pais;

	  		$rta = $this->db->select('idDepartamento, departamento')
	  						->from('departamento');
	  		if(isset($w))
	  			$rta=$rta->where($w);

	  		$rta = $rta ->order_by('departamento','ASC')
	  						->get()
	  						->result_array();

	  		if(!empty($rta))
	  		{
	  			$data = array();
	  			foreach ($rta as $value) 
	  			{
	  				$data[$value['idDepartamento']] = $value['departamento'];
	  			}
	  		}

	  		return (!empty($data)) ? $data : null;
	  	}

	  	public function provincias($id_departamento=null)
	  	{
	  		if(!empty($id_departamento))
	  		{
	  			$w['idDepartamento'] = $id_departamento;
	  		}

	  		$rta = $this->db->select('idProvincia, provincia')
	  						->from('provincia');
	  		if (!empty($w)) {
	  			$rta = $rta->where($w);
	  		}
	  						
	  			$rta = $rta->order_by('provincia','ASC')
	  						->get()
	  						->result_array();

	  		if(!empty($rta))
	  		{
	  			$data = array();
	  			foreach ($rta as $value) 
	  			{
	  				$data[$value['idProvincia']] = $value['provincia'];
	  			}
	  		}

	  		return (!empty($data)) ? $data : null;
	  	}

	  	public function distritos($id_provincia=null)
	  	{
	  		if(!empty($id_provincia))
	  		{
	  			$w['idProvincia'] = $id_provincia;
	  		}

	  		$rta = $this->db->select('idDistrito, distrito')
	  						->from('distrito')
	  						->where($w)
	  						->order_by('distrito','ASC')
	  						->get()
	  						->result_array();
	  		if(!empty($rta))
	  		{
	  			$data = array();
	  			foreach ($rta as $value) 
	  			{
	  				$data[$value['idDistrito']] = $value['distrito'];
	  			}
	  		}

	  		return (!empty($data)) ? $data : null;
	  	}

	  	public function get_one_distrito($id_distrito=null)
	  	{
	  		if(!empty($id_distrito))
	  		{
	  			$w['ds.idDistrito'] = $id_distrito;

	  			$rta = $this->db->select('ds.idDistrito, ds.distrito, pr.idProvincia, pr.provincia, dp.idDepartamento, dp.departamento')
	  							->from('distrito ds')
	  							->join('provincia pr','ds.idProvincia=pr.idProvincia','left')
	  							->join('departamento dp','pr.idDepartamento=dp.idDepartamento')
	  							->where($w)
	  							->get()
	  							->row_array();
	  			
	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function operadoras()
	  	{
	  		$w['estado'] = 1;
	  		$d = $this->db->select('id_operador, operador')
	  					  ->from('tb_operador')
	  					  ->where($w)
	  					  ->get()
	  					  ->result_array();
	  		if(!empty($d))
	  		{
	  			$rt = array();
	  			foreach ($d as $v) 
	  			{
	  				$rt[$v['id_operador']] = $v['operador'];
	  			}
	  		}
	  		return (!empty($rt)) ? $rt : null;
	  	}

	  	public function pais()
	  	{
	  		$r = $this->db->select('id_pais, pais')
	  					  ->from('tb_pais')
	  					  ->get()
	  					  ->result_array();

	  		if(!empty($r))
	  		{
	  			$rt_ = array();
	  			foreach ($r as $v) 
	  			{
	  				$rt_[$v['id_pais']] = $v['pais'];
	  			}
	  		}

	  		return (!empty($rt_)) ? $rt_ : null;
	  	}
	}
?>