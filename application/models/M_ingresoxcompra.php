<?php

class M_ingresoxcompra extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function buscar_documento($param='')
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $query = $this->db->select('doc.id_documento, doc.codigo, doc.serie, doc.fecha_ingreso, doc.txt_proveedor, (select CONCAT_WS(":",tp.tipomoneda,tp.simbolo) from tb_tipomoneda tp where tp.id_tipomoneda=doc.id_tipomoneda) tipomoneda, ROUND(doc.factor_cambio,3) tipo_cambio, ROUND(doc.precio_total,3) precio_total, (SELECT MIN(docc.id_documento) FROM tb_documento docc) first_id')
                              ->from('tb_documento doc');
            if(!empty($whe))
                $query = $query->where($whe);

            $query = $query->limit($limit, $pages)
                           ->order_by('doc.fecha_ingreso', 'DESC');
            $rta['all_data'] = $query->get()->result_array();

            $query = $this->db->select('doc.id_documento')
                              ->from('tb_documento doc');
            if(!empty($whe))
                $query = $query->where($whe);

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function proveedor_arti($param='')
    {
        if(!empty($param))
        {   
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;
            
            $whe['pn.estado'] = 1;
            $whe['pn.es_proveedor'] = 1;

            if(!empty($param['ruc_dni']))
            {
                $liekruc = trim($param['ruc_dni']);
            }

            if(!empty($param['razon_soc']))
            {
                $likerz = trim($param['razon_soc']);
            }

            if(isset($param['nombres_com']) && strlen(trim($param['nombres_com']))>0)
            {
                $likenomb = trim($param['nombres_com']);   
            }

            $sq = $this
                        ->db
                        ->select("pn.id_persona, 1 tipo_persona, pn.nombres nombre_comercial, pn.apellidos razon_social, IF(pn.id_documentoidentidad=1,pn.dni,LPAD(pn.dni_genera,8,'0')) ruc")
                        ->from('tb_persona pn')
                        ->where($whe);

            if(isset($liekruc))
            {
                $sq = $sq->group_start()
                            ->like('pn.dni', $liekruc)
                        ->group_end();
            }

            if(isset($likerz))
            {
                $sq = $sq->group_start()
                            ->like('pn.nombres', $likerz)
                        ->group_end();
            }

            if(isset($likenomb))
            {
                $sq = $sq->group_start()
                            ->like('pn.apellidos', $likenomb)
                        ->group_end();
            }

            $query1 = $sq->get_compiled_select();

            $whe2['pj.estado'] = 1;
            $whe2['pj.es_proveedor'] = 1;

            $sq = $this
                    ->db
                    ->select("pj.id_persona_juridica id_persona, 2 tipo_persona, nombre_comercial, razon_social, ruc")
                    ->from('tb_persona_juridica pj')
                    ->where($whe2);

            if(isset($liekruc))
            {
                $sq = $sq->group_start()
                            ->like('pj.ruc', $liekruc)
                        ->group_end();
            }

            if(isset($likerz))
            {
                $sq = $sq->group_start()
                            ->like('pj.razon_social', $likerz)
                        ->group_end();
            }

            if(isset($likenomb))
            {
                $sq = $sq->group_start()
                            ->like('pj.nombre_comercial', $likenomb)
                        ->group_end();
            }
        
            $query2 = $sq->get_compiled_select();
            $sql = "select * from ((".$query1.") union all (".$query2.")) usi ORDER BY nombre_comercial desc limit ".$pages.", ".$limit.";";
            $rta['all_data'] = $this->db->query($sql)->result_array();
            $rta['total_registros'] = count($rta['all_data']);
            $rta['cantidad_pag'] = ceil($rta['total_registros']/$limit);
        }
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE); 
    }

    public function save_ingresoxcompra($data='')
    {
        //print_r($data); exit();
        if(isset($data['p_total']))
        {
            $docu['codigo'] = $data['codigo'];
            $docu['serie'] = $data['serie'];
            $docu['fecha_ingreso'] = date('Y-m-d',strtotime($data['fecha_ingreso']));
            $docu['factor_cambio'] = $data['factor_cambio'];
            $docu['txt_proveedor'] = $data['ruc_dni'];
            $docu['factor_impuesto'] = $data['factor_impuesto'];
            $docu['id_persona'] = $data['id_persona'];
            $docu['tipo_persona'] = $data['tipo_persona'];
            $docu['id_tipomoneda'] = $data['id_tipomoneda'];
            $docu['precio_total'] = $data['p_total'];
            $docu['impuesto'] = $data['p_total']*$docu['factor_impuesto'];
            $docu['costo_total'] = $data['p_total'] - $docu['impuesto'];
            $docu['id_tipodocumento'] = (!empty($data['id_tipodocumento'])) ? $data['id_tipodocumento'] : null;

            $id_documento = ($this->db->insert('tb_documento',$docu)) ? $this->db->insert_id() : null;

            if(!empty($id_documento))
            {
                //print_r($data);
                $det['id_documento'] = $id_documento;
                foreach ($data['id_codigomarca'] as $id_codigo => $arr) 
                {
                    foreach ($arr as $id_marca => $val) {
                        $det['precio_total'] = $data['pt'][$id_codigo][$id_marca];
                        $det['cantidad'] = $data['cant'][$id_codigo][$id_marca];
                        $det['precio_unitario'] = $data['pu'][$id_codigo][$id_marca];
                        $det['impuesto_total'] = $det['precio_total']*$data['factor_impuesto'];
                        $det['impuesto_unitario'] = $det['precio_unitario']*$data['factor_impuesto'];
                        $det['costo_total'] = $det['precio_total'] - $det['impuesto_total'];
                        $det['costo_unitario'] = $det['costo_total'] - $det['impuesto_unitario'];
                        $det['id_articulo'] = $id_codigo;
                        $det['id_marca'] = $id_marca;
                        $det['txt_articulo'] = $data['txtcod'][$id_codigo];
                        $det['txt_marca'] = $data['txtmar'][$id_marca];
                        $det['id_almacen'] = $data['alm'][$id_codigo][$id_marca];
                        $det['txt_almacen'] = $data['txtalm'][$det['id_almacen']];

                        $id_documento_det = ($this->db->insert('tb_documento_det',$det)) ? $this->db->insert_id() : null;
                        if(!empty($id_documento_det))
                        {
                            $w['id_almacen'] = $det['id_almacen'];
                            $w['id_marca'] = $id_marca;
                            $w['id_codigo'] = $id_codigo;

                            $d = $this->db->select('id_kardex, stock, id_almacen')
                                          ->from('tb_kardex')
                                          ->where($w)
                                          ->order_by('id_kardex','desc')
                                          ->limit(1)
                                          ->get()
                                          ->row_array();

                            $k['tipo'] = 1;
                            $k['txt_tipo'] = "Ingreso";
                            $k['id_codigo'] = $id_codigo;
                            $k['id_marca'] = $id_marca;
                            $k['txt_articulo'] = $det['txt_articulo'];
                            $k['txt_marca'] = $det['txt_marca'];
                            $k['id_documento_det'] = $id_documento_det;
                            $k['cantidad'] = $det['cantidad'];
                            $k['id_tipomovimiento'] = (!empty($d)) ? 1 : 3;
                            $k['stock'] = (!empty($d)) ? $d['stock']+$det['cantidad'] : $det['cantidad'];
                            $k['id_almacen'] = $det['id_almacen'];
                            $k['txt_almacen'] = $det['txt_almacen'];
                            $k['id_tipomoneda'] = $docu['id_tipomoneda'];
                            $k['factor_cambio_moneda'] = $docu['factor_cambio'];
                            $k['precio_total'] = $det['precio_total'];
                            $k['precio_unitario'] = $det['precio_unitario'];
                            $k['costo_total'] = $det['costo_total'];
                            $k['costo_unitario'] = $det['costo_unitario'];
                            $k['impuesto_total'] = $det['impuesto_total'];
                            $k['impuesto_unitario'] = $det['impuesto_unitario'];
                            $k['fecha_ingreso'] = date('Y-m-d H:i:s');

                            $response = ($this->db->insert('tb_kardex',$k)) ? $this->db->insert_id() : null;
                        }
                    }
                }
            }
        }
        return (!empty($response)) ? $response : null;
    }

    public function save_add_articulo($data='')
    {
        if(!empty($data['id_codigo']))
        {
            $sign = $data['sign'];
            $w['cod.id_codigo'] = $data['id_codigo']; 
            $w['ma.id_marca'] = $data['id_marca'];

            $pt = $data['precio_total'];
            $pu = $data['precio_unitario'];
            $cant = $data['cantidad'];
            $id_marca = $data['id_marca'];
            $id_codigo = $data['id_codigo'];

            $r = $this->db->select('cod.id_codigo, cod.codigo, ma.id_marca, cod.descripcion, ma.marca, calm.id_almacen, alm.almacen')
                          ->from('tb_codigo cod')
                          ->join('tb_marca ma', 'ma.id_marca='.$id_marca,'left')
                          ->join('tb_codigo_almacen calm','calm.id_codigo='.$id_codigo.' and calm.id_marca='.$id_marca,'left')
                          ->join('tb_almacen alm','alm.id_almacen=calm.id_almacen','left')
                          ->where($w)
                          ->get()
                          ->row_array();

            if(!empty($r))
            {

                $this->load->model("m_almacen");
                $almacens = $this->m_almacen->get_almxcod(array("id_codigo"=>$id_codigo, "id_marca" => $id_marca));
                $r['cbx_alm'] = cbx_simple($almacens,$r['id_almacen']);

                $precio_unitario = (isset($data['edit'])) ? '' : number_format($pu,2);
                $precio_total = (isset($data['edit'])) ? '' : number_format($pt,2);
                $cantidad = (isset($data['edit'])) ? '' : number_format($cant,2);
                $type_input = (isset($data['edit'])) ? 'text' : 'hidden';
                $herramietnas = (isset($data['edit'])) ? "<a class='btn btn-danger delete_art btn-xs'><i class='fa fa-pencil'></i></a>" : "<a class='btn btn-warning edit btn-xs' data-toggle='modal' data-target='#addarticulo'><i class='fa fa-pencil'></i></a> <a class='btn btn-danger delete_art btn-xs'><i class='fa fa-trash-o'></i></a>";

                $tx  = "<tr class='artirow' tdcodma='[".$id_codigo."][".$id_marca."]' idcod=".$id_codigo." idmarca=".$id_marca.">";
                $tx .= "<td class='cod'>".$r['codigo']."<input type='hidden' name='id_codigomarca[".$id_codigo."][".$id_marca."]' value='[".$id_codigo."][".$id_marca."]'><input type='hidden' class='txcod' name='txtcod[".$id_codigo."]' value='".$r['codigo']."'>";
                $tx .= "<td class ='desc'>".$r['descripcion']."</td>";
                $tx .= "<td class ='marca'>".$r['marca']."<input type='hidden' class='txmar' name='txtmar[".$id_marca."]' value='".$r['marca']."'></td>";
                $tx .= "<td class ='cant'>".$cantidad."<input type='".$type_input."' value='".$cant."' name='cant[".$id_codigo."][".$id_marca."]'> </td>" ;
                $tx .= "<td class='puni'><span class='pull-left'><b><i>".$sign."</i></b></span>".$precio_unitario."<input type='".$type_input."' value='".$pu."' name='pu[".$id_codigo."][".$id_marca."]'></td>";
                $tx .= "<td class='ptot'><span class='pull-left'><b><i>".$sign."</i></b></span>".$precio_total."<input type='".$type_input."' value='".$pt."' name='pt[".$id_codigo."][".$id_marca."]'></td>";
                $tx .= "<td class='alm'><select class='sele' name='alm[".$id_codigo."][".$id_marca."]'>".$r['cbx_alm']."</select><input type='hidden' name='txtalm[".$r['id_almacen']."]' value='".$r['almacen']."'></td>";
                $tx .= "<td class='text-center'>".$herramietnas."</td>";
                $tx .="</tr>";
            }
        }

        return (!empty($tx)) ? $tx : null;
    }

    public function get_docu($id_documento)
    {
        if(!empty($id_documento))
        {
            $w['doc.id_documento'] = $id_documento;

            $rta['main'] = $this->db->select('doc.id_documento, doc.codigo, doc.serie, doc.fecha_ingreso, doc.txt_proveedor, (select CONCAT_WS(":",tp.tipomoneda,tp.simbolo) from tb_tipomoneda tp where tp.id_tipomoneda=doc.id_tipomoneda) tipomoneda, (select td.tipodocumento from tb_tipodocumento td where td.id_tipodocumento=doc.id_tipodocumento) tipodocumento, doc.id_tipodocumento, doc.id_persona, doc.tipo_persona, doc.id_tipomoneda')
                            ->from('tb_documento doc')
                            ->where($w)
                            ->get()
                            ->row_array();
            if(!empty($rta['main']))
            {
                $w_['det.id_documento'] = $id_documento;
                $rta['det'] = $this->db->select('det.txt_articulo, det.txt_marca, det.txt_almacen, round(det.cantidad,2) cantidad, round(det.precio_total,2) precio_total, round(det.precio_unitario,2) precio_unitario, (select cod.descripcion from tb_codigo cod where cod.id_codigo=det.id_articulo) descripcion, det.precio_unitario precio_unitario_bruto, det.id_articulo, det.id_marca')
                                       ->from('tb_documento_det det')
                                       ->where($w_)
                                       ->order_by('det.id_documento_det','asc')
                                       ->get()
                                       ->result_array();
            }   
            return(!empty($rta['main'])) ? $rta : null;
        }
    }
    public function edit_documentokardex($data='')
    {
        $resp['status']     = 0;
        $resp['message']    = '';

        if(!empty($data['id_documento']))
        {
            $id_documento = $data['id_documento'];
            $whu['id_documento'] = $id_documento;
            $impuesto_total = 0;
            $costo_total = 0;
            $precio_total = 0;
            foreach ($data['pu'] as $ida => $val) 
            {
                foreach ($val as $idm => $val_) 
                {
                    $whu['id_articulo'] = $ida;
                    $whu['id_marca'] = $idm;

                    $e = $this->db->select('id_documento_det')
                              ->from('tb_documento_det')
                              ->where($whu)
                              ->get()
                              ->row_array();

                    if(!empty($e['id_documento_det']))
                    {
                        $dta['id_documento']        = $id_documento;
                        $dta['precio_total']        = $data['pt'][$ida][$idm];
                        //$dta['cantidad']            = $data['cant'][$ida][$idm];
                        $dta['precio_unitario']     = $data['pu'][$ida][$idm];
                        $dta['impuesto_total']      = $dta['precio_total']*0.18;
                        $dta['impuesto_unitario']   = $dta['precio_unitario']*0.18;
                        $dta['costo_total']         = $dta['precio_total'] - $dta['impuesto_total'];
                        $dta['costo_unitario']      = $dta['costo_total'] - $dta['impuesto_unitario'];
                        $costo_total += $dta['costo_total'];
                        $impuesto_total += $dta['impuesto_total'];
                        $precio_total += $dta['precio_total'];
                        $resp['status'] = save_data('tb_documento_det',$dta,$whu);

                        /*if(!empty($resp['status']))
                        {
                            $wha['id_codigo']        = $ida;
                            $wha['id_marca']         = $idm;
                            $wha['id_documento_det'] = $e['id_documento_det'];

                            $k['cantidad']          = $dta['cantidad'];
                            $k['stock']             = $dta['cantidad'];
                            $k['precio_total']      = $dta['precio_total'];
                            $k['precio_unitario']   = $dta['precio_unitario'];
                            $k['costo_total']       = $dta['costo_total'];
                            $k['costo_unitario']    = $dta['costo_unitario'];
                            $k['impuesto_total']    = $dta['impuesto_total'];
                            $k['impuesto_unitario'] = $dta['impuesto_unitario'];
                            $k['fecha_ingreso']     = date('Y-m-d H:i:s');

                            $resp['status'] = save_data('tb_kardex',$k,$wha);
                        }*/
                    }
                    else{

                        $det['id_documento']        = $id_documento;
                        $det['precio_total']        = $data['pt'][$ida][$idm];
                        //$det['cantidad']            = $data['cant'][$ida][$idm];
                        $det['precio_unitario']     = $data['pu'][$ida][$idm];
                        $det['impuesto_total']      = $det['precio_total']*0.18;
                        $det['impuesto_unitario']   = $det['precio_unitario']*0.18;
                        $det['costo_total']         = $det['precio_total'] - $det['impuesto_total'];
                        $det['costo_unitario']      = $det['costo_total'] - $det['impuesto_unitario'];
                        $det['id_articulo']         = $ida;
                        $det['id_marca']            = $idm;
                        $det['txt_articulo']        = $data['txtcod'][$ida];
                        $det['txt_marca']           = $data['txtmar'][$idm];
                        $det['id_almacen']          = $data['alm'][$ida][$idm];
                        $det['txt_almacen']         = $data['txtalm'][$det['id_almacen']];
                        $costo_total += $det['costo_total'];
                        $impuesto_total += $det['impuesto_total'];
                        $precio_total += $det['precio_total'];
                        $id_documento_det = (save_data('tb_documento_det',$det));
                        /*
                        if(!empty($id_documento_det))
                        {
                            $k['tipo']                  = 1;
                            $k['txt_tipo']              = "Ingreso";
                            $k['id_codigo']             = $ida;
                            $k['id_marca']              = $idm;
                            $k['txt_articulo']          = $det['txt_articulo'];
                            $k['txt_marca']             = $det['txt_marca'];
                            $k['id_documento_det']      = $id_documento_det;
                            $k['cantidad']              = $det['cantidad'];
                            $k['id_tipomovimiento']     = 3;
                            $k['stock']                 = $det['cantidad'];
                            $k['id_almacen']            = $det['id_almacen'];
                            $k['txt_almacen']           = $det['txt_almacen'];
                            $k['precio_total']          = $det['precio_total'];
                            $k['precio_unitario']       = $det['precio_unitario'];
                            $k['costo_total']           = $det['costo_total'];
                            $k['costo_unitario']        = $det['costo_unitario'];
                            $k['impuesto_total']        = $det['impuesto_total'];
                            $k['impuesto_unitario']     = $det['impuesto_unitario'];
                            $k['fecha_ingreso']         = date('Y-m-d H:i:s');

                            $resp['status'] = (save_data('tb_kardex',$k));
                        }
                        */
                    }                    
                }
            }

            $uptt['impuesto'] = $impuesto_total;
            $uptt['costo_total'] = $costo_total;
            $uptt['precio_total'] = $precio_total;

            $w['id_documento'] = $id_documento;

            $resp['status'] = save_data('tb_documento',$uptt,$w);
        }

        return $resp;
    }

    public function script1()
    {
        $all_compras = $this->db->select('det.cantidad, det.id_articulo, det.id_marca, det.cantidad, det.id_documento_det, k.id_kardex, det.id_almacen, (SELECT doc.factor_cambio FROM tb_documento doc WHERE det.id_documento=doc.id_documento) factor_cambio, (SELECT doc.id_tipomoneda FROM tb_documento doc WHERE det.id_documento=doc.id_documento) id_tipomoneda, det.precio_total, det.costo_total, det.costo_unitario, det.impuesto_unitario, det.impuesto_total, det.precio_unitario')
                               ->from('tb_documento_det det')
                               ->join('tb_kardex k','det.id_documento_det=k.id_documento_det','left')
                               ->get()
                               ->result_array();

        if(!empty($all_compras))
        {
            foreach ($all_compras as $key => $va) {
                if(empty($va['id_kardex']))
                {
                    $whe['id_codigo'] = $va['id_articulo'];
                    $whe['id_marca'] = $va['id_marca'];
                    $whe['id_almacen'] = $va['id_almacen'];

                    $kardex = $this->db->select('stock, id_codigo, id_marca, id_kardex, id_documento_det')
                                       ->from('tb_kardex')
                                       ->where($whe)
                                       ->order_by('id_kardex','desc')
                                       ->get()
                                       ->row_array();
                    
                    $k['tipo'] = 1;
                    $k['txt_tipo'] = "Ingreso";
                    $k['id_codigo'] = $va['id_articulo'];
                    $k['id_marca'] = $va['id_marca'];
                    $k['id_documento_det'] = $va['id_documento_det'];
                    $k['cantidad'] = $va['cantidad'];
                    $k['id_tipomovimiento'] = (!empty($kardex)) ? 1 : 3;
                    $k['stock'] = (!empty($kardex)) ? $kardex['stock']+$va['cantidad'] : $va['cantidad'];
                    $k['id_almacen'] = $va['id_almacen'];
                    $k['id_tipomoneda'] = $va['id_tipomoneda'];
                    $k['factor_cambio_moneda'] = $va['factor_cambio'];
                    $k['precio_total'] = $va['precio_total'];
                    $k['precio_unitario'] = $va['precio_unitario'];
                    $k['costo_total'] = $va['costo_total'];
                    $k['costo_unitario'] = $va['costo_unitario'];
                    $k['impuesto_total'] = $va['impuesto_total'];
                    $k['impuesto_unitario'] = $va['impuesto_unitario'];
                    $k['fecha_ingreso'] = date('Y-m-d H:i:s');

                    save_data('tb_kardex',$k);
                }
            }
        }
    }

    public function script2()
    {
        $kardex = $this->db->select('*')
                          ->from('tb_kardex')
                          ->order_by('id_kardex','desc')
                          ->get()
                          ->result_array();

        if(!empty($kardex))
        {   
            $myDataKardex = [];
            foreach ($kardex as $key => $va) {
                if(!isset($myDataKardex[$va['id_codigo']][$va['id_marca']]))
                {
                    $myDataKardex[$va['id_codigo']][$va['id_marca']] = $va['stock'];
                }
            }
        }
        $compras = $this->db->get('tb_documento_det')->result_array();

        if(!empty($compras))
        {
            $myDatacompras = [];
            foreach ($compras as $key => $va) 
            {
                $myDatacompras[$va['id_articulo']][$va['id_marca']] = (isset($myDatacompras[$va['id_articulo']][$va['id_marca']])) ? $myDatacompras[$va['id_articulo']][$va['id_marca']] + $va['cantidad']: $va['cantidad'];
            }
        }

        if(!empty($myDatacompras) && !empty($myDataKardex))
        {
            foreach ($myDatacompras as $idart => $arr) 
            {
                foreach ($arr as $idmarca => $cantCOmpras) 
                {
                    echo "\/********************************/\n";
                    echo "Articulo:".$idart."\n";
                    echo "Marca:".$idmarca."\n";
                    echo "Cantidad Total Comprada:".$cantCOmpras."\n";
                    echo "Stock Actual:".$myDataKardex[$idart][$idmarca]."\n";
                    if($cantCOmpras == $myDataKardex[$idart][$idmarca])
                    {
                        echo "Estado: Coinciden\n";
                    }
                    else
                    {
                        echo "Estado: No coinciden\n";
                        echo "SELECT * FROM tb_kardex WHERE id_codigo=$idart AND id_marca = $idmarca;\n";
                    }
                }
            }
        }
    }
}