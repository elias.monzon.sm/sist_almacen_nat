<?php

	class M_personajuridica extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function buscar_personajud($param = '')
	  	{
	  		
	  		if(isset($param['page']))
	  		{	
	  			if(!empty($param['r_social']))
	  				$like['pj.razon_social'] = $param['r_social'];
	  			
	  			if(!empty($param['n_comercial']))
	  				$like['pj.nombre_comercial'] = $param['n_comercial'];
	  			
	  			if(!empty($param['ruc']))
	  				$like['pj.ruc'] = $param['ruc'];

	  			if(!empty($param['fech_ani']))
	  				$like['pj.aniversario'] = date('Y-m-d',strtotime($param['fech_ani']));

	  			if(!empty($param['solo_clientes']))
	  				$w['pj.es_cliente'] = 1;

	  			if(!empty($param['solo_proveedor']))
					  $w['pj.es_proveedor'] = 1;
					  
	  			if(isset($param['estado']))
	  				$w['pj.estado'] = $param['estado'];

	  			$page = $param['page'];
	  			$limit = 10;
	  			$pages = $page*$limit;
	  			unset($param['page']);

	  			$rta['all_data'] = $this->db->select('pj.id_persona_juridica, pj.ruc, pj.nombre_comercial, pj.direccion_fiscal, pj.razon_social, date_format(pj.aniversario,"%d-%m-%Y") aniversario, pj.es_cliente, pj.es_proveedor, pj.estado, IF(pj.id_persona_juridica=(SELECT mi.id_persona_juridica from tb_miempresa mi where pj.id_persona_juridica=mi.id_persona_juridica),1,0) miempresa')
	  										->from('tb_persona_juridica pj');
	  			if(!empty($w))
	  			{
	  				$rta['all_data'] = $rta['all_data']->where($w);
	  			}
	  			if(!empty($param['w_n']))
	  			{
	  				$rta['all_data'] = $rta['all_data']->where_not_in('pj.id_persona_juridica',explode(",", $param['w_n']));
	  			}
	  			if(!empty($like))
	  			{
	  				$rta['all_data'] = $rta['all_data']->like($like);
	  			}
	  				$rta['all_data'] = $rta['all_data']->limit($limit,$pages)
	  										->order_by('nombre_comercial','ASC')
	  										->get()
	  										->result_array();
				  //print_r($this->db->last_query());
				  
	  			$rta['total_registros'] = $this->db->select('pj.id_persona_juridica')
	  												->from('tb_persona_juridica pj');
	  			if(!empty($w))
	  			{
	  				$rta['total_registros'] = $rta['total_registros']->where($w);
	  			}
	  			if(!empty($param['w_n']))
	  			{
	  				$rta['total_registros'] = $rta['total_registros']->where_not_in('pj.id_persona_juridica',explode(",", $param['w_n']));
	  			}
	  			if(!empty($like))
	  			{
	  				$rta['total_registros'] = $rta['total_registros']->like($like);
	  			}
	  				$rta['total_registros'] = $rta['total_registros']->count_all_results();

	  			$rta['cantidad_pag'] = ceil($rta['total_registros']/$limit);

	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function save_personajud($data='')
	  	{
	  		//print_r($data); die();
	  		$err['error_msg'] = "ERROR";
	        $err['error_code'] = "0";
	        $err['id'] = "";

	        if(!empty($data) && is_array($data)) 
	        {
                $fecha = date("Y-m-d H:i:s");

                $data['aniversario'] = (!empty($data['aniversario'])) ? date('Y-m-d',strtotime($data['aniversario'])) : null;
                $id_persona_juridica = $data['id_persona_juridica'];
                unset($data['id_persona_juridica']);                

                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['ruc']))
                {
                    $whe['ruc'] = ($data['ruc']);

                    $query = $this->db->select('id_persona_juridica');
                    $form = $query->from('tb_persona_juridica');
                    
                    if($id_persona_juridica>0)
                    {
                        $whe['id_persona_juridica !='] = $id_persona_juridica;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                
                if($err['error_code']=="1")
                {
                    if(empty($id_persona_juridica))
                    {
                    	$id_persona_juridica = ($this->db->insert('tb_persona_juridica', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_persona_juridica) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $contacto_ =(isset($data['contacto'])) ? $data['contacto'] : null;
						unset($data['contacto']);
						if(isset($data['id_pais'])){
							$data['id_provincia'] = (isset($data['id_provincia'])) ? $data['id_provincia'] : null;
							$data['id_departamento'] = (isset($data['id_departamento'])) ? $data['id_departamento'] : null;
							$data['id_distrito'] = (isset($data['id_distrito'])) ? $data['id_distrito'] : null;
							$this->db->delete('tb_contactopersona',array("id_persona"=>$id_persona_juridica, "tipo_persona"=>"1"));
						}
						$data['es_cliente'] = (!empty($data['es_cliente'])) ? $data['es_cliente'] : null;
						$data['es_proveedor'] = (!empty($data['es_proveedor'])) ? $data['es_proveedor'] : null;
						$this->db->where('id_persona_juridica', $id_persona_juridica);                        
						$err['error_code'] = ($this->db->update('tb_persona_juridica', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);

                    	if(!empty($contacto_) && $err['error_code']=="1")
			  			{
			  				$insert_['id_persona'] = $id_persona_juridica;
			  				$insert_['tipo_persona'] = 1;
			  				if(!empty($contacto_['cel']))
			  				{	
								//print_r($contacto_);
			  					foreach ($contacto_['cel'] as $k_ => $val) 
				  				{
									$insert_['id_operador'] = $contacto_['op'][$k_];
									$insert_['id_tipo'] = 1;
									$insert_['celular'] = $val;
				  					$err['contacto'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
				  				}
			  				}

			  				if(!empty($contacto_['telf']))
			  				{
			  					if(isset($insert_['id_operador'])){unset($insert_['id_operador']);}
			  					if(isset($insert_['celular'])){unset($insert_['celular']);}

			  					foreach ($contacto_['telf'] as $k_ => $val) 
				  				{
									$insert_['id_tipo'] = 2;
									$insert_['telefono'] = $val;
									$insert_['anexo'] = $contacto_['anex'][$k_];
				  					$err['contacto'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
				  				}
			  				}
			  			}
                    }
                }
                    
                $err['id'] = $id_persona_juridica;               
	            
	        }        
	        return $err;
	  	}

	  	public function get_one_personajud($param='')
	  	{
	  		if(!empty($param['id_persona_juridica']))
	  		{
	  			$w['pj.id_persona_juridica'] = $param['id_persona_juridica'];

	  			$rta = $this->db->select('pj.id_persona_juridica, pj.nombre_comercial, pj.razon_social, pj.ruc, pj.aniversario,  pj.es_cliente, pj.es_proveedor,  pj.estado')
	  							->from('tb_persona_juridica pj')
	  							->where($w)
	  							->get()
	  							->row_array();

	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function inactive_persona($id_persona= null)
	  	{
	  		if(!empty($id_persona))
	  		{
	  			$this->db->where('id_persona_juridica',$id_persona);
	  			return ($this->db->update('tb_persona_juridica',array("estado"=>'0'))) ? true : false;
	  		}
	  	}

	  	public function config($tab='', $id_persona='')
	  	{
	  		if(!empty($tab) && !empty($id_persona))
	  		{
	  			switch ($tab) 
	  			{
					  case 'general':
	  					$w['p.id_persona_juridica'] = $id_persona;
	  					
						$rt = $this->db->select('p.id_persona_juridica, p.ruc, p.razon_social, p.nombre_comercial, p.direccion_fiscal, p.email,  p.pagina_web, p.id_pais, p.id_departamento, p.id_provincia, p.id_distrito, p.aniversario, p.estado')
									   ->from('tb_persona_juridica p')
									   ->where($w)
									   ->get()
									   ->row_array();
						unset($w['p.id_persona_juridica']);
						$w['p.id_persona'] = $id_persona;
						$w['p.tipo_persona'] = 1;
						$w['p.id_tipo'] = 1;

						$rt['contacto_cel'] = $this->db->get_where('tb_contactopersona p',$w)->result_array();
						
						$w['p.id_tipo'] = 2;									
	  					$rt['contacto_telef'] = $this->db->get_where('tb_contactopersona p',$w)->result_array();

	  				break;
	  				case 'contacto':
	  					
	  				break;
	  			}

	  			return (!empty($rt)) ? $rt : null;
 	  		}
	  	}

		public function get_ruc($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona_juridica, razon_social, ruc, nombre_comercial')
								->from('tb_persona_juridica')
								->where($w)
								->like('ruc',$query);
				if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona_juridica',explode(",", $w_not));

				$rta = $rta->order_by('ruc','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$data[$key]['value'] = $value['ruc'].":".$value['razon_social'];
						$data[$key]['data']['id_persona_juridica'] = $value['id_persona_juridica'];
						$data[$key]['data']['ruc'] = $value['ruc'];
						$data[$key]['data']['razon_social'] = $value['razon_social'];
						$data[$key]['data']['nombre_comercial'] = $value['nombre_comercial'];
					}
				}

				return $data;
			}
		}

		public function get_nombrecomercial($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona_juridica, razon_social, ruc, nombre_comercial')
								->from('tb_persona_juridica')
								->where($w)
								->like('nombre_comercial',$query);
				if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona_juridica',explode(",", $w_not));

				$rta = $rta->order_by('ruc','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$data[$key]['value'] = $value['ruc'].":".$value['nombre_comercial'];
						$data[$key]['data']['id_persona_juridica'] = $value['id_persona_juridica'];
						$data[$key]['data']['ruc'] = $value['ruc'];
						$data[$key]['data']['razon_social'] = $value['razon_social'];
						$data[$key]['data']['nombre_comercial'] = $value['nombre_comercial'];
					}
				}

				return $data;
			}
		}

		public function get_razonsocial($query = null , $w_not = null)
		{
			if(!empty($query))
			{   
				$w['estado'] = 1;
				$w['es_proveedor'] = 1;

				$rta = $this->db->select('id_persona_juridica, razon_social, ruc, nombre_comercial')
								->from('tb_persona_juridica')
								->where($w)
								->like('razon_social',$query);
				if(!empty($w_not))
					$rta = $rta->where_not_in('id_persona_juridica',explode(",", $w_not));

				$rta = $rta->order_by('ruc','asc')->get()->result_array();
				//print_r($this->db->last_query());
				$data = array();

				if(!empty($rta))
				{   
					foreach ($rta as $key => $value) 
					{
						$data[$key]['value'] = $value['ruc'].":".$value['razon_social'];
						$data[$key]['data']['id_persona_juridica'] = $value['id_persona_juridica'];
						$data[$key]['data']['ruc'] = $value['ruc'];
						$data[$key]['data']['razon_social'] = $value['razon_social'];
						$data[$key]['data']['nombre_comercial'] = $value['nombre_comercial'];
					}
				}

				return $data;
			}
		}
	}
?>