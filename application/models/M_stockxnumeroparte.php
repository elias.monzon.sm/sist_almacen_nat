<?php

	class M_stockxnumeroparte extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

		public function buscar_stockxnumparte($param='')
	  	{
	  		if(!empty($param['id_codigo']))
	  		{
	  			$w['id_codigo'] = $param['id_codigo'];

	  			if(!empty($param['id_marca']))
	  			{
	  				$w['id_marca'] = $param['id_marca'];
	  			}

	  			$r = $this->db->select('id_kardex, fecha_ingreso, id_codigo, id_marca')
	  						  ->from('tb_kardex')
	  						  ->where($w)
	  						  ->order_by('id_kardex','asc')
	  						  ->get()
	  						  ->result_array();

	  			if(!empty($r))
	  			{
	  				foreach ($r as $v) 
	  				{
	  					$win[$v['id_codigo']][$v['id_marca']] = $v['id_kardex'];
	  				}
	  				if(!empty($win))
	  				{
	  					$win_ = [];
	  					foreach ($win as $arr) 
	  					{
	  						foreach ($arr as $idk) 
	  						{
	  							$win_[] = $idk;
	  						}
	  					}
	  				}

	  				$alldata = $this->db->select('k.id_kardex, k.id_codigo, k.id_marca, k.fecha_ingreso, k.stock, c.descripcion, m.marca, cv.precio_venta, round((k.stock)*(cv.precio_venta),3) valortotal, tm.simbolo, c.codigo')
	  									->from('tb_kardex k')
	  									->join('tb_codigo c','c.id_codigo=k.id_codigo','left')
	  									->join('tb_marca m','m.id_marca=k.id_marca','left')
	  									->join('tb_codigo_venta cv','k.id_codigo=cv.id_codigo and k.id_marca=cv.id_marca','left')
	  									->join('tb_tipomoneda tm','tm.id_tipomoneda=cv.id_tipomoneda','left')
	  									->where_in('k.id_kardex',$win_)
	  									->where_in('k.id_tipomovimiento',array('1','3','2'))
	  									->get()
	  									->result_array();
	  			}

	  			return (!empty($alldata)) ? $alldata : null;
	  		}
	  	}
	}
?>