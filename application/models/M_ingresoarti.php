<?php

	class M_ingresoarti extends CI_Model {

	  public function __construct() 
	  {
	    parent::__construct();
	  }

	  public function get_stockxarti($id_articulo=null)
	  {
	  	if(!empty($id_articulo))
	  	{
	  		$w['as.id_articulo'] = $id_articulo;

	  		$rta = $this->db->select('as.id_articulo, truncate(as.stock,2) stock, truncate(a.precio_unitario,2) precio_unitario, TRUNCATE((a.precio_unitario)*(as.stock),2) preciostock')
	  						->from('tb_articulo_stock as')
	  						->join('tb_articulo a','as.id_articulo=a.id_articulo','left')
	  						->where($w)
	  						->get()
	  						->row_array();

	  		return (!empty($rta)) ? $rta : null;
	  	}
	  }

	  public function save_add_stock($data=null)
	  {
	  	if(!empty($data['id_articulo']))
	  	{
	  		$datos_existentes = $this->get_stockxarti($data['id_articulo']);
	  		if(!empty($datos_existentes['stock']))
	  		{
		  		$data_insert=array();
	  			$data_insert['stock'] = $datos_existentes['stock'] + $data['stock'];
	  			$resp =($this->db->where("id_articulo",$data['id_articulo'])
	  					 ->update('tb_articulo_stock',$data_insert)) ? true : false;
	  		}
	  		else
	  		{
	  			$resp = ($this->db->insert('tb_articulo_stock',$data)) ? true: false;
	  		}

	  		return $resp;
	  	}
	  }
	}

?>