<?php

	class M_configempresa extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function save_miempresa($data= null)
	  	{
	  		$rta['type'] = '';
	  		$rta['id_persona_juridica'] = '';
	  		$rta['empresa'] = '';

	  		if(!empty($data))
	  		{
	  			$id_persona_juridica = $data['id_persona_juridica'];
	  			$data['aniversario'] = (!empty($data['aniversario'])) ? date('Y-m-d',strtotime($data['aniversario'])) : null;
	  			unset($data['id_persona_juridica']);
	  			unset($data['id_departamento']);
	  			unset($data['id_provincia']);

	  			$Ok = false;
	  			//Verifica si hay un ruc ya existente

	  			$w['ruc'] = $data['ruc'];
  				if($id_persona_juridica>0)
  					$w['id_persona_juridica =!'] = $id_persona_juridica;

  				$this->db->select('id_persona_juridica')
  						->from('tb_persona_juridica')
  						->where($w);
  				if($this->db->count_all_results()>0)
  					$rta['type'] = 'Error, ya existe';
  				else
  					$Ok = true;

  				if($Ok==true)
  				{
  					$contacto_ = (isset($data['contacto'])) ? $data['contacto'] : null;
  					unset($data['contacto']);
		  			if($id_persona_juridica>0)
		  			{
						$this->db->where('id_persona_juridica',$id_persona_juridica);

						$rta['type'] = 'Edit';
						$rta['id_persona_juridica'] = ($this->db->update('tb_persona_juridica',$data)) ? $id_persona_juridica : null;
		  				$rta['empresa'] = (!empty($rta['id_persona_juridica'])) ? 'SUCCESS' : 'ERROR';
		  				$this->db->delete('tb_contactopersona',array("id_persona"=>$id_persona_juridica, "tipo_persona"=>"2"));
		  			}
		  			else 
		  			{
	  					//Si no hay procede a guardar los datos en la tabla
	  					$rta['type'] = 'New';
	  					$data['estado'] = 1;
	  					$rta['id_persona_juridica'] = ($this->db->insert('tb_persona_juridica',$data))? $this->db->insert_id() : null;
	  					
	  					if(!empty($rta['id_persona_juridica']))
	  					{
	  						$data_['id_persona_juridica'] = ($this->db->empty_table('tb_miempresa')) ? $rta['id_persona_juridica'] : null;
	  						$data_['estado'] = 1;
	  						$rta['empresa'] = ($this->db->insert('tb_miempresa',$data_)) ? 'SUCCESS' : 'ERROR';
	  					}
		  			}
		  			//print_r($contacto_);
		  			if(!empty($contacto_) && $rta['empresa']=="SUCCESS")
		  			{
		  				$insert_['id_persona'] = $rta['id_persona_juridica'];
		  				$insert_['tipo_persona'] = 2;
		  				if(!empty($contacto_['cel']))
		  				{
		  					foreach ($contacto_['cel'] as $k_ => $val) 
			  				{
								$insert_['id_operador'] = $contacto_['op'][$k_];
								$insert_['id_tipo'] = 1;
								$insert_['celular'] = $val;
			  					$rta['empresa'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
			  				}
		  				}

		  				if(!empty($contacto_['telf']))
		  				{
		  					if(isset($insert_['id_operador'])){unset($insert_['id_operador']);}
		  					if(isset($insert_['celular'])){unset($insert_['celular']);}

		  					foreach ($contacto_['telf'] as $k_ => $val) 
			  				{
								$insert_['id_tipo'] = 2;
								$insert_['telefono'] = $val;
								$insert_['anexo'] = $contacto_['anex'][$k_];
			  					$rta['empresa'] = ($this->db->insert('tb_contactopersona',$insert_)) ? 'SUCCESS' : 'ERROR';
			  				}
		  				}
		  			}
		  		}
	  		}
	  		return $rta;
	  	}

	  	public function get_miempresa()
	  	{
	  		$r1 = $this->db->select('id_persona_juridica')
	  					   ->from('tb_miempresa')
	  					   ->get()
	  					   ->row_array();
	  		if(!empty($r1['id_persona_juridica']))
	  		{
	  			$w['id_persona_juridica'] = $r1['id_persona_juridica'];

	  			$rta = $this->db->select('id_persona_juridica, ruc, razon_social, nombre_comercial, direccion_fiscal,  id_distrito, pagina_web, aniversario, email')
	  							->from('tb_persona_juridica')
	  							->where($w)
	  							->get()
	  							->row_array();

	  			$w2_['id_persona'] = $r1['id_persona_juridica'];
	  			$w2_['tipo_persona'] = 2;
	  			$w2_['id_tipo'] = 1;
	  			$rta['contacto_cel'] = $this->db->get_where('tb_contactopersona',$w2_)->result_array();

	  			$w2_['id_persona'] = $r1['id_persona_juridica'];
	  			$w2_['tipo_persona'] = 2;
	  			$w2_['id_tipo'] = 2;
	  			$rta['contacto_tlf'] = $this->db->get_where('tb_contactopersona',$w2_)->result_array();
	  			return (!empty($rta)) ? $rta : null;
	  		}
	  	}
	}
?>