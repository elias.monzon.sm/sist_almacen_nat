<?php

class M_usuarios extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_usuarios($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_usuario = $data['id_usuario'];
                unset($data['id_usuario']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['usuario']))
                {
                    $whe['usuario'] = trim($data['usuario']);

                    $query = $this->db->select('id_usuario');
                    $form = $query->from('tb_usuarios');
                    
                    if($id_usuario>0)
                    {
                        $whe['id_usuario !='] = $id_usuario;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_usuario==0)
                    {
                        $id_usuario = ($this->db->insert('tb_usuarios', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_usuario) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_usuario', $id_usuario);
                        $err['error_code'] = ($this->db->update('tb_usuarios', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_usuario;               
            }
        }        
        return $err;
    }

    public function get_one_usuario($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_usuario, alm.usuario, alm.estado, md5(alm.pwd) pwd')
                              ->from('tb_usuarios alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_usuario']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function deltete_usuario($data = "")
    {
        if($data === FALSE) {}
        else
        {
            $id_usuario = (isset($data['id_usuario']) && $data['id_usuario']>0) ? ($data['id_usuario']) : (0);
            if($id_usuario >0)
            {
                unset($data['id_usuario']);
                $fecha = date("Y-m-d H:i:s");

                $data['fecha_modificacion'] = $fecha;
                $data['id_usuario_modificado'] = $this->id_user;

                $this->db->where('id_usuario', $data['id_usuario']);
                $id_usuario = ($this->db->update('tb_usuarios', $data)) ? ($data['id_usuario']) : (FALSE);
            }                
        }
        return (isset($id_usuario) && $id_usuario>0) ? (TRUE) : (FALSE);
    }

    public function buscar_usuarios($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $limit = $this->result_limit;

            $query = $this->db->select('u.id_usuario, u.usuario, u.pwd, u.estado')
                              ->from('tb_usuarios u');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['usuarios']) && strlen(trim($param['usuarios']))>0)
                $query = $query->like(array("usuarios"=>$param['usuarios']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('u.usuario', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('u.id_usuario')
                              ->from('tb_usuarios u');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("usuario"=>$param['alm']));

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function validar_usuario($id_usuario = "", $usuario)
    {
        $success = FALSE;
        if($usuario === "") {}
        else
        {
            $whe['usuario'] = trim($usuario);

            $query = $this->db->select('id_usuario');
            $form = $query->from('tb_usuarios');                     
            if($id_usuario>0)
            {
                $whe['id_usuario !='] = $id_usuario;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function add_permiso($data = null)
    {
        if(!empty($data['id_usuario']))
        {
            return ($this->db->insert('tb_usuarios_menu',$data)) ? $this->db->insert_id() : false;
        }
    }

    public function delete_permiso($data = null)
    {
        if(!empty($data['id_usuario']))
        {
            $this->db->where($data);
            return ($this->db->delete('tb_usuarios_menu')) ? true : false;
        }
    }

    public function menus_permitidos($id_usuario='')
    {
        if(!empty($id_usuario))
        {
            $w['id_usuario'] = $id_usuario;

            $data = $this->db->select('id_menu')
                             ->from('tb_usuarios_menu')
                             ->where($w)
                             ->get()
                             ->result_array();

            if(!empty($data))
            {
                foreach ($data as $value) 
                {
                    $rta[$value['id_menu']] = $value['id_menu'];
                }
            }

            return (!empty($rta)) ? $rta : null;
        }
    }
    
}