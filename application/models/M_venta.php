<?php

	class M_venta extends CI_Model {

	  public function __construct() 
	  {
	    parent::__construct();
	  }

	  public function get_ventas($param = '')
	  {
	  	$rta = $this->db->select('a.articulo, v.precio_unitario, v.cantidad, v.precio_total, v.fecha_venta')
	  					->from('tb_venta_detalle v')
	  					->join('tb_articulo a','v.id_articulo=a.id_articulo')
	  					->get()
	  					->result_array();

	  	return $rta;
	  }

	  public function save_articulo($data)
	  {
	  	if(!empty($data))
	  	{	
	  		$this->load->model("m_ingresoarti");
	  		$fecha = date('Y-m-d H:i:s');
	  		foreach ($data['arti'] as $id_articulo) 
	  		{
	  			$dato = array();
	  			$dato['id_articulo'] = $id_articulo;
	  			$dato['precio_unitario'] = $data['preu'][$id_articulo];
	  			$dato['precio_total'] = $data['pret'][$id_articulo];
	  			$dato['cantidad'] = $data['cant'][$id_articulo];
	  			$dato['fecha_venta'] = $fecha;
	  			//print_r($dato);
	  			$resp = ($this->db->insert('tb_venta_detalle',$dato)) ? true : false;
	  			if($resp==true)
	  			{	
	  				$r = $this->m_ingresoarti->get_stockxarti($id_articulo);
	  				$d2['stock'] = $r['stock'] - $dato['cantidad'];
	  				$this->db->where('id_articulo',$id_articulo)->update('tb_articulo_stock',$d2);
	  			}
	  		}
	  	}

	  	return $resp;
	  }
	}


?>