<?php

	class M_articulo extends CI_Model {

	  public function __construct() 
	  {
	    parent::__construct();
	  }

	  public function buscar_articulos($param = "")
	  {
	  	$w = array();
	  	$like = array();
	  	if(!empty($param['id_marca']))
	  	{
	  		$w['ma.id_marca'] = $param['id_marca'];
	  	}

	  	if(!empty($param['id_cod']))
	  	{
	  		$w['c.id_codigo'] = $param['id_cod'];
	  	}

	  	if(!empty($param['arti']))
	  	{
	  		$like['cod.articulo'] = $param['arti'];
	  	}

	  	$rta = $this->db->select('cod.id_articulo, cod.articulo, ma.marca, c.codigo, cod.precio_unitario')
	  					->from('tb_articulo cod')
	  					->join('tb_marca ma','cod.id_marca=ma.id_marca','left')
	  					->join('tb_codigo c','c.id_codigo=cod.id_codigo','left')
	  					->where($w)
	  					->like($like)
	  					->order_by('ma.marca','asc')
	  					->order_by('c.codigo','asc')
	  					->get()
	  					->result_array();
	  	return $rta;
	  }

	  public function save_articulo($data = null)
	  {
	  	if(!empty($data))
	  	{
	  		$id_articulo = ($this->db->insert('tb_articulo', $data)) ? ($this->db->insert_id()) : (FALSE);

	  		return $id_articulo;
	  	}
	  }
	}

?>