<?php

	class M_reporteventas extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function buscar_ventas($param = null)
	  	{
	  		if(!empty($param))
	  		{
	  			$w['date(fecha_ingreso) >='] = date('Y-m-d',strtotime($param['fecha_ini']));
	  			$w['date(fecha_ingreso) <='] = date('Y-m-d',strtotime($param['fecha_fin']));

	  			$rta = $this->db->select('ven.id_venta, ven.codigo, ven.serie, ven.fecha_ingreso, m.tipomoneda, m.simbolo, ROUND(ven.precio_total,3) precio_total, IF(ven.tipo_persona=2,(select pj.razon_social from tb_persona_juridica pj where ven.id_persona=pj.id_persona_juridica),(select CONCAT_WS(",",pn.apellidos,pn.nombres) from tb_persona pn where pn.id_persona=ven.id_persona)) cliente, td.tipodocumento')
	  							->from('tb_ventas ven')
	                            ->join('tb_tipomoneda m','ven.id_tipomoneda=m.id_tipomoneda','left')
	                            ->join('tb_tipodocumento td','td.id_tipodocumento=ven.id_tipodocumento','left')
	                            ->where($w)
	                            ->get()
	                            ->result_array();
	  		}

	  		return (!empty($rta)) ? $rta : null;
	  	}
	}
?>