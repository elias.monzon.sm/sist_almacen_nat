<?php

class M_tipomoneda extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_tipomoneda($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_tipomoneda = $data['id_tipomoneda'];
                unset($data['id_tipomoneda']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['tipomoneda']))
                {
                    $whe['tipomoneda'] = trim($data['tipomoneda']);

                    $query = $this->db->select('id_tipomoneda');
                    $form = $query->from('tb_tipomoneda');
                    
                    if($id_tipomoneda>0)
                    {
                        $whe['id_tipomoneda !='] = $id_tipomoneda;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_tipomoneda==0)
                    {
                        $id_tipomoneda = ($this->db->insert('tb_tipomoneda', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_tipomoneda) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_tipomoneda', $id_tipomoneda);
                        $err['error_code'] = ($this->db->update('tb_tipomoneda', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_tipomoneda;               
            }
        }        
        return $err;
    }

    public function get_one_tipomoneda($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_tipomoneda, alm.factor_cambio, alm.tipomoneda, alm.estado, alm.abreviatura, alm.simbolo')
                              ->from('tb_tipomoneda alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_tipomoneda']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function deltete_tipomoneda($data = "")
    {
        if($data === FALSE) {}
        else
        {
            $id_tipomoneda = (isset($data['id_tipomoneda']) && $data['id_tipomoneda']>0) ? ($data['id_tipomoneda']) : (0);
            if($id_tipomoneda >0)
            {
                unset($data['id_tipomoneda']);
                $fecha = date("Y-m-d H:i:s");

                $data['fecha_modificacion'] = $fecha;
                $data['id_usuario_modificado'] = $this->id_user;

                $this->db->where('id_tipomoneda', $data['id_tipomoneda']);
                $id_tipomoneda = ($this->db->update('tb_tipomoneda', $data)) ? ($data['id_tipomoneda']) : (FALSE);
            }                
        }
        return (isset($id_tipomoneda) && $id_tipomoneda>0) ? (TRUE) : (FALSE);
    }

    public function buscar_tipomonedas($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $limit = $this->result_limit;

            $query = $this->db->select('alm.id_tipomoneda, alm.factor_cambio, alm.tipomoneda, alm.estado, alm.abreviatura, alm.simbolo')
                              ->from('tb_tipomoneda alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("tipomoneda"=>$param['alm']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('alm.tipomoneda', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('alm.id_tipomoneda')
                              ->from('tb_tipomoneda alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("tipomoneda"=>$param['alm']));

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function validar_tipomoneda($id_tipomoneda = "", $tipomoneda)
    {
        $success = FALSE;
        if($tipomoneda === "") {}
        else
        {
            $whe['tipomoneda'] = trim($tipomoneda);

            $query = $this->db->select('id_tipomoneda');
            $form = $query->from('tb_tipomoneda');                     
            if($id_tipomoneda>0)
            {
                $whe['id_tipomoneda !='] = $id_tipomoneda;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_tipomoneda_autocomplete($query = null , $w_not = null)
    {
        if(!empty($query))
        {   
            $w['estado'] = 1;
            $rta = $this->db->select('id_tipomoneda, tipomoneda')
                            ->from('tb_tipomoneda')
                            ->where($w)
                            ->like('tipomoneda',$query);
            if(!empty($w_not))
                $rta = $rta->where_not_in('id_tipomoneda',explode(",", $w_not));

            $rta = $rta->order_by('tipomoneda','asc')->get()->result_array();

            $data = array();
            if(!empty($rta))
            {   
                foreach ($rta as $key => $value) 
                {
                    $data[$key]['value'] = $value['tipomoneda'];
                    $data[$key]['data'] = $value['id_tipomoneda'];
                }
            }

            return $data;
        }
    }

    public function alltipomonedas()
    {
        $r = $this->db->select('id_tipomoneda, tipomoneda')
                      ->from('tb_tipomoneda')
                      ->order_by('tipomoneda')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $v) 
            {
                $data[$v['id_tipomoneda']] = $v['tipomoneda'];
            }
        }

        return (!empty($data)) ? $data : null;
    }

    public function cbx_tipomoneda($sele='')
    {
        $w['estado'] = 1;
        $r= $this->db->select('id_tipomoneda, tipomoneda, factor_cambio, simbolo')
                    ->from('tb_tipomoneda')
                    ->where($w)
                    ->get()
                    ->result_array();

        $tx = "<option>Crear Tipo de Moneda</option>";
        if(!empty($r))
        {
            $cl_ = (empty($sele)) ? "selected" : null;
            $tx = "<option disabled ".$cl_.">Seleccione Tipo de Moneda</option>";            
            foreach ($r as $value) 
            {
                $cl = ($value['id_tipomoneda']==$sele) ? "selected" : null;
                $tx .="<option value=".$value['id_tipomoneda']." ".$cl." sign='".$value['simbolo']."' factor='".$value['factor_cambio']."'>".$value['tipomoneda']."</option>";
            }
        }

        return $tx;
    }
}