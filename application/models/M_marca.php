<?php

class M_marca extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_marca($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_marca = $data['id_marca'];
                unset($data['id_marca']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['marca']))
                {
                    $whe['marca'] = trim($data['marca']);

                    $query = $this->db->select('id_marca');
                    $form = $query->from('tb_marca');
                    
                    if($id_marca>0)
                    {
                        $whe['id_marca !='] = $id_marca;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_marca==0)
                    {
                        $id_marca = ($this->db->insert('tb_marca', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_marca) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_marca', $id_marca);
                        $err['error_code'] = ($this->db->update('tb_marca', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_marca;               
            }
        }        
        return $err;
    }

    public function get_one_marca($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_marca, alm.marca, alm.estado')
                              ->from('tb_marca alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_marca']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function deltete_marca($data = "")
    {
        if($data === FALSE) {}
        else
        {
            $id_marca = (isset($data['id_marca']) && $data['id_marca']>0) ? ($data['id_marca']) : (0);
            if($id_marca >0)
            {
                unset($data['id_marca']);
                $fecha = date("Y-m-d H:i:s");

                $data['fecha_modificacion'] = $fecha;
                $data['id_usuario_modificado'] = $this->id_user;

                $this->db->where('id_marca', $data['id_marca']);
                $id_marca = ($this->db->update('tb_marca', $data)) ? ($data['id_marca']) : (FALSE);
            }                
        }
        return (isset($id_marca) && $id_marca>0) ? (TRUE) : (FALSE);
    }

    public function buscar_marcas($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $query = $this->db->select('alm.id_marca, alm.marca, alm.estado')
                              ->from('tb_marca alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("marca"=>$param['alm']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('alm.marca', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('alm.id_marca')
                              ->from('tb_marca alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("marca"=>$param['alm']));

            $total_registros = $query->count_all_results();

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }


    public function validar_marca($id_marca = "", $marca)
    {
        $success = FALSE;
        if($marca === "") {}
        else
        {
            $whe['marca'] = trim($marca);

            $query = $this->db->select('id_marca');
            $form = $query->from('tb_marca');                     
            if($id_marca>0)
            {
                $whe['id_marca !='] = $id_marca;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_marca_autocomplete($query = null , $w_not = null)
    {
        if(!empty($query))
        {   
            $w['estado'] = 1;
            $rta = $this->db->select('id_marca, marca')
                            ->from('tb_marca')
                            ->where($w)
                            ->like('marca',$query);
            if(!empty($w_not))
                $rta = $rta->where_not_in('id_marca',explode(",", $w_not));

            $rta = $rta->order_by('marca','asc')->get()->result_array();

            $data = array();
            if(!empty($rta))
            {   
                foreach ($rta as $key => $value) 
                {
                    $data[$key]['value'] = $value['marca'];
                    $data[$key]['data'] = $value['id_marca'];
                }
            }

            return $data;
        }
    }

    public function allmarcas()
    {
        $r = $this->db->select('id_marca, marca')
                      ->from('tb_marca')
                      ->order_by('marca')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $v) 
            {
                $data[$v['id_marca']] = $v['marca'];
            }
        }

        return (!empty($data)) ? $data : null;
    }

    function get_marcaxcodigo($param = null)
    {
        if(!empty($param['id_codigo']))
        {
            $w['id_codigo'] = $param['id_codigo'];

            $r = $this->db->select('id_codigo, id_marca')
                          ->from('tb_codigo')
                          ->where($w)
                          ->get()
                          ->row_array();

            if(!empty(strlen($r['id_marca'])))
            {
                $marcas = $this->db->select('id_marca, marca')
                                   ->from('tb_marca')
                                   ->where_in('id_marca',explode(",",$r['id_marca']))
                                   ->get()
                                   ->result_array();
            }
        }

        return (!empty($marcas)) ? $marcas : null;
    }
}