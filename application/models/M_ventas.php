<?php

class M_ventas extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function buscar_documentoventa($param='')
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $limit = $this->result_limit;

            $query = $this->db->select('v.id_venta, v.codigo, v.serie, v.fecha_ingreso, (select CONCAT_WS(":",tp.tipomoneda,tp.simbolo) from tb_tipomoneda tp where tp.id_tipomoneda=v.id_tipomoneda) tipomoneda, ROUND(v.precio_total,3) precio_total, IF(v.tipo_persona=2,(select pj.razon_social from tb_persona_juridica pj where v.id_persona=pj.id_persona_juridica),(select CONCAT_WS(" ",pn.apellidos,pn.nombres) from tb_persona pn where pn.id_persona=v.id_persona)) cliente')
                              ->from('tb_ventas v');
            if(!empty($whe))
                $query = $query->where($whe);

            $query = $query->limit($limit, $pages)
                           ->order_by('v.fecha_ingreso', 'DESC');
            $rta['all_data'] = $query->get()->result_array();
            $query = $this->db->select('v.id_venta')
                              ->from('tb_ventas v');
            if(!empty($whe))
                $query = $query->where($whe);

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function get_docu($id_documento)
    {
        if(!empty($id_documento))
        {
            $w['ven.id_venta'] = $id_documento;

            $rta['main'] = $this->db->select('IF(ven.tipo_persona=1,(SELECT CONCAT_WS(",",p.nombres,p.apellidos) FROM tb_persona p WHERE ven.id_persona=p.id_persona),(SELECT pj.nombre_comercial FROM tb_persona_juridica pj WHERE ven.id_persona=pj.id_persona_juridica)) cliente, ven.fecha_ingreso, ven.codigo, ven.serie, td.tipodocumento, m.tipomoneda, ven.factor_impuesto, ven.precio_total, m.simbolo')
                            ->from('tb_ventas ven')
                            ->join('tb_tipomoneda m','ven.id_tipomoneda=m.id_tipomoneda','left')
                            ->join('tb_tipodocumento td','td.id_tipodocumento=ven.id_tipodocumento','left')
                            ->where($w)
                            ->get()
                            ->row_array();

            if(!empty($rta['main']))
            {
                $w_['det.id_venta'] = $id_documento;
                $w_['det.es_articulo'] = 1;

                $sql1 = $this->db->select('cod.codigo, cod.descripcion_completa, ma.marca, det.cantidad, det.precio_unitario, det.precio_total, alm.almacen')
                                ->from('tb_ventas_det det')
                                ->join('tb_marca ma','det.id_marca=ma.id_marca','left')
                                ->join('tb_codigo cod','cod.id_codigo=det.id_codigo','left')
                                ->join('tb_almacen alm','alm.id_almacen=det.id_almacen','left')
                                ->where($w_)
                                ->get_compiled_select();

                $w_['det.es_articulo'] = 2;

                $sql2 = $this->db->select(' "" codigo, s.servicio descripcion_completa, "" marca, det.cantidad, det.precio_unitario, det.precio_total, "" almacen')
                                ->from('tb_ventas_det det')
                                ->join('tb_servicios s','s.id_servicio=det.id_servicio')
                                ->where($w_)
                                ->get_compiled_select();

                $sql = "select * from ((".$sql1.") union all (".$sql2.")) usd;";
                $rta['main']['det'] = $this->db->query($sql)->result_array();
            }

            return(!empty($rta['main'])) ? $rta : null;
        }
    }

    public function get_artiventa($param='')
    {
        if(isset($param['page']))
        {   
            $page = $param['page'];
            unset($param['page']);      

            $limit = $this->result_limit;
            $pages = $page*$limit;

            $w['codp.estado'] = 1;
            $w['cod.estado'] = 1;

            $wnotline = "";
            if(!empty($param['w_not']))
            {
                $wnot = "((".$param['w_not']."))";
                $wnotline = " AND (codp.id_codigo,codp.id_marca) NOT IN".$wnot;
            }

            $like = '';
            if(!empty($param['marca']))
            {
                $like .=' AND ma.marca like "%'.$param['marca'].'%"';
            }

            if(!empty($param['descripcion']))
            {
                $like .=' AND cod.descripcion like "%'.$param['descripcion'].'%"';
            }

            if(!empty($param['descripcion_2']))
            {
                $like .=' AND cod.descripcion_2 like "%'.$param['descripcion_2'].'%"';
            }

            if(!empty($param['codigo']))
            {
                $like .=' AND cod.codigo like "%'.$param['codigo'].'%"';
            }

            $query = "SELECT cod.id_codigo, codp.id_marca , ma.marca, cod.codigo, cod.descripcion, cod.descripcion_2
                      FROM tb_codigo_venta codp
                      LEFT JOIN tb_codigo cod on cod.id_codigo=codp.id_codigo
                      LEFT JOIN tb_marca ma on ma.id_marca=codp.id_marca
                      WHERE cod.estado=1 AND codp.id_tipomoneda=".$param['id_tipomoneda'].$wnotline.$like; 
            $query_limited = $query." limit ".$pages.",".$limit;
                      

            $rta['all_data'] = $this->db->query($query_limited)->result_array();

            $total_registros =  count($this->db->query($query)->result_array());

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
            
            return (!empty($rta['all_data'])) ? $rta : null;
        }
    }

    public function get_serventa($param='')
    {
        if(isset($param['page']))
        {   
            $page = $param['page'];
            unset($param['page']);      

            $limit = $this->result_limit;
            $pages = $page*$limit;

            $w['s.estado'] = 1;

            if(!empty($param['servicio']))
            {
                $like['s.servicio'] = $param['servicio'];
            }

            $rta['all_data'] = $this->db->select('s.id_servicio, s.servicio')
                              ->from('tb_servicios s')
                              ->where($w);
            if(!empty($like))
            {
                $rta['all_data'] = $rta['all_data']->like($like);
            }
                $rta['all_data'] = $rta['all_data']->limit($limit,$pages)
                              ->get()
                              ->result_array();
                      

            $total_registros =  $this->db->select('s.id_servicio')
                                        ->from('tb_servicios s')
                                        ->where($w);
            if(!empty($like))
            {
                $total_registros = $total_registros->like($like);
            }
                $total_registros = $total_registros->count_all_results();

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
            return (!empty($rta['all_data'])) ? $rta : null;
        }
    }

    public function get_addartiventa($param = '')
    {
        if(!empty($param['id_codigo']) && !empty($param['id_marca']))
        {
            $w['v.id_codigo'] = $param['id_codigo'];
            $w['v.id_marca'] = $param['id_marca'];

            $rta['precio_uni'] = $this->db->select('tm.simbolo, tm.factor_cambio, v.precio_venta')
                                          ->from('tb_codigo_venta v')
                                          ->join('tb_tipomoneda tm','v.id_tipomoneda=tm.id_tipomoneda','left')
                                          ->where($w)
                                          ->get()
                                          ->row_array();
            $this->load->model("m_almacen");
            $rta['cbx'] = $this->m_almacen->get_almxstockcod(array("id_codigo"=>$param['id_codigo'], "id_marca" => $param['id_marca'])); 
        }
        return (!empty($rta)) ? $rta : null;
    }
    
    public function get_addserviventa($param = '')
    {
        if(!empty($param['id_servicio']))
        {
            $w['s.id_servicio'] = $param['id_servicio'];
            $rta = $this->db->select('tm.simbolo, tm.factor_cambio')
                              ->from('tb_servicios s')
                              ->join('tb_tipomoneda tm','tm.id_tipomoneda=s.id_tipomoneda','left')
                              ->where($w)
                              ->get()
                              ->row_array();
        }

        return (!empty($rta)) ? $rta : null;
    }

    public function get_clientes($param='')
    {
        if(isset($param['page']))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;
            
            $whe['pn.estado'] = 1;
            $whe['pn.es_cliente'] = 1;

            if(!empty($param['ruc_dni']))
            {
                $liekruc = trim($param['ruc_dni']);
            }

            if(!empty($param['razon_soc']))
            {
                $likerz = trim($param['razon_soc']);
            }

            if(isset($param['nombres_com']) && strlen(trim($param['nombres_com']))>0)
            {
                $likenomb = trim($param['nombres_com']);   
            }

            $sq = $this
                        ->db
                        ->select("pn.id_persona, 1 tipo_persona, pn.nombres nombre_comercial, pn.apellidos razon_social, IF(pn.id_documentoidentidad=1,pn.dni,LPAD(pn.dni_genera,8,'0')) ruc")
                        ->from('tb_persona pn')
                        ->where($whe);

            if(isset($liekruc))
            {
                $sq = $sq->group_start()
                            ->like('pn.dni', $liekruc)
                        ->group_end();
            }

            if(isset($likerz))
            {
                $sq = $sq->group_start()
                            ->like('pn.nombres', $likerz)
                        ->group_end();
            }

            if(isset($likenomb))
            {
                $sq = $sq->group_start()
                            ->like('pn.apellidos', $likenomb)
                        ->group_end();
            }

            $query1 = $sq->get_compiled_select();

            $whe2['pj.estado'] = 1;
            $whe2['pj.es_cliente'] = 1;

            $sq = $this
                    ->db
                    ->select("pj.id_persona_juridica id_persona, 2 tipo_persona, nombre_comercial, razon_social, ruc")
                    ->from('tb_persona_juridica pj')
                    ->where($whe2);

            if(isset($liekruc))
            {
                $sq = $sq->group_start()
                            ->like('pj.ruc', $liekruc)
                        ->group_end();
            }

            if(isset($likerz))
            {
                $sq = $sq->group_start()
                            ->like('pj.razon_social', $likerz)
                        ->group_end();
            }

            if(isset($likenomb))
            {
                $sq = $sq->group_start()
                            ->like('pj.nombre_comercial', $likenomb)
                        ->group_end();
            }
        
            $query2 = $sq->get_compiled_select();

            $sql = "select * from ((".$query1.") union all (".$query2.")) usi ORDER BY nombre_comercial desc limit ".$pages.", ".$limit.";";
            $sq2 = "select * from ((".$query1.") union all (".$query2.")) usi";

            $rta['all_data'] = $this->db->query($sql)->result_array();
            $rta['total_registros'] = count($this->db->query($sq2)->result_array());
            $rta['cantidad_pag'] = ceil($rta['total_registros']/$limit);
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
        
    }

    public function save_salidaxventa($data=null)
    {
        if(!empty($data))
        {

            $fecha = date('Y-m-d H:i:s');
            $venta['codigo'] = (!empty($data['codigo'])) ? $data['codigo'] : null;
            $venta['id_tipomoneda'] = (!empty($data['id_tipomoneda'])) ? $data['id_tipomoneda'] : null;
            $venta['serie'] = (!empty($data['serie'])) ? $data['serie'] : null;
            $venta['fecha_ingreso'] = (!empty($data['fecha_ingreso'])) ? date('Y-m-d',strtotime($data['fecha_ingreso'])) : null;
            $venta['id_persona'] = (!empty($data['id_persona'])) ? $data['id_persona'] : null;
            $venta['tipo_persona'] = (!empty($data['tipo_persona'])) ? $data['tipo_persona'] : null;
            $venta['id_tipodocumento'] = (!empty($data['id_tipodocumento'])) ? $data['id_tipodocumento'] : null;
            $venta['factor_impuesto'] = (!empty($data['factor_impuesto'])) ? $data['factor_impuesto'] : null;
            $venta['precio_total'] = (!empty($data['p_total'])) ? $data['p_total'] : null;
            $venta['afecta_kardex'] = (!empty($data['afecta_kardex'])) ? $data['afecta_kardex'] : null;
            $venta['fecha_creacion'] = date('Y-m-d H:i:s');
            $venta_det['id_venta'] = ($this->db->insert('tb_ventas',$venta)) ? $this->db->insert_id() : null;

            if(!empty($venta_det['id_venta']))
            {
                if(!empty($data['pu']))
                {
                    foreach ($data['pu'] as $id_codigo => $dta) 
                    {
                        $venta_det['es_articulo'] = 1;
                        foreach ($dta as $id_marca => $val) 
                        {
                            $venta_det['id_codigo'] = $id_codigo;
                            $venta_det['id_marca'] = $id_marca;
                            $venta_det['cantidad'] = $data['cant'][$id_codigo][$id_marca];
                            $venta_det['precio_unitario'] = $val;
                            $venta_det['precio_total'] = $data['pt'][$id_codigo][$id_marca];
                            $venta_det['id_almacen'] = $data['alm'][$id_codigo][$id_marca];
                            $venta_det['factor_cambio'] = $data['factor_cambio'][$id_codigo][$id_marca];

                            $id_venta_det = ($this->db->insert('tb_ventas_det',$venta_det)) ? $this->db->insert_id() : null;
                            if(!empty($venta['afecta_kardex']))
                            {
                                $w['id_codigo'] = $venta_det['id_codigo'];
                                $w['id_marca'] = $venta_det['id_marca'];
                                $w['id_almacen'] = $venta_det['id_almacen'];

                                $stock = $this->db->select('stock')
                                                  ->from('tb_kardex')
                                                  ->where($w)
                                                  ->order_by('fecha_ingreso','desc')
                                                  ->get()
                                                  ->row_array();
                                if(!empty($stock['stock']))
                                {
                                    $kardex = $w;
                                    $kardex['stock'] = $stock['stock'] - $venta_det['cantidad'];
                                    $kardex['txt_tipo'] = "Salida";
                                    $kardex['tipo'] = 2;
                                    $kardex['id_documento_det'] = $id_venta_det;
                                    $kardex['id_tipomovimiento'] = 2;
                                    $kardex['cantidad'] = $venta_det['cantidad'];
                                    $kardex['fecha_ingreso'] = $fecha;
                                    $kardex['factor_cambio_moneda'] = $venta_det['factor_cambio'];
                                    $id_kardex = ($this->db->insert('tb_kardex',$kardex)) ? $this->db->insert_id() : null;
                                }
                            }
                        }
                    }
                }

                if(!empty($data['puserv']))
                {
                    $venta_detserv['id_venta'] = $venta_det['id_venta'];
                    $venta_detserv['es_articulo'] = 2;
                    foreach ($data['puserv'] as $id_serv => $pu) {
                        $venta_detserv['precio_unitario'] = $pu;
                        $venta_detserv['id_servicio'] = $data['serv'][$id_serv];
                        $venta_detserv['cantidad'] =  $data['cantserv'][$id_serv];
                        $venta_detserv['precio_total'] = $data['ptserv'][$id_serv];
                        $venta_detserv['factor_cambio'] = $data['servfactor_cambio'][$id_serv];
                        $id_venta_det = ($this->db->insert('tb_ventas_det',$venta_detserv)) ? $this->db->insert_id() : null;
                    }
                }
            }
        }

        return (!empty($id_venta_det)) ? true : false;
    }
}