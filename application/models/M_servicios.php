<?php

class M_servicios extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_servicio($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_servicio = $data['id_servicio'];
                unset($data['id_servicio']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";

                if(isset($data['servicio']))
                {
                    $whe['servicio'] = trim($data['servicio']);

                    $query = $this->db->select('id_servicio');
                    $form = $query->from('tb_servicios');
                    
                    if($id_servicio>0)
                    {
                        $whe['id_servicio !='] = $id_servicio;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_servicio==0)
                    {
                        $id_servicio = ($this->db->insert('tb_servicios', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_servicio) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_servicio', $id_servicio);
                        $err['error_code'] = ($this->db->update('tb_servicios', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_servicio;               
            }
        }        
        return $err;
    }

    public function get_one_servicio($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_servicio, alm.servicio, alm.estado, alm.id_tipomoneda')
                              ->from('tb_servicios alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_servicio']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function deltete_servicio($data = "")
    {
        if($data === FALSE) {}
        else
        {
            $id_servicio = (isset($data['id_servicio']) && $data['id_servicio']>0) ? ($data['id_servicio']) : (0);
            if($id_servicio >0)
            {
                unset($data['id_servicio']);
                $fecha = date("Y-m-d H:i:s");

                $data['fecha_modificacion'] = $fecha;
                $data['id_usuario_modificado'] = $this->id_user;

                $this->db->where('id_servicio', $data['id_servicio']);
                $id_servicio = ($this->db->update('tb_servicios', $data)) ? ($data['id_servicio']) : (FALSE);
            }                
        }
        return (isset($id_servicio) && $id_servicio>0) ? (TRUE) : (FALSE);
    }

    public function buscar_servicios($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $limit = $this->result_limit;
            if(!empty($param['id_tipomoneda']))
            {
                $whe['alm.id_tipomoneda'] = $param['id_tipomoneda'];
            }

            $query = $this->db->select('alm.id_servicio, alm.servicio, alm.estado, tm.tipomoneda')
                              ->from('tb_servicios alm')
                              ->join('tb_tipomoneda tm','alm.id_tipomoneda=tm.id_tipomoneda','left');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("servicio"=>$param['alm']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('alm.servicio', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('alm.id_servicio')
                              ->from('tb_servicios alm')
                              ->join('tb_tipomoneda tm','alm.id_tipomoneda=tm.id_tipomoneda','left');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("servicio"=>$param['alm']));
//43402110
            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function combox_servicio($id_servicio = "")
    {
        $whe['estado'] = 1;
        $whe['id_sucursal'] = $this->id_sucu;
        $slected = ($id_servicio === FALSE) ? ("") : ($id_servicio);      
        $rta = $this->db->select('id_servicio, servicio')->from('tb_servicios')->where($whe)->order_by('servicio', 'ASC')->get()->result_array(); //print_r($this->db->last_query()); print_r($rta);
        if(isset($rta[0]['id_servicio']))
        {
            $combox = "<option value=''>Seleccione servicio</option>";
            foreach ($rta as $key => $value) {
                $sele = ($value['id_servicio'] == $slected) ? ("selected") : ("");
                $combox .= "<option ".$sele." value='".$value['id_servicio']."'>".$value['servicio']."</option>";
            }
        }

        return (isset($combox)) ? ($combox) : ("");
    }

    public function get_combox_servicio()
    {
        $whe['estado'] = 1;     
        $whe['id_sucursal'] = $this->id_sucu;  
        $rta = $this->db->select('id_servicio, servicio')->from('tb_servicios')->where($whe)->order_by('servicio', 'ASC')->get()->result_array(); //print_r($this->db->last_query()); print_r($rta);
        if(isset($rta[0]['id_servicio']))
        {
            $combox = "<option value=''>Seleccione servicio</option>";
            foreach ($rta as $key => $value) {
                $combox .= "<option ".$sele." value='".$value['id_servicio']."'>".$value['servicio']."</option>";
            }
        }
        return (isset($combox)) ? ($combox) : ("");
    }

    public function validar_servicio($id_servicio = "", $servicio)
    {
        $success = FALSE;
        if($servicio === "") {}
        else
        {
            $whe['servicio'] = trim($servicio);

            $query = $this->db->select('id_servicio');
            $form = $query->from('tb_servicios');                     
            if($id_servicio>0)
            {
                $whe['id_servicio !='] = $id_servicio;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_all_amacen($param = "")
    {
        if($param === "") {}
        else
        {
            $whe = $param;
        }

        $query = $this->db->select('alm.id_servicio, alm.servicio, alm.estado');
        $form = $query->from('tb_servicios alm');
        $inner = $form->join('tb_sucursal sucu', 'alm.id_sucursal=sucu.id_sucursal', 'inner');
        $where = $inner->where($whe);
        $data = $where->order_by('alm.servicio', 'ASC')->get()->result_array();
        return $data;
    }

    public function cbox_servicio($parm = NULL)
    { 
        $selected = (!empty($parm['ka.id_servicio'])) ? ($parm['ka.id_servicio']) : ("");
        $whe['alm.estado'] = 1;
        $whe['sucu.id_sucursal'] = $this->id_sucu;
        $rta = $this
                ->db
                ->select('alm.id_servicio, alm.servicio, alm.estado')
                ->from('tb_servicios alm')
                ->join('tb_sucursal sucu', 'alm.id_sucursal=sucu.id_sucursal', 'inner')
                ->where($whe)
                ->order_by('alm.servicio', 'ASC')
                ->get()
                ->result_array(); //print_r($this->db->last_query()); print_r($rta);
        $combox = "<option value=''>Crear Almacén</option>";
        if(isset($rta[0]['id_servicio']))
        {
            $combox = "<option value=''>Seleccione Almacén</option>";
            foreach ($rta as $key => $value) {
                $se = ($value['id_servicio'] == $selected) ? ("selected") : ("");
                $combox .= "<option ".$se." value='".$value['id_servicio']."'>".$value['servicio']."</option>";
            }
        }
        return (isset($combox)) ? ($combox) : ("");
    }

    public function get_servicio_autocomplete($query = null , $w_not = null)
    {
        if(!empty($query))
        {   
            $w['estado'] = 1;
            $rta = $this->db->select('id_servicio, servicio')
                            ->from('tb_servicios')
                            ->where($w)
                            ->like('servicio',$query);
            if(!empty($w_not))
                $rta = $rta->where_not_in('id_servicio',explode(",", $w_not));

            $rta = $rta->order_by('servicio','asc')->get()->result_array();

            $data = array();
            if(!empty($rta))
            {   
                foreach ($rta as $key => $value) 
                {
                    $data[$key]['value'] = $value['servicio'];
                    $data[$key]['data'] = $value['id_servicio'];
                }
            }

            return $data;
        }
    }

    public function allservicios()
    {
        $r = $this->db->select('id_servicio, servicio')
                      ->from('tb_servicios')
                      ->order_by('servicio')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $v) 
            {
                $data[$v['id_servicio']] = $v['servicio'];
            }
        }

        return (!empty($data)) ? $data : null;
    }
}