<?php

class M_login extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function iniciar_sesion($user='', $pwd = '')
    {
        if(!empty($user) && !empty($pwd))
        {
            $i['user'] = $user;
            $i['pwd'] = $pwd;
            $i['fecha_ingreso'] = date('Y-m-d H:i:s');
            $this->db->insert('tb_logins',$i);

            $w['usuario'] = $user;
            $w['pwd'] = $pwd;
            $w['estado'] = 1;
            $r = $this->db->select('id_usuario, usuario')
                          ->from('tb_usuarios')
                          ->where($w)
                          ->get()
                          ->row_array();

            return (!empty($r)) ? $r : null;              

        }
    }
}