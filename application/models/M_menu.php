<?php

	class M_menu extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function armar_menu_lat()
	  	{	
	  		if(!$this->session->userdata('id_usuario'))
	  		{
	  			redirect(base_url()."login");
	  		}
	  		else
	  		{
	  			$w['um.id_usuario'] = $this->session->userdata('id_usuario');
	  			$w['m.estado'] = 1;
	  			
		  		$modulos_permitidos = $this->db->select('group_concat(um.id_menu) as id_menu')
		  									   ->from('tb_usuarios_menu um')
		  									   ->join('tb_menu m','m.id_menu=um.id_menu','left')
		  									   ->where($w)
		  									   ->get()
		  									   ->row_array();

		  		if(!empty($modulos_permitidos))
		  		{							   
			  		
		  			$w_['estado'] = 1;
		  			$ids_padres = $this->db->select('id_padre as ids')
		  								   ->from('tb_menu')
		  								   ->where_in('id_menu', explode(",", $modulos_permitidos['id_menu']))
		  								   ->where($w_)
		  								   ->group_by('id_padre')
		  								   ->get()
		  								   ->result_array();
		  			if(!empty($ids_padres))
		  			{
		  				foreach ($ids_padres as $v) 
		  				{
		  					$win[] = $v['ids'];
		  				}

		  				$w1['id_padre'] = 0;
				  		$w1['estado'] = 1;
				  		$rta['padre'] = $this->db->select('id_menu, concat_ws(":",menu,icono) menu')
				  							   ->from('tb_menu')
				  							   ->where($w1)
				  							   ->where_in('id_menu', $win)
				  							   ->order_by('orden')
				  							   ->group_by('id_menu')
				  							   ->get()
				  							   ->result_array();
				  		//print_r($rta['padre']);
				  		//print_r($modulos_permitidos['id_menu']);
				  		if(!empty($rta['padre']))
				  		{
				  			$rta['child'] = $this->db->select('id_menu, concat_ws(":",menu,url,icono) menu, id_padre')
							  					 ->from('tb_menu')
							  					 ->where_in('id_menu',explode(",", $modulos_permitidos['id_menu']))
							  					 ->order_by('orden')
							  					 ->get()
							  					 ->result_array();
				  			//print_r($rta['child']);

				  			if(!empty($rta['child']))
				  			{
				  				$menus = array();
				  				foreach ($rta['padre'] as $v1) 
				  				{
				  					foreach ($rta['child'] as $k => $v) 
					  				{
					  					if($v['id_padre'] == $v1['id_menu'])
					  					{
					  						$menus['menu_lat'][$v1['id_menu']]['menu'] = $v1['menu'];
					  						$menus['menu_lat'][$v1['id_menu']]['hijos'][$v['id_menu']] = $v['menu'];
					  						unset($rta['child'][$k]);
					  					}
					  				}
				  				}
				  			}
				  		}
				  		return (!empty($menus)) ? $menus : null;
		  			}
			  	}
			  	else
			  	{
			  		redirect(base_url()."menu");
			  	}
		  	}
	  	}

	  	public function data_mod($url_mod = "")
	  	{
	  		if(!empty($url_mod))
	  		{
	  			$w['url'] = $url_mod;

	  			$rta = $this->db->select('menu, url, icono')
	  							->from('tb_menu')
	  							->where($w)
	  							->get()
	  							->row_array();

	  			return(!empty($rta)) ? $rta : null;
	  		}
	  	}

	  	public function get_menus()
	  	{
	  		$w1['id_padre'] = 0;
	  		$w1['estado'] = 1;
	  		$rta['padre'] = $this->db->select('id_menu, menu')
	  							   ->from('tb_menu')
	  							   ->where($w1)
	  							   ->order_by('orden')
	  							   ->get()
	  							   ->result_array();

	  		if(!empty($rta['padre']))
	  		{
	  			foreach ($rta['padre'] as $val) 
	  			{
	  				$w_i[] = $val['id_menu'];
	  			}
	  			
	  			$w_['estado'] = 1;
	  			$rta['child'] = $this->db->select('id_menu, menu, id_padre')
				  					 ->from('tb_menu')
				  					 ->where($w_)
				  					 ->where_in('id_padre',$w_i)
				  					 ->order_by('orden')
				  					 ->get()
				  					 ->result_array();
	  			
	  			if(!empty($rta['child']))
	  			{
	  				$menus = array();
	  				foreach ($rta['padre'] as $v1) 
	  				{
	  					foreach ($rta['child'] as $k => $v) 
		  				{
		  					if($v['id_padre'] == $v1['id_menu'])
		  					{
		  						$menus['all_data'][$v1['id_menu']]['menu'] = $v1['menu'];
		  						$menus['all_data'][$v1['id_menu']]['hijos'][$v['id_menu']] = $v['menu'];
		  						unset($rta['child'][$k]);
		  					}
		  				}
	  				}
	  			}
	  		}
	  		return (!empty($menus)) ? $menus : null;
	  	}
	}
?>