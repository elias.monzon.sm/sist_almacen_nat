<?php

	class M_valorxalmacen extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

		public function stockxalmacen($id_almacen='')
	  	{
	  		if(!empty($id_almacen))
	  		{
	  			$w['id_almacen'] = $id_almacen;

	  			$r = $this->db->select('id_kardex, fecha_ingreso, id_codigo, id_marca')
	  						  ->from('tb_kardex')
	  						  ->where($w)
	  						  ->order_by('fecha_ingreso','asc')
	  						  ->get()
	  						  ->result_array();

	  			if(!empty($r))
	  			{
	  				foreach ($r as $v) 
	  				{
	  					$win[$v['id_codigo']][$v['id_marca']] = $v['id_kardex'];
	  				}
	  				if(!empty($win))
	  				{
	  					$win_ = [];
	  					foreach ($win as $arr) 
	  					{
	  						foreach ($arr as $idk) 
	  						{
	  							$win_[] = $idk;
	  						}
	  					}
	  				}

	  				$alldata = $this->db->select('k.id_kardex, k.id_codigo, k.id_marca, k.fecha_ingreso, k.stock, c.descripcion, m.marca, cv.precio_venta, round((k.stock)*(cv.precio_venta),3) valortotal, tm.simbolo, c.codigo')
	  									->from('tb_kardex k')
	  									->join('tb_codigo c','c.id_codigo=k.id_codigo','left')
	  									->join('tb_marca m','m.id_marca=k.id_marca','left')
	  									->join('tb_codigo_venta cv','k.id_codigo=cv.id_codigo and k.id_marca=cv.id_marca','left')
	  									->join('tb_tipomoneda tm','tm.id_tipomoneda=cv.id_tipomoneda','left')
	  									->where_in('k.id_kardex',$win_)
	  									->where_in('k.id_tipomovimiento',array('1','3','2'))
	  									->get()
	  									->result_array();
	  			}

	  			return (!empty($alldata)) ? $alldata : null;
	  		}
	  	}

	  	public function stockcero()
	  	{
			$rt = $this->db->select('ca.id_codigo, ca.id_almacen, ca.id_marca, ca.stock_minimo, (SELECT k.stock from tb_kardex k where k.id_almacen=ca.id_almacen and k.id_codigo=ca.id_codigo and k.id_marca=ca.id_marca order by id_kardex desc limit 1) stock, ma.marca, cod.descripcion, cod.codigo, alm.almacen, round((SELECT(stock))*(cv.precio_venta),3) valortotal, tm.simbolo')
				  ->from('tb_codigo_almacen ca')
				  ->join('tb_almacen alm', 'ca.id_almacen=alm.id_almacen','left')
				  ->join('tb_codigo cod','cod.id_codigo=ca.id_codigo','left')
				  ->join('tb_marca ma','ma.id_marca=ca.id_marca','left')
				  ->join('tb_codigo_venta cv','ca.id_codigo=cv.id_codigo and ca.id_marca=cv.id_marca','left')
				  ->join('tb_tipomoneda tm','tm.id_tipomoneda=cv.id_tipomoneda','left')
				  ->get()
				  ->result_array();
			if(!empty($rt))
			{
				foreach ($rt as $key => $value) {
					if($value['stock']>$value['stock_minimo'])
					{
						unset($rt[$key]);
					}
					else
					{
						$rt[$key]['stock'] = floatval($value['stock']);
					}
				}
			}

  			return (!empty($rt)) ? $rt : null;
	  	}	  	
	}
?>