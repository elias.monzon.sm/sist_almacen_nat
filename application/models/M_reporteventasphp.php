<?php

	class M_reporteventas extends CI_Model {

	  	public function __construct() 
	  	{
	    	parent::__construct();
	  	}

	  	public function buscar_ventas($param = null)
	  	{
	  		if(!empty($param))
	  		{
	  			$w['date(fecha_ingreso) >='] = date('Y-m-d',strtotime($param['fecha_ini']));
	  			$w['date(fecha_ingreso) <='] = date('Y-m-d',strtotime($param['fecha_fin']));

	  			$rta = $this->db->select('v.id_venta, v.codigo, v.serie, v.fecha_ingreso, (select CONCAT_WS(":",tp.tipomoneda,tp.simbolo) from tb_tipomoneda tp where tp.id_tipomoneda=v.id_tipomoneda) tipomoneda, ROUND(v.precio_total,3) precio_total, IF(v.tipo_persona=2,(select pj.razon_social from tb_persona_juridica pj where v.id_persona=pj.id_persona_juridica),(select CONCAT_WS(" ",pn.apellidos,pn.nombres) from tb_persona pn where pn.id_persona=v.id_persona)) cliente')
	  							->from('tb_ventas ven')
	                            ->join('tb_tipomoneda m','ven.id_tipomoneda=m.id_tipomoneda','left')
	                            ->join('tb_tipodocumento td','td.id_tipodocumento=ven.id_tipodocumento','left')
	                            ->where($w)
	                            ->get()
	                            ->result_array();
	  		}

	  		return (!empty($rta)) ? $rta : null;
	  	}
	}
?>