<?php

class M_tipodocumento extends CI_Model {

    public function __construct() 
    {
        parent::__construct();
    }
    
    public function save_tipodocumento($data)
    {
        $err['error_msg'] = "ERROR";
        $err['error_code'] = "0";
        $err['id'] = "";

        if($data === FALSE) {}
        else 
        {
            if(is_array($data))
            {
                $fecha = date("Y-m-d H:i:s");

                $id_tipodocumento = $data['id_tipodocumento'];
                unset($data['id_tipodocumento']);                
                $err['error_msg'] = "OK";
                $err['error_code'] = "1";
                if(isset($data['tipodocumento']))
                {
                    $data['afecta_stock'] = (isset($data['afecta_stock'])) ? $data['afecta_stock'] : "0";
                }

                if(isset($data['tipodocumento']))
                {
                    $whe['tipodocumento'] = trim($data['tipodocumento']);

                    $query = $this->db->select('id_tipodocumento');
                    $form = $query->from('tb_tipodocumento');
                    
                    if($id_tipodocumento>0)
                    {
                        $whe['id_tipodocumento !='] = $id_tipodocumento;
                    }
                    $where = $form->where($whe);
                    $cant = $where->count_all_results(); //print_r($this->db->last_query());

                    $err['error_code'] = ($cant>0) ? ("0") : ($err['error_code']);
                    $err['error_msg'] = ($err['error_code']=="0") ? ("Ya Existe") : ($err['error_msg']);
                }
                //die();
                if($err['error_code']=="1")
                {
                    if($id_tipodocumento==0)
                    {
                        $id_tipodocumento = ($this->db->insert('tb_tipodocumento', $data)) ? ($this->db->insert_id()) : (FALSE);
                        $err['error_code'] = ($id_tipodocumento) ? ("1") : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Guardo") : ($err['error_msg']);
                    }
                    else
                    {
                        $this->db->where('id_tipodocumento', $id_tipodocumento);
                        $err['error_code'] = ($this->db->update('tb_tipodocumento', $data)) ? ($err['error_code']) : ("0");
                        $err['error_msg'] = ($err['error_code']=="0") ? ("No Actualizo") : ($err['error_msg']);
                    }
                }                    
                $err['id'] = $id_tipodocumento;               
            }
        }        
        return $err;
    }

    public function get_one_tipodocumento($array_ = "")
    {
        if($array_ === "") {}
        else
        {
            $whe = $array_;

            $data = $this->db->select('alm.id_tipodocumento, alm.tipodocumento, alm.estado, alm.afecta_stock')
                              ->from('tb_tipodocumento alm')
                             ->where($whe)
                             ->get()
                             ->row_array();
        }
        return (isset($data['id_tipodocumento']) && is_array($data)) ? ($data) : (FALSE);
    }

    public function deltete_tipodocumento($data = "")
    {
        if($data === FALSE) {}
        else
        {
            $id_tipodocumento = (isset($data['id_tipodocumento']) && $data['id_tipodocumento']>0) ? ($data['id_tipodocumento']) : (0);
            if($id_tipodocumento >0)
            {
                unset($data['id_tipodocumento']);
                $fecha = date("Y-m-d H:i:s");

                $data['fecha_modificacion'] = $fecha;
                $data['id_usuario_modificado'] = $this->id_user;

                $this->db->where('id_tipodocumento', $data['id_tipodocumento']);
                $id_tipodocumento = ($this->db->update('tb_tipodocumento', $data)) ? ($data['id_tipodocumento']) : (FALSE);
            }                
        }
        return (isset($id_tipodocumento) && $id_tipodocumento>0) ? (TRUE) : (FALSE);
    }

    public function buscar_tipodocumentos($param = "")
    {
        if(!empty($param))
        {
            $page = $param['page'];
            $limit = $this->result_limit;
            unset($param['page']);      

            $pages = $page*$limit;

            $limit = $this->result_limit;

            $query = $this->db->select('alm.id_tipodocumento, alm.tipodocumento, alm.estado, alm.afecta_stock')
                              ->from('tb_tipodocumento alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("tipodocumento"=>$param['alm']));                

            $query = $query->limit($limit, $pages)
                           ->order_by('alm.tipodocumento', 'ASC');
            $rta['all_data'] = $query->get()->result_array();
     
            $query = $this->db->select('alm.id_tipodocumento')
                              ->from('tb_tipodocumento alm');
            if(!empty($whe))
                $query = $query->where($whe);

            if(isset($param['alm']) && strlen(trim($param['alm']))>0)
                $query = $query->like(array("tipodocumento"=>$param['alm']));

            $total_registros = $query->count_all_results(); //print_r($rtatotal);

            $rta['total_registros'] = $total_registros;
            $rta['cantidad_pag'] = (($total_registros % $limit) < $limit && ($total_registros % $limit) != 0) ? (intval($total_registros / $limit) + 1) : (intval($total_registros / $limit));
        }
             
        return (isset($rta['all_data'][0])) ? ($rta) : (FALSE);
    }

    public function combox_tipodocumento($id_tipodocumento = "")
    {
        $whe['estado'] = 1;
        $whe['id_sucursal'] = $this->id_sucu;
        $slected = ($id_tipodocumento === FALSE) ? ("") : ($id_tipodocumento);      
        $rta = $this->db->select('id_tipodocumento, tipodocumento')->from('tb_tipodocumento')->where($whe)->order_by('tipodocumento', 'ASC')->get()->result_array(); //print_r($this->db->last_query()); print_r($rta);
        if(isset($rta[0]['id_tipodocumento']))
        {
            $combox = "<option value=''>Seleccione tipodocumento</option>";
            foreach ($rta as $key => $value) {
                $sele = ($value['id_tipodocumento'] == $slected) ? ("selected") : ("");
                $combox .= "<option ".$sele." value='".$value['id_tipodocumento']."'>".$value['tipodocumento']."</option>";
            }
        }

        return (isset($combox)) ? ($combox) : ("");
    }

    public function get_combox_tipodocumento()
    {
        $whe['estado'] = 1;     
        $whe['id_sucursal'] = $this->id_sucu;  
        $rta = $this->db->select('id_tipodocumento, tipodocumento')->from('tb_tipodocumento')->where($whe)->order_by('tipodocumento', 'ASC')->get()->result_array(); //print_r($this->db->last_query()); print_r($rta);
        if(isset($rta[0]['id_tipodocumento']))
        {
            $combox = "<option value=''>Seleccione tipodocumento</option>";
            foreach ($rta as $key => $value) {
                $combox .= "<option ".$sele." value='".$value['id_tipodocumento']."'>".$value['tipodocumento']."</option>";
            }
        }
        return (isset($combox)) ? ($combox) : ("");
    }

    public function validar_tipodocumento($id_tipodocumento = "", $tipodocumento)
    {
        $success = FALSE;
        if($tipodocumento === "") {}
        else
        {
            $whe['tipodocumento'] = trim($tipodocumento);

            $query = $this->db->select('id_tipodocumento');
            $form = $query->from('tb_tipodocumento');                     
            if($id_tipodocumento>0)
            {
                $whe['id_tipodocumento !='] = $id_tipodocumento;
            }

            $where = $form->where($whe);
            $cant = $where->count_all_results(); //print_r($this->db->last_query());

            $success= ($cant>0) ? (FALSE) : (TRUE);              
        }
        return $success;
    }

    public function get_all_amacen($param = "")
    {
        if($param === "") {}
        else
        {
            $whe = $param;
        }

        $query = $this->db->select('alm.id_tipodocumento, alm.tipodocumento, alm.estado');
        $form = $query->from('tb_tipodocumento alm');
        $inner = $form->join('tb_sucursal sucu', 'alm.id_sucursal=sucu.id_sucursal', 'inner');
        $where = $inner->where($whe);
        $data = $where->order_by('alm.tipodocumento', 'ASC')->get()->result_array();
        return $data;
    }

    public function cbox_tipodocumento($parm = NULL)
    { 
        $selected = (!empty($parm['ka.id_tipodocumento'])) ? ($parm['ka.id_tipodocumento']) : ("");
        $whe['alm.estado'] = 1;
        $whe['sucu.id_sucursal'] = $this->id_sucu;
        $rta = $this
                ->db
                ->select('alm.id_tipodocumento, alm.tipodocumento, alm.estado')
                ->from('tb_tipodocumento alm')
                ->join('tb_sucursal sucu', 'alm.id_sucursal=sucu.id_sucursal', 'inner')
                ->where($whe)
                ->order_by('alm.tipodocumento', 'ASC')
                ->get()
                ->result_array(); //print_r($this->db->last_query()); print_r($rta);
        $combox = "<option value=''>Crear Almacén</option>";
        if(isset($rta[0]['id_tipodocumento']))
        {
            $combox = "<option value=''>Seleccione Almacén</option>";
            foreach ($rta as $key => $value) {
                $se = ($value['id_tipodocumento'] == $selected) ? ("selected") : ("");
                $combox .= "<option ".$se." value='".$value['id_tipodocumento']."'>".$value['tipodocumento']."</option>";
            }
        }
        return (isset($combox)) ? ($combox) : ("");
    }

    public function get_tipodocumento_autocomplete($query = null , $w_not = null)
    {
        if(!empty($query))
        {   
            $w['estado'] = 1;
            $rta = $this->db->select('id_tipodocumento, tipodocumento')
                            ->from('tb_tipodocumento')
                            ->where($w)
                            ->like('tipodocumento',$query);
            if(!empty($w_not))
                $rta = $rta->where_not_in('id_tipodocumento',explode(",", $w_not));

            $rta = $rta->order_by('tipodocumento','asc')->get()->result_array();

            $data = array();
            if(!empty($rta))
            {   
                foreach ($rta as $key => $value) 
                {
                    $data[$key]['value'] = $value['tipodocumento'];
                    $data[$key]['data'] = $value['id_tipodocumento'];
                }
            }

            return $data;
        }
    }

    public function alltipodocumentos()
    {
        $r = $this->db->select('id_tipodocumento, tipodocumento')
                      ->from('tb_tipodocumento')
                      ->order_by('tipodocumento')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $v) 
            {
                $data[$v['id_tipodocumento']] = $v['tipodocumento'];
            }
        }

        return (!empty($data)) ? $data : null;
    }

    public function cbx_tipodocstock()
    {
        $w['afecta_stock'] = 1;
        $w['estado'] = 1;
        $r = $this->db->select('id_tipodocumento, tipodocumento')
                      ->from('tb_tipodocumento')
                      ->where($w)
                      ->order_by('tipodocumento','asc')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $value) {
                $r2[$value['id_tipodocumento']] = $value['tipodocumento'];
            }
        }

        return (!empty($r2)) ? $r2 : null;
    }

    public function cbx_tipodoc()
    {
        $w['estado'] = 1;
        $r = $this->db->select('id_tipodocumento, tipodocumento')
                      ->from('tb_tipodocumento')
                      ->where($w)
                      ->order_by('tipodocumento','asc')
                      ->get()
                      ->result_array();

        if(!empty($r))
        {
            foreach ($r as $value) {
                $r2[$value['id_tipodocumento']] = $value['tipodocumento'];
            }
        }

        return (!empty($r2)) ? $r2 : null;
    }

    public function cbx_tipodocventa()
    {
        $w['estado'] = 1;
        $r = $this->db->select('id_tipodocumento, tipodocumento, afecta_stock')
                      ->from('tb_tipodocumento')
                      ->where($w)
                      ->order_by('tipodocumento','asc')
                      ->get()
                      ->result_array();

        $tx = "<option>Crear Elementos</option>";
        if(!empty($r))
        {
            $tx = "<option disabled selected>Seleccione un documento</option>";
            foreach ($r as $value) {
                $tx .="<option value='".$value['id_tipodocumento']."' maneja_stock=".$value['afecta_stock'].">".$value['tipodocumento']."</option>";
            }
        }

        return (!empty($tx)) ? $tx : null;
    }
}