<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function hace_cuantos_minutos($fecha, $fechama=""){

	$ahora = (!empty($fechama)) ? ($fechama) : (time());

	$diferencia = intval(($ahora - strtotime($fecha)) / 60);

	return $diferencia;
}

function hace_cuantas_horas($fecha, $fechama=""){
	$ahora = (!empty($fechama)) ? ($fechama) : (time());

	$diferencia = intval(($ahora - strtotime($fecha)) / 3600);

	return $diferencia;
}

function hace_cuantos_dias($fecha, $fechama=""){
	$ahora = (!empty($fechama)) ? ($fechama) : (time());

	$diferencia = intval(($ahora - strtotime($fecha)) / 86400);

	return $diferencia;
}

function hace_cuantos_meses($fecha, $fechama=""){
	$ahora = (!empty($fechama)) ? ($fechama) : (time());

	$diferencia = intval(($ahora - strtotime($fecha)) / 2592000);

	return $diferencia;
}

function hace_cuantos_anios($fecha, $fechama=""){
	$ahora = (!empty($fechama)) ? ($fechama) : (time());

	$diferencia = intval(($ahora - strtotime($fecha)) / 31104000);

	return $diferencia;
}

function hace_cuanto_tiempo($fecha, $fechama=""){
	$minutos = hace_cuantos_minutos($fecha,$fechama);

	if($minutos < 1){
		return " unos segundos";
	}else if($minutos < 60){

		return $minutos." minutos";
	}else{
		$horas = hace_cuantas_horas($fecha,$fechama);

		if($horas < 24){
			return $horas." horas";
		}else{
			$dias = hace_cuantos_dias($fecha,$fechama);

			if($dias < 30){
				return $dias. " días";
			}else{
				$meses = hace_cuantos_meses($fecha,$fechama);

				if($meses < 12){
					return $meses." meses";
				}else{
					$anios = hace_cuantos_anios($fecha,$fechama);

					return $anios . " años";
				}
			}
		}
	}
}


//////////////////////////////////

function get_nombre_mes($fecha){
	$meses = array(
		"Enero",
		"Febrero",
		"Marzo",
		"Abril",
		"Mayo",
		"Junio",
		"Julio",
		"Agosto",
		"Setiembre",
		"Octubre",
		"Noviembre",
		"Diciembre"
	);

	return $meses[date("n", strtotime($fecha)) - 1];
}

function formato_fecha_dia_mes_anio($fecha){
	$dia = date("j", strtotime($fecha));
	$mes = get_nombre_mes($fecha);
	$anio = date("Y", strtotime($fecha));

	return $dia." de ".$mes." de ".$anio;
}

function formato_fecha_dia_mes($fecha){
	$dia = get_nombre_dia($fecha);
	$nud = date("j", strtotime($fecha));
	$mes = get_nombre_mes($fecha);
	$hor = date("H:i", strtotime($fecha));
	return $dia." ".$nud." de ".$mes ." ".$hor;
}

///////////////////////////////////

function get_nombre_dia($fecha){
	$dias = array(
		"Lunes",
		"Martes",
		"Miércoles",
		"Jueves",
		"Viernes",
		"Sábado",
		"Domingo"
	);

	return $dias[date("N", strtotime($fecha)) - 1];
}

function week_day_today($fecha){

	$hoy = strtotime(date("Y-m-d", time()));

	$dia = strtotime(date("Y-m-d", strtotime($fecha)));

	if($hoy == $dia){
		return "Hoy";
	}else{
		$dia = get_nombre_dia($fecha);

		return $dia . " " . date("j", strtotime($fecha));
	}
}

function infodia($fech = NULL)
{
    $dia = NULL;
    if(!empty($fech))
    {
        $fiecha = date("Y-m-d", $fech);

        $dati = explode("-",$fiecha);

        $year=$dati[0];
        $month=$dati[1];
        $day=$dati[2];

        # Obtenemos el numero de la semana
        $semw = date("W",mktime(0,0,0,$month,$day,$year));
        $dia['semana'] = $semw;
        $dia['anio'] = $year;

        # Obtenemos el día de la semana de la fecha dada
        $diasemana=date("w",mktime(0,0,0,$month,$day,$year));

        # el 0 equivale al domingo...
        $diasemana = ($diasemana==0) ? (7) : ($diasemana);

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $dia['fini'] = date("Y-m-d",mktime(0,0,0,$month,$day-$diasemana+1,$year));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $dia['ffin'] = date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diasemana),$year));
    }
    return $dia;  
}
