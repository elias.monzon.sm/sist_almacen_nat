<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function responseCode($success = true, $message = '', $data = array(), $code = 1) {
	    header("HTTP/1.0 200 OK");
	    header('Content-type: application/json');
	    $data2["success"] = $success;
	    $data2["message"] = $message;
	    $data2["code"] = $code;
	    $data2["data"] = $data;
	    if (isset($_GET['callback'])) {
	        print_r($_GET['callback'] . '(' . (json_encode($data2)) . ');');
	        //print_r(prettyPrint(json_encode($data)));
	    } else {
	        print_r(prettyPrint(json_encode($data2)));
	    }
	    die();
	    //print_r(prettyPrint(json_encode($data2)));
	}

function responseAutocompletar($query, $suggestions) {
	    header("HTTP/1.0 200 OK");
	    header('Content-type: application/json');
	    $data2["query"] = $query;
	    $data2["suggestions"] = $suggestions;
	    print_r(prettyPrint(json_encode($data2)));
	    die();
	    //print_r(prettyPrint(json_encode($data2)));
	}

	function prettyPrint($json) {
	    $result = '';
	    $level = 0;
	    $prev_char = '';
	    $in_quotes = false;
	    $ends_line_level = NULL;
	    $json_length = strlen($json);

	    for ($i = 0; $i < $json_length; $i++) {
	        $char = $json[$i];
	        $new_line_level = NULL;
	        $post = "";
	        if ($ends_line_level !== NULL) {
	            $new_line_level = $ends_line_level;
	            $ends_line_level = NULL;
	        }
	        if ($char === '"' && $prev_char != '\\') {
	            $in_quotes = !$in_quotes;
	        } else if (!$in_quotes) {
	            switch ($char) {
	                case '}': case ']':
	                    $level--;
	                    $ends_line_level = NULL;
	                    $new_line_level = $level;
	                    break;

	                case '{': case '[':
	                    $level++;
	                case ',':
	                    $ends_line_level = $level;
	                    break;

	                case ':':
	                    $post = " ";
	                    break;

	                case " ": case "\t": case "\n": case "\r":
	                    $char = "";
	                    $ends_line_level = $new_line_level;
	                    $new_line_level = NULL;
	                    break;
	            }
	        }
	        if ($new_line_level !== NULL) {
	            $result .= "\n" . str_repeat("\t", $new_line_level);
	        }
	        $result .= $char . $post;
	        $prev_char = $char;
	    }

	    return $result;
	}