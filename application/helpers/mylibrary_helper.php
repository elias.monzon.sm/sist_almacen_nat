<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if(!function_exists('cbx_simple'))
	{
		function cbx_simple($data = array(),$selected = null, $head_txt = "")
		{
			$txt = "<option value>No hay datos</option>";
			if(!empty($data))
			{	
				$txt = "";
				if(!empty($head_txt))
				{
					$select = (!empty($selected)) ? null : "selected='selected'";
					$txt .= "<option value disabled='disabled' ".$select."><b>".$head_txt."</b></option>";
				}
				foreach ($data as $key => $value) 
				{
					$attr = (!empty($selected) && $key==$selected) ? "selected='selected'" : null;
					$txt .= "<option value ='".$key."' ".$attr.">".$value."</option>";
				}
			}
			return (!empty($txt)) ? $txt : null;
		}
	}

	if(!function_exists('get_impuesto'))
	{
		function get_impuesto($id_impuesto='')
		{
			if(!empty($id_impuesto))
			{
				$ci =& get_instance();

				$w['id_impuesto'] = $id_impuesto;
				$r = $ci->db->select('valor')
							  ->from('tb_impuesto')
							  ->where($w)
							  ->get()
							  ->row_array();

				return (!empty($r['valor'])) ? $r['valor'] : null;
			}
		}
	}

	if(!function_exists('stockcero'))
	{
		function stockcero()
		{
			$ci =& get_instance();

			$stockcero = 0;
			$r = $ci->db->select('id_codigo, id_almacen, id_marca, stock_minimo')
						  ->from('tb_codigo_almacen')
						  ->get()
						  ->result_array();
			if(!empty($r))
			{
				foreach ($r as $key => $value) 
				{
					$w['id_codigo'] = $value['id_codigo'];
					$w['id_almacen'] = $value['id_almacen'];
					$w['id_marca'] = $value['id_marca'];
					$quey = $ci->db->select('k.id_kardex,IF('.intval($value['stock_minimo']).'>=k.stock OR k.stock= 0,"1","2") stockbien, k.stock')
								 ->from('tb_kardex k')
								 ->where($w)
								 ->order_by('id_kardex','desc')
								 ->limit(1)
								 ->get()
								 ->row_array();
					if($quey['stockbien']==1 || empty($quey))
					{
						$stockcero++;
					}
				}
			}

			return $stockcero;
		}
	}

	
	function save_data($table = null, $data = null, $where = null)
    {
        if(!empty($table) && !empty($data))
        {
            $ci =& get_instance();
            if(!empty($where))
            {
                $ci->db->where($where);
                $ok = ($ci->db->update($table,$data)) ? 1 : null;
            }
            else
            {
                if(count_dimension($data) == 1)
                    $ok = ($ci->db->insert($table,$data)) ? $ci->db->insert_id() : null;    
                else
                    $ok = ($ci->db->insert_batch($table,$data)) ? 1 : null;
            }
        }

        return (!empty($ok)) ? $ok : null;
    }

    function count_dimension($Array, $count = 0) {
       if(is_array($Array)) {
          return count_dimension(current($Array), ++$count);
       } else {
          return $count;
       }
    }
?>